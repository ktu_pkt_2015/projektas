# Programavimo kalbų teorijos projektai

### Apie repozitoriją

Čia kaupiami 2015 PKT modulio studentų projektiniai darbai.

### Projekto nuostatos

Kiekvieną projekto komandą sudaro 3 studentai.

Kiekviena komanda sukuria katalogą ir ten kaupia savo projekto failus. Kompiliuotų failų kelti nereikia, tik išeities tekstus ir projekto failus, taip, kad bet kada galėtų parsisiųsti ir sukompiliuoti projektą. Katalogo pavadinimas - komandos pavadinimas.