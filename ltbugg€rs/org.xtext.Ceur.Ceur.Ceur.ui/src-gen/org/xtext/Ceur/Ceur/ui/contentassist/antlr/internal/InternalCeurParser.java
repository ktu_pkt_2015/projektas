package org.xtext.Ceur.Ceur.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.Ceur.Ceur.services.CeurGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCeurParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_CHARACTERS", "RULE_INTEGERS", "RULE_BOOLVAL", "RULE_ID", "RULE_DIGIT", "RULE_ZERO", "RULE_NUMBERS", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'+'", "'-'", "'*'", "'/'", "'=='", "'<'", "'>'", "'('", "')'", "'kol'", "'{'", "'}'", "'jei'", "'kitaip'", "'='", "'char'", "'bool'", "'int'", "'\\u20AC'", "'void'", "'import'", "'.*'", "'.'", "'spausdinti'", "'ir'", "'arba'"
    };
    public static final int RULE_STRING=12;
    public static final int RULE_SL_COMMENT=14;
    public static final int T__19=19;
    public static final int T__37=37;
    public static final int RULE_NUMBERS=10;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int RULE_BOOLVAL=6;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=7;
    public static final int RULE_WS=15;
    public static final int RULE_ZERO=9;
    public static final int RULE_DIGIT=8;
    public static final int RULE_ANY_OTHER=16;
    public static final int RULE_INTEGERS=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=11;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=13;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int RULE_CHARACTERS=4;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalCeurParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCeurParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCeurParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g"; }


     
     	private CeurGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(CeurGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleDomainmodel"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:60:1: entryRuleDomainmodel : ruleDomainmodel EOF ;
    public final void entryRuleDomainmodel() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:61:1: ( ruleDomainmodel EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:62:1: ruleDomainmodel EOF
            {
             before(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel61);
            ruleDomainmodel();

            state._fsp--;

             after(grammarAccess.getDomainmodelRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDomainmodel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:69:1: ruleDomainmodel : ( ( rule__Domainmodel__ElementsAssignment )* ) ;
    public final void ruleDomainmodel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:73:2: ( ( ( rule__Domainmodel__ElementsAssignment )* ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:74:1: ( ( rule__Domainmodel__ElementsAssignment )* )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:74:1: ( ( rule__Domainmodel__ElementsAssignment )* )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:75:1: ( rule__Domainmodel__ElementsAssignment )*
            {
             before(grammarAccess.getDomainmodelAccess().getElementsAssignment()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:76:1: ( rule__Domainmodel__ElementsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==36) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:76:2: rule__Domainmodel__ElementsAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Domainmodel__ElementsAssignment_in_ruleDomainmodel94);
            	    rule__Domainmodel__ElementsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getDomainmodelAccess().getElementsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleType"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:88:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:89:1: ( ruleType EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:90:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_ruleType_in_entryRuleType122);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleType129); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:97:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:101:2: ( ( ( rule__Type__Alternatives ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:102:1: ( ( rule__Type__Alternatives ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:102:1: ( ( rule__Type__Alternatives ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:103:1: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:104:1: ( rule__Type__Alternatives )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:104:2: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_rule__Type__Alternatives_in_ruleType155);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRulePrintL"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:116:1: entryRulePrintL : rulePrintL EOF ;
    public final void entryRulePrintL() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:117:1: ( rulePrintL EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:118:1: rulePrintL EOF
            {
             before(grammarAccess.getPrintLRule()); 
            pushFollow(FOLLOW_rulePrintL_in_entryRulePrintL182);
            rulePrintL();

            state._fsp--;

             after(grammarAccess.getPrintLRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrintL189); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrintL"


    // $ANTLR start "rulePrintL"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:125:1: rulePrintL : ( ( rule__PrintL__Group__0 ) ) ;
    public final void rulePrintL() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:129:2: ( ( ( rule__PrintL__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:130:1: ( ( rule__PrintL__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:130:1: ( ( rule__PrintL__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:131:1: ( rule__PrintL__Group__0 )
            {
             before(grammarAccess.getPrintLAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:132:1: ( rule__PrintL__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:132:2: rule__PrintL__Group__0
            {
            pushFollow(FOLLOW_rule__PrintL__Group__0_in_rulePrintL215);
            rule__PrintL__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPrintLAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrintL"


    // $ANTLR start "entryRuleStatement_While"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:144:1: entryRuleStatement_While : ruleStatement_While EOF ;
    public final void entryRuleStatement_While() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:145:1: ( ruleStatement_While EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:146:1: ruleStatement_While EOF
            {
             before(grammarAccess.getStatement_WhileRule()); 
            pushFollow(FOLLOW_ruleStatement_While_in_entryRuleStatement_While242);
            ruleStatement_While();

            state._fsp--;

             after(grammarAccess.getStatement_WhileRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatement_While249); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStatement_While"


    // $ANTLR start "ruleStatement_While"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:153:1: ruleStatement_While : ( ( rule__Statement_While__Group__0 ) ) ;
    public final void ruleStatement_While() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:157:2: ( ( ( rule__Statement_While__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:158:1: ( ( rule__Statement_While__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:158:1: ( ( rule__Statement_While__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:159:1: ( rule__Statement_While__Group__0 )
            {
             before(grammarAccess.getStatement_WhileAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:160:1: ( rule__Statement_While__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:160:2: rule__Statement_While__Group__0
            {
            pushFollow(FOLLOW_rule__Statement_While__Group__0_in_ruleStatement_While275);
            rule__Statement_While__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStatement_WhileAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStatement_While"


    // $ANTLR start "entryRuleIf_statment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:172:1: entryRuleIf_statment : ruleIf_statment EOF ;
    public final void entryRuleIf_statment() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:173:1: ( ruleIf_statment EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:174:1: ruleIf_statment EOF
            {
             before(grammarAccess.getIf_statmentRule()); 
            pushFollow(FOLLOW_ruleIf_statment_in_entryRuleIf_statment302);
            ruleIf_statment();

            state._fsp--;

             after(grammarAccess.getIf_statmentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIf_statment309); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIf_statment"


    // $ANTLR start "ruleIf_statment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:181:1: ruleIf_statment : ( ( rule__If_statment__Group__0 ) ) ;
    public final void ruleIf_statment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:185:2: ( ( ( rule__If_statment__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:186:1: ( ( rule__If_statment__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:186:1: ( ( rule__If_statment__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:187:1: ( rule__If_statment__Group__0 )
            {
             before(grammarAccess.getIf_statmentAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:188:1: ( rule__If_statment__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:188:2: rule__If_statment__Group__0
            {
            pushFollow(FOLLOW_rule__If_statment__Group__0_in_ruleIf_statment335);
            rule__If_statment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIf_statmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIf_statment"


    // $ANTLR start "entryRuleElse"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:200:1: entryRuleElse : ruleElse EOF ;
    public final void entryRuleElse() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:201:1: ( ruleElse EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:202:1: ruleElse EOF
            {
             before(grammarAccess.getElseRule()); 
            pushFollow(FOLLOW_ruleElse_in_entryRuleElse362);
            ruleElse();

            state._fsp--;

             after(grammarAccess.getElseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleElse369); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleElse"


    // $ANTLR start "ruleElse"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:209:1: ruleElse : ( ( rule__Else__Group__0 ) ) ;
    public final void ruleElse() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:213:2: ( ( ( rule__Else__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:214:1: ( ( rule__Else__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:214:1: ( ( rule__Else__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:215:1: ( rule__Else__Group__0 )
            {
             before(grammarAccess.getElseAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:216:1: ( rule__Else__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:216:2: rule__Else__Group__0
            {
            pushFollow(FOLLOW_rule__Else__Group__0_in_ruleElse395);
            rule__Else__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getElseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleElse"


    // $ANTLR start "entryRuleCharacterDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:228:1: entryRuleCharacterDeclarationAss : ruleCharacterDeclarationAss EOF ;
    public final void entryRuleCharacterDeclarationAss() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:229:1: ( ruleCharacterDeclarationAss EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:230:1: ruleCharacterDeclarationAss EOF
            {
             before(grammarAccess.getCharacterDeclarationAssRule()); 
            pushFollow(FOLLOW_ruleCharacterDeclarationAss_in_entryRuleCharacterDeclarationAss422);
            ruleCharacterDeclarationAss();

            state._fsp--;

             after(grammarAccess.getCharacterDeclarationAssRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterDeclarationAss429); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCharacterDeclarationAss"


    // $ANTLR start "ruleCharacterDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:237:1: ruleCharacterDeclarationAss : ( ( rule__CharacterDeclarationAss__Group__0 ) ) ;
    public final void ruleCharacterDeclarationAss() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:241:2: ( ( ( rule__CharacterDeclarationAss__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:242:1: ( ( rule__CharacterDeclarationAss__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:242:1: ( ( rule__CharacterDeclarationAss__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:243:1: ( rule__CharacterDeclarationAss__Group__0 )
            {
             before(grammarAccess.getCharacterDeclarationAssAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:244:1: ( rule__CharacterDeclarationAss__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:244:2: rule__CharacterDeclarationAss__Group__0
            {
            pushFollow(FOLLOW_rule__CharacterDeclarationAss__Group__0_in_ruleCharacterDeclarationAss455);
            rule__CharacterDeclarationAss__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCharacterDeclarationAssAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCharacterDeclarationAss"


    // $ANTLR start "entryRuleCharacterDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:256:1: entryRuleCharacterDeclaration : ruleCharacterDeclaration EOF ;
    public final void entryRuleCharacterDeclaration() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:257:1: ( ruleCharacterDeclaration EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:258:1: ruleCharacterDeclaration EOF
            {
             before(grammarAccess.getCharacterDeclarationRule()); 
            pushFollow(FOLLOW_ruleCharacterDeclaration_in_entryRuleCharacterDeclaration482);
            ruleCharacterDeclaration();

            state._fsp--;

             after(grammarAccess.getCharacterDeclarationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterDeclaration489); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCharacterDeclaration"


    // $ANTLR start "ruleCharacterDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:265:1: ruleCharacterDeclaration : ( ( rule__CharacterDeclaration__Group__0 ) ) ;
    public final void ruleCharacterDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:269:2: ( ( ( rule__CharacterDeclaration__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:270:1: ( ( rule__CharacterDeclaration__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:270:1: ( ( rule__CharacterDeclaration__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:271:1: ( rule__CharacterDeclaration__Group__0 )
            {
             before(grammarAccess.getCharacterDeclarationAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:272:1: ( rule__CharacterDeclaration__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:272:2: rule__CharacterDeclaration__Group__0
            {
            pushFollow(FOLLOW_rule__CharacterDeclaration__Group__0_in_ruleCharacterDeclaration515);
            rule__CharacterDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCharacterDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCharacterDeclaration"


    // $ANTLR start "entryRuleCharacterTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:284:1: entryRuleCharacterTypeExpression : ruleCharacterTypeExpression EOF ;
    public final void entryRuleCharacterTypeExpression() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:285:1: ( ruleCharacterTypeExpression EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:286:1: ruleCharacterTypeExpression EOF
            {
             before(grammarAccess.getCharacterTypeExpressionRule()); 
            pushFollow(FOLLOW_ruleCharacterTypeExpression_in_entryRuleCharacterTypeExpression542);
            ruleCharacterTypeExpression();

            state._fsp--;

             after(grammarAccess.getCharacterTypeExpressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterTypeExpression549); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCharacterTypeExpression"


    // $ANTLR start "ruleCharacterTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:293:1: ruleCharacterTypeExpression : ( ( rule__CharacterTypeExpression__CharAssignAssignment ) ) ;
    public final void ruleCharacterTypeExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:297:2: ( ( ( rule__CharacterTypeExpression__CharAssignAssignment ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:298:1: ( ( rule__CharacterTypeExpression__CharAssignAssignment ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:298:1: ( ( rule__CharacterTypeExpression__CharAssignAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:299:1: ( rule__CharacterTypeExpression__CharAssignAssignment )
            {
             before(grammarAccess.getCharacterTypeExpressionAccess().getCharAssignAssignment()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:300:1: ( rule__CharacterTypeExpression__CharAssignAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:300:2: rule__CharacterTypeExpression__CharAssignAssignment
            {
            pushFollow(FOLLOW_rule__CharacterTypeExpression__CharAssignAssignment_in_ruleCharacterTypeExpression575);
            rule__CharacterTypeExpression__CharAssignAssignment();

            state._fsp--;


            }

             after(grammarAccess.getCharacterTypeExpressionAccess().getCharAssignAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCharacterTypeExpression"


    // $ANTLR start "entryRuleCharacterAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:312:1: entryRuleCharacterAssignment : ruleCharacterAssignment EOF ;
    public final void entryRuleCharacterAssignment() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:313:1: ( ruleCharacterAssignment EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:314:1: ruleCharacterAssignment EOF
            {
             before(grammarAccess.getCharacterAssignmentRule()); 
            pushFollow(FOLLOW_ruleCharacterAssignment_in_entryRuleCharacterAssignment602);
            ruleCharacterAssignment();

            state._fsp--;

             after(grammarAccess.getCharacterAssignmentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterAssignment609); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCharacterAssignment"


    // $ANTLR start "ruleCharacterAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:321:1: ruleCharacterAssignment : ( ( rule__CharacterAssignment__ValueAssignment ) ) ;
    public final void ruleCharacterAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:325:2: ( ( ( rule__CharacterAssignment__ValueAssignment ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:326:1: ( ( rule__CharacterAssignment__ValueAssignment ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:326:1: ( ( rule__CharacterAssignment__ValueAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:327:1: ( rule__CharacterAssignment__ValueAssignment )
            {
             before(grammarAccess.getCharacterAssignmentAccess().getValueAssignment()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:328:1: ( rule__CharacterAssignment__ValueAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:328:2: rule__CharacterAssignment__ValueAssignment
            {
            pushFollow(FOLLOW_rule__CharacterAssignment__ValueAssignment_in_ruleCharacterAssignment635);
            rule__CharacterAssignment__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getCharacterAssignmentAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCharacterAssignment"


    // $ANTLR start "entryRuleCharacterVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:340:1: entryRuleCharacterVarVar : ruleCharacterVarVar EOF ;
    public final void entryRuleCharacterVarVar() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:341:1: ( ruleCharacterVarVar EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:342:1: ruleCharacterVarVar EOF
            {
             before(grammarAccess.getCharacterVarVarRule()); 
            pushFollow(FOLLOW_ruleCharacterVarVar_in_entryRuleCharacterVarVar662);
            ruleCharacterVarVar();

            state._fsp--;

             after(grammarAccess.getCharacterVarVarRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterVarVar669); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCharacterVarVar"


    // $ANTLR start "ruleCharacterVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:349:1: ruleCharacterVarVar : ( ( rule__CharacterVarVar__Group__0 ) ) ;
    public final void ruleCharacterVarVar() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:353:2: ( ( ( rule__CharacterVarVar__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:354:1: ( ( rule__CharacterVarVar__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:354:1: ( ( rule__CharacterVarVar__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:355:1: ( rule__CharacterVarVar__Group__0 )
            {
             before(grammarAccess.getCharacterVarVarAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:356:1: ( rule__CharacterVarVar__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:356:2: rule__CharacterVarVar__Group__0
            {
            pushFollow(FOLLOW_rule__CharacterVarVar__Group__0_in_ruleCharacterVarVar695);
            rule__CharacterVarVar__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCharacterVarVarAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCharacterVarVar"


    // $ANTLR start "entryRuleBooleanDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:368:1: entryRuleBooleanDeclarationAss : ruleBooleanDeclarationAss EOF ;
    public final void entryRuleBooleanDeclarationAss() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:369:1: ( ruleBooleanDeclarationAss EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:370:1: ruleBooleanDeclarationAss EOF
            {
             before(grammarAccess.getBooleanDeclarationAssRule()); 
            pushFollow(FOLLOW_ruleBooleanDeclarationAss_in_entryRuleBooleanDeclarationAss722);
            ruleBooleanDeclarationAss();

            state._fsp--;

             after(grammarAccess.getBooleanDeclarationAssRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanDeclarationAss729); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanDeclarationAss"


    // $ANTLR start "ruleBooleanDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:377:1: ruleBooleanDeclarationAss : ( ( rule__BooleanDeclarationAss__Group__0 ) ) ;
    public final void ruleBooleanDeclarationAss() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:381:2: ( ( ( rule__BooleanDeclarationAss__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:382:1: ( ( rule__BooleanDeclarationAss__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:382:1: ( ( rule__BooleanDeclarationAss__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:383:1: ( rule__BooleanDeclarationAss__Group__0 )
            {
             before(grammarAccess.getBooleanDeclarationAssAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:384:1: ( rule__BooleanDeclarationAss__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:384:2: rule__BooleanDeclarationAss__Group__0
            {
            pushFollow(FOLLOW_rule__BooleanDeclarationAss__Group__0_in_ruleBooleanDeclarationAss755);
            rule__BooleanDeclarationAss__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanDeclarationAssAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanDeclarationAss"


    // $ANTLR start "entryRuleBooleanDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:396:1: entryRuleBooleanDeclaration : ruleBooleanDeclaration EOF ;
    public final void entryRuleBooleanDeclaration() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:397:1: ( ruleBooleanDeclaration EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:398:1: ruleBooleanDeclaration EOF
            {
             before(grammarAccess.getBooleanDeclarationRule()); 
            pushFollow(FOLLOW_ruleBooleanDeclaration_in_entryRuleBooleanDeclaration782);
            ruleBooleanDeclaration();

            state._fsp--;

             after(grammarAccess.getBooleanDeclarationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanDeclaration789); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanDeclaration"


    // $ANTLR start "ruleBooleanDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:405:1: ruleBooleanDeclaration : ( ( rule__BooleanDeclaration__Group__0 ) ) ;
    public final void ruleBooleanDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:409:2: ( ( ( rule__BooleanDeclaration__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:410:1: ( ( rule__BooleanDeclaration__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:410:1: ( ( rule__BooleanDeclaration__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:411:1: ( rule__BooleanDeclaration__Group__0 )
            {
             before(grammarAccess.getBooleanDeclarationAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:412:1: ( rule__BooleanDeclaration__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:412:2: rule__BooleanDeclaration__Group__0
            {
            pushFollow(FOLLOW_rule__BooleanDeclaration__Group__0_in_ruleBooleanDeclaration815);
            rule__BooleanDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanDeclaration"


    // $ANTLR start "entryRuleBooleanTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:424:1: entryRuleBooleanTypeExpression : ruleBooleanTypeExpression EOF ;
    public final void entryRuleBooleanTypeExpression() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:425:1: ( ruleBooleanTypeExpression EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:426:1: ruleBooleanTypeExpression EOF
            {
             before(grammarAccess.getBooleanTypeExpressionRule()); 
            pushFollow(FOLLOW_ruleBooleanTypeExpression_in_entryRuleBooleanTypeExpression842);
            ruleBooleanTypeExpression();

            state._fsp--;

             after(grammarAccess.getBooleanTypeExpressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanTypeExpression849); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanTypeExpression"


    // $ANTLR start "ruleBooleanTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:433:1: ruleBooleanTypeExpression : ( ( rule__BooleanTypeExpression__Alternatives ) ) ;
    public final void ruleBooleanTypeExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:437:2: ( ( ( rule__BooleanTypeExpression__Alternatives ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:438:1: ( ( rule__BooleanTypeExpression__Alternatives ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:438:1: ( ( rule__BooleanTypeExpression__Alternatives ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:439:1: ( rule__BooleanTypeExpression__Alternatives )
            {
             before(grammarAccess.getBooleanTypeExpressionAccess().getAlternatives()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:440:1: ( rule__BooleanTypeExpression__Alternatives )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:440:2: rule__BooleanTypeExpression__Alternatives
            {
            pushFollow(FOLLOW_rule__BooleanTypeExpression__Alternatives_in_ruleBooleanTypeExpression875);
            rule__BooleanTypeExpression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBooleanTypeExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanTypeExpression"


    // $ANTLR start "entryRuleBooleanVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:452:1: entryRuleBooleanVarVar : ruleBooleanVarVar EOF ;
    public final void entryRuleBooleanVarVar() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:453:1: ( ruleBooleanVarVar EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:454:1: ruleBooleanVarVar EOF
            {
             before(grammarAccess.getBooleanVarVarRule()); 
            pushFollow(FOLLOW_ruleBooleanVarVar_in_entryRuleBooleanVarVar902);
            ruleBooleanVarVar();

            state._fsp--;

             after(grammarAccess.getBooleanVarVarRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanVarVar909); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanVarVar"


    // $ANTLR start "ruleBooleanVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:461:1: ruleBooleanVarVar : ( ( rule__BooleanVarVar__Alternatives ) ) ;
    public final void ruleBooleanVarVar() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:465:2: ( ( ( rule__BooleanVarVar__Alternatives ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:466:1: ( ( rule__BooleanVarVar__Alternatives ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:466:1: ( ( rule__BooleanVarVar__Alternatives ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:467:1: ( rule__BooleanVarVar__Alternatives )
            {
             before(grammarAccess.getBooleanVarVarAccess().getAlternatives()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:468:1: ( rule__BooleanVarVar__Alternatives )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:468:2: rule__BooleanVarVar__Alternatives
            {
            pushFollow(FOLLOW_rule__BooleanVarVar__Alternatives_in_ruleBooleanVarVar935);
            rule__BooleanVarVar__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBooleanVarVarAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanVarVar"


    // $ANTLR start "entryRuleBooleanAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:480:1: entryRuleBooleanAssignment : ruleBooleanAssignment EOF ;
    public final void entryRuleBooleanAssignment() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:481:1: ( ruleBooleanAssignment EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:482:1: ruleBooleanAssignment EOF
            {
             before(grammarAccess.getBooleanAssignmentRule()); 
            pushFollow(FOLLOW_ruleBooleanAssignment_in_entryRuleBooleanAssignment962);
            ruleBooleanAssignment();

            state._fsp--;

             after(grammarAccess.getBooleanAssignmentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanAssignment969); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanAssignment"


    // $ANTLR start "ruleBooleanAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:489:1: ruleBooleanAssignment : ( ( rule__BooleanAssignment__ValueAssignment ) ) ;
    public final void ruleBooleanAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:493:2: ( ( ( rule__BooleanAssignment__ValueAssignment ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:494:1: ( ( rule__BooleanAssignment__ValueAssignment ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:494:1: ( ( rule__BooleanAssignment__ValueAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:495:1: ( rule__BooleanAssignment__ValueAssignment )
            {
             before(grammarAccess.getBooleanAssignmentAccess().getValueAssignment()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:496:1: ( rule__BooleanAssignment__ValueAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:496:2: rule__BooleanAssignment__ValueAssignment
            {
            pushFollow(FOLLOW_rule__BooleanAssignment__ValueAssignment_in_ruleBooleanAssignment995);
            rule__BooleanAssignment__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBooleanAssignmentAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanAssignment"


    // $ANTLR start "entryRuleBooleanOperation"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:508:1: entryRuleBooleanOperation : ruleBooleanOperation EOF ;
    public final void entryRuleBooleanOperation() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:509:1: ( ruleBooleanOperation EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:510:1: ruleBooleanOperation EOF
            {
             before(grammarAccess.getBooleanOperationRule()); 
            pushFollow(FOLLOW_ruleBooleanOperation_in_entryRuleBooleanOperation1022);
            ruleBooleanOperation();

            state._fsp--;

             after(grammarAccess.getBooleanOperationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanOperation1029); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanOperation"


    // $ANTLR start "ruleBooleanOperation"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:517:1: ruleBooleanOperation : ( ( rule__BooleanOperation__Group__0 ) ) ;
    public final void ruleBooleanOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:521:2: ( ( ( rule__BooleanOperation__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:522:1: ( ( rule__BooleanOperation__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:522:1: ( ( rule__BooleanOperation__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:523:1: ( rule__BooleanOperation__Group__0 )
            {
             before(grammarAccess.getBooleanOperationAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:524:1: ( rule__BooleanOperation__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:524:2: rule__BooleanOperation__Group__0
            {
            pushFollow(FOLLOW_rule__BooleanOperation__Group__0_in_ruleBooleanOperation1055);
            rule__BooleanOperation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanOperationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanOperation"


    // $ANTLR start "entryRuleIntegerToBool"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:536:1: entryRuleIntegerToBool : ruleIntegerToBool EOF ;
    public final void entryRuleIntegerToBool() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:537:1: ( ruleIntegerToBool EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:538:1: ruleIntegerToBool EOF
            {
             before(grammarAccess.getIntegerToBoolRule()); 
            pushFollow(FOLLOW_ruleIntegerToBool_in_entryRuleIntegerToBool1082);
            ruleIntegerToBool();

            state._fsp--;

             after(grammarAccess.getIntegerToBoolRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerToBool1089); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerToBool"


    // $ANTLR start "ruleIntegerToBool"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:545:1: ruleIntegerToBool : ( ( rule__IntegerToBool__Group__0 ) ) ;
    public final void ruleIntegerToBool() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:549:2: ( ( ( rule__IntegerToBool__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:550:1: ( ( rule__IntegerToBool__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:550:1: ( ( rule__IntegerToBool__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:551:1: ( rule__IntegerToBool__Group__0 )
            {
             before(grammarAccess.getIntegerToBoolAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:552:1: ( rule__IntegerToBool__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:552:2: rule__IntegerToBool__Group__0
            {
            pushFollow(FOLLOW_rule__IntegerToBool__Group__0_in_ruleIntegerToBool1115);
            rule__IntegerToBool__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerToBoolAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerToBool"


    // $ANTLR start "entryRuleIntegerDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:564:1: entryRuleIntegerDeclarationAss : ruleIntegerDeclarationAss EOF ;
    public final void entryRuleIntegerDeclarationAss() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:565:1: ( ruleIntegerDeclarationAss EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:566:1: ruleIntegerDeclarationAss EOF
            {
             before(grammarAccess.getIntegerDeclarationAssRule()); 
            pushFollow(FOLLOW_ruleIntegerDeclarationAss_in_entryRuleIntegerDeclarationAss1142);
            ruleIntegerDeclarationAss();

            state._fsp--;

             after(grammarAccess.getIntegerDeclarationAssRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerDeclarationAss1149); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerDeclarationAss"


    // $ANTLR start "ruleIntegerDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:573:1: ruleIntegerDeclarationAss : ( ( rule__IntegerDeclarationAss__Group__0 ) ) ;
    public final void ruleIntegerDeclarationAss() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:577:2: ( ( ( rule__IntegerDeclarationAss__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:578:1: ( ( rule__IntegerDeclarationAss__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:578:1: ( ( rule__IntegerDeclarationAss__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:579:1: ( rule__IntegerDeclarationAss__Group__0 )
            {
             before(grammarAccess.getIntegerDeclarationAssAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:580:1: ( rule__IntegerDeclarationAss__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:580:2: rule__IntegerDeclarationAss__Group__0
            {
            pushFollow(FOLLOW_rule__IntegerDeclarationAss__Group__0_in_ruleIntegerDeclarationAss1175);
            rule__IntegerDeclarationAss__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerDeclarationAssAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerDeclarationAss"


    // $ANTLR start "entryRuleIntegerDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:592:1: entryRuleIntegerDeclaration : ruleIntegerDeclaration EOF ;
    public final void entryRuleIntegerDeclaration() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:593:1: ( ruleIntegerDeclaration EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:594:1: ruleIntegerDeclaration EOF
            {
             before(grammarAccess.getIntegerDeclarationRule()); 
            pushFollow(FOLLOW_ruleIntegerDeclaration_in_entryRuleIntegerDeclaration1202);
            ruleIntegerDeclaration();

            state._fsp--;

             after(grammarAccess.getIntegerDeclarationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerDeclaration1209); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerDeclaration"


    // $ANTLR start "ruleIntegerDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:601:1: ruleIntegerDeclaration : ( ( rule__IntegerDeclaration__Group__0 ) ) ;
    public final void ruleIntegerDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:605:2: ( ( ( rule__IntegerDeclaration__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:606:1: ( ( rule__IntegerDeclaration__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:606:1: ( ( rule__IntegerDeclaration__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:607:1: ( rule__IntegerDeclaration__Group__0 )
            {
             before(grammarAccess.getIntegerDeclarationAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:608:1: ( rule__IntegerDeclaration__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:608:2: rule__IntegerDeclaration__Group__0
            {
            pushFollow(FOLLOW_rule__IntegerDeclaration__Group__0_in_ruleIntegerDeclaration1235);
            rule__IntegerDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerDeclaration"


    // $ANTLR start "entryRuleIntegerVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:620:1: entryRuleIntegerVarVar : ruleIntegerVarVar EOF ;
    public final void entryRuleIntegerVarVar() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:621:1: ( ruleIntegerVarVar EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:622:1: ruleIntegerVarVar EOF
            {
             before(grammarAccess.getIntegerVarVarRule()); 
            pushFollow(FOLLOW_ruleIntegerVarVar_in_entryRuleIntegerVarVar1262);
            ruleIntegerVarVar();

            state._fsp--;

             after(grammarAccess.getIntegerVarVarRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerVarVar1269); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerVarVar"


    // $ANTLR start "ruleIntegerVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:629:1: ruleIntegerVarVar : ( ( rule__IntegerVarVar__Group__0 ) ) ;
    public final void ruleIntegerVarVar() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:633:2: ( ( ( rule__IntegerVarVar__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:634:1: ( ( rule__IntegerVarVar__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:634:1: ( ( rule__IntegerVarVar__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:635:1: ( rule__IntegerVarVar__Group__0 )
            {
             before(grammarAccess.getIntegerVarVarAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:636:1: ( rule__IntegerVarVar__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:636:2: rule__IntegerVarVar__Group__0
            {
            pushFollow(FOLLOW_rule__IntegerVarVar__Group__0_in_ruleIntegerVarVar1295);
            rule__IntegerVarVar__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerVarVarAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerVarVar"


    // $ANTLR start "entryRuleIntegerTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:648:1: entryRuleIntegerTypeExpression : ruleIntegerTypeExpression EOF ;
    public final void entryRuleIntegerTypeExpression() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:649:1: ( ruleIntegerTypeExpression EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:650:1: ruleIntegerTypeExpression EOF
            {
             before(grammarAccess.getIntegerTypeExpressionRule()); 
            pushFollow(FOLLOW_ruleIntegerTypeExpression_in_entryRuleIntegerTypeExpression1322);
            ruleIntegerTypeExpression();

            state._fsp--;

             after(grammarAccess.getIntegerTypeExpressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerTypeExpression1329); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerTypeExpression"


    // $ANTLR start "ruleIntegerTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:657:1: ruleIntegerTypeExpression : ( ( rule__IntegerTypeExpression__Alternatives ) ) ;
    public final void ruleIntegerTypeExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:661:2: ( ( ( rule__IntegerTypeExpression__Alternatives ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:662:1: ( ( rule__IntegerTypeExpression__Alternatives ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:662:1: ( ( rule__IntegerTypeExpression__Alternatives ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:663:1: ( rule__IntegerTypeExpression__Alternatives )
            {
             before(grammarAccess.getIntegerTypeExpressionAccess().getAlternatives()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:664:1: ( rule__IntegerTypeExpression__Alternatives )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:664:2: rule__IntegerTypeExpression__Alternatives
            {
            pushFollow(FOLLOW_rule__IntegerTypeExpression__Alternatives_in_ruleIntegerTypeExpression1355);
            rule__IntegerTypeExpression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getIntegerTypeExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerTypeExpression"


    // $ANTLR start "entryRuleIntegerAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:676:1: entryRuleIntegerAssignment : ruleIntegerAssignment EOF ;
    public final void entryRuleIntegerAssignment() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:677:1: ( ruleIntegerAssignment EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:678:1: ruleIntegerAssignment EOF
            {
             before(grammarAccess.getIntegerAssignmentRule()); 
            pushFollow(FOLLOW_ruleIntegerAssignment_in_entryRuleIntegerAssignment1382);
            ruleIntegerAssignment();

            state._fsp--;

             after(grammarAccess.getIntegerAssignmentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerAssignment1389); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerAssignment"


    // $ANTLR start "ruleIntegerAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:685:1: ruleIntegerAssignment : ( ( rule__IntegerAssignment__ValueAssignment ) ) ;
    public final void ruleIntegerAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:689:2: ( ( ( rule__IntegerAssignment__ValueAssignment ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:690:1: ( ( rule__IntegerAssignment__ValueAssignment ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:690:1: ( ( rule__IntegerAssignment__ValueAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:691:1: ( rule__IntegerAssignment__ValueAssignment )
            {
             before(grammarAccess.getIntegerAssignmentAccess().getValueAssignment()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:692:1: ( rule__IntegerAssignment__ValueAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:692:2: rule__IntegerAssignment__ValueAssignment
            {
            pushFollow(FOLLOW_rule__IntegerAssignment__ValueAssignment_in_ruleIntegerAssignment1415);
            rule__IntegerAssignment__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getIntegerAssignmentAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerAssignment"


    // $ANTLR start "entryRuleIntegerOperation"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:704:1: entryRuleIntegerOperation : ruleIntegerOperation EOF ;
    public final void entryRuleIntegerOperation() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:705:1: ( ruleIntegerOperation EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:706:1: ruleIntegerOperation EOF
            {
             before(grammarAccess.getIntegerOperationRule()); 
            pushFollow(FOLLOW_ruleIntegerOperation_in_entryRuleIntegerOperation1442);
            ruleIntegerOperation();

            state._fsp--;

             after(grammarAccess.getIntegerOperationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerOperation1449); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerOperation"


    // $ANTLR start "ruleIntegerOperation"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:713:1: ruleIntegerOperation : ( ( rule__IntegerOperation__Group__0 ) ) ;
    public final void ruleIntegerOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:717:2: ( ( ( rule__IntegerOperation__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:718:1: ( ( rule__IntegerOperation__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:718:1: ( ( rule__IntegerOperation__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:719:1: ( rule__IntegerOperation__Group__0 )
            {
             before(grammarAccess.getIntegerOperationAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:720:1: ( rule__IntegerOperation__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:720:2: rule__IntegerOperation__Group__0
            {
            pushFollow(FOLLOW_rule__IntegerOperation__Group__0_in_ruleIntegerOperation1475);
            rule__IntegerOperation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerOperationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerOperation"


    // $ANTLR start "entryRuleVariableName"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:732:1: entryRuleVariableName : ruleVariableName EOF ;
    public final void entryRuleVariableName() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:733:1: ( ruleVariableName EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:734:1: ruleVariableName EOF
            {
             before(grammarAccess.getVariableNameRule()); 
            pushFollow(FOLLOW_ruleVariableName_in_entryRuleVariableName1502);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getVariableNameRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariableName1509); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableName"


    // $ANTLR start "ruleVariableName"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:741:1: ruleVariableName : ( ( rule__VariableName__Group__0 ) ) ;
    public final void ruleVariableName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:745:2: ( ( ( rule__VariableName__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:746:1: ( ( rule__VariableName__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:746:1: ( ( rule__VariableName__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:747:1: ( rule__VariableName__Group__0 )
            {
             before(grammarAccess.getVariableNameAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:748:1: ( rule__VariableName__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:748:2: rule__VariableName__Group__0
            {
            pushFollow(FOLLOW_rule__VariableName__Group__0_in_ruleVariableName1535);
            rule__VariableName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVariableNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableName"


    // $ANTLR start "entryRuleCallFunction"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:760:1: entryRuleCallFunction : ruleCallFunction EOF ;
    public final void entryRuleCallFunction() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:761:1: ( ruleCallFunction EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:762:1: ruleCallFunction EOF
            {
             before(grammarAccess.getCallFunctionRule()); 
            pushFollow(FOLLOW_ruleCallFunction_in_entryRuleCallFunction1562);
            ruleCallFunction();

            state._fsp--;

             after(grammarAccess.getCallFunctionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCallFunction1569); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCallFunction"


    // $ANTLR start "ruleCallFunction"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:769:1: ruleCallFunction : ( ( rule__CallFunction__Group__0 ) ) ;
    public final void ruleCallFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:773:2: ( ( ( rule__CallFunction__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:774:1: ( ( rule__CallFunction__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:774:1: ( ( rule__CallFunction__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:775:1: ( rule__CallFunction__Group__0 )
            {
             before(grammarAccess.getCallFunctionAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:776:1: ( rule__CallFunction__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:776:2: rule__CallFunction__Group__0
            {
            pushFollow(FOLLOW_rule__CallFunction__Group__0_in_ruleCallFunction1595);
            rule__CallFunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCallFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCallFunction"


    // $ANTLR start "entryRuleFunctionName"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:788:1: entryRuleFunctionName : ruleFunctionName EOF ;
    public final void entryRuleFunctionName() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:789:1: ( ruleFunctionName EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:790:1: ruleFunctionName EOF
            {
             before(grammarAccess.getFunctionNameRule()); 
            pushFollow(FOLLOW_ruleFunctionName_in_entryRuleFunctionName1622);
            ruleFunctionName();

            state._fsp--;

             after(grammarAccess.getFunctionNameRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunctionName1629); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunctionName"


    // $ANTLR start "ruleFunctionName"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:797:1: ruleFunctionName : ( ( rule__FunctionName__ValueAssignment ) ) ;
    public final void ruleFunctionName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:801:2: ( ( ( rule__FunctionName__ValueAssignment ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:802:1: ( ( rule__FunctionName__ValueAssignment ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:802:1: ( ( rule__FunctionName__ValueAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:803:1: ( rule__FunctionName__ValueAssignment )
            {
             before(grammarAccess.getFunctionNameAccess().getValueAssignment()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:804:1: ( rule__FunctionName__ValueAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:804:2: rule__FunctionName__ValueAssignment
            {
            pushFollow(FOLLOW_rule__FunctionName__ValueAssignment_in_ruleFunctionName1655);
            rule__FunctionName__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getFunctionNameAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunctionName"


    // $ANTLR start "entryRuleBOOLOPERATOR"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:816:1: entryRuleBOOLOPERATOR : ruleBOOLOPERATOR EOF ;
    public final void entryRuleBOOLOPERATOR() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:817:1: ( ruleBOOLOPERATOR EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:818:1: ruleBOOLOPERATOR EOF
            {
             before(grammarAccess.getBOOLOPERATORRule()); 
            pushFollow(FOLLOW_ruleBOOLOPERATOR_in_entryRuleBOOLOPERATOR1682);
            ruleBOOLOPERATOR();

            state._fsp--;

             after(grammarAccess.getBOOLOPERATORRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBOOLOPERATOR1689); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBOOLOPERATOR"


    // $ANTLR start "ruleBOOLOPERATOR"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:825:1: ruleBOOLOPERATOR : ( ( rule__BOOLOPERATOR__Alternatives ) ) ;
    public final void ruleBOOLOPERATOR() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:829:2: ( ( ( rule__BOOLOPERATOR__Alternatives ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:830:1: ( ( rule__BOOLOPERATOR__Alternatives ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:830:1: ( ( rule__BOOLOPERATOR__Alternatives ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:831:1: ( rule__BOOLOPERATOR__Alternatives )
            {
             before(grammarAccess.getBOOLOPERATORAccess().getAlternatives()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:832:1: ( rule__BOOLOPERATOR__Alternatives )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:832:2: rule__BOOLOPERATOR__Alternatives
            {
            pushFollow(FOLLOW_rule__BOOLOPERATOR__Alternatives_in_ruleBOOLOPERATOR1715);
            rule__BOOLOPERATOR__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBOOLOPERATORAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBOOLOPERATOR"


    // $ANTLR start "entryRuleFunction"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:844:1: entryRuleFunction : ruleFunction EOF ;
    public final void entryRuleFunction() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:845:1: ( ruleFunction EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:846:1: ruleFunction EOF
            {
             before(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_ruleFunction_in_entryRuleFunction1742);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getFunctionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunction1749); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:853:1: ruleFunction : ( ( rule__Function__Group__0 ) ) ;
    public final void ruleFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:857:2: ( ( ( rule__Function__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:858:1: ( ( rule__Function__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:858:1: ( ( rule__Function__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:859:1: ( rule__Function__Group__0 )
            {
             before(grammarAccess.getFunctionAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:860:1: ( rule__Function__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:860:2: rule__Function__Group__0
            {
            pushFollow(FOLLOW_rule__Function__Group__0_in_ruleFunction1775);
            rule__Function__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleImport"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:872:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:873:1: ( ruleImport EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:874:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_ruleImport_in_entryRuleImport1802);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImport1809); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:881:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:885:2: ( ( ( rule__Import__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:886:1: ( ( rule__Import__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:886:1: ( ( rule__Import__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:887:1: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:888:1: ( rule__Import__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:888:2: rule__Import__Group__0
            {
            pushFollow(FOLLOW_rule__Import__Group__0_in_ruleImport1835);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:900:1: entryRuleQualifiedNameWithWildcard : ruleQualifiedNameWithWildcard EOF ;
    public final void entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:901:1: ( ruleQualifiedNameWithWildcard EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:902:1: ruleQualifiedNameWithWildcard EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard1862);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard1869); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:909:1: ruleQualifiedNameWithWildcard : ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) ;
    public final void ruleQualifiedNameWithWildcard() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:913:2: ( ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:914:1: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:914:1: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:915:1: ( rule__QualifiedNameWithWildcard__Group__0 )
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:916:1: ( rule__QualifiedNameWithWildcard__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:916:2: rule__QualifiedNameWithWildcard__Group__0
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group__0_in_ruleQualifiedNameWithWildcard1895);
            rule__QualifiedNameWithWildcard__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:928:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:929:1: ( ruleQualifiedName EOF )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:930:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName1922);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedName1929); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:937:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:941:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:942:1: ( ( rule__QualifiedName__Group__0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:942:1: ( ( rule__QualifiedName__Group__0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:943:1: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:944:1: ( rule__QualifiedName__Group__0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:944:2: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName1955);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "ruleOPERATOR"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:957:1: ruleOPERATOR : ( ( rule__OPERATOR__Alternatives ) ) ;
    public final void ruleOPERATOR() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:961:1: ( ( ( rule__OPERATOR__Alternatives ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:962:1: ( ( rule__OPERATOR__Alternatives ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:962:1: ( ( rule__OPERATOR__Alternatives ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:963:1: ( rule__OPERATOR__Alternatives )
            {
             before(grammarAccess.getOPERATORAccess().getAlternatives()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:964:1: ( rule__OPERATOR__Alternatives )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:964:2: rule__OPERATOR__Alternatives
            {
            pushFollow(FOLLOW_rule__OPERATOR__Alternatives_in_ruleOPERATOR1992);
            rule__OPERATOR__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOPERATORAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOPERATOR"


    // $ANTLR start "ruleSPECIALINTOPERATOR"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:976:1: ruleSPECIALINTOPERATOR : ( ( rule__SPECIALINTOPERATOR__Alternatives ) ) ;
    public final void ruleSPECIALINTOPERATOR() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:980:1: ( ( ( rule__SPECIALINTOPERATOR__Alternatives ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:981:1: ( ( rule__SPECIALINTOPERATOR__Alternatives ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:981:1: ( ( rule__SPECIALINTOPERATOR__Alternatives ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:982:1: ( rule__SPECIALINTOPERATOR__Alternatives )
            {
             before(grammarAccess.getSPECIALINTOPERATORAccess().getAlternatives()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:983:1: ( rule__SPECIALINTOPERATOR__Alternatives )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:983:2: rule__SPECIALINTOPERATOR__Alternatives
            {
            pushFollow(FOLLOW_rule__SPECIALINTOPERATOR__Alternatives_in_ruleSPECIALINTOPERATOR2028);
            rule__SPECIALINTOPERATOR__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSPECIALINTOPERATORAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSPECIALINTOPERATOR"


    // $ANTLR start "rule__Type__Alternatives"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:994:1: rule__Type__Alternatives : ( ( ( rule__Type__IntegerAssignment_0 ) ) | ( ( rule__Type__IntegervarvarAssignment_1 ) ) | ( ( rule__Type__BooleanAssignment_2 ) ) | ( ( rule__Type__BooleanvarvarAssignment_3 ) ) | ( ( rule__Type__CharacterAssignment_4 ) ) | ( ( rule__Type__CharactervarvarAssignment_5 ) ) | ( ( rule__Type__WhileAssignment_6 ) ) | ( ( rule__Type__IfAssignment_7 ) ) | ( ( rule__Type__PrintAssignment_8 ) ) | ( ( rule__Type__FunctionAssignment_9 ) ) | ( ruleImport ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:998:1: ( ( ( rule__Type__IntegerAssignment_0 ) ) | ( ( rule__Type__IntegervarvarAssignment_1 ) ) | ( ( rule__Type__BooleanAssignment_2 ) ) | ( ( rule__Type__BooleanvarvarAssignment_3 ) ) | ( ( rule__Type__CharacterAssignment_4 ) ) | ( ( rule__Type__CharactervarvarAssignment_5 ) ) | ( ( rule__Type__WhileAssignment_6 ) ) | ( ( rule__Type__IfAssignment_7 ) ) | ( ( rule__Type__PrintAssignment_8 ) ) | ( ( rule__Type__FunctionAssignment_9 ) ) | ( ruleImport ) )
            int alt2=11;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:999:1: ( ( rule__Type__IntegerAssignment_0 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:999:1: ( ( rule__Type__IntegerAssignment_0 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1000:1: ( rule__Type__IntegerAssignment_0 )
                    {
                     before(grammarAccess.getTypeAccess().getIntegerAssignment_0()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1001:1: ( rule__Type__IntegerAssignment_0 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1001:2: rule__Type__IntegerAssignment_0
                    {
                    pushFollow(FOLLOW_rule__Type__IntegerAssignment_0_in_rule__Type__Alternatives2063);
                    rule__Type__IntegerAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getIntegerAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1005:6: ( ( rule__Type__IntegervarvarAssignment_1 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1005:6: ( ( rule__Type__IntegervarvarAssignment_1 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1006:1: ( rule__Type__IntegervarvarAssignment_1 )
                    {
                     before(grammarAccess.getTypeAccess().getIntegervarvarAssignment_1()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1007:1: ( rule__Type__IntegervarvarAssignment_1 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1007:2: rule__Type__IntegervarvarAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Type__IntegervarvarAssignment_1_in_rule__Type__Alternatives2081);
                    rule__Type__IntegervarvarAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getIntegervarvarAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1011:6: ( ( rule__Type__BooleanAssignment_2 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1011:6: ( ( rule__Type__BooleanAssignment_2 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1012:1: ( rule__Type__BooleanAssignment_2 )
                    {
                     before(grammarAccess.getTypeAccess().getBooleanAssignment_2()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1013:1: ( rule__Type__BooleanAssignment_2 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1013:2: rule__Type__BooleanAssignment_2
                    {
                    pushFollow(FOLLOW_rule__Type__BooleanAssignment_2_in_rule__Type__Alternatives2099);
                    rule__Type__BooleanAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getBooleanAssignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1017:6: ( ( rule__Type__BooleanvarvarAssignment_3 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1017:6: ( ( rule__Type__BooleanvarvarAssignment_3 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1018:1: ( rule__Type__BooleanvarvarAssignment_3 )
                    {
                     before(grammarAccess.getTypeAccess().getBooleanvarvarAssignment_3()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1019:1: ( rule__Type__BooleanvarvarAssignment_3 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1019:2: rule__Type__BooleanvarvarAssignment_3
                    {
                    pushFollow(FOLLOW_rule__Type__BooleanvarvarAssignment_3_in_rule__Type__Alternatives2117);
                    rule__Type__BooleanvarvarAssignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getBooleanvarvarAssignment_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1023:6: ( ( rule__Type__CharacterAssignment_4 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1023:6: ( ( rule__Type__CharacterAssignment_4 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1024:1: ( rule__Type__CharacterAssignment_4 )
                    {
                     before(grammarAccess.getTypeAccess().getCharacterAssignment_4()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1025:1: ( rule__Type__CharacterAssignment_4 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1025:2: rule__Type__CharacterAssignment_4
                    {
                    pushFollow(FOLLOW_rule__Type__CharacterAssignment_4_in_rule__Type__Alternatives2135);
                    rule__Type__CharacterAssignment_4();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getCharacterAssignment_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1029:6: ( ( rule__Type__CharactervarvarAssignment_5 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1029:6: ( ( rule__Type__CharactervarvarAssignment_5 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1030:1: ( rule__Type__CharactervarvarAssignment_5 )
                    {
                     before(grammarAccess.getTypeAccess().getCharactervarvarAssignment_5()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1031:1: ( rule__Type__CharactervarvarAssignment_5 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1031:2: rule__Type__CharactervarvarAssignment_5
                    {
                    pushFollow(FOLLOW_rule__Type__CharactervarvarAssignment_5_in_rule__Type__Alternatives2153);
                    rule__Type__CharactervarvarAssignment_5();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getCharactervarvarAssignment_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1035:6: ( ( rule__Type__WhileAssignment_6 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1035:6: ( ( rule__Type__WhileAssignment_6 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1036:1: ( rule__Type__WhileAssignment_6 )
                    {
                     before(grammarAccess.getTypeAccess().getWhileAssignment_6()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1037:1: ( rule__Type__WhileAssignment_6 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1037:2: rule__Type__WhileAssignment_6
                    {
                    pushFollow(FOLLOW_rule__Type__WhileAssignment_6_in_rule__Type__Alternatives2171);
                    rule__Type__WhileAssignment_6();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getWhileAssignment_6()); 

                    }


                    }
                    break;
                case 8 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1041:6: ( ( rule__Type__IfAssignment_7 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1041:6: ( ( rule__Type__IfAssignment_7 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1042:1: ( rule__Type__IfAssignment_7 )
                    {
                     before(grammarAccess.getTypeAccess().getIfAssignment_7()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1043:1: ( rule__Type__IfAssignment_7 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1043:2: rule__Type__IfAssignment_7
                    {
                    pushFollow(FOLLOW_rule__Type__IfAssignment_7_in_rule__Type__Alternatives2189);
                    rule__Type__IfAssignment_7();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getIfAssignment_7()); 

                    }


                    }
                    break;
                case 9 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1047:6: ( ( rule__Type__PrintAssignment_8 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1047:6: ( ( rule__Type__PrintAssignment_8 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1048:1: ( rule__Type__PrintAssignment_8 )
                    {
                     before(grammarAccess.getTypeAccess().getPrintAssignment_8()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1049:1: ( rule__Type__PrintAssignment_8 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1049:2: rule__Type__PrintAssignment_8
                    {
                    pushFollow(FOLLOW_rule__Type__PrintAssignment_8_in_rule__Type__Alternatives2207);
                    rule__Type__PrintAssignment_8();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getPrintAssignment_8()); 

                    }


                    }
                    break;
                case 10 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1053:6: ( ( rule__Type__FunctionAssignment_9 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1053:6: ( ( rule__Type__FunctionAssignment_9 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1054:1: ( rule__Type__FunctionAssignment_9 )
                    {
                     before(grammarAccess.getTypeAccess().getFunctionAssignment_9()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1055:1: ( rule__Type__FunctionAssignment_9 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1055:2: rule__Type__FunctionAssignment_9
                    {
                    pushFollow(FOLLOW_rule__Type__FunctionAssignment_9_in_rule__Type__Alternatives2225);
                    rule__Type__FunctionAssignment_9();

                    state._fsp--;


                    }

                     after(grammarAccess.getTypeAccess().getFunctionAssignment_9()); 

                    }


                    }
                    break;
                case 11 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1059:6: ( ruleImport )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1059:6: ( ruleImport )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1060:1: ruleImport
                    {
                     before(grammarAccess.getTypeAccess().getImportParserRuleCall_10()); 
                    pushFollow(FOLLOW_ruleImport_in_rule__Type__Alternatives2243);
                    ruleImport();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getImportParserRuleCall_10()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__PrintL__ValueAlternatives_2_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1070:1: rule__PrintL__ValueAlternatives_2_0 : ( ( RULE_CHARACTERS ) | ( RULE_INTEGERS ) | ( RULE_BOOLVAL ) | ( ruleVariableName ) );
    public final void rule__PrintL__ValueAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1074:1: ( ( RULE_CHARACTERS ) | ( RULE_INTEGERS ) | ( RULE_BOOLVAL ) | ( ruleVariableName ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case RULE_CHARACTERS:
                {
                alt3=1;
                }
                break;
            case RULE_INTEGERS:
                {
                alt3=2;
                }
                break;
            case RULE_BOOLVAL:
                {
                alt3=3;
                }
                break;
            case 35:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1075:1: ( RULE_CHARACTERS )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1075:1: ( RULE_CHARACTERS )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1076:1: RULE_CHARACTERS
                    {
                     before(grammarAccess.getPrintLAccess().getValueCHARACTERSTerminalRuleCall_2_0_0()); 
                    match(input,RULE_CHARACTERS,FOLLOW_RULE_CHARACTERS_in_rule__PrintL__ValueAlternatives_2_02275); 
                     after(grammarAccess.getPrintLAccess().getValueCHARACTERSTerminalRuleCall_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1081:6: ( RULE_INTEGERS )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1081:6: ( RULE_INTEGERS )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1082:1: RULE_INTEGERS
                    {
                     before(grammarAccess.getPrintLAccess().getValueINTEGERSTerminalRuleCall_2_0_1()); 
                    match(input,RULE_INTEGERS,FOLLOW_RULE_INTEGERS_in_rule__PrintL__ValueAlternatives_2_02292); 
                     after(grammarAccess.getPrintLAccess().getValueINTEGERSTerminalRuleCall_2_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1087:6: ( RULE_BOOLVAL )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1087:6: ( RULE_BOOLVAL )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1088:1: RULE_BOOLVAL
                    {
                     before(grammarAccess.getPrintLAccess().getValueBOOLVALTerminalRuleCall_2_0_2()); 
                    match(input,RULE_BOOLVAL,FOLLOW_RULE_BOOLVAL_in_rule__PrintL__ValueAlternatives_2_02309); 
                     after(grammarAccess.getPrintLAccess().getValueBOOLVALTerminalRuleCall_2_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1093:6: ( ruleVariableName )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1093:6: ( ruleVariableName )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1094:1: ruleVariableName
                    {
                     before(grammarAccess.getPrintLAccess().getValueVariableNameParserRuleCall_2_0_3()); 
                    pushFollow(FOLLOW_ruleVariableName_in_rule__PrintL__ValueAlternatives_2_02326);
                    ruleVariableName();

                    state._fsp--;

                     after(grammarAccess.getPrintLAccess().getValueVariableNameParserRuleCall_2_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__ValueAlternatives_2_0"


    // $ANTLR start "rule__CharacterAssignment__ValueAlternatives_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1104:1: rule__CharacterAssignment__ValueAlternatives_0 : ( ( RULE_CHARACTERS ) | ( ruleVariableName ) );
    public final void rule__CharacterAssignment__ValueAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1108:1: ( ( RULE_CHARACTERS ) | ( ruleVariableName ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_CHARACTERS) ) {
                alt4=1;
            }
            else if ( (LA4_0==35) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1109:1: ( RULE_CHARACTERS )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1109:1: ( RULE_CHARACTERS )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1110:1: RULE_CHARACTERS
                    {
                     before(grammarAccess.getCharacterAssignmentAccess().getValueCHARACTERSTerminalRuleCall_0_0()); 
                    match(input,RULE_CHARACTERS,FOLLOW_RULE_CHARACTERS_in_rule__CharacterAssignment__ValueAlternatives_02358); 
                     after(grammarAccess.getCharacterAssignmentAccess().getValueCHARACTERSTerminalRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1115:6: ( ruleVariableName )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1115:6: ( ruleVariableName )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1116:1: ruleVariableName
                    {
                     before(grammarAccess.getCharacterAssignmentAccess().getValueVariableNameParserRuleCall_0_1()); 
                    pushFollow(FOLLOW_ruleVariableName_in_rule__CharacterAssignment__ValueAlternatives_02375);
                    ruleVariableName();

                    state._fsp--;

                     after(grammarAccess.getCharacterAssignmentAccess().getValueVariableNameParserRuleCall_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterAssignment__ValueAlternatives_0"


    // $ANTLR start "rule__BooleanTypeExpression__Alternatives"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1126:1: rule__BooleanTypeExpression__Alternatives : ( ( ( rule__BooleanTypeExpression__BoolAssignAssignment_0 ) ) | ( ( rule__BooleanTypeExpression__BoolOpAssignment_1 ) ) | ( ( rule__BooleanTypeExpression__IntToBoolAssignment_2 ) ) );
    public final void rule__BooleanTypeExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1130:1: ( ( ( rule__BooleanTypeExpression__BoolAssignAssignment_0 ) ) | ( ( rule__BooleanTypeExpression__BoolOpAssignment_1 ) ) | ( ( rule__BooleanTypeExpression__IntToBoolAssignment_2 ) ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case RULE_BOOLVAL:
                {
                int LA5_1 = input.LA(2);

                if ( ((LA5_1>=41 && LA5_1<=42)) ) {
                    alt5=2;
                }
                else if ( (LA5_1==EOF||LA5_1==RULE_INTEGERS||LA5_1==RULE_ID||(LA5_1>=25 && LA5_1<=26)||(LA5_1>=28 && LA5_1<=29)||(LA5_1>=32 && LA5_1<=35)||LA5_1==37||LA5_1==40) ) {
                    alt5=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }
                }
                break;
            case 35:
                {
                int LA5_2 = input.LA(2);

                if ( (LA5_2==RULE_ID) ) {
                    switch ( input.LA(3) ) {
                    case EOF:
                    case RULE_INTEGERS:
                    case RULE_ID:
                    case 25:
                    case 26:
                    case 28:
                    case 29:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 37:
                    case 40:
                        {
                        alt5=1;
                        }
                        break;
                    case 41:
                    case 42:
                        {
                        alt5=2;
                        }
                        break;
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                        {
                        alt5=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 5, 6, input);

                        throw nvae;
                    }

                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INTEGERS:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1131:1: ( ( rule__BooleanTypeExpression__BoolAssignAssignment_0 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1131:1: ( ( rule__BooleanTypeExpression__BoolAssignAssignment_0 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1132:1: ( rule__BooleanTypeExpression__BoolAssignAssignment_0 )
                    {
                     before(grammarAccess.getBooleanTypeExpressionAccess().getBoolAssignAssignment_0()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1133:1: ( rule__BooleanTypeExpression__BoolAssignAssignment_0 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1133:2: rule__BooleanTypeExpression__BoolAssignAssignment_0
                    {
                    pushFollow(FOLLOW_rule__BooleanTypeExpression__BoolAssignAssignment_0_in_rule__BooleanTypeExpression__Alternatives2407);
                    rule__BooleanTypeExpression__BoolAssignAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanTypeExpressionAccess().getBoolAssignAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1137:6: ( ( rule__BooleanTypeExpression__BoolOpAssignment_1 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1137:6: ( ( rule__BooleanTypeExpression__BoolOpAssignment_1 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1138:1: ( rule__BooleanTypeExpression__BoolOpAssignment_1 )
                    {
                     before(grammarAccess.getBooleanTypeExpressionAccess().getBoolOpAssignment_1()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1139:1: ( rule__BooleanTypeExpression__BoolOpAssignment_1 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1139:2: rule__BooleanTypeExpression__BoolOpAssignment_1
                    {
                    pushFollow(FOLLOW_rule__BooleanTypeExpression__BoolOpAssignment_1_in_rule__BooleanTypeExpression__Alternatives2425);
                    rule__BooleanTypeExpression__BoolOpAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanTypeExpressionAccess().getBoolOpAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1143:6: ( ( rule__BooleanTypeExpression__IntToBoolAssignment_2 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1143:6: ( ( rule__BooleanTypeExpression__IntToBoolAssignment_2 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1144:1: ( rule__BooleanTypeExpression__IntToBoolAssignment_2 )
                    {
                     before(grammarAccess.getBooleanTypeExpressionAccess().getIntToBoolAssignment_2()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1145:1: ( rule__BooleanTypeExpression__IntToBoolAssignment_2 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1145:2: rule__BooleanTypeExpression__IntToBoolAssignment_2
                    {
                    pushFollow(FOLLOW_rule__BooleanTypeExpression__IntToBoolAssignment_2_in_rule__BooleanTypeExpression__Alternatives2443);
                    rule__BooleanTypeExpression__IntToBoolAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanTypeExpressionAccess().getIntToBoolAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanTypeExpression__Alternatives"


    // $ANTLR start "rule__BooleanVarVar__Alternatives"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1154:1: rule__BooleanVarVar__Alternatives : ( ( ( rule__BooleanVarVar__Group_0__0 ) ) | ( ( rule__BooleanVarVar__Value2Assignment_1 ) ) );
    public final void rule__BooleanVarVar__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1158:1: ( ( ( rule__BooleanVarVar__Group_0__0 ) ) | ( ( rule__BooleanVarVar__Value2Assignment_1 ) ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==35) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==RULE_ID) ) {
                    int LA6_3 = input.LA(3);

                    if ( (LA6_3==31) ) {
                        alt6=1;
                    }
                    else if ( ((LA6_3>=17 && LA6_3<=23)) ) {
                        alt6=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 3, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA6_0==RULE_INTEGERS) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1159:1: ( ( rule__BooleanVarVar__Group_0__0 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1159:1: ( ( rule__BooleanVarVar__Group_0__0 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1160:1: ( rule__BooleanVarVar__Group_0__0 )
                    {
                     before(grammarAccess.getBooleanVarVarAccess().getGroup_0()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1161:1: ( rule__BooleanVarVar__Group_0__0 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1161:2: rule__BooleanVarVar__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__BooleanVarVar__Group_0__0_in_rule__BooleanVarVar__Alternatives2476);
                    rule__BooleanVarVar__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanVarVarAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1165:6: ( ( rule__BooleanVarVar__Value2Assignment_1 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1165:6: ( ( rule__BooleanVarVar__Value2Assignment_1 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1166:1: ( rule__BooleanVarVar__Value2Assignment_1 )
                    {
                     before(grammarAccess.getBooleanVarVarAccess().getValue2Assignment_1()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1167:1: ( rule__BooleanVarVar__Value2Assignment_1 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1167:2: rule__BooleanVarVar__Value2Assignment_1
                    {
                    pushFollow(FOLLOW_rule__BooleanVarVar__Value2Assignment_1_in_rule__BooleanVarVar__Alternatives2494);
                    rule__BooleanVarVar__Value2Assignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanVarVarAccess().getValue2Assignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__Alternatives"


    // $ANTLR start "rule__BooleanAssignment__ValueAlternatives_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1176:1: rule__BooleanAssignment__ValueAlternatives_0 : ( ( RULE_BOOLVAL ) | ( ruleVariableName ) );
    public final void rule__BooleanAssignment__ValueAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1180:1: ( ( RULE_BOOLVAL ) | ( ruleVariableName ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_BOOLVAL) ) {
                alt7=1;
            }
            else if ( (LA7_0==35) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1181:1: ( RULE_BOOLVAL )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1181:1: ( RULE_BOOLVAL )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1182:1: RULE_BOOLVAL
                    {
                     before(grammarAccess.getBooleanAssignmentAccess().getValueBOOLVALTerminalRuleCall_0_0()); 
                    match(input,RULE_BOOLVAL,FOLLOW_RULE_BOOLVAL_in_rule__BooleanAssignment__ValueAlternatives_02527); 
                     after(grammarAccess.getBooleanAssignmentAccess().getValueBOOLVALTerminalRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1187:6: ( ruleVariableName )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1187:6: ( ruleVariableName )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1188:1: ruleVariableName
                    {
                     before(grammarAccess.getBooleanAssignmentAccess().getValueVariableNameParserRuleCall_0_1()); 
                    pushFollow(FOLLOW_ruleVariableName_in_rule__BooleanAssignment__ValueAlternatives_02544);
                    ruleVariableName();

                    state._fsp--;

                     after(grammarAccess.getBooleanAssignmentAccess().getValueVariableNameParserRuleCall_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAssignment__ValueAlternatives_0"


    // $ANTLR start "rule__IntegerTypeExpression__Alternatives"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1198:1: rule__IntegerTypeExpression__Alternatives : ( ( ( rule__IntegerTypeExpression__IntegerAssignAssignment_0 ) ) | ( ( rule__IntegerTypeExpression__IntegerOpAssignment_1 ) ) );
    public final void rule__IntegerTypeExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1202:1: ( ( ( rule__IntegerTypeExpression__IntegerAssignAssignment_0 ) ) | ( ( rule__IntegerTypeExpression__IntegerOpAssignment_1 ) ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_INTEGERS) ) {
                int LA8_1 = input.LA(2);

                if ( (LA8_1==EOF||LA8_1==RULE_INTEGERS||LA8_1==RULE_ID||(LA8_1>=21 && LA8_1<=23)||(LA8_1>=25 && LA8_1<=26)||(LA8_1>=28 && LA8_1<=29)||(LA8_1>=32 && LA8_1<=35)||LA8_1==37||LA8_1==40) ) {
                    alt8=1;
                }
                else if ( ((LA8_1>=17 && LA8_1<=20)) ) {
                    alt8=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA8_0==35) ) {
                int LA8_2 = input.LA(2);

                if ( (LA8_2==RULE_ID) ) {
                    int LA8_5 = input.LA(3);

                    if ( (LA8_5==EOF||LA8_5==RULE_INTEGERS||LA8_5==RULE_ID||(LA8_5>=21 && LA8_5<=23)||(LA8_5>=25 && LA8_5<=26)||(LA8_5>=28 && LA8_5<=29)||(LA8_5>=32 && LA8_5<=35)||LA8_5==37||LA8_5==40) ) {
                        alt8=1;
                    }
                    else if ( ((LA8_5>=17 && LA8_5<=20)) ) {
                        alt8=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 8, 5, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1203:1: ( ( rule__IntegerTypeExpression__IntegerAssignAssignment_0 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1203:1: ( ( rule__IntegerTypeExpression__IntegerAssignAssignment_0 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1204:1: ( rule__IntegerTypeExpression__IntegerAssignAssignment_0 )
                    {
                     before(grammarAccess.getIntegerTypeExpressionAccess().getIntegerAssignAssignment_0()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1205:1: ( rule__IntegerTypeExpression__IntegerAssignAssignment_0 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1205:2: rule__IntegerTypeExpression__IntegerAssignAssignment_0
                    {
                    pushFollow(FOLLOW_rule__IntegerTypeExpression__IntegerAssignAssignment_0_in_rule__IntegerTypeExpression__Alternatives2576);
                    rule__IntegerTypeExpression__IntegerAssignAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getIntegerTypeExpressionAccess().getIntegerAssignAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1209:6: ( ( rule__IntegerTypeExpression__IntegerOpAssignment_1 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1209:6: ( ( rule__IntegerTypeExpression__IntegerOpAssignment_1 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1210:1: ( rule__IntegerTypeExpression__IntegerOpAssignment_1 )
                    {
                     before(grammarAccess.getIntegerTypeExpressionAccess().getIntegerOpAssignment_1()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1211:1: ( rule__IntegerTypeExpression__IntegerOpAssignment_1 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1211:2: rule__IntegerTypeExpression__IntegerOpAssignment_1
                    {
                    pushFollow(FOLLOW_rule__IntegerTypeExpression__IntegerOpAssignment_1_in_rule__IntegerTypeExpression__Alternatives2594);
                    rule__IntegerTypeExpression__IntegerOpAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getIntegerTypeExpressionAccess().getIntegerOpAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerTypeExpression__Alternatives"


    // $ANTLR start "rule__IntegerAssignment__ValueAlternatives_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1220:1: rule__IntegerAssignment__ValueAlternatives_0 : ( ( RULE_INTEGERS ) | ( ruleVariableName ) );
    public final void rule__IntegerAssignment__ValueAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1224:1: ( ( RULE_INTEGERS ) | ( ruleVariableName ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_INTEGERS) ) {
                alt9=1;
            }
            else if ( (LA9_0==35) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1225:1: ( RULE_INTEGERS )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1225:1: ( RULE_INTEGERS )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1226:1: RULE_INTEGERS
                    {
                     before(grammarAccess.getIntegerAssignmentAccess().getValueINTEGERSTerminalRuleCall_0_0()); 
                    match(input,RULE_INTEGERS,FOLLOW_RULE_INTEGERS_in_rule__IntegerAssignment__ValueAlternatives_02627); 
                     after(grammarAccess.getIntegerAssignmentAccess().getValueINTEGERSTerminalRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1231:6: ( ruleVariableName )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1231:6: ( ruleVariableName )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1232:1: ruleVariableName
                    {
                     before(grammarAccess.getIntegerAssignmentAccess().getValueVariableNameParserRuleCall_0_1()); 
                    pushFollow(FOLLOW_ruleVariableName_in_rule__IntegerAssignment__ValueAlternatives_02644);
                    ruleVariableName();

                    state._fsp--;

                     after(grammarAccess.getIntegerAssignmentAccess().getValueVariableNameParserRuleCall_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerAssignment__ValueAlternatives_0"


    // $ANTLR start "rule__BOOLOPERATOR__Alternatives"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1242:1: rule__BOOLOPERATOR__Alternatives : ( ( ( rule__BOOLOPERATOR__ANDAssignment_0 ) ) | ( ( rule__BOOLOPERATOR__ORAssignment_1 ) ) );
    public final void rule__BOOLOPERATOR__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1246:1: ( ( ( rule__BOOLOPERATOR__ANDAssignment_0 ) ) | ( ( rule__BOOLOPERATOR__ORAssignment_1 ) ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==41) ) {
                alt10=1;
            }
            else if ( (LA10_0==42) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1247:1: ( ( rule__BOOLOPERATOR__ANDAssignment_0 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1247:1: ( ( rule__BOOLOPERATOR__ANDAssignment_0 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1248:1: ( rule__BOOLOPERATOR__ANDAssignment_0 )
                    {
                     before(grammarAccess.getBOOLOPERATORAccess().getANDAssignment_0()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1249:1: ( rule__BOOLOPERATOR__ANDAssignment_0 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1249:2: rule__BOOLOPERATOR__ANDAssignment_0
                    {
                    pushFollow(FOLLOW_rule__BOOLOPERATOR__ANDAssignment_0_in_rule__BOOLOPERATOR__Alternatives2676);
                    rule__BOOLOPERATOR__ANDAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBOOLOPERATORAccess().getANDAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1253:6: ( ( rule__BOOLOPERATOR__ORAssignment_1 ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1253:6: ( ( rule__BOOLOPERATOR__ORAssignment_1 ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1254:1: ( rule__BOOLOPERATOR__ORAssignment_1 )
                    {
                     before(grammarAccess.getBOOLOPERATORAccess().getORAssignment_1()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1255:1: ( rule__BOOLOPERATOR__ORAssignment_1 )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1255:2: rule__BOOLOPERATOR__ORAssignment_1
                    {
                    pushFollow(FOLLOW_rule__BOOLOPERATOR__ORAssignment_1_in_rule__BOOLOPERATOR__Alternatives2694);
                    rule__BOOLOPERATOR__ORAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getBOOLOPERATORAccess().getORAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLOPERATOR__Alternatives"


    // $ANTLR start "rule__OPERATOR__Alternatives"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1264:1: rule__OPERATOR__Alternatives : ( ( ( '+' ) ) | ( ( '-' ) ) | ( ( '*' ) ) | ( ( '/' ) ) );
    public final void rule__OPERATOR__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1268:1: ( ( ( '+' ) ) | ( ( '-' ) ) | ( ( '*' ) ) | ( ( '/' ) ) )
            int alt11=4;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt11=1;
                }
                break;
            case 18:
                {
                alt11=2;
                }
                break;
            case 19:
                {
                alt11=3;
                }
                break;
            case 20:
                {
                alt11=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1269:1: ( ( '+' ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1269:1: ( ( '+' ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1270:1: ( '+' )
                    {
                     before(grammarAccess.getOPERATORAccess().getADDITIONEnumLiteralDeclaration_0()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1271:1: ( '+' )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1271:3: '+'
                    {
                    match(input,17,FOLLOW_17_in_rule__OPERATOR__Alternatives2728); 

                    }

                     after(grammarAccess.getOPERATORAccess().getADDITIONEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1276:6: ( ( '-' ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1276:6: ( ( '-' ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1277:1: ( '-' )
                    {
                     before(grammarAccess.getOPERATORAccess().getSUBRACTIONEnumLiteralDeclaration_1()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1278:1: ( '-' )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1278:3: '-'
                    {
                    match(input,18,FOLLOW_18_in_rule__OPERATOR__Alternatives2749); 

                    }

                     after(grammarAccess.getOPERATORAccess().getSUBRACTIONEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1283:6: ( ( '*' ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1283:6: ( ( '*' ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1284:1: ( '*' )
                    {
                     before(grammarAccess.getOPERATORAccess().getMULTIPLYEnumLiteralDeclaration_2()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1285:1: ( '*' )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1285:3: '*'
                    {
                    match(input,19,FOLLOW_19_in_rule__OPERATOR__Alternatives2770); 

                    }

                     after(grammarAccess.getOPERATORAccess().getMULTIPLYEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1290:6: ( ( '/' ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1290:6: ( ( '/' ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1291:1: ( '/' )
                    {
                     before(grammarAccess.getOPERATORAccess().getDIVISIONEnumLiteralDeclaration_3()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1292:1: ( '/' )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1292:3: '/'
                    {
                    match(input,20,FOLLOW_20_in_rule__OPERATOR__Alternatives2791); 

                    }

                     after(grammarAccess.getOPERATORAccess().getDIVISIONEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OPERATOR__Alternatives"


    // $ANTLR start "rule__SPECIALINTOPERATOR__Alternatives"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1302:1: rule__SPECIALINTOPERATOR__Alternatives : ( ( ( '==' ) ) | ( ( '<' ) ) | ( ( '>' ) ) );
    public final void rule__SPECIALINTOPERATOR__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1306:1: ( ( ( '==' ) ) | ( ( '<' ) ) | ( ( '>' ) ) )
            int alt12=3;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt12=1;
                }
                break;
            case 22:
                {
                alt12=2;
                }
                break;
            case 23:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1307:1: ( ( '==' ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1307:1: ( ( '==' ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1308:1: ( '==' )
                    {
                     before(grammarAccess.getSPECIALINTOPERATORAccess().getEQUALSEnumLiteralDeclaration_0()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1309:1: ( '==' )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1309:3: '=='
                    {
                    match(input,21,FOLLOW_21_in_rule__SPECIALINTOPERATOR__Alternatives2827); 

                    }

                     after(grammarAccess.getSPECIALINTOPERATORAccess().getEQUALSEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1314:6: ( ( '<' ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1314:6: ( ( '<' ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1315:1: ( '<' )
                    {
                     before(grammarAccess.getSPECIALINTOPERATORAccess().getLESSEnumLiteralDeclaration_1()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1316:1: ( '<' )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1316:3: '<'
                    {
                    match(input,22,FOLLOW_22_in_rule__SPECIALINTOPERATOR__Alternatives2848); 

                    }

                     after(grammarAccess.getSPECIALINTOPERATORAccess().getLESSEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1321:6: ( ( '>' ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1321:6: ( ( '>' ) )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1322:1: ( '>' )
                    {
                     before(grammarAccess.getSPECIALINTOPERATORAccess().getMOREEnumLiteralDeclaration_2()); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1323:1: ( '>' )
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1323:3: '>'
                    {
                    match(input,23,FOLLOW_23_in_rule__SPECIALINTOPERATOR__Alternatives2869); 

                    }

                     after(grammarAccess.getSPECIALINTOPERATORAccess().getMOREEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SPECIALINTOPERATOR__Alternatives"


    // $ANTLR start "rule__PrintL__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1335:1: rule__PrintL__Group__0 : rule__PrintL__Group__0__Impl rule__PrintL__Group__1 ;
    public final void rule__PrintL__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1339:1: ( rule__PrintL__Group__0__Impl rule__PrintL__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1340:2: rule__PrintL__Group__0__Impl rule__PrintL__Group__1
            {
            pushFollow(FOLLOW_rule__PrintL__Group__0__Impl_in_rule__PrintL__Group__02902);
            rule__PrintL__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PrintL__Group__1_in_rule__PrintL__Group__02905);
            rule__PrintL__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__Group__0"


    // $ANTLR start "rule__PrintL__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1347:1: rule__PrintL__Group__0__Impl : ( ( rule__PrintL__PrintAssignment_0 ) ) ;
    public final void rule__PrintL__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1351:1: ( ( ( rule__PrintL__PrintAssignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1352:1: ( ( rule__PrintL__PrintAssignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1352:1: ( ( rule__PrintL__PrintAssignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1353:1: ( rule__PrintL__PrintAssignment_0 )
            {
             before(grammarAccess.getPrintLAccess().getPrintAssignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1354:1: ( rule__PrintL__PrintAssignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1354:2: rule__PrintL__PrintAssignment_0
            {
            pushFollow(FOLLOW_rule__PrintL__PrintAssignment_0_in_rule__PrintL__Group__0__Impl2932);
            rule__PrintL__PrintAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPrintLAccess().getPrintAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__Group__0__Impl"


    // $ANTLR start "rule__PrintL__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1364:1: rule__PrintL__Group__1 : rule__PrintL__Group__1__Impl rule__PrintL__Group__2 ;
    public final void rule__PrintL__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1368:1: ( rule__PrintL__Group__1__Impl rule__PrintL__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1369:2: rule__PrintL__Group__1__Impl rule__PrintL__Group__2
            {
            pushFollow(FOLLOW_rule__PrintL__Group__1__Impl_in_rule__PrintL__Group__12962);
            rule__PrintL__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PrintL__Group__2_in_rule__PrintL__Group__12965);
            rule__PrintL__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__Group__1"


    // $ANTLR start "rule__PrintL__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1376:1: rule__PrintL__Group__1__Impl : ( '(' ) ;
    public final void rule__PrintL__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1380:1: ( ( '(' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1381:1: ( '(' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1381:1: ( '(' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1382:1: '('
            {
             before(grammarAccess.getPrintLAccess().getLeftParenthesisKeyword_1()); 
            match(input,24,FOLLOW_24_in_rule__PrintL__Group__1__Impl2993); 
             after(grammarAccess.getPrintLAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__Group__1__Impl"


    // $ANTLR start "rule__PrintL__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1395:1: rule__PrintL__Group__2 : rule__PrintL__Group__2__Impl rule__PrintL__Group__3 ;
    public final void rule__PrintL__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1399:1: ( rule__PrintL__Group__2__Impl rule__PrintL__Group__3 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1400:2: rule__PrintL__Group__2__Impl rule__PrintL__Group__3
            {
            pushFollow(FOLLOW_rule__PrintL__Group__2__Impl_in_rule__PrintL__Group__23024);
            rule__PrintL__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PrintL__Group__3_in_rule__PrintL__Group__23027);
            rule__PrintL__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__Group__2"


    // $ANTLR start "rule__PrintL__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1407:1: rule__PrintL__Group__2__Impl : ( ( rule__PrintL__ValueAssignment_2 ) ) ;
    public final void rule__PrintL__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1411:1: ( ( ( rule__PrintL__ValueAssignment_2 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1412:1: ( ( rule__PrintL__ValueAssignment_2 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1412:1: ( ( rule__PrintL__ValueAssignment_2 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1413:1: ( rule__PrintL__ValueAssignment_2 )
            {
             before(grammarAccess.getPrintLAccess().getValueAssignment_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1414:1: ( rule__PrintL__ValueAssignment_2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1414:2: rule__PrintL__ValueAssignment_2
            {
            pushFollow(FOLLOW_rule__PrintL__ValueAssignment_2_in_rule__PrintL__Group__2__Impl3054);
            rule__PrintL__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getPrintLAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__Group__2__Impl"


    // $ANTLR start "rule__PrintL__Group__3"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1424:1: rule__PrintL__Group__3 : rule__PrintL__Group__3__Impl ;
    public final void rule__PrintL__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1428:1: ( rule__PrintL__Group__3__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1429:2: rule__PrintL__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__PrintL__Group__3__Impl_in_rule__PrintL__Group__33084);
            rule__PrintL__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__Group__3"


    // $ANTLR start "rule__PrintL__Group__3__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1435:1: rule__PrintL__Group__3__Impl : ( ')' ) ;
    public final void rule__PrintL__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1439:1: ( ( ')' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1440:1: ( ')' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1440:1: ( ')' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1441:1: ')'
            {
             before(grammarAccess.getPrintLAccess().getRightParenthesisKeyword_3()); 
            match(input,25,FOLLOW_25_in_rule__PrintL__Group__3__Impl3112); 
             after(grammarAccess.getPrintLAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__Group__3__Impl"


    // $ANTLR start "rule__Statement_While__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1462:1: rule__Statement_While__Group__0 : rule__Statement_While__Group__0__Impl rule__Statement_While__Group__1 ;
    public final void rule__Statement_While__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1466:1: ( rule__Statement_While__Group__0__Impl rule__Statement_While__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1467:2: rule__Statement_While__Group__0__Impl rule__Statement_While__Group__1
            {
            pushFollow(FOLLOW_rule__Statement_While__Group__0__Impl_in_rule__Statement_While__Group__03151);
            rule__Statement_While__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Statement_While__Group__1_in_rule__Statement_While__Group__03154);
            rule__Statement_While__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__0"


    // $ANTLR start "rule__Statement_While__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1474:1: rule__Statement_While__Group__0__Impl : ( 'kol' ) ;
    public final void rule__Statement_While__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1478:1: ( ( 'kol' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1479:1: ( 'kol' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1479:1: ( 'kol' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1480:1: 'kol'
            {
             before(grammarAccess.getStatement_WhileAccess().getKolKeyword_0()); 
            match(input,26,FOLLOW_26_in_rule__Statement_While__Group__0__Impl3182); 
             after(grammarAccess.getStatement_WhileAccess().getKolKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__0__Impl"


    // $ANTLR start "rule__Statement_While__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1493:1: rule__Statement_While__Group__1 : rule__Statement_While__Group__1__Impl rule__Statement_While__Group__2 ;
    public final void rule__Statement_While__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1497:1: ( rule__Statement_While__Group__1__Impl rule__Statement_While__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1498:2: rule__Statement_While__Group__1__Impl rule__Statement_While__Group__2
            {
            pushFollow(FOLLOW_rule__Statement_While__Group__1__Impl_in_rule__Statement_While__Group__13213);
            rule__Statement_While__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Statement_While__Group__2_in_rule__Statement_While__Group__13216);
            rule__Statement_While__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__1"


    // $ANTLR start "rule__Statement_While__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1505:1: rule__Statement_While__Group__1__Impl : ( '(' ) ;
    public final void rule__Statement_While__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1509:1: ( ( '(' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1510:1: ( '(' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1510:1: ( '(' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1511:1: '('
            {
             before(grammarAccess.getStatement_WhileAccess().getLeftParenthesisKeyword_1()); 
            match(input,24,FOLLOW_24_in_rule__Statement_While__Group__1__Impl3244); 
             after(grammarAccess.getStatement_WhileAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__1__Impl"


    // $ANTLR start "rule__Statement_While__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1524:1: rule__Statement_While__Group__2 : rule__Statement_While__Group__2__Impl rule__Statement_While__Group__3 ;
    public final void rule__Statement_While__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1528:1: ( rule__Statement_While__Group__2__Impl rule__Statement_While__Group__3 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1529:2: rule__Statement_While__Group__2__Impl rule__Statement_While__Group__3
            {
            pushFollow(FOLLOW_rule__Statement_While__Group__2__Impl_in_rule__Statement_While__Group__23275);
            rule__Statement_While__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Statement_While__Group__3_in_rule__Statement_While__Group__23278);
            rule__Statement_While__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__2"


    // $ANTLR start "rule__Statement_While__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1536:1: rule__Statement_While__Group__2__Impl : ( ( rule__Statement_While__CondOneAssignment_2 ) ) ;
    public final void rule__Statement_While__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1540:1: ( ( ( rule__Statement_While__CondOneAssignment_2 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1541:1: ( ( rule__Statement_While__CondOneAssignment_2 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1541:1: ( ( rule__Statement_While__CondOneAssignment_2 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1542:1: ( rule__Statement_While__CondOneAssignment_2 )
            {
             before(grammarAccess.getStatement_WhileAccess().getCondOneAssignment_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1543:1: ( rule__Statement_While__CondOneAssignment_2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1543:2: rule__Statement_While__CondOneAssignment_2
            {
            pushFollow(FOLLOW_rule__Statement_While__CondOneAssignment_2_in_rule__Statement_While__Group__2__Impl3305);
            rule__Statement_While__CondOneAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getStatement_WhileAccess().getCondOneAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__2__Impl"


    // $ANTLR start "rule__Statement_While__Group__3"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1553:1: rule__Statement_While__Group__3 : rule__Statement_While__Group__3__Impl rule__Statement_While__Group__4 ;
    public final void rule__Statement_While__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1557:1: ( rule__Statement_While__Group__3__Impl rule__Statement_While__Group__4 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1558:2: rule__Statement_While__Group__3__Impl rule__Statement_While__Group__4
            {
            pushFollow(FOLLOW_rule__Statement_While__Group__3__Impl_in_rule__Statement_While__Group__33335);
            rule__Statement_While__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Statement_While__Group__4_in_rule__Statement_While__Group__33338);
            rule__Statement_While__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__3"


    // $ANTLR start "rule__Statement_While__Group__3__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1565:1: rule__Statement_While__Group__3__Impl : ( ')' ) ;
    public final void rule__Statement_While__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1569:1: ( ( ')' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1570:1: ( ')' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1570:1: ( ')' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1571:1: ')'
            {
             before(grammarAccess.getStatement_WhileAccess().getRightParenthesisKeyword_3()); 
            match(input,25,FOLLOW_25_in_rule__Statement_While__Group__3__Impl3366); 
             after(grammarAccess.getStatement_WhileAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__3__Impl"


    // $ANTLR start "rule__Statement_While__Group__4"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1584:1: rule__Statement_While__Group__4 : rule__Statement_While__Group__4__Impl rule__Statement_While__Group__5 ;
    public final void rule__Statement_While__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1588:1: ( rule__Statement_While__Group__4__Impl rule__Statement_While__Group__5 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1589:2: rule__Statement_While__Group__4__Impl rule__Statement_While__Group__5
            {
            pushFollow(FOLLOW_rule__Statement_While__Group__4__Impl_in_rule__Statement_While__Group__43397);
            rule__Statement_While__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Statement_While__Group__5_in_rule__Statement_While__Group__43400);
            rule__Statement_While__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__4"


    // $ANTLR start "rule__Statement_While__Group__4__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1596:1: rule__Statement_While__Group__4__Impl : ( '{' ) ;
    public final void rule__Statement_While__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1600:1: ( ( '{' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1601:1: ( '{' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1601:1: ( '{' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1602:1: '{'
            {
             before(grammarAccess.getStatement_WhileAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,27,FOLLOW_27_in_rule__Statement_While__Group__4__Impl3428); 
             after(grammarAccess.getStatement_WhileAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__4__Impl"


    // $ANTLR start "rule__Statement_While__Group__5"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1615:1: rule__Statement_While__Group__5 : rule__Statement_While__Group__5__Impl rule__Statement_While__Group__6 ;
    public final void rule__Statement_While__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1619:1: ( rule__Statement_While__Group__5__Impl rule__Statement_While__Group__6 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1620:2: rule__Statement_While__Group__5__Impl rule__Statement_While__Group__6
            {
            pushFollow(FOLLOW_rule__Statement_While__Group__5__Impl_in_rule__Statement_While__Group__53459);
            rule__Statement_While__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Statement_While__Group__6_in_rule__Statement_While__Group__53462);
            rule__Statement_While__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__5"


    // $ANTLR start "rule__Statement_While__Group__5__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1627:1: rule__Statement_While__Group__5__Impl : ( ( rule__Statement_While__OperationsAssignment_5 )* ) ;
    public final void rule__Statement_While__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1631:1: ( ( ( rule__Statement_While__OperationsAssignment_5 )* ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1632:1: ( ( rule__Statement_While__OperationsAssignment_5 )* )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1632:1: ( ( rule__Statement_While__OperationsAssignment_5 )* )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1633:1: ( rule__Statement_While__OperationsAssignment_5 )*
            {
             before(grammarAccess.getStatement_WhileAccess().getOperationsAssignment_5()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1634:1: ( rule__Statement_While__OperationsAssignment_5 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_INTEGERS||LA13_0==RULE_ID||LA13_0==26||LA13_0==29||(LA13_0>=32 && LA13_0<=35)||LA13_0==37||LA13_0==40) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1634:2: rule__Statement_While__OperationsAssignment_5
            	    {
            	    pushFollow(FOLLOW_rule__Statement_While__OperationsAssignment_5_in_rule__Statement_While__Group__5__Impl3489);
            	    rule__Statement_While__OperationsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getStatement_WhileAccess().getOperationsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__5__Impl"


    // $ANTLR start "rule__Statement_While__Group__6"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1644:1: rule__Statement_While__Group__6 : rule__Statement_While__Group__6__Impl ;
    public final void rule__Statement_While__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1648:1: ( rule__Statement_While__Group__6__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1649:2: rule__Statement_While__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__Statement_While__Group__6__Impl_in_rule__Statement_While__Group__63520);
            rule__Statement_While__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__6"


    // $ANTLR start "rule__Statement_While__Group__6__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1655:1: rule__Statement_While__Group__6__Impl : ( '}' ) ;
    public final void rule__Statement_While__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1659:1: ( ( '}' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1660:1: ( '}' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1660:1: ( '}' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1661:1: '}'
            {
             before(grammarAccess.getStatement_WhileAccess().getRightCurlyBracketKeyword_6()); 
            match(input,28,FOLLOW_28_in_rule__Statement_While__Group__6__Impl3548); 
             after(grammarAccess.getStatement_WhileAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__Group__6__Impl"


    // $ANTLR start "rule__If_statment__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1688:1: rule__If_statment__Group__0 : rule__If_statment__Group__0__Impl rule__If_statment__Group__1 ;
    public final void rule__If_statment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1692:1: ( rule__If_statment__Group__0__Impl rule__If_statment__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1693:2: rule__If_statment__Group__0__Impl rule__If_statment__Group__1
            {
            pushFollow(FOLLOW_rule__If_statment__Group__0__Impl_in_rule__If_statment__Group__03593);
            rule__If_statment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__If_statment__Group__1_in_rule__If_statment__Group__03596);
            rule__If_statment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__0"


    // $ANTLR start "rule__If_statment__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1700:1: rule__If_statment__Group__0__Impl : ( 'jei' ) ;
    public final void rule__If_statment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1704:1: ( ( 'jei' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1705:1: ( 'jei' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1705:1: ( 'jei' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1706:1: 'jei'
            {
             before(grammarAccess.getIf_statmentAccess().getJeiKeyword_0()); 
            match(input,29,FOLLOW_29_in_rule__If_statment__Group__0__Impl3624); 
             after(grammarAccess.getIf_statmentAccess().getJeiKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__0__Impl"


    // $ANTLR start "rule__If_statment__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1719:1: rule__If_statment__Group__1 : rule__If_statment__Group__1__Impl rule__If_statment__Group__2 ;
    public final void rule__If_statment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1723:1: ( rule__If_statment__Group__1__Impl rule__If_statment__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1724:2: rule__If_statment__Group__1__Impl rule__If_statment__Group__2
            {
            pushFollow(FOLLOW_rule__If_statment__Group__1__Impl_in_rule__If_statment__Group__13655);
            rule__If_statment__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__If_statment__Group__2_in_rule__If_statment__Group__13658);
            rule__If_statment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__1"


    // $ANTLR start "rule__If_statment__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1731:1: rule__If_statment__Group__1__Impl : ( '(' ) ;
    public final void rule__If_statment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1735:1: ( ( '(' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1736:1: ( '(' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1736:1: ( '(' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1737:1: '('
            {
             before(grammarAccess.getIf_statmentAccess().getLeftParenthesisKeyword_1()); 
            match(input,24,FOLLOW_24_in_rule__If_statment__Group__1__Impl3686); 
             after(grammarAccess.getIf_statmentAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__1__Impl"


    // $ANTLR start "rule__If_statment__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1750:1: rule__If_statment__Group__2 : rule__If_statment__Group__2__Impl rule__If_statment__Group__3 ;
    public final void rule__If_statment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1754:1: ( rule__If_statment__Group__2__Impl rule__If_statment__Group__3 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1755:2: rule__If_statment__Group__2__Impl rule__If_statment__Group__3
            {
            pushFollow(FOLLOW_rule__If_statment__Group__2__Impl_in_rule__If_statment__Group__23717);
            rule__If_statment__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__If_statment__Group__3_in_rule__If_statment__Group__23720);
            rule__If_statment__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__2"


    // $ANTLR start "rule__If_statment__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1762:1: rule__If_statment__Group__2__Impl : ( ( rule__If_statment__CondOneAssignment_2 ) ) ;
    public final void rule__If_statment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1766:1: ( ( ( rule__If_statment__CondOneAssignment_2 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1767:1: ( ( rule__If_statment__CondOneAssignment_2 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1767:1: ( ( rule__If_statment__CondOneAssignment_2 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1768:1: ( rule__If_statment__CondOneAssignment_2 )
            {
             before(grammarAccess.getIf_statmentAccess().getCondOneAssignment_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1769:1: ( rule__If_statment__CondOneAssignment_2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1769:2: rule__If_statment__CondOneAssignment_2
            {
            pushFollow(FOLLOW_rule__If_statment__CondOneAssignment_2_in_rule__If_statment__Group__2__Impl3747);
            rule__If_statment__CondOneAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getIf_statmentAccess().getCondOneAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__2__Impl"


    // $ANTLR start "rule__If_statment__Group__3"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1779:1: rule__If_statment__Group__3 : rule__If_statment__Group__3__Impl rule__If_statment__Group__4 ;
    public final void rule__If_statment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1783:1: ( rule__If_statment__Group__3__Impl rule__If_statment__Group__4 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1784:2: rule__If_statment__Group__3__Impl rule__If_statment__Group__4
            {
            pushFollow(FOLLOW_rule__If_statment__Group__3__Impl_in_rule__If_statment__Group__33777);
            rule__If_statment__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__If_statment__Group__4_in_rule__If_statment__Group__33780);
            rule__If_statment__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__3"


    // $ANTLR start "rule__If_statment__Group__3__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1791:1: rule__If_statment__Group__3__Impl : ( ')' ) ;
    public final void rule__If_statment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1795:1: ( ( ')' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1796:1: ( ')' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1796:1: ( ')' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1797:1: ')'
            {
             before(grammarAccess.getIf_statmentAccess().getRightParenthesisKeyword_3()); 
            match(input,25,FOLLOW_25_in_rule__If_statment__Group__3__Impl3808); 
             after(grammarAccess.getIf_statmentAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__3__Impl"


    // $ANTLR start "rule__If_statment__Group__4"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1810:1: rule__If_statment__Group__4 : rule__If_statment__Group__4__Impl rule__If_statment__Group__5 ;
    public final void rule__If_statment__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1814:1: ( rule__If_statment__Group__4__Impl rule__If_statment__Group__5 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1815:2: rule__If_statment__Group__4__Impl rule__If_statment__Group__5
            {
            pushFollow(FOLLOW_rule__If_statment__Group__4__Impl_in_rule__If_statment__Group__43839);
            rule__If_statment__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__If_statment__Group__5_in_rule__If_statment__Group__43842);
            rule__If_statment__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__4"


    // $ANTLR start "rule__If_statment__Group__4__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1822:1: rule__If_statment__Group__4__Impl : ( '{' ) ;
    public final void rule__If_statment__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1826:1: ( ( '{' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1827:1: ( '{' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1827:1: ( '{' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1828:1: '{'
            {
             before(grammarAccess.getIf_statmentAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,27,FOLLOW_27_in_rule__If_statment__Group__4__Impl3870); 
             after(grammarAccess.getIf_statmentAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__4__Impl"


    // $ANTLR start "rule__If_statment__Group__5"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1841:1: rule__If_statment__Group__5 : rule__If_statment__Group__5__Impl rule__If_statment__Group__6 ;
    public final void rule__If_statment__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1845:1: ( rule__If_statment__Group__5__Impl rule__If_statment__Group__6 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1846:2: rule__If_statment__Group__5__Impl rule__If_statment__Group__6
            {
            pushFollow(FOLLOW_rule__If_statment__Group__5__Impl_in_rule__If_statment__Group__53901);
            rule__If_statment__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__If_statment__Group__6_in_rule__If_statment__Group__53904);
            rule__If_statment__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__5"


    // $ANTLR start "rule__If_statment__Group__5__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1853:1: rule__If_statment__Group__5__Impl : ( ( rule__If_statment__OperationsAssignment_5 )* ) ;
    public final void rule__If_statment__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1857:1: ( ( ( rule__If_statment__OperationsAssignment_5 )* ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1858:1: ( ( rule__If_statment__OperationsAssignment_5 )* )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1858:1: ( ( rule__If_statment__OperationsAssignment_5 )* )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1859:1: ( rule__If_statment__OperationsAssignment_5 )*
            {
             before(grammarAccess.getIf_statmentAccess().getOperationsAssignment_5()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1860:1: ( rule__If_statment__OperationsAssignment_5 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_INTEGERS||LA14_0==RULE_ID||LA14_0==26||LA14_0==29||(LA14_0>=32 && LA14_0<=35)||LA14_0==37||LA14_0==40) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1860:2: rule__If_statment__OperationsAssignment_5
            	    {
            	    pushFollow(FOLLOW_rule__If_statment__OperationsAssignment_5_in_rule__If_statment__Group__5__Impl3931);
            	    rule__If_statment__OperationsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getIf_statmentAccess().getOperationsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__5__Impl"


    // $ANTLR start "rule__If_statment__Group__6"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1870:1: rule__If_statment__Group__6 : rule__If_statment__Group__6__Impl rule__If_statment__Group__7 ;
    public final void rule__If_statment__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1874:1: ( rule__If_statment__Group__6__Impl rule__If_statment__Group__7 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1875:2: rule__If_statment__Group__6__Impl rule__If_statment__Group__7
            {
            pushFollow(FOLLOW_rule__If_statment__Group__6__Impl_in_rule__If_statment__Group__63962);
            rule__If_statment__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__If_statment__Group__7_in_rule__If_statment__Group__63965);
            rule__If_statment__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__6"


    // $ANTLR start "rule__If_statment__Group__6__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1882:1: rule__If_statment__Group__6__Impl : ( '}' ) ;
    public final void rule__If_statment__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1886:1: ( ( '}' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1887:1: ( '}' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1887:1: ( '}' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1888:1: '}'
            {
             before(grammarAccess.getIf_statmentAccess().getRightCurlyBracketKeyword_6()); 
            match(input,28,FOLLOW_28_in_rule__If_statment__Group__6__Impl3993); 
             after(grammarAccess.getIf_statmentAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__6__Impl"


    // $ANTLR start "rule__If_statment__Group__7"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1901:1: rule__If_statment__Group__7 : rule__If_statment__Group__7__Impl ;
    public final void rule__If_statment__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1905:1: ( rule__If_statment__Group__7__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1906:2: rule__If_statment__Group__7__Impl
            {
            pushFollow(FOLLOW_rule__If_statment__Group__7__Impl_in_rule__If_statment__Group__74024);
            rule__If_statment__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__7"


    // $ANTLR start "rule__If_statment__Group__7__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1912:1: rule__If_statment__Group__7__Impl : ( ( rule__If_statment__ElseAssignment_7 )? ) ;
    public final void rule__If_statment__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1916:1: ( ( ( rule__If_statment__ElseAssignment_7 )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1917:1: ( ( rule__If_statment__ElseAssignment_7 )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1917:1: ( ( rule__If_statment__ElseAssignment_7 )? )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1918:1: ( rule__If_statment__ElseAssignment_7 )?
            {
             before(grammarAccess.getIf_statmentAccess().getElseAssignment_7()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1919:1: ( rule__If_statment__ElseAssignment_7 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==30) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1919:2: rule__If_statment__ElseAssignment_7
                    {
                    pushFollow(FOLLOW_rule__If_statment__ElseAssignment_7_in_rule__If_statment__Group__7__Impl4051);
                    rule__If_statment__ElseAssignment_7();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getIf_statmentAccess().getElseAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__Group__7__Impl"


    // $ANTLR start "rule__Else__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1945:1: rule__Else__Group__0 : rule__Else__Group__0__Impl rule__Else__Group__1 ;
    public final void rule__Else__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1949:1: ( rule__Else__Group__0__Impl rule__Else__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1950:2: rule__Else__Group__0__Impl rule__Else__Group__1
            {
            pushFollow(FOLLOW_rule__Else__Group__0__Impl_in_rule__Else__Group__04098);
            rule__Else__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Else__Group__1_in_rule__Else__Group__04101);
            rule__Else__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__0"


    // $ANTLR start "rule__Else__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1957:1: rule__Else__Group__0__Impl : ( 'kitaip' ) ;
    public final void rule__Else__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1961:1: ( ( 'kitaip' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1962:1: ( 'kitaip' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1962:1: ( 'kitaip' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1963:1: 'kitaip'
            {
             before(grammarAccess.getElseAccess().getKitaipKeyword_0()); 
            match(input,30,FOLLOW_30_in_rule__Else__Group__0__Impl4129); 
             after(grammarAccess.getElseAccess().getKitaipKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__0__Impl"


    // $ANTLR start "rule__Else__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1976:1: rule__Else__Group__1 : rule__Else__Group__1__Impl rule__Else__Group__2 ;
    public final void rule__Else__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1980:1: ( rule__Else__Group__1__Impl rule__Else__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1981:2: rule__Else__Group__1__Impl rule__Else__Group__2
            {
            pushFollow(FOLLOW_rule__Else__Group__1__Impl_in_rule__Else__Group__14160);
            rule__Else__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Else__Group__2_in_rule__Else__Group__14163);
            rule__Else__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__1"


    // $ANTLR start "rule__Else__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1988:1: rule__Else__Group__1__Impl : ( '{' ) ;
    public final void rule__Else__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1992:1: ( ( '{' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1993:1: ( '{' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1993:1: ( '{' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1994:1: '{'
            {
             before(grammarAccess.getElseAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,27,FOLLOW_27_in_rule__Else__Group__1__Impl4191); 
             after(grammarAccess.getElseAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__1__Impl"


    // $ANTLR start "rule__Else__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2007:1: rule__Else__Group__2 : rule__Else__Group__2__Impl rule__Else__Group__3 ;
    public final void rule__Else__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2011:1: ( rule__Else__Group__2__Impl rule__Else__Group__3 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2012:2: rule__Else__Group__2__Impl rule__Else__Group__3
            {
            pushFollow(FOLLOW_rule__Else__Group__2__Impl_in_rule__Else__Group__24222);
            rule__Else__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Else__Group__3_in_rule__Else__Group__24225);
            rule__Else__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__2"


    // $ANTLR start "rule__Else__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2019:1: rule__Else__Group__2__Impl : ( ( rule__Else__OperationsAssignment_2 )* ) ;
    public final void rule__Else__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2023:1: ( ( ( rule__Else__OperationsAssignment_2 )* ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2024:1: ( ( rule__Else__OperationsAssignment_2 )* )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2024:1: ( ( rule__Else__OperationsAssignment_2 )* )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2025:1: ( rule__Else__OperationsAssignment_2 )*
            {
             before(grammarAccess.getElseAccess().getOperationsAssignment_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2026:1: ( rule__Else__OperationsAssignment_2 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_INTEGERS||LA16_0==RULE_ID||LA16_0==26||LA16_0==29||(LA16_0>=32 && LA16_0<=35)||LA16_0==37||LA16_0==40) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2026:2: rule__Else__OperationsAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__Else__OperationsAssignment_2_in_rule__Else__Group__2__Impl4252);
            	    rule__Else__OperationsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getElseAccess().getOperationsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__2__Impl"


    // $ANTLR start "rule__Else__Group__3"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2036:1: rule__Else__Group__3 : rule__Else__Group__3__Impl ;
    public final void rule__Else__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2040:1: ( rule__Else__Group__3__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2041:2: rule__Else__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Else__Group__3__Impl_in_rule__Else__Group__34283);
            rule__Else__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__3"


    // $ANTLR start "rule__Else__Group__3__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2047:1: rule__Else__Group__3__Impl : ( '}' ) ;
    public final void rule__Else__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2051:1: ( ( '}' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2052:1: ( '}' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2052:1: ( '}' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2053:1: '}'
            {
             before(grammarAccess.getElseAccess().getRightCurlyBracketKeyword_3()); 
            match(input,28,FOLLOW_28_in_rule__Else__Group__3__Impl4311); 
             after(grammarAccess.getElseAccess().getRightCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__3__Impl"


    // $ANTLR start "rule__CharacterDeclarationAss__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2074:1: rule__CharacterDeclarationAss__Group__0 : rule__CharacterDeclarationAss__Group__0__Impl rule__CharacterDeclarationAss__Group__1 ;
    public final void rule__CharacterDeclarationAss__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2078:1: ( rule__CharacterDeclarationAss__Group__0__Impl rule__CharacterDeclarationAss__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2079:2: rule__CharacterDeclarationAss__Group__0__Impl rule__CharacterDeclarationAss__Group__1
            {
            pushFollow(FOLLOW_rule__CharacterDeclarationAss__Group__0__Impl_in_rule__CharacterDeclarationAss__Group__04350);
            rule__CharacterDeclarationAss__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CharacterDeclarationAss__Group__1_in_rule__CharacterDeclarationAss__Group__04353);
            rule__CharacterDeclarationAss__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__Group__0"


    // $ANTLR start "rule__CharacterDeclarationAss__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2086:1: rule__CharacterDeclarationAss__Group__0__Impl : ( ( rule__CharacterDeclarationAss__AssignAssignment_0 ) ) ;
    public final void rule__CharacterDeclarationAss__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2090:1: ( ( ( rule__CharacterDeclarationAss__AssignAssignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2091:1: ( ( rule__CharacterDeclarationAss__AssignAssignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2091:1: ( ( rule__CharacterDeclarationAss__AssignAssignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2092:1: ( rule__CharacterDeclarationAss__AssignAssignment_0 )
            {
             before(grammarAccess.getCharacterDeclarationAssAccess().getAssignAssignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2093:1: ( rule__CharacterDeclarationAss__AssignAssignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2093:2: rule__CharacterDeclarationAss__AssignAssignment_0
            {
            pushFollow(FOLLOW_rule__CharacterDeclarationAss__AssignAssignment_0_in_rule__CharacterDeclarationAss__Group__0__Impl4380);
            rule__CharacterDeclarationAss__AssignAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getCharacterDeclarationAssAccess().getAssignAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__Group__0__Impl"


    // $ANTLR start "rule__CharacterDeclarationAss__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2103:1: rule__CharacterDeclarationAss__Group__1 : rule__CharacterDeclarationAss__Group__1__Impl ;
    public final void rule__CharacterDeclarationAss__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2107:1: ( rule__CharacterDeclarationAss__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2108:2: rule__CharacterDeclarationAss__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__CharacterDeclarationAss__Group__1__Impl_in_rule__CharacterDeclarationAss__Group__14410);
            rule__CharacterDeclarationAss__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__Group__1"


    // $ANTLR start "rule__CharacterDeclarationAss__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2114:1: rule__CharacterDeclarationAss__Group__1__Impl : ( ( rule__CharacterDeclarationAss__Group_1__0 )? ) ;
    public final void rule__CharacterDeclarationAss__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2118:1: ( ( ( rule__CharacterDeclarationAss__Group_1__0 )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2119:1: ( ( rule__CharacterDeclarationAss__Group_1__0 )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2119:1: ( ( rule__CharacterDeclarationAss__Group_1__0 )? )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2120:1: ( rule__CharacterDeclarationAss__Group_1__0 )?
            {
             before(grammarAccess.getCharacterDeclarationAssAccess().getGroup_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2121:1: ( rule__CharacterDeclarationAss__Group_1__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==31) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2121:2: rule__CharacterDeclarationAss__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__CharacterDeclarationAss__Group_1__0_in_rule__CharacterDeclarationAss__Group__1__Impl4437);
                    rule__CharacterDeclarationAss__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCharacterDeclarationAssAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__Group__1__Impl"


    // $ANTLR start "rule__CharacterDeclarationAss__Group_1__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2135:1: rule__CharacterDeclarationAss__Group_1__0 : rule__CharacterDeclarationAss__Group_1__0__Impl rule__CharacterDeclarationAss__Group_1__1 ;
    public final void rule__CharacterDeclarationAss__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2139:1: ( rule__CharacterDeclarationAss__Group_1__0__Impl rule__CharacterDeclarationAss__Group_1__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2140:2: rule__CharacterDeclarationAss__Group_1__0__Impl rule__CharacterDeclarationAss__Group_1__1
            {
            pushFollow(FOLLOW_rule__CharacterDeclarationAss__Group_1__0__Impl_in_rule__CharacterDeclarationAss__Group_1__04472);
            rule__CharacterDeclarationAss__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CharacterDeclarationAss__Group_1__1_in_rule__CharacterDeclarationAss__Group_1__04475);
            rule__CharacterDeclarationAss__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__Group_1__0"


    // $ANTLR start "rule__CharacterDeclarationAss__Group_1__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2147:1: rule__CharacterDeclarationAss__Group_1__0__Impl : ( '=' ) ;
    public final void rule__CharacterDeclarationAss__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2151:1: ( ( '=' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2152:1: ( '=' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2152:1: ( '=' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2153:1: '='
            {
             before(grammarAccess.getCharacterDeclarationAssAccess().getEqualsSignKeyword_1_0()); 
            match(input,31,FOLLOW_31_in_rule__CharacterDeclarationAss__Group_1__0__Impl4503); 
             after(grammarAccess.getCharacterDeclarationAssAccess().getEqualsSignKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__Group_1__0__Impl"


    // $ANTLR start "rule__CharacterDeclarationAss__Group_1__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2166:1: rule__CharacterDeclarationAss__Group_1__1 : rule__CharacterDeclarationAss__Group_1__1__Impl ;
    public final void rule__CharacterDeclarationAss__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2170:1: ( rule__CharacterDeclarationAss__Group_1__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2171:2: rule__CharacterDeclarationAss__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__CharacterDeclarationAss__Group_1__1__Impl_in_rule__CharacterDeclarationAss__Group_1__14534);
            rule__CharacterDeclarationAss__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__Group_1__1"


    // $ANTLR start "rule__CharacterDeclarationAss__Group_1__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2177:1: rule__CharacterDeclarationAss__Group_1__1__Impl : ( ( rule__CharacterDeclarationAss__ValueAssignment_1_1 ) ) ;
    public final void rule__CharacterDeclarationAss__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2181:1: ( ( ( rule__CharacterDeclarationAss__ValueAssignment_1_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2182:1: ( ( rule__CharacterDeclarationAss__ValueAssignment_1_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2182:1: ( ( rule__CharacterDeclarationAss__ValueAssignment_1_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2183:1: ( rule__CharacterDeclarationAss__ValueAssignment_1_1 )
            {
             before(grammarAccess.getCharacterDeclarationAssAccess().getValueAssignment_1_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2184:1: ( rule__CharacterDeclarationAss__ValueAssignment_1_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2184:2: rule__CharacterDeclarationAss__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_rule__CharacterDeclarationAss__ValueAssignment_1_1_in_rule__CharacterDeclarationAss__Group_1__1__Impl4561);
            rule__CharacterDeclarationAss__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getCharacterDeclarationAssAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__Group_1__1__Impl"


    // $ANTLR start "rule__CharacterDeclaration__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2198:1: rule__CharacterDeclaration__Group__0 : rule__CharacterDeclaration__Group__0__Impl rule__CharacterDeclaration__Group__1 ;
    public final void rule__CharacterDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2202:1: ( rule__CharacterDeclaration__Group__0__Impl rule__CharacterDeclaration__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2203:2: rule__CharacterDeclaration__Group__0__Impl rule__CharacterDeclaration__Group__1
            {
            pushFollow(FOLLOW_rule__CharacterDeclaration__Group__0__Impl_in_rule__CharacterDeclaration__Group__04595);
            rule__CharacterDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CharacterDeclaration__Group__1_in_rule__CharacterDeclaration__Group__04598);
            rule__CharacterDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclaration__Group__0"


    // $ANTLR start "rule__CharacterDeclaration__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2210:1: rule__CharacterDeclaration__Group__0__Impl : ( 'char' ) ;
    public final void rule__CharacterDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2214:1: ( ( 'char' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2215:1: ( 'char' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2215:1: ( 'char' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2216:1: 'char'
            {
             before(grammarAccess.getCharacterDeclarationAccess().getCharKeyword_0()); 
            match(input,32,FOLLOW_32_in_rule__CharacterDeclaration__Group__0__Impl4626); 
             after(grammarAccess.getCharacterDeclarationAccess().getCharKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclaration__Group__0__Impl"


    // $ANTLR start "rule__CharacterDeclaration__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2229:1: rule__CharacterDeclaration__Group__1 : rule__CharacterDeclaration__Group__1__Impl ;
    public final void rule__CharacterDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2233:1: ( rule__CharacterDeclaration__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2234:2: rule__CharacterDeclaration__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__CharacterDeclaration__Group__1__Impl_in_rule__CharacterDeclaration__Group__14657);
            rule__CharacterDeclaration__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclaration__Group__1"


    // $ANTLR start "rule__CharacterDeclaration__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2240:1: rule__CharacterDeclaration__Group__1__Impl : ( ( rule__CharacterDeclaration__IdentifierAssignment_1 ) ) ;
    public final void rule__CharacterDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2244:1: ( ( ( rule__CharacterDeclaration__IdentifierAssignment_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2245:1: ( ( rule__CharacterDeclaration__IdentifierAssignment_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2245:1: ( ( rule__CharacterDeclaration__IdentifierAssignment_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2246:1: ( rule__CharacterDeclaration__IdentifierAssignment_1 )
            {
             before(grammarAccess.getCharacterDeclarationAccess().getIdentifierAssignment_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2247:1: ( rule__CharacterDeclaration__IdentifierAssignment_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2247:2: rule__CharacterDeclaration__IdentifierAssignment_1
            {
            pushFollow(FOLLOW_rule__CharacterDeclaration__IdentifierAssignment_1_in_rule__CharacterDeclaration__Group__1__Impl4684);
            rule__CharacterDeclaration__IdentifierAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCharacterDeclarationAccess().getIdentifierAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclaration__Group__1__Impl"


    // $ANTLR start "rule__CharacterVarVar__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2261:1: rule__CharacterVarVar__Group__0 : rule__CharacterVarVar__Group__0__Impl rule__CharacterVarVar__Group__1 ;
    public final void rule__CharacterVarVar__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2265:1: ( rule__CharacterVarVar__Group__0__Impl rule__CharacterVarVar__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2266:2: rule__CharacterVarVar__Group__0__Impl rule__CharacterVarVar__Group__1
            {
            pushFollow(FOLLOW_rule__CharacterVarVar__Group__0__Impl_in_rule__CharacterVarVar__Group__04718);
            rule__CharacterVarVar__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CharacterVarVar__Group__1_in_rule__CharacterVarVar__Group__04721);
            rule__CharacterVarVar__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterVarVar__Group__0"


    // $ANTLR start "rule__CharacterVarVar__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2273:1: rule__CharacterVarVar__Group__0__Impl : ( ( rule__CharacterVarVar__IdentifierAssignment_0 ) ) ;
    public final void rule__CharacterVarVar__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2277:1: ( ( ( rule__CharacterVarVar__IdentifierAssignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2278:1: ( ( rule__CharacterVarVar__IdentifierAssignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2278:1: ( ( rule__CharacterVarVar__IdentifierAssignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2279:1: ( rule__CharacterVarVar__IdentifierAssignment_0 )
            {
             before(grammarAccess.getCharacterVarVarAccess().getIdentifierAssignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2280:1: ( rule__CharacterVarVar__IdentifierAssignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2280:2: rule__CharacterVarVar__IdentifierAssignment_0
            {
            pushFollow(FOLLOW_rule__CharacterVarVar__IdentifierAssignment_0_in_rule__CharacterVarVar__Group__0__Impl4748);
            rule__CharacterVarVar__IdentifierAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getCharacterVarVarAccess().getIdentifierAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterVarVar__Group__0__Impl"


    // $ANTLR start "rule__CharacterVarVar__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2290:1: rule__CharacterVarVar__Group__1 : rule__CharacterVarVar__Group__1__Impl rule__CharacterVarVar__Group__2 ;
    public final void rule__CharacterVarVar__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2294:1: ( rule__CharacterVarVar__Group__1__Impl rule__CharacterVarVar__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2295:2: rule__CharacterVarVar__Group__1__Impl rule__CharacterVarVar__Group__2
            {
            pushFollow(FOLLOW_rule__CharacterVarVar__Group__1__Impl_in_rule__CharacterVarVar__Group__14778);
            rule__CharacterVarVar__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CharacterVarVar__Group__2_in_rule__CharacterVarVar__Group__14781);
            rule__CharacterVarVar__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterVarVar__Group__1"


    // $ANTLR start "rule__CharacterVarVar__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2302:1: rule__CharacterVarVar__Group__1__Impl : ( '=' ) ;
    public final void rule__CharacterVarVar__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2306:1: ( ( '=' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2307:1: ( '=' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2307:1: ( '=' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2308:1: '='
            {
             before(grammarAccess.getCharacterVarVarAccess().getEqualsSignKeyword_1()); 
            match(input,31,FOLLOW_31_in_rule__CharacterVarVar__Group__1__Impl4809); 
             after(grammarAccess.getCharacterVarVarAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterVarVar__Group__1__Impl"


    // $ANTLR start "rule__CharacterVarVar__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2321:1: rule__CharacterVarVar__Group__2 : rule__CharacterVarVar__Group__2__Impl ;
    public final void rule__CharacterVarVar__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2325:1: ( rule__CharacterVarVar__Group__2__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2326:2: rule__CharacterVarVar__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__CharacterVarVar__Group__2__Impl_in_rule__CharacterVarVar__Group__24840);
            rule__CharacterVarVar__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterVarVar__Group__2"


    // $ANTLR start "rule__CharacterVarVar__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2332:1: rule__CharacterVarVar__Group__2__Impl : ( ( rule__CharacterVarVar__ValueAssignment_2 ) ) ;
    public final void rule__CharacterVarVar__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2336:1: ( ( ( rule__CharacterVarVar__ValueAssignment_2 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2337:1: ( ( rule__CharacterVarVar__ValueAssignment_2 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2337:1: ( ( rule__CharacterVarVar__ValueAssignment_2 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2338:1: ( rule__CharacterVarVar__ValueAssignment_2 )
            {
             before(grammarAccess.getCharacterVarVarAccess().getValueAssignment_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2339:1: ( rule__CharacterVarVar__ValueAssignment_2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2339:2: rule__CharacterVarVar__ValueAssignment_2
            {
            pushFollow(FOLLOW_rule__CharacterVarVar__ValueAssignment_2_in_rule__CharacterVarVar__Group__2__Impl4867);
            rule__CharacterVarVar__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCharacterVarVarAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterVarVar__Group__2__Impl"


    // $ANTLR start "rule__BooleanDeclarationAss__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2355:1: rule__BooleanDeclarationAss__Group__0 : rule__BooleanDeclarationAss__Group__0__Impl rule__BooleanDeclarationAss__Group__1 ;
    public final void rule__BooleanDeclarationAss__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2359:1: ( rule__BooleanDeclarationAss__Group__0__Impl rule__BooleanDeclarationAss__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2360:2: rule__BooleanDeclarationAss__Group__0__Impl rule__BooleanDeclarationAss__Group__1
            {
            pushFollow(FOLLOW_rule__BooleanDeclarationAss__Group__0__Impl_in_rule__BooleanDeclarationAss__Group__04903);
            rule__BooleanDeclarationAss__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BooleanDeclarationAss__Group__1_in_rule__BooleanDeclarationAss__Group__04906);
            rule__BooleanDeclarationAss__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__Group__0"


    // $ANTLR start "rule__BooleanDeclarationAss__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2367:1: rule__BooleanDeclarationAss__Group__0__Impl : ( ( rule__BooleanDeclarationAss__AssignAssignment_0 ) ) ;
    public final void rule__BooleanDeclarationAss__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2371:1: ( ( ( rule__BooleanDeclarationAss__AssignAssignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2372:1: ( ( rule__BooleanDeclarationAss__AssignAssignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2372:1: ( ( rule__BooleanDeclarationAss__AssignAssignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2373:1: ( rule__BooleanDeclarationAss__AssignAssignment_0 )
            {
             before(grammarAccess.getBooleanDeclarationAssAccess().getAssignAssignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2374:1: ( rule__BooleanDeclarationAss__AssignAssignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2374:2: rule__BooleanDeclarationAss__AssignAssignment_0
            {
            pushFollow(FOLLOW_rule__BooleanDeclarationAss__AssignAssignment_0_in_rule__BooleanDeclarationAss__Group__0__Impl4933);
            rule__BooleanDeclarationAss__AssignAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanDeclarationAssAccess().getAssignAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__Group__0__Impl"


    // $ANTLR start "rule__BooleanDeclarationAss__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2384:1: rule__BooleanDeclarationAss__Group__1 : rule__BooleanDeclarationAss__Group__1__Impl ;
    public final void rule__BooleanDeclarationAss__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2388:1: ( rule__BooleanDeclarationAss__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2389:2: rule__BooleanDeclarationAss__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__BooleanDeclarationAss__Group__1__Impl_in_rule__BooleanDeclarationAss__Group__14963);
            rule__BooleanDeclarationAss__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__Group__1"


    // $ANTLR start "rule__BooleanDeclarationAss__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2395:1: rule__BooleanDeclarationAss__Group__1__Impl : ( ( rule__BooleanDeclarationAss__Group_1__0 )? ) ;
    public final void rule__BooleanDeclarationAss__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2399:1: ( ( ( rule__BooleanDeclarationAss__Group_1__0 )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2400:1: ( ( rule__BooleanDeclarationAss__Group_1__0 )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2400:1: ( ( rule__BooleanDeclarationAss__Group_1__0 )? )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2401:1: ( rule__BooleanDeclarationAss__Group_1__0 )?
            {
             before(grammarAccess.getBooleanDeclarationAssAccess().getGroup_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2402:1: ( rule__BooleanDeclarationAss__Group_1__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==31) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2402:2: rule__BooleanDeclarationAss__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__BooleanDeclarationAss__Group_1__0_in_rule__BooleanDeclarationAss__Group__1__Impl4990);
                    rule__BooleanDeclarationAss__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBooleanDeclarationAssAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__Group__1__Impl"


    // $ANTLR start "rule__BooleanDeclarationAss__Group_1__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2416:1: rule__BooleanDeclarationAss__Group_1__0 : rule__BooleanDeclarationAss__Group_1__0__Impl rule__BooleanDeclarationAss__Group_1__1 ;
    public final void rule__BooleanDeclarationAss__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2420:1: ( rule__BooleanDeclarationAss__Group_1__0__Impl rule__BooleanDeclarationAss__Group_1__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2421:2: rule__BooleanDeclarationAss__Group_1__0__Impl rule__BooleanDeclarationAss__Group_1__1
            {
            pushFollow(FOLLOW_rule__BooleanDeclarationAss__Group_1__0__Impl_in_rule__BooleanDeclarationAss__Group_1__05025);
            rule__BooleanDeclarationAss__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BooleanDeclarationAss__Group_1__1_in_rule__BooleanDeclarationAss__Group_1__05028);
            rule__BooleanDeclarationAss__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__Group_1__0"


    // $ANTLR start "rule__BooleanDeclarationAss__Group_1__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2428:1: rule__BooleanDeclarationAss__Group_1__0__Impl : ( '=' ) ;
    public final void rule__BooleanDeclarationAss__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2432:1: ( ( '=' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2433:1: ( '=' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2433:1: ( '=' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2434:1: '='
            {
             before(grammarAccess.getBooleanDeclarationAssAccess().getEqualsSignKeyword_1_0()); 
            match(input,31,FOLLOW_31_in_rule__BooleanDeclarationAss__Group_1__0__Impl5056); 
             after(grammarAccess.getBooleanDeclarationAssAccess().getEqualsSignKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__Group_1__0__Impl"


    // $ANTLR start "rule__BooleanDeclarationAss__Group_1__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2447:1: rule__BooleanDeclarationAss__Group_1__1 : rule__BooleanDeclarationAss__Group_1__1__Impl ;
    public final void rule__BooleanDeclarationAss__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2451:1: ( rule__BooleanDeclarationAss__Group_1__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2452:2: rule__BooleanDeclarationAss__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__BooleanDeclarationAss__Group_1__1__Impl_in_rule__BooleanDeclarationAss__Group_1__15087);
            rule__BooleanDeclarationAss__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__Group_1__1"


    // $ANTLR start "rule__BooleanDeclarationAss__Group_1__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2458:1: rule__BooleanDeclarationAss__Group_1__1__Impl : ( ( rule__BooleanDeclarationAss__ValueAssignment_1_1 ) ) ;
    public final void rule__BooleanDeclarationAss__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2462:1: ( ( ( rule__BooleanDeclarationAss__ValueAssignment_1_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2463:1: ( ( rule__BooleanDeclarationAss__ValueAssignment_1_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2463:1: ( ( rule__BooleanDeclarationAss__ValueAssignment_1_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2464:1: ( rule__BooleanDeclarationAss__ValueAssignment_1_1 )
            {
             before(grammarAccess.getBooleanDeclarationAssAccess().getValueAssignment_1_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2465:1: ( rule__BooleanDeclarationAss__ValueAssignment_1_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2465:2: rule__BooleanDeclarationAss__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_rule__BooleanDeclarationAss__ValueAssignment_1_1_in_rule__BooleanDeclarationAss__Group_1__1__Impl5114);
            rule__BooleanDeclarationAss__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBooleanDeclarationAssAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__Group_1__1__Impl"


    // $ANTLR start "rule__BooleanDeclaration__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2479:1: rule__BooleanDeclaration__Group__0 : rule__BooleanDeclaration__Group__0__Impl rule__BooleanDeclaration__Group__1 ;
    public final void rule__BooleanDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2483:1: ( rule__BooleanDeclaration__Group__0__Impl rule__BooleanDeclaration__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2484:2: rule__BooleanDeclaration__Group__0__Impl rule__BooleanDeclaration__Group__1
            {
            pushFollow(FOLLOW_rule__BooleanDeclaration__Group__0__Impl_in_rule__BooleanDeclaration__Group__05148);
            rule__BooleanDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BooleanDeclaration__Group__1_in_rule__BooleanDeclaration__Group__05151);
            rule__BooleanDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclaration__Group__0"


    // $ANTLR start "rule__BooleanDeclaration__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2491:1: rule__BooleanDeclaration__Group__0__Impl : ( 'bool' ) ;
    public final void rule__BooleanDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2495:1: ( ( 'bool' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2496:1: ( 'bool' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2496:1: ( 'bool' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2497:1: 'bool'
            {
             before(grammarAccess.getBooleanDeclarationAccess().getBoolKeyword_0()); 
            match(input,33,FOLLOW_33_in_rule__BooleanDeclaration__Group__0__Impl5179); 
             after(grammarAccess.getBooleanDeclarationAccess().getBoolKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclaration__Group__0__Impl"


    // $ANTLR start "rule__BooleanDeclaration__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2510:1: rule__BooleanDeclaration__Group__1 : rule__BooleanDeclaration__Group__1__Impl ;
    public final void rule__BooleanDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2514:1: ( rule__BooleanDeclaration__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2515:2: rule__BooleanDeclaration__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__BooleanDeclaration__Group__1__Impl_in_rule__BooleanDeclaration__Group__15210);
            rule__BooleanDeclaration__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclaration__Group__1"


    // $ANTLR start "rule__BooleanDeclaration__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2521:1: rule__BooleanDeclaration__Group__1__Impl : ( ( rule__BooleanDeclaration__IdentifierAssignment_1 ) ) ;
    public final void rule__BooleanDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2525:1: ( ( ( rule__BooleanDeclaration__IdentifierAssignment_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2526:1: ( ( rule__BooleanDeclaration__IdentifierAssignment_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2526:1: ( ( rule__BooleanDeclaration__IdentifierAssignment_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2527:1: ( rule__BooleanDeclaration__IdentifierAssignment_1 )
            {
             before(grammarAccess.getBooleanDeclarationAccess().getIdentifierAssignment_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2528:1: ( rule__BooleanDeclaration__IdentifierAssignment_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2528:2: rule__BooleanDeclaration__IdentifierAssignment_1
            {
            pushFollow(FOLLOW_rule__BooleanDeclaration__IdentifierAssignment_1_in_rule__BooleanDeclaration__Group__1__Impl5237);
            rule__BooleanDeclaration__IdentifierAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBooleanDeclarationAccess().getIdentifierAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclaration__Group__1__Impl"


    // $ANTLR start "rule__BooleanVarVar__Group_0__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2542:1: rule__BooleanVarVar__Group_0__0 : rule__BooleanVarVar__Group_0__0__Impl rule__BooleanVarVar__Group_0__1 ;
    public final void rule__BooleanVarVar__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2546:1: ( rule__BooleanVarVar__Group_0__0__Impl rule__BooleanVarVar__Group_0__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2547:2: rule__BooleanVarVar__Group_0__0__Impl rule__BooleanVarVar__Group_0__1
            {
            pushFollow(FOLLOW_rule__BooleanVarVar__Group_0__0__Impl_in_rule__BooleanVarVar__Group_0__05271);
            rule__BooleanVarVar__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BooleanVarVar__Group_0__1_in_rule__BooleanVarVar__Group_0__05274);
            rule__BooleanVarVar__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__Group_0__0"


    // $ANTLR start "rule__BooleanVarVar__Group_0__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2554:1: rule__BooleanVarVar__Group_0__0__Impl : ( ( rule__BooleanVarVar__IdentifierAssignment_0_0 ) ) ;
    public final void rule__BooleanVarVar__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2558:1: ( ( ( rule__BooleanVarVar__IdentifierAssignment_0_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2559:1: ( ( rule__BooleanVarVar__IdentifierAssignment_0_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2559:1: ( ( rule__BooleanVarVar__IdentifierAssignment_0_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2560:1: ( rule__BooleanVarVar__IdentifierAssignment_0_0 )
            {
             before(grammarAccess.getBooleanVarVarAccess().getIdentifierAssignment_0_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2561:1: ( rule__BooleanVarVar__IdentifierAssignment_0_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2561:2: rule__BooleanVarVar__IdentifierAssignment_0_0
            {
            pushFollow(FOLLOW_rule__BooleanVarVar__IdentifierAssignment_0_0_in_rule__BooleanVarVar__Group_0__0__Impl5301);
            rule__BooleanVarVar__IdentifierAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanVarVarAccess().getIdentifierAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__Group_0__0__Impl"


    // $ANTLR start "rule__BooleanVarVar__Group_0__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2571:1: rule__BooleanVarVar__Group_0__1 : rule__BooleanVarVar__Group_0__1__Impl rule__BooleanVarVar__Group_0__2 ;
    public final void rule__BooleanVarVar__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2575:1: ( rule__BooleanVarVar__Group_0__1__Impl rule__BooleanVarVar__Group_0__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2576:2: rule__BooleanVarVar__Group_0__1__Impl rule__BooleanVarVar__Group_0__2
            {
            pushFollow(FOLLOW_rule__BooleanVarVar__Group_0__1__Impl_in_rule__BooleanVarVar__Group_0__15331);
            rule__BooleanVarVar__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BooleanVarVar__Group_0__2_in_rule__BooleanVarVar__Group_0__15334);
            rule__BooleanVarVar__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__Group_0__1"


    // $ANTLR start "rule__BooleanVarVar__Group_0__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2583:1: rule__BooleanVarVar__Group_0__1__Impl : ( '=' ) ;
    public final void rule__BooleanVarVar__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2587:1: ( ( '=' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2588:1: ( '=' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2588:1: ( '=' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2589:1: '='
            {
             before(grammarAccess.getBooleanVarVarAccess().getEqualsSignKeyword_0_1()); 
            match(input,31,FOLLOW_31_in_rule__BooleanVarVar__Group_0__1__Impl5362); 
             after(grammarAccess.getBooleanVarVarAccess().getEqualsSignKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__Group_0__1__Impl"


    // $ANTLR start "rule__BooleanVarVar__Group_0__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2602:1: rule__BooleanVarVar__Group_0__2 : rule__BooleanVarVar__Group_0__2__Impl ;
    public final void rule__BooleanVarVar__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2606:1: ( rule__BooleanVarVar__Group_0__2__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2607:2: rule__BooleanVarVar__Group_0__2__Impl
            {
            pushFollow(FOLLOW_rule__BooleanVarVar__Group_0__2__Impl_in_rule__BooleanVarVar__Group_0__25393);
            rule__BooleanVarVar__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__Group_0__2"


    // $ANTLR start "rule__BooleanVarVar__Group_0__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2613:1: rule__BooleanVarVar__Group_0__2__Impl : ( ( rule__BooleanVarVar__Value1Assignment_0_2 ) ) ;
    public final void rule__BooleanVarVar__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2617:1: ( ( ( rule__BooleanVarVar__Value1Assignment_0_2 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2618:1: ( ( rule__BooleanVarVar__Value1Assignment_0_2 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2618:1: ( ( rule__BooleanVarVar__Value1Assignment_0_2 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2619:1: ( rule__BooleanVarVar__Value1Assignment_0_2 )
            {
             before(grammarAccess.getBooleanVarVarAccess().getValue1Assignment_0_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2620:1: ( rule__BooleanVarVar__Value1Assignment_0_2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2620:2: rule__BooleanVarVar__Value1Assignment_0_2
            {
            pushFollow(FOLLOW_rule__BooleanVarVar__Value1Assignment_0_2_in_rule__BooleanVarVar__Group_0__2__Impl5420);
            rule__BooleanVarVar__Value1Assignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getBooleanVarVarAccess().getValue1Assignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__Group_0__2__Impl"


    // $ANTLR start "rule__BooleanOperation__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2636:1: rule__BooleanOperation__Group__0 : rule__BooleanOperation__Group__0__Impl rule__BooleanOperation__Group__1 ;
    public final void rule__BooleanOperation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2640:1: ( rule__BooleanOperation__Group__0__Impl rule__BooleanOperation__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2641:2: rule__BooleanOperation__Group__0__Impl rule__BooleanOperation__Group__1
            {
            pushFollow(FOLLOW_rule__BooleanOperation__Group__0__Impl_in_rule__BooleanOperation__Group__05456);
            rule__BooleanOperation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BooleanOperation__Group__1_in_rule__BooleanOperation__Group__05459);
            rule__BooleanOperation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanOperation__Group__0"


    // $ANTLR start "rule__BooleanOperation__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2648:1: rule__BooleanOperation__Group__0__Impl : ( ( rule__BooleanOperation__Operand1Assignment_0 ) ) ;
    public final void rule__BooleanOperation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2652:1: ( ( ( rule__BooleanOperation__Operand1Assignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2653:1: ( ( rule__BooleanOperation__Operand1Assignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2653:1: ( ( rule__BooleanOperation__Operand1Assignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2654:1: ( rule__BooleanOperation__Operand1Assignment_0 )
            {
             before(grammarAccess.getBooleanOperationAccess().getOperand1Assignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2655:1: ( rule__BooleanOperation__Operand1Assignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2655:2: rule__BooleanOperation__Operand1Assignment_0
            {
            pushFollow(FOLLOW_rule__BooleanOperation__Operand1Assignment_0_in_rule__BooleanOperation__Group__0__Impl5486);
            rule__BooleanOperation__Operand1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanOperationAccess().getOperand1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanOperation__Group__0__Impl"


    // $ANTLR start "rule__BooleanOperation__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2665:1: rule__BooleanOperation__Group__1 : rule__BooleanOperation__Group__1__Impl rule__BooleanOperation__Group__2 ;
    public final void rule__BooleanOperation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2669:1: ( rule__BooleanOperation__Group__1__Impl rule__BooleanOperation__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2670:2: rule__BooleanOperation__Group__1__Impl rule__BooleanOperation__Group__2
            {
            pushFollow(FOLLOW_rule__BooleanOperation__Group__1__Impl_in_rule__BooleanOperation__Group__15516);
            rule__BooleanOperation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BooleanOperation__Group__2_in_rule__BooleanOperation__Group__15519);
            rule__BooleanOperation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanOperation__Group__1"


    // $ANTLR start "rule__BooleanOperation__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2677:1: rule__BooleanOperation__Group__1__Impl : ( ( rule__BooleanOperation__OperatorAssignment_1 ) ) ;
    public final void rule__BooleanOperation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2681:1: ( ( ( rule__BooleanOperation__OperatorAssignment_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2682:1: ( ( rule__BooleanOperation__OperatorAssignment_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2682:1: ( ( rule__BooleanOperation__OperatorAssignment_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2683:1: ( rule__BooleanOperation__OperatorAssignment_1 )
            {
             before(grammarAccess.getBooleanOperationAccess().getOperatorAssignment_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2684:1: ( rule__BooleanOperation__OperatorAssignment_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2684:2: rule__BooleanOperation__OperatorAssignment_1
            {
            pushFollow(FOLLOW_rule__BooleanOperation__OperatorAssignment_1_in_rule__BooleanOperation__Group__1__Impl5546);
            rule__BooleanOperation__OperatorAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBooleanOperationAccess().getOperatorAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanOperation__Group__1__Impl"


    // $ANTLR start "rule__BooleanOperation__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2694:1: rule__BooleanOperation__Group__2 : rule__BooleanOperation__Group__2__Impl ;
    public final void rule__BooleanOperation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2698:1: ( rule__BooleanOperation__Group__2__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2699:2: rule__BooleanOperation__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__BooleanOperation__Group__2__Impl_in_rule__BooleanOperation__Group__25576);
            rule__BooleanOperation__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanOperation__Group__2"


    // $ANTLR start "rule__BooleanOperation__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2705:1: rule__BooleanOperation__Group__2__Impl : ( ( rule__BooleanOperation__Operand2Assignment_2 ) ) ;
    public final void rule__BooleanOperation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2709:1: ( ( ( rule__BooleanOperation__Operand2Assignment_2 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2710:1: ( ( rule__BooleanOperation__Operand2Assignment_2 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2710:1: ( ( rule__BooleanOperation__Operand2Assignment_2 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2711:1: ( rule__BooleanOperation__Operand2Assignment_2 )
            {
             before(grammarAccess.getBooleanOperationAccess().getOperand2Assignment_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2712:1: ( rule__BooleanOperation__Operand2Assignment_2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2712:2: rule__BooleanOperation__Operand2Assignment_2
            {
            pushFollow(FOLLOW_rule__BooleanOperation__Operand2Assignment_2_in_rule__BooleanOperation__Group__2__Impl5603);
            rule__BooleanOperation__Operand2Assignment_2();

            state._fsp--;


            }

             after(grammarAccess.getBooleanOperationAccess().getOperand2Assignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanOperation__Group__2__Impl"


    // $ANTLR start "rule__IntegerToBool__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2728:1: rule__IntegerToBool__Group__0 : rule__IntegerToBool__Group__0__Impl rule__IntegerToBool__Group__1 ;
    public final void rule__IntegerToBool__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2732:1: ( rule__IntegerToBool__Group__0__Impl rule__IntegerToBool__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2733:2: rule__IntegerToBool__Group__0__Impl rule__IntegerToBool__Group__1
            {
            pushFollow(FOLLOW_rule__IntegerToBool__Group__0__Impl_in_rule__IntegerToBool__Group__05639);
            rule__IntegerToBool__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IntegerToBool__Group__1_in_rule__IntegerToBool__Group__05642);
            rule__IntegerToBool__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerToBool__Group__0"


    // $ANTLR start "rule__IntegerToBool__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2740:1: rule__IntegerToBool__Group__0__Impl : ( ( rule__IntegerToBool__Operand1Assignment_0 ) ) ;
    public final void rule__IntegerToBool__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2744:1: ( ( ( rule__IntegerToBool__Operand1Assignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2745:1: ( ( rule__IntegerToBool__Operand1Assignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2745:1: ( ( rule__IntegerToBool__Operand1Assignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2746:1: ( rule__IntegerToBool__Operand1Assignment_0 )
            {
             before(grammarAccess.getIntegerToBoolAccess().getOperand1Assignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2747:1: ( rule__IntegerToBool__Operand1Assignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2747:2: rule__IntegerToBool__Operand1Assignment_0
            {
            pushFollow(FOLLOW_rule__IntegerToBool__Operand1Assignment_0_in_rule__IntegerToBool__Group__0__Impl5669);
            rule__IntegerToBool__Operand1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerToBoolAccess().getOperand1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerToBool__Group__0__Impl"


    // $ANTLR start "rule__IntegerToBool__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2757:1: rule__IntegerToBool__Group__1 : rule__IntegerToBool__Group__1__Impl rule__IntegerToBool__Group__2 ;
    public final void rule__IntegerToBool__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2761:1: ( rule__IntegerToBool__Group__1__Impl rule__IntegerToBool__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2762:2: rule__IntegerToBool__Group__1__Impl rule__IntegerToBool__Group__2
            {
            pushFollow(FOLLOW_rule__IntegerToBool__Group__1__Impl_in_rule__IntegerToBool__Group__15699);
            rule__IntegerToBool__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IntegerToBool__Group__2_in_rule__IntegerToBool__Group__15702);
            rule__IntegerToBool__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerToBool__Group__1"


    // $ANTLR start "rule__IntegerToBool__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2769:1: rule__IntegerToBool__Group__1__Impl : ( ( rule__IntegerToBool__OperatorAssignment_1 ) ) ;
    public final void rule__IntegerToBool__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2773:1: ( ( ( rule__IntegerToBool__OperatorAssignment_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2774:1: ( ( rule__IntegerToBool__OperatorAssignment_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2774:1: ( ( rule__IntegerToBool__OperatorAssignment_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2775:1: ( rule__IntegerToBool__OperatorAssignment_1 )
            {
             before(grammarAccess.getIntegerToBoolAccess().getOperatorAssignment_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2776:1: ( rule__IntegerToBool__OperatorAssignment_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2776:2: rule__IntegerToBool__OperatorAssignment_1
            {
            pushFollow(FOLLOW_rule__IntegerToBool__OperatorAssignment_1_in_rule__IntegerToBool__Group__1__Impl5729);
            rule__IntegerToBool__OperatorAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIntegerToBoolAccess().getOperatorAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerToBool__Group__1__Impl"


    // $ANTLR start "rule__IntegerToBool__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2786:1: rule__IntegerToBool__Group__2 : rule__IntegerToBool__Group__2__Impl ;
    public final void rule__IntegerToBool__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2790:1: ( rule__IntegerToBool__Group__2__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2791:2: rule__IntegerToBool__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__IntegerToBool__Group__2__Impl_in_rule__IntegerToBool__Group__25759);
            rule__IntegerToBool__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerToBool__Group__2"


    // $ANTLR start "rule__IntegerToBool__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2797:1: rule__IntegerToBool__Group__2__Impl : ( ( rule__IntegerToBool__Operand2Assignment_2 ) ) ;
    public final void rule__IntegerToBool__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2801:1: ( ( ( rule__IntegerToBool__Operand2Assignment_2 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2802:1: ( ( rule__IntegerToBool__Operand2Assignment_2 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2802:1: ( ( rule__IntegerToBool__Operand2Assignment_2 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2803:1: ( rule__IntegerToBool__Operand2Assignment_2 )
            {
             before(grammarAccess.getIntegerToBoolAccess().getOperand2Assignment_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2804:1: ( rule__IntegerToBool__Operand2Assignment_2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2804:2: rule__IntegerToBool__Operand2Assignment_2
            {
            pushFollow(FOLLOW_rule__IntegerToBool__Operand2Assignment_2_in_rule__IntegerToBool__Group__2__Impl5786);
            rule__IntegerToBool__Operand2Assignment_2();

            state._fsp--;


            }

             after(grammarAccess.getIntegerToBoolAccess().getOperand2Assignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerToBool__Group__2__Impl"


    // $ANTLR start "rule__IntegerDeclarationAss__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2820:1: rule__IntegerDeclarationAss__Group__0 : rule__IntegerDeclarationAss__Group__0__Impl rule__IntegerDeclarationAss__Group__1 ;
    public final void rule__IntegerDeclarationAss__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2824:1: ( rule__IntegerDeclarationAss__Group__0__Impl rule__IntegerDeclarationAss__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2825:2: rule__IntegerDeclarationAss__Group__0__Impl rule__IntegerDeclarationAss__Group__1
            {
            pushFollow(FOLLOW_rule__IntegerDeclarationAss__Group__0__Impl_in_rule__IntegerDeclarationAss__Group__05822);
            rule__IntegerDeclarationAss__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IntegerDeclarationAss__Group__1_in_rule__IntegerDeclarationAss__Group__05825);
            rule__IntegerDeclarationAss__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__Group__0"


    // $ANTLR start "rule__IntegerDeclarationAss__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2832:1: rule__IntegerDeclarationAss__Group__0__Impl : ( ( rule__IntegerDeclarationAss__AssignAssignment_0 ) ) ;
    public final void rule__IntegerDeclarationAss__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2836:1: ( ( ( rule__IntegerDeclarationAss__AssignAssignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2837:1: ( ( rule__IntegerDeclarationAss__AssignAssignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2837:1: ( ( rule__IntegerDeclarationAss__AssignAssignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2838:1: ( rule__IntegerDeclarationAss__AssignAssignment_0 )
            {
             before(grammarAccess.getIntegerDeclarationAssAccess().getAssignAssignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2839:1: ( rule__IntegerDeclarationAss__AssignAssignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2839:2: rule__IntegerDeclarationAss__AssignAssignment_0
            {
            pushFollow(FOLLOW_rule__IntegerDeclarationAss__AssignAssignment_0_in_rule__IntegerDeclarationAss__Group__0__Impl5852);
            rule__IntegerDeclarationAss__AssignAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerDeclarationAssAccess().getAssignAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__Group__0__Impl"


    // $ANTLR start "rule__IntegerDeclarationAss__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2849:1: rule__IntegerDeclarationAss__Group__1 : rule__IntegerDeclarationAss__Group__1__Impl ;
    public final void rule__IntegerDeclarationAss__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2853:1: ( rule__IntegerDeclarationAss__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2854:2: rule__IntegerDeclarationAss__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__IntegerDeclarationAss__Group__1__Impl_in_rule__IntegerDeclarationAss__Group__15882);
            rule__IntegerDeclarationAss__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__Group__1"


    // $ANTLR start "rule__IntegerDeclarationAss__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2860:1: rule__IntegerDeclarationAss__Group__1__Impl : ( ( rule__IntegerDeclarationAss__Group_1__0 )? ) ;
    public final void rule__IntegerDeclarationAss__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2864:1: ( ( ( rule__IntegerDeclarationAss__Group_1__0 )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2865:1: ( ( rule__IntegerDeclarationAss__Group_1__0 )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2865:1: ( ( rule__IntegerDeclarationAss__Group_1__0 )? )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2866:1: ( rule__IntegerDeclarationAss__Group_1__0 )?
            {
             before(grammarAccess.getIntegerDeclarationAssAccess().getGroup_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2867:1: ( rule__IntegerDeclarationAss__Group_1__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==31) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2867:2: rule__IntegerDeclarationAss__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__IntegerDeclarationAss__Group_1__0_in_rule__IntegerDeclarationAss__Group__1__Impl5909);
                    rule__IntegerDeclarationAss__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getIntegerDeclarationAssAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__Group__1__Impl"


    // $ANTLR start "rule__IntegerDeclarationAss__Group_1__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2881:1: rule__IntegerDeclarationAss__Group_1__0 : rule__IntegerDeclarationAss__Group_1__0__Impl rule__IntegerDeclarationAss__Group_1__1 ;
    public final void rule__IntegerDeclarationAss__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2885:1: ( rule__IntegerDeclarationAss__Group_1__0__Impl rule__IntegerDeclarationAss__Group_1__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2886:2: rule__IntegerDeclarationAss__Group_1__0__Impl rule__IntegerDeclarationAss__Group_1__1
            {
            pushFollow(FOLLOW_rule__IntegerDeclarationAss__Group_1__0__Impl_in_rule__IntegerDeclarationAss__Group_1__05944);
            rule__IntegerDeclarationAss__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IntegerDeclarationAss__Group_1__1_in_rule__IntegerDeclarationAss__Group_1__05947);
            rule__IntegerDeclarationAss__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__Group_1__0"


    // $ANTLR start "rule__IntegerDeclarationAss__Group_1__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2893:1: rule__IntegerDeclarationAss__Group_1__0__Impl : ( '=' ) ;
    public final void rule__IntegerDeclarationAss__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2897:1: ( ( '=' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2898:1: ( '=' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2898:1: ( '=' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2899:1: '='
            {
             before(grammarAccess.getIntegerDeclarationAssAccess().getEqualsSignKeyword_1_0()); 
            match(input,31,FOLLOW_31_in_rule__IntegerDeclarationAss__Group_1__0__Impl5975); 
             after(grammarAccess.getIntegerDeclarationAssAccess().getEqualsSignKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__Group_1__0__Impl"


    // $ANTLR start "rule__IntegerDeclarationAss__Group_1__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2912:1: rule__IntegerDeclarationAss__Group_1__1 : rule__IntegerDeclarationAss__Group_1__1__Impl ;
    public final void rule__IntegerDeclarationAss__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2916:1: ( rule__IntegerDeclarationAss__Group_1__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2917:2: rule__IntegerDeclarationAss__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__IntegerDeclarationAss__Group_1__1__Impl_in_rule__IntegerDeclarationAss__Group_1__16006);
            rule__IntegerDeclarationAss__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__Group_1__1"


    // $ANTLR start "rule__IntegerDeclarationAss__Group_1__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2923:1: rule__IntegerDeclarationAss__Group_1__1__Impl : ( ( rule__IntegerDeclarationAss__ValueAssignment_1_1 ) ) ;
    public final void rule__IntegerDeclarationAss__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2927:1: ( ( ( rule__IntegerDeclarationAss__ValueAssignment_1_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2928:1: ( ( rule__IntegerDeclarationAss__ValueAssignment_1_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2928:1: ( ( rule__IntegerDeclarationAss__ValueAssignment_1_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2929:1: ( rule__IntegerDeclarationAss__ValueAssignment_1_1 )
            {
             before(grammarAccess.getIntegerDeclarationAssAccess().getValueAssignment_1_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2930:1: ( rule__IntegerDeclarationAss__ValueAssignment_1_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2930:2: rule__IntegerDeclarationAss__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_rule__IntegerDeclarationAss__ValueAssignment_1_1_in_rule__IntegerDeclarationAss__Group_1__1__Impl6033);
            rule__IntegerDeclarationAss__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getIntegerDeclarationAssAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__Group_1__1__Impl"


    // $ANTLR start "rule__IntegerDeclaration__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2944:1: rule__IntegerDeclaration__Group__0 : rule__IntegerDeclaration__Group__0__Impl rule__IntegerDeclaration__Group__1 ;
    public final void rule__IntegerDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2948:1: ( rule__IntegerDeclaration__Group__0__Impl rule__IntegerDeclaration__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2949:2: rule__IntegerDeclaration__Group__0__Impl rule__IntegerDeclaration__Group__1
            {
            pushFollow(FOLLOW_rule__IntegerDeclaration__Group__0__Impl_in_rule__IntegerDeclaration__Group__06067);
            rule__IntegerDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IntegerDeclaration__Group__1_in_rule__IntegerDeclaration__Group__06070);
            rule__IntegerDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclaration__Group__0"


    // $ANTLR start "rule__IntegerDeclaration__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2956:1: rule__IntegerDeclaration__Group__0__Impl : ( 'int' ) ;
    public final void rule__IntegerDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2960:1: ( ( 'int' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2961:1: ( 'int' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2961:1: ( 'int' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2962:1: 'int'
            {
             before(grammarAccess.getIntegerDeclarationAccess().getIntKeyword_0()); 
            match(input,34,FOLLOW_34_in_rule__IntegerDeclaration__Group__0__Impl6098); 
             after(grammarAccess.getIntegerDeclarationAccess().getIntKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclaration__Group__0__Impl"


    // $ANTLR start "rule__IntegerDeclaration__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2975:1: rule__IntegerDeclaration__Group__1 : rule__IntegerDeclaration__Group__1__Impl ;
    public final void rule__IntegerDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2979:1: ( rule__IntegerDeclaration__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2980:2: rule__IntegerDeclaration__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__IntegerDeclaration__Group__1__Impl_in_rule__IntegerDeclaration__Group__16129);
            rule__IntegerDeclaration__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclaration__Group__1"


    // $ANTLR start "rule__IntegerDeclaration__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2986:1: rule__IntegerDeclaration__Group__1__Impl : ( ( rule__IntegerDeclaration__IdentifierAssignment_1 ) ) ;
    public final void rule__IntegerDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2990:1: ( ( ( rule__IntegerDeclaration__IdentifierAssignment_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2991:1: ( ( rule__IntegerDeclaration__IdentifierAssignment_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2991:1: ( ( rule__IntegerDeclaration__IdentifierAssignment_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2992:1: ( rule__IntegerDeclaration__IdentifierAssignment_1 )
            {
             before(grammarAccess.getIntegerDeclarationAccess().getIdentifierAssignment_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2993:1: ( rule__IntegerDeclaration__IdentifierAssignment_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:2993:2: rule__IntegerDeclaration__IdentifierAssignment_1
            {
            pushFollow(FOLLOW_rule__IntegerDeclaration__IdentifierAssignment_1_in_rule__IntegerDeclaration__Group__1__Impl6156);
            rule__IntegerDeclaration__IdentifierAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIntegerDeclarationAccess().getIdentifierAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclaration__Group__1__Impl"


    // $ANTLR start "rule__IntegerVarVar__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3007:1: rule__IntegerVarVar__Group__0 : rule__IntegerVarVar__Group__0__Impl rule__IntegerVarVar__Group__1 ;
    public final void rule__IntegerVarVar__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3011:1: ( rule__IntegerVarVar__Group__0__Impl rule__IntegerVarVar__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3012:2: rule__IntegerVarVar__Group__0__Impl rule__IntegerVarVar__Group__1
            {
            pushFollow(FOLLOW_rule__IntegerVarVar__Group__0__Impl_in_rule__IntegerVarVar__Group__06190);
            rule__IntegerVarVar__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IntegerVarVar__Group__1_in_rule__IntegerVarVar__Group__06193);
            rule__IntegerVarVar__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerVarVar__Group__0"


    // $ANTLR start "rule__IntegerVarVar__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3019:1: rule__IntegerVarVar__Group__0__Impl : ( ( rule__IntegerVarVar__IdentifierAssignment_0 ) ) ;
    public final void rule__IntegerVarVar__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3023:1: ( ( ( rule__IntegerVarVar__IdentifierAssignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3024:1: ( ( rule__IntegerVarVar__IdentifierAssignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3024:1: ( ( rule__IntegerVarVar__IdentifierAssignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3025:1: ( rule__IntegerVarVar__IdentifierAssignment_0 )
            {
             before(grammarAccess.getIntegerVarVarAccess().getIdentifierAssignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3026:1: ( rule__IntegerVarVar__IdentifierAssignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3026:2: rule__IntegerVarVar__IdentifierAssignment_0
            {
            pushFollow(FOLLOW_rule__IntegerVarVar__IdentifierAssignment_0_in_rule__IntegerVarVar__Group__0__Impl6220);
            rule__IntegerVarVar__IdentifierAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerVarVarAccess().getIdentifierAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerVarVar__Group__0__Impl"


    // $ANTLR start "rule__IntegerVarVar__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3036:1: rule__IntegerVarVar__Group__1 : rule__IntegerVarVar__Group__1__Impl rule__IntegerVarVar__Group__2 ;
    public final void rule__IntegerVarVar__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3040:1: ( rule__IntegerVarVar__Group__1__Impl rule__IntegerVarVar__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3041:2: rule__IntegerVarVar__Group__1__Impl rule__IntegerVarVar__Group__2
            {
            pushFollow(FOLLOW_rule__IntegerVarVar__Group__1__Impl_in_rule__IntegerVarVar__Group__16250);
            rule__IntegerVarVar__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IntegerVarVar__Group__2_in_rule__IntegerVarVar__Group__16253);
            rule__IntegerVarVar__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerVarVar__Group__1"


    // $ANTLR start "rule__IntegerVarVar__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3048:1: rule__IntegerVarVar__Group__1__Impl : ( '=' ) ;
    public final void rule__IntegerVarVar__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3052:1: ( ( '=' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3053:1: ( '=' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3053:1: ( '=' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3054:1: '='
            {
             before(grammarAccess.getIntegerVarVarAccess().getEqualsSignKeyword_1()); 
            match(input,31,FOLLOW_31_in_rule__IntegerVarVar__Group__1__Impl6281); 
             after(grammarAccess.getIntegerVarVarAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerVarVar__Group__1__Impl"


    // $ANTLR start "rule__IntegerVarVar__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3067:1: rule__IntegerVarVar__Group__2 : rule__IntegerVarVar__Group__2__Impl ;
    public final void rule__IntegerVarVar__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3071:1: ( rule__IntegerVarVar__Group__2__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3072:2: rule__IntegerVarVar__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__IntegerVarVar__Group__2__Impl_in_rule__IntegerVarVar__Group__26312);
            rule__IntegerVarVar__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerVarVar__Group__2"


    // $ANTLR start "rule__IntegerVarVar__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3078:1: rule__IntegerVarVar__Group__2__Impl : ( ( rule__IntegerVarVar__ValueAssignment_2 ) ) ;
    public final void rule__IntegerVarVar__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3082:1: ( ( ( rule__IntegerVarVar__ValueAssignment_2 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3083:1: ( ( rule__IntegerVarVar__ValueAssignment_2 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3083:1: ( ( rule__IntegerVarVar__ValueAssignment_2 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3084:1: ( rule__IntegerVarVar__ValueAssignment_2 )
            {
             before(grammarAccess.getIntegerVarVarAccess().getValueAssignment_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3085:1: ( rule__IntegerVarVar__ValueAssignment_2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3085:2: rule__IntegerVarVar__ValueAssignment_2
            {
            pushFollow(FOLLOW_rule__IntegerVarVar__ValueAssignment_2_in_rule__IntegerVarVar__Group__2__Impl6339);
            rule__IntegerVarVar__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getIntegerVarVarAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerVarVar__Group__2__Impl"


    // $ANTLR start "rule__IntegerOperation__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3101:1: rule__IntegerOperation__Group__0 : rule__IntegerOperation__Group__0__Impl rule__IntegerOperation__Group__1 ;
    public final void rule__IntegerOperation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3105:1: ( rule__IntegerOperation__Group__0__Impl rule__IntegerOperation__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3106:2: rule__IntegerOperation__Group__0__Impl rule__IntegerOperation__Group__1
            {
            pushFollow(FOLLOW_rule__IntegerOperation__Group__0__Impl_in_rule__IntegerOperation__Group__06375);
            rule__IntegerOperation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IntegerOperation__Group__1_in_rule__IntegerOperation__Group__06378);
            rule__IntegerOperation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerOperation__Group__0"


    // $ANTLR start "rule__IntegerOperation__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3113:1: rule__IntegerOperation__Group__0__Impl : ( ( rule__IntegerOperation__Operand1Assignment_0 ) ) ;
    public final void rule__IntegerOperation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3117:1: ( ( ( rule__IntegerOperation__Operand1Assignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3118:1: ( ( rule__IntegerOperation__Operand1Assignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3118:1: ( ( rule__IntegerOperation__Operand1Assignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3119:1: ( rule__IntegerOperation__Operand1Assignment_0 )
            {
             before(grammarAccess.getIntegerOperationAccess().getOperand1Assignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3120:1: ( rule__IntegerOperation__Operand1Assignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3120:2: rule__IntegerOperation__Operand1Assignment_0
            {
            pushFollow(FOLLOW_rule__IntegerOperation__Operand1Assignment_0_in_rule__IntegerOperation__Group__0__Impl6405);
            rule__IntegerOperation__Operand1Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerOperationAccess().getOperand1Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerOperation__Group__0__Impl"


    // $ANTLR start "rule__IntegerOperation__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3130:1: rule__IntegerOperation__Group__1 : rule__IntegerOperation__Group__1__Impl rule__IntegerOperation__Group__2 ;
    public final void rule__IntegerOperation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3134:1: ( rule__IntegerOperation__Group__1__Impl rule__IntegerOperation__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3135:2: rule__IntegerOperation__Group__1__Impl rule__IntegerOperation__Group__2
            {
            pushFollow(FOLLOW_rule__IntegerOperation__Group__1__Impl_in_rule__IntegerOperation__Group__16435);
            rule__IntegerOperation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__IntegerOperation__Group__2_in_rule__IntegerOperation__Group__16438);
            rule__IntegerOperation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerOperation__Group__1"


    // $ANTLR start "rule__IntegerOperation__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3142:1: rule__IntegerOperation__Group__1__Impl : ( ( rule__IntegerOperation__OperatorAssignment_1 ) ) ;
    public final void rule__IntegerOperation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3146:1: ( ( ( rule__IntegerOperation__OperatorAssignment_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3147:1: ( ( rule__IntegerOperation__OperatorAssignment_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3147:1: ( ( rule__IntegerOperation__OperatorAssignment_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3148:1: ( rule__IntegerOperation__OperatorAssignment_1 )
            {
             before(grammarAccess.getIntegerOperationAccess().getOperatorAssignment_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3149:1: ( rule__IntegerOperation__OperatorAssignment_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3149:2: rule__IntegerOperation__OperatorAssignment_1
            {
            pushFollow(FOLLOW_rule__IntegerOperation__OperatorAssignment_1_in_rule__IntegerOperation__Group__1__Impl6465);
            rule__IntegerOperation__OperatorAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIntegerOperationAccess().getOperatorAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerOperation__Group__1__Impl"


    // $ANTLR start "rule__IntegerOperation__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3159:1: rule__IntegerOperation__Group__2 : rule__IntegerOperation__Group__2__Impl ;
    public final void rule__IntegerOperation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3163:1: ( rule__IntegerOperation__Group__2__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3164:2: rule__IntegerOperation__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__IntegerOperation__Group__2__Impl_in_rule__IntegerOperation__Group__26495);
            rule__IntegerOperation__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerOperation__Group__2"


    // $ANTLR start "rule__IntegerOperation__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3170:1: rule__IntegerOperation__Group__2__Impl : ( ( rule__IntegerOperation__Operand2Assignment_2 ) ) ;
    public final void rule__IntegerOperation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3174:1: ( ( ( rule__IntegerOperation__Operand2Assignment_2 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3175:1: ( ( rule__IntegerOperation__Operand2Assignment_2 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3175:1: ( ( rule__IntegerOperation__Operand2Assignment_2 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3176:1: ( rule__IntegerOperation__Operand2Assignment_2 )
            {
             before(grammarAccess.getIntegerOperationAccess().getOperand2Assignment_2()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3177:1: ( rule__IntegerOperation__Operand2Assignment_2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3177:2: rule__IntegerOperation__Operand2Assignment_2
            {
            pushFollow(FOLLOW_rule__IntegerOperation__Operand2Assignment_2_in_rule__IntegerOperation__Group__2__Impl6522);
            rule__IntegerOperation__Operand2Assignment_2();

            state._fsp--;


            }

             after(grammarAccess.getIntegerOperationAccess().getOperand2Assignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerOperation__Group__2__Impl"


    // $ANTLR start "rule__VariableName__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3193:1: rule__VariableName__Group__0 : rule__VariableName__Group__0__Impl rule__VariableName__Group__1 ;
    public final void rule__VariableName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3197:1: ( rule__VariableName__Group__0__Impl rule__VariableName__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3198:2: rule__VariableName__Group__0__Impl rule__VariableName__Group__1
            {
            pushFollow(FOLLOW_rule__VariableName__Group__0__Impl_in_rule__VariableName__Group__06558);
            rule__VariableName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VariableName__Group__1_in_rule__VariableName__Group__06561);
            rule__VariableName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableName__Group__0"


    // $ANTLR start "rule__VariableName__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3205:1: rule__VariableName__Group__0__Impl : ( '\\u20AC' ) ;
    public final void rule__VariableName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3209:1: ( ( '\\u20AC' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3210:1: ( '\\u20AC' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3210:1: ( '\\u20AC' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3211:1: '\\u20AC'
            {
             before(grammarAccess.getVariableNameAccess().getEuroSignKeyword_0()); 
            match(input,35,FOLLOW_35_in_rule__VariableName__Group__0__Impl6589); 
             after(grammarAccess.getVariableNameAccess().getEuroSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableName__Group__0__Impl"


    // $ANTLR start "rule__VariableName__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3224:1: rule__VariableName__Group__1 : rule__VariableName__Group__1__Impl ;
    public final void rule__VariableName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3228:1: ( rule__VariableName__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3229:2: rule__VariableName__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__VariableName__Group__1__Impl_in_rule__VariableName__Group__16620);
            rule__VariableName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableName__Group__1"


    // $ANTLR start "rule__VariableName__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3235:1: rule__VariableName__Group__1__Impl : ( RULE_ID ) ;
    public final void rule__VariableName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3239:1: ( ( RULE_ID ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3240:1: ( RULE_ID )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3240:1: ( RULE_ID )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3241:1: RULE_ID
            {
             before(grammarAccess.getVariableNameAccess().getIDTerminalRuleCall_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__VariableName__Group__1__Impl6647); 
             after(grammarAccess.getVariableNameAccess().getIDTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableName__Group__1__Impl"


    // $ANTLR start "rule__CallFunction__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3256:1: rule__CallFunction__Group__0 : rule__CallFunction__Group__0__Impl rule__CallFunction__Group__1 ;
    public final void rule__CallFunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3260:1: ( rule__CallFunction__Group__0__Impl rule__CallFunction__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3261:2: rule__CallFunction__Group__0__Impl rule__CallFunction__Group__1
            {
            pushFollow(FOLLOW_rule__CallFunction__Group__0__Impl_in_rule__CallFunction__Group__06680);
            rule__CallFunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CallFunction__Group__1_in_rule__CallFunction__Group__06683);
            rule__CallFunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CallFunction__Group__0"


    // $ANTLR start "rule__CallFunction__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3268:1: rule__CallFunction__Group__0__Impl : ( ( rule__CallFunction__OperationNameAssignment_0 ) ) ;
    public final void rule__CallFunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3272:1: ( ( ( rule__CallFunction__OperationNameAssignment_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3273:1: ( ( rule__CallFunction__OperationNameAssignment_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3273:1: ( ( rule__CallFunction__OperationNameAssignment_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3274:1: ( rule__CallFunction__OperationNameAssignment_0 )
            {
             before(grammarAccess.getCallFunctionAccess().getOperationNameAssignment_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3275:1: ( rule__CallFunction__OperationNameAssignment_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3275:2: rule__CallFunction__OperationNameAssignment_0
            {
            pushFollow(FOLLOW_rule__CallFunction__OperationNameAssignment_0_in_rule__CallFunction__Group__0__Impl6710);
            rule__CallFunction__OperationNameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getCallFunctionAccess().getOperationNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CallFunction__Group__0__Impl"


    // $ANTLR start "rule__CallFunction__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3285:1: rule__CallFunction__Group__1 : rule__CallFunction__Group__1__Impl rule__CallFunction__Group__2 ;
    public final void rule__CallFunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3289:1: ( rule__CallFunction__Group__1__Impl rule__CallFunction__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3290:2: rule__CallFunction__Group__1__Impl rule__CallFunction__Group__2
            {
            pushFollow(FOLLOW_rule__CallFunction__Group__1__Impl_in_rule__CallFunction__Group__16740);
            rule__CallFunction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CallFunction__Group__2_in_rule__CallFunction__Group__16743);
            rule__CallFunction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CallFunction__Group__1"


    // $ANTLR start "rule__CallFunction__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3297:1: rule__CallFunction__Group__1__Impl : ( '(' ) ;
    public final void rule__CallFunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3301:1: ( ( '(' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3302:1: ( '(' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3302:1: ( '(' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3303:1: '('
            {
             before(grammarAccess.getCallFunctionAccess().getLeftParenthesisKeyword_1()); 
            match(input,24,FOLLOW_24_in_rule__CallFunction__Group__1__Impl6771); 
             after(grammarAccess.getCallFunctionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CallFunction__Group__1__Impl"


    // $ANTLR start "rule__CallFunction__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3316:1: rule__CallFunction__Group__2 : rule__CallFunction__Group__2__Impl ;
    public final void rule__CallFunction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3320:1: ( rule__CallFunction__Group__2__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3321:2: rule__CallFunction__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__CallFunction__Group__2__Impl_in_rule__CallFunction__Group__26802);
            rule__CallFunction__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CallFunction__Group__2"


    // $ANTLR start "rule__CallFunction__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3327:1: rule__CallFunction__Group__2__Impl : ( ')' ) ;
    public final void rule__CallFunction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3331:1: ( ( ')' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3332:1: ( ')' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3332:1: ( ')' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3333:1: ')'
            {
             before(grammarAccess.getCallFunctionAccess().getRightParenthesisKeyword_2()); 
            match(input,25,FOLLOW_25_in_rule__CallFunction__Group__2__Impl6830); 
             after(grammarAccess.getCallFunctionAccess().getRightParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CallFunction__Group__2__Impl"


    // $ANTLR start "rule__Function__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3352:1: rule__Function__Group__0 : rule__Function__Group__0__Impl rule__Function__Group__1 ;
    public final void rule__Function__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3356:1: ( rule__Function__Group__0__Impl rule__Function__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3357:2: rule__Function__Group__0__Impl rule__Function__Group__1
            {
            pushFollow(FOLLOW_rule__Function__Group__0__Impl_in_rule__Function__Group__06867);
            rule__Function__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__1_in_rule__Function__Group__06870);
            rule__Function__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0"


    // $ANTLR start "rule__Function__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3364:1: rule__Function__Group__0__Impl : ( 'void' ) ;
    public final void rule__Function__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3368:1: ( ( 'void' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3369:1: ( 'void' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3369:1: ( 'void' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3370:1: 'void'
            {
             before(grammarAccess.getFunctionAccess().getVoidKeyword_0()); 
            match(input,36,FOLLOW_36_in_rule__Function__Group__0__Impl6898); 
             after(grammarAccess.getFunctionAccess().getVoidKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0__Impl"


    // $ANTLR start "rule__Function__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3383:1: rule__Function__Group__1 : rule__Function__Group__1__Impl rule__Function__Group__2 ;
    public final void rule__Function__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3387:1: ( rule__Function__Group__1__Impl rule__Function__Group__2 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3388:2: rule__Function__Group__1__Impl rule__Function__Group__2
            {
            pushFollow(FOLLOW_rule__Function__Group__1__Impl_in_rule__Function__Group__16929);
            rule__Function__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__2_in_rule__Function__Group__16932);
            rule__Function__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1"


    // $ANTLR start "rule__Function__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3395:1: rule__Function__Group__1__Impl : ( ( rule__Function__NameAssignment_1 ) ) ;
    public final void rule__Function__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3399:1: ( ( ( rule__Function__NameAssignment_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3400:1: ( ( rule__Function__NameAssignment_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3400:1: ( ( rule__Function__NameAssignment_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3401:1: ( rule__Function__NameAssignment_1 )
            {
             before(grammarAccess.getFunctionAccess().getNameAssignment_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3402:1: ( rule__Function__NameAssignment_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3402:2: rule__Function__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Function__NameAssignment_1_in_rule__Function__Group__1__Impl6959);
            rule__Function__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1__Impl"


    // $ANTLR start "rule__Function__Group__2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3412:1: rule__Function__Group__2 : rule__Function__Group__2__Impl rule__Function__Group__3 ;
    public final void rule__Function__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3416:1: ( rule__Function__Group__2__Impl rule__Function__Group__3 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3417:2: rule__Function__Group__2__Impl rule__Function__Group__3
            {
            pushFollow(FOLLOW_rule__Function__Group__2__Impl_in_rule__Function__Group__26989);
            rule__Function__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__3_in_rule__Function__Group__26992);
            rule__Function__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2"


    // $ANTLR start "rule__Function__Group__2__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3424:1: rule__Function__Group__2__Impl : ( '(' ) ;
    public final void rule__Function__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3428:1: ( ( '(' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3429:1: ( '(' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3429:1: ( '(' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3430:1: '('
            {
             before(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2()); 
            match(input,24,FOLLOW_24_in_rule__Function__Group__2__Impl7020); 
             after(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2__Impl"


    // $ANTLR start "rule__Function__Group__3"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3443:1: rule__Function__Group__3 : rule__Function__Group__3__Impl rule__Function__Group__4 ;
    public final void rule__Function__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3447:1: ( rule__Function__Group__3__Impl rule__Function__Group__4 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3448:2: rule__Function__Group__3__Impl rule__Function__Group__4
            {
            pushFollow(FOLLOW_rule__Function__Group__3__Impl_in_rule__Function__Group__37051);
            rule__Function__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__4_in_rule__Function__Group__37054);
            rule__Function__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3"


    // $ANTLR start "rule__Function__Group__3__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3455:1: rule__Function__Group__3__Impl : ( ')' ) ;
    public final void rule__Function__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3459:1: ( ( ')' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3460:1: ( ')' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3460:1: ( ')' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3461:1: ')'
            {
             before(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_3()); 
            match(input,25,FOLLOW_25_in_rule__Function__Group__3__Impl7082); 
             after(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3__Impl"


    // $ANTLR start "rule__Function__Group__4"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3474:1: rule__Function__Group__4 : rule__Function__Group__4__Impl rule__Function__Group__5 ;
    public final void rule__Function__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3478:1: ( rule__Function__Group__4__Impl rule__Function__Group__5 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3479:2: rule__Function__Group__4__Impl rule__Function__Group__5
            {
            pushFollow(FOLLOW_rule__Function__Group__4__Impl_in_rule__Function__Group__47113);
            rule__Function__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__5_in_rule__Function__Group__47116);
            rule__Function__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4"


    // $ANTLR start "rule__Function__Group__4__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3486:1: rule__Function__Group__4__Impl : ( '{' ) ;
    public final void rule__Function__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3490:1: ( ( '{' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3491:1: ( '{' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3491:1: ( '{' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3492:1: '{'
            {
             before(grammarAccess.getFunctionAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,27,FOLLOW_27_in_rule__Function__Group__4__Impl7144); 
             after(grammarAccess.getFunctionAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4__Impl"


    // $ANTLR start "rule__Function__Group__5"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3505:1: rule__Function__Group__5 : rule__Function__Group__5__Impl rule__Function__Group__6 ;
    public final void rule__Function__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3509:1: ( rule__Function__Group__5__Impl rule__Function__Group__6 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3510:2: rule__Function__Group__5__Impl rule__Function__Group__6
            {
            pushFollow(FOLLOW_rule__Function__Group__5__Impl_in_rule__Function__Group__57175);
            rule__Function__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__6_in_rule__Function__Group__57178);
            rule__Function__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5"


    // $ANTLR start "rule__Function__Group__5__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3517:1: rule__Function__Group__5__Impl : ( ( rule__Function__FeaturesAssignment_5 )* ) ;
    public final void rule__Function__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3521:1: ( ( ( rule__Function__FeaturesAssignment_5 )* ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3522:1: ( ( rule__Function__FeaturesAssignment_5 )* )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3522:1: ( ( rule__Function__FeaturesAssignment_5 )* )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3523:1: ( rule__Function__FeaturesAssignment_5 )*
            {
             before(grammarAccess.getFunctionAccess().getFeaturesAssignment_5()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3524:1: ( rule__Function__FeaturesAssignment_5 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==RULE_INTEGERS||LA20_0==RULE_ID||LA20_0==26||LA20_0==29||(LA20_0>=32 && LA20_0<=35)||LA20_0==37||LA20_0==40) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3524:2: rule__Function__FeaturesAssignment_5
            	    {
            	    pushFollow(FOLLOW_rule__Function__FeaturesAssignment_5_in_rule__Function__Group__5__Impl7205);
            	    rule__Function__FeaturesAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getFunctionAccess().getFeaturesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5__Impl"


    // $ANTLR start "rule__Function__Group__6"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3534:1: rule__Function__Group__6 : rule__Function__Group__6__Impl ;
    public final void rule__Function__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3538:1: ( rule__Function__Group__6__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3539:2: rule__Function__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__Function__Group__6__Impl_in_rule__Function__Group__67236);
            rule__Function__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__6"


    // $ANTLR start "rule__Function__Group__6__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3545:1: rule__Function__Group__6__Impl : ( '}' ) ;
    public final void rule__Function__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3549:1: ( ( '}' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3550:1: ( '}' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3550:1: ( '}' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3551:1: '}'
            {
             before(grammarAccess.getFunctionAccess().getRightCurlyBracketKeyword_6()); 
            match(input,28,FOLLOW_28_in_rule__Function__Group__6__Impl7264); 
             after(grammarAccess.getFunctionAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__6__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3578:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3582:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3583:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_rule__Import__Group__0__Impl_in_rule__Import__Group__07309);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Import__Group__1_in_rule__Import__Group__07312);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3590:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3594:1: ( ( 'import' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3595:1: ( 'import' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3595:1: ( 'import' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3596:1: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,37,FOLLOW_37_in_rule__Import__Group__0__Impl7340); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3609:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3613:1: ( rule__Import__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3614:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Import__Group__1__Impl_in_rule__Import__Group__17371);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3620:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3624:1: ( ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3625:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3625:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3626:1: ( rule__Import__ImportedNamespaceAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3627:1: ( rule__Import__ImportedNamespaceAssignment_1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3627:2: rule__Import__ImportedNamespaceAssignment_1
            {
            pushFollow(FOLLOW_rule__Import__ImportedNamespaceAssignment_1_in_rule__Import__Group__1__Impl7398);
            rule__Import__ImportedNamespaceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3641:1: rule__QualifiedNameWithWildcard__Group__0 : rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 ;
    public final void rule__QualifiedNameWithWildcard__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3645:1: ( rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3646:2: rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group__0__Impl_in_rule__QualifiedNameWithWildcard__Group__07432);
            rule__QualifiedNameWithWildcard__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group__1_in_rule__QualifiedNameWithWildcard__Group__07435);
            rule__QualifiedNameWithWildcard__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3653:1: rule__QualifiedNameWithWildcard__Group__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3657:1: ( ( ruleQualifiedName ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3658:1: ( ruleQualifiedName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3658:1: ( ruleQualifiedName )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3659:1: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Group__0__Impl7462);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3670:1: rule__QualifiedNameWithWildcard__Group__1 : rule__QualifiedNameWithWildcard__Group__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3674:1: ( rule__QualifiedNameWithWildcard__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3675:2: rule__QualifiedNameWithWildcard__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedNameWithWildcard__Group__1__Impl_in_rule__QualifiedNameWithWildcard__Group__17491);
            rule__QualifiedNameWithWildcard__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3681:1: rule__QualifiedNameWithWildcard__Group__1__Impl : ( ( '.*' )? ) ;
    public final void rule__QualifiedNameWithWildcard__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3685:1: ( ( ( '.*' )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3686:1: ( ( '.*' )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3686:1: ( ( '.*' )? )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3687:1: ( '.*' )?
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3688:1: ( '.*' )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==38) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3689:2: '.*'
                    {
                    match(input,38,FOLLOW_38_in_rule__QualifiedNameWithWildcard__Group__1__Impl7520); 

                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3704:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3708:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3709:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__07557);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__07560);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3716:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3720:1: ( ( RULE_ID ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3721:1: ( RULE_ID )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3721:1: ( RULE_ID )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3722:1: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__QualifiedName__Group__0__Impl7587); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3733:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3737:1: ( rule__QualifiedName__Group__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3738:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__17616);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3744:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3748:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3749:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3749:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3750:1: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3751:1: ( rule__QualifiedName__Group_1__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==39) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3751:2: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__QualifiedName__Group_1__0_in_rule__QualifiedName__Group__1__Impl7643);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3765:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3769:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3770:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group_1__0__Impl_in_rule__QualifiedName__Group_1__07678);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedName__Group_1__1_in_rule__QualifiedName__Group_1__07681);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3777:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3781:1: ( ( '.' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3782:1: ( '.' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3782:1: ( '.' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3783:1: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,39,FOLLOW_39_in_rule__QualifiedName__Group_1__0__Impl7709); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3796:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3800:1: ( rule__QualifiedName__Group_1__1__Impl )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3801:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group_1__1__Impl_in_rule__QualifiedName__Group_1__17740);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3807:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3811:1: ( ( RULE_ID ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3812:1: ( RULE_ID )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3812:1: ( RULE_ID )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3813:1: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__QualifiedName__Group_1__1__Impl7767); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Domainmodel__ElementsAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3829:1: rule__Domainmodel__ElementsAssignment : ( ruleFunction ) ;
    public final void rule__Domainmodel__ElementsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3833:1: ( ( ruleFunction ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3834:1: ( ruleFunction )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3834:1: ( ruleFunction )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3835:1: ruleFunction
            {
             before(grammarAccess.getDomainmodelAccess().getElementsFunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleFunction_in_rule__Domainmodel__ElementsAssignment7805);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getDomainmodelAccess().getElementsFunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__ElementsAssignment"


    // $ANTLR start "rule__Type__IntegerAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3844:1: rule__Type__IntegerAssignment_0 : ( ruleIntegerDeclarationAss ) ;
    public final void rule__Type__IntegerAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3848:1: ( ( ruleIntegerDeclarationAss ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3849:1: ( ruleIntegerDeclarationAss )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3849:1: ( ruleIntegerDeclarationAss )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3850:1: ruleIntegerDeclarationAss
            {
             before(grammarAccess.getTypeAccess().getIntegerIntegerDeclarationAssParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleIntegerDeclarationAss_in_rule__Type__IntegerAssignment_07836);
            ruleIntegerDeclarationAss();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getIntegerIntegerDeclarationAssParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__IntegerAssignment_0"


    // $ANTLR start "rule__Type__IntegervarvarAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3859:1: rule__Type__IntegervarvarAssignment_1 : ( ruleIntegerVarVar ) ;
    public final void rule__Type__IntegervarvarAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3863:1: ( ( ruleIntegerVarVar ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3864:1: ( ruleIntegerVarVar )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3864:1: ( ruleIntegerVarVar )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3865:1: ruleIntegerVarVar
            {
             before(grammarAccess.getTypeAccess().getIntegervarvarIntegerVarVarParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleIntegerVarVar_in_rule__Type__IntegervarvarAssignment_17867);
            ruleIntegerVarVar();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getIntegervarvarIntegerVarVarParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__IntegervarvarAssignment_1"


    // $ANTLR start "rule__Type__BooleanAssignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3874:1: rule__Type__BooleanAssignment_2 : ( ruleBooleanDeclarationAss ) ;
    public final void rule__Type__BooleanAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3878:1: ( ( ruleBooleanDeclarationAss ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3879:1: ( ruleBooleanDeclarationAss )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3879:1: ( ruleBooleanDeclarationAss )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3880:1: ruleBooleanDeclarationAss
            {
             before(grammarAccess.getTypeAccess().getBooleanBooleanDeclarationAssParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleBooleanDeclarationAss_in_rule__Type__BooleanAssignment_27898);
            ruleBooleanDeclarationAss();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getBooleanBooleanDeclarationAssParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__BooleanAssignment_2"


    // $ANTLR start "rule__Type__BooleanvarvarAssignment_3"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3889:1: rule__Type__BooleanvarvarAssignment_3 : ( ruleBooleanVarVar ) ;
    public final void rule__Type__BooleanvarvarAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3893:1: ( ( ruleBooleanVarVar ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3894:1: ( ruleBooleanVarVar )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3894:1: ( ruleBooleanVarVar )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3895:1: ruleBooleanVarVar
            {
             before(grammarAccess.getTypeAccess().getBooleanvarvarBooleanVarVarParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleBooleanVarVar_in_rule__Type__BooleanvarvarAssignment_37929);
            ruleBooleanVarVar();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getBooleanvarvarBooleanVarVarParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__BooleanvarvarAssignment_3"


    // $ANTLR start "rule__Type__CharacterAssignment_4"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3904:1: rule__Type__CharacterAssignment_4 : ( ruleCharacterDeclarationAss ) ;
    public final void rule__Type__CharacterAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3908:1: ( ( ruleCharacterDeclarationAss ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3909:1: ( ruleCharacterDeclarationAss )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3909:1: ( ruleCharacterDeclarationAss )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3910:1: ruleCharacterDeclarationAss
            {
             before(grammarAccess.getTypeAccess().getCharacterCharacterDeclarationAssParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleCharacterDeclarationAss_in_rule__Type__CharacterAssignment_47960);
            ruleCharacterDeclarationAss();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getCharacterCharacterDeclarationAssParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__CharacterAssignment_4"


    // $ANTLR start "rule__Type__CharactervarvarAssignment_5"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3919:1: rule__Type__CharactervarvarAssignment_5 : ( ruleCharacterVarVar ) ;
    public final void rule__Type__CharactervarvarAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3923:1: ( ( ruleCharacterVarVar ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3924:1: ( ruleCharacterVarVar )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3924:1: ( ruleCharacterVarVar )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3925:1: ruleCharacterVarVar
            {
             before(grammarAccess.getTypeAccess().getCharactervarvarCharacterVarVarParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleCharacterVarVar_in_rule__Type__CharactervarvarAssignment_57991);
            ruleCharacterVarVar();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getCharactervarvarCharacterVarVarParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__CharactervarvarAssignment_5"


    // $ANTLR start "rule__Type__WhileAssignment_6"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3934:1: rule__Type__WhileAssignment_6 : ( ruleStatement_While ) ;
    public final void rule__Type__WhileAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3938:1: ( ( ruleStatement_While ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3939:1: ( ruleStatement_While )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3939:1: ( ruleStatement_While )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3940:1: ruleStatement_While
            {
             before(grammarAccess.getTypeAccess().getWhileStatement_WhileParserRuleCall_6_0()); 
            pushFollow(FOLLOW_ruleStatement_While_in_rule__Type__WhileAssignment_68022);
            ruleStatement_While();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getWhileStatement_WhileParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__WhileAssignment_6"


    // $ANTLR start "rule__Type__IfAssignment_7"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3949:1: rule__Type__IfAssignment_7 : ( ruleIf_statment ) ;
    public final void rule__Type__IfAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3953:1: ( ( ruleIf_statment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3954:1: ( ruleIf_statment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3954:1: ( ruleIf_statment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3955:1: ruleIf_statment
            {
             before(grammarAccess.getTypeAccess().getIfIf_statmentParserRuleCall_7_0()); 
            pushFollow(FOLLOW_ruleIf_statment_in_rule__Type__IfAssignment_78053);
            ruleIf_statment();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getIfIf_statmentParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__IfAssignment_7"


    // $ANTLR start "rule__Type__PrintAssignment_8"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3964:1: rule__Type__PrintAssignment_8 : ( rulePrintL ) ;
    public final void rule__Type__PrintAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3968:1: ( ( rulePrintL ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3969:1: ( rulePrintL )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3969:1: ( rulePrintL )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3970:1: rulePrintL
            {
             before(grammarAccess.getTypeAccess().getPrintPrintLParserRuleCall_8_0()); 
            pushFollow(FOLLOW_rulePrintL_in_rule__Type__PrintAssignment_88084);
            rulePrintL();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getPrintPrintLParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__PrintAssignment_8"


    // $ANTLR start "rule__Type__FunctionAssignment_9"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3979:1: rule__Type__FunctionAssignment_9 : ( ruleCallFunction ) ;
    public final void rule__Type__FunctionAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3983:1: ( ( ruleCallFunction ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3984:1: ( ruleCallFunction )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3984:1: ( ruleCallFunction )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3985:1: ruleCallFunction
            {
             before(grammarAccess.getTypeAccess().getFunctionCallFunctionParserRuleCall_9_0()); 
            pushFollow(FOLLOW_ruleCallFunction_in_rule__Type__FunctionAssignment_98115);
            ruleCallFunction();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getFunctionCallFunctionParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__FunctionAssignment_9"


    // $ANTLR start "rule__PrintL__PrintAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3994:1: rule__PrintL__PrintAssignment_0 : ( ( 'spausdinti' ) ) ;
    public final void rule__PrintL__PrintAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3998:1: ( ( ( 'spausdinti' ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3999:1: ( ( 'spausdinti' ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:3999:1: ( ( 'spausdinti' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4000:1: ( 'spausdinti' )
            {
             before(grammarAccess.getPrintLAccess().getPrintSpausdintiKeyword_0_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4001:1: ( 'spausdinti' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4002:1: 'spausdinti'
            {
             before(grammarAccess.getPrintLAccess().getPrintSpausdintiKeyword_0_0()); 
            match(input,40,FOLLOW_40_in_rule__PrintL__PrintAssignment_08151); 
             after(grammarAccess.getPrintLAccess().getPrintSpausdintiKeyword_0_0()); 

            }

             after(grammarAccess.getPrintLAccess().getPrintSpausdintiKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__PrintAssignment_0"


    // $ANTLR start "rule__PrintL__ValueAssignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4017:1: rule__PrintL__ValueAssignment_2 : ( ( rule__PrintL__ValueAlternatives_2_0 ) ) ;
    public final void rule__PrintL__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4021:1: ( ( ( rule__PrintL__ValueAlternatives_2_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4022:1: ( ( rule__PrintL__ValueAlternatives_2_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4022:1: ( ( rule__PrintL__ValueAlternatives_2_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4023:1: ( rule__PrintL__ValueAlternatives_2_0 )
            {
             before(grammarAccess.getPrintLAccess().getValueAlternatives_2_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4024:1: ( rule__PrintL__ValueAlternatives_2_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4024:2: rule__PrintL__ValueAlternatives_2_0
            {
            pushFollow(FOLLOW_rule__PrintL__ValueAlternatives_2_0_in_rule__PrintL__ValueAssignment_28190);
            rule__PrintL__ValueAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getPrintLAccess().getValueAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrintL__ValueAssignment_2"


    // $ANTLR start "rule__Statement_While__CondOneAssignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4033:1: rule__Statement_While__CondOneAssignment_2 : ( ruleBooleanTypeExpression ) ;
    public final void rule__Statement_While__CondOneAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4037:1: ( ( ruleBooleanTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4038:1: ( ruleBooleanTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4038:1: ( ruleBooleanTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4039:1: ruleBooleanTypeExpression
            {
             before(grammarAccess.getStatement_WhileAccess().getCondOneBooleanTypeExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleBooleanTypeExpression_in_rule__Statement_While__CondOneAssignment_28223);
            ruleBooleanTypeExpression();

            state._fsp--;

             after(grammarAccess.getStatement_WhileAccess().getCondOneBooleanTypeExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__CondOneAssignment_2"


    // $ANTLR start "rule__Statement_While__OperationsAssignment_5"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4048:1: rule__Statement_While__OperationsAssignment_5 : ( ruleType ) ;
    public final void rule__Statement_While__OperationsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4052:1: ( ( ruleType ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4053:1: ( ruleType )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4053:1: ( ruleType )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4054:1: ruleType
            {
             before(grammarAccess.getStatement_WhileAccess().getOperationsTypeParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleType_in_rule__Statement_While__OperationsAssignment_58254);
            ruleType();

            state._fsp--;

             after(grammarAccess.getStatement_WhileAccess().getOperationsTypeParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement_While__OperationsAssignment_5"


    // $ANTLR start "rule__If_statment__CondOneAssignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4063:1: rule__If_statment__CondOneAssignment_2 : ( ruleBooleanTypeExpression ) ;
    public final void rule__If_statment__CondOneAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4067:1: ( ( ruleBooleanTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4068:1: ( ruleBooleanTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4068:1: ( ruleBooleanTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4069:1: ruleBooleanTypeExpression
            {
             before(grammarAccess.getIf_statmentAccess().getCondOneBooleanTypeExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleBooleanTypeExpression_in_rule__If_statment__CondOneAssignment_28285);
            ruleBooleanTypeExpression();

            state._fsp--;

             after(grammarAccess.getIf_statmentAccess().getCondOneBooleanTypeExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__CondOneAssignment_2"


    // $ANTLR start "rule__If_statment__OperationsAssignment_5"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4078:1: rule__If_statment__OperationsAssignment_5 : ( ruleType ) ;
    public final void rule__If_statment__OperationsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4082:1: ( ( ruleType ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4083:1: ( ruleType )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4083:1: ( ruleType )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4084:1: ruleType
            {
             before(grammarAccess.getIf_statmentAccess().getOperationsTypeParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleType_in_rule__If_statment__OperationsAssignment_58316);
            ruleType();

            state._fsp--;

             after(grammarAccess.getIf_statmentAccess().getOperationsTypeParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__OperationsAssignment_5"


    // $ANTLR start "rule__If_statment__ElseAssignment_7"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4093:1: rule__If_statment__ElseAssignment_7 : ( ruleElse ) ;
    public final void rule__If_statment__ElseAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4097:1: ( ( ruleElse ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4098:1: ( ruleElse )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4098:1: ( ruleElse )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4099:1: ruleElse
            {
             before(grammarAccess.getIf_statmentAccess().getElseElseParserRuleCall_7_0()); 
            pushFollow(FOLLOW_ruleElse_in_rule__If_statment__ElseAssignment_78347);
            ruleElse();

            state._fsp--;

             after(grammarAccess.getIf_statmentAccess().getElseElseParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If_statment__ElseAssignment_7"


    // $ANTLR start "rule__Else__OperationsAssignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4108:1: rule__Else__OperationsAssignment_2 : ( ruleType ) ;
    public final void rule__Else__OperationsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4112:1: ( ( ruleType ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4113:1: ( ruleType )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4113:1: ( ruleType )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4114:1: ruleType
            {
             before(grammarAccess.getElseAccess().getOperationsTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleType_in_rule__Else__OperationsAssignment_28378);
            ruleType();

            state._fsp--;

             after(grammarAccess.getElseAccess().getOperationsTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__OperationsAssignment_2"


    // $ANTLR start "rule__CharacterDeclarationAss__AssignAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4123:1: rule__CharacterDeclarationAss__AssignAssignment_0 : ( ruleCharacterDeclaration ) ;
    public final void rule__CharacterDeclarationAss__AssignAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4127:1: ( ( ruleCharacterDeclaration ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4128:1: ( ruleCharacterDeclaration )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4128:1: ( ruleCharacterDeclaration )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4129:1: ruleCharacterDeclaration
            {
             before(grammarAccess.getCharacterDeclarationAssAccess().getAssignCharacterDeclarationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleCharacterDeclaration_in_rule__CharacterDeclarationAss__AssignAssignment_08409);
            ruleCharacterDeclaration();

            state._fsp--;

             after(grammarAccess.getCharacterDeclarationAssAccess().getAssignCharacterDeclarationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__AssignAssignment_0"


    // $ANTLR start "rule__CharacterDeclarationAss__ValueAssignment_1_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4138:1: rule__CharacterDeclarationAss__ValueAssignment_1_1 : ( ruleCharacterTypeExpression ) ;
    public final void rule__CharacterDeclarationAss__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4142:1: ( ( ruleCharacterTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4143:1: ( ruleCharacterTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4143:1: ( ruleCharacterTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4144:1: ruleCharacterTypeExpression
            {
             before(grammarAccess.getCharacterDeclarationAssAccess().getValueCharacterTypeExpressionParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleCharacterTypeExpression_in_rule__CharacterDeclarationAss__ValueAssignment_1_18440);
            ruleCharacterTypeExpression();

            state._fsp--;

             after(grammarAccess.getCharacterDeclarationAssAccess().getValueCharacterTypeExpressionParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclarationAss__ValueAssignment_1_1"


    // $ANTLR start "rule__CharacterDeclaration__IdentifierAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4153:1: rule__CharacterDeclaration__IdentifierAssignment_1 : ( ruleVariableName ) ;
    public final void rule__CharacterDeclaration__IdentifierAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4157:1: ( ( ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4158:1: ( ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4158:1: ( ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4159:1: ruleVariableName
            {
             before(grammarAccess.getCharacterDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleVariableName_in_rule__CharacterDeclaration__IdentifierAssignment_18471);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getCharacterDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterDeclaration__IdentifierAssignment_1"


    // $ANTLR start "rule__CharacterTypeExpression__CharAssignAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4168:1: rule__CharacterTypeExpression__CharAssignAssignment : ( ruleCharacterAssignment ) ;
    public final void rule__CharacterTypeExpression__CharAssignAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4172:1: ( ( ruleCharacterAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4173:1: ( ruleCharacterAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4173:1: ( ruleCharacterAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4174:1: ruleCharacterAssignment
            {
             before(grammarAccess.getCharacterTypeExpressionAccess().getCharAssignCharacterAssignmentParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleCharacterAssignment_in_rule__CharacterTypeExpression__CharAssignAssignment8502);
            ruleCharacterAssignment();

            state._fsp--;

             after(grammarAccess.getCharacterTypeExpressionAccess().getCharAssignCharacterAssignmentParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterTypeExpression__CharAssignAssignment"


    // $ANTLR start "rule__CharacterAssignment__ValueAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4183:1: rule__CharacterAssignment__ValueAssignment : ( ( rule__CharacterAssignment__ValueAlternatives_0 ) ) ;
    public final void rule__CharacterAssignment__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4187:1: ( ( ( rule__CharacterAssignment__ValueAlternatives_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4188:1: ( ( rule__CharacterAssignment__ValueAlternatives_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4188:1: ( ( rule__CharacterAssignment__ValueAlternatives_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4189:1: ( rule__CharacterAssignment__ValueAlternatives_0 )
            {
             before(grammarAccess.getCharacterAssignmentAccess().getValueAlternatives_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4190:1: ( rule__CharacterAssignment__ValueAlternatives_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4190:2: rule__CharacterAssignment__ValueAlternatives_0
            {
            pushFollow(FOLLOW_rule__CharacterAssignment__ValueAlternatives_0_in_rule__CharacterAssignment__ValueAssignment8533);
            rule__CharacterAssignment__ValueAlternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getCharacterAssignmentAccess().getValueAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterAssignment__ValueAssignment"


    // $ANTLR start "rule__CharacterVarVar__IdentifierAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4199:1: rule__CharacterVarVar__IdentifierAssignment_0 : ( ruleVariableName ) ;
    public final void rule__CharacterVarVar__IdentifierAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4203:1: ( ( ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4204:1: ( ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4204:1: ( ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4205:1: ruleVariableName
            {
             before(grammarAccess.getCharacterVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleVariableName_in_rule__CharacterVarVar__IdentifierAssignment_08566);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getCharacterVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterVarVar__IdentifierAssignment_0"


    // $ANTLR start "rule__CharacterVarVar__ValueAssignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4214:1: rule__CharacterVarVar__ValueAssignment_2 : ( RULE_CHARACTERS ) ;
    public final void rule__CharacterVarVar__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4218:1: ( ( RULE_CHARACTERS ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4219:1: ( RULE_CHARACTERS )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4219:1: ( RULE_CHARACTERS )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4220:1: RULE_CHARACTERS
            {
             before(grammarAccess.getCharacterVarVarAccess().getValueCHARACTERSTerminalRuleCall_2_0()); 
            match(input,RULE_CHARACTERS,FOLLOW_RULE_CHARACTERS_in_rule__CharacterVarVar__ValueAssignment_28597); 
             after(grammarAccess.getCharacterVarVarAccess().getValueCHARACTERSTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CharacterVarVar__ValueAssignment_2"


    // $ANTLR start "rule__BooleanDeclarationAss__AssignAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4229:1: rule__BooleanDeclarationAss__AssignAssignment_0 : ( ruleBooleanDeclaration ) ;
    public final void rule__BooleanDeclarationAss__AssignAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4233:1: ( ( ruleBooleanDeclaration ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4234:1: ( ruleBooleanDeclaration )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4234:1: ( ruleBooleanDeclaration )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4235:1: ruleBooleanDeclaration
            {
             before(grammarAccess.getBooleanDeclarationAssAccess().getAssignBooleanDeclarationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleBooleanDeclaration_in_rule__BooleanDeclarationAss__AssignAssignment_08628);
            ruleBooleanDeclaration();

            state._fsp--;

             after(grammarAccess.getBooleanDeclarationAssAccess().getAssignBooleanDeclarationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__AssignAssignment_0"


    // $ANTLR start "rule__BooleanDeclarationAss__ValueAssignment_1_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4244:1: rule__BooleanDeclarationAss__ValueAssignment_1_1 : ( ruleBooleanTypeExpression ) ;
    public final void rule__BooleanDeclarationAss__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4248:1: ( ( ruleBooleanTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4249:1: ( ruleBooleanTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4249:1: ( ruleBooleanTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4250:1: ruleBooleanTypeExpression
            {
             before(grammarAccess.getBooleanDeclarationAssAccess().getValueBooleanTypeExpressionParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleBooleanTypeExpression_in_rule__BooleanDeclarationAss__ValueAssignment_1_18659);
            ruleBooleanTypeExpression();

            state._fsp--;

             after(grammarAccess.getBooleanDeclarationAssAccess().getValueBooleanTypeExpressionParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclarationAss__ValueAssignment_1_1"


    // $ANTLR start "rule__BooleanDeclaration__IdentifierAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4259:1: rule__BooleanDeclaration__IdentifierAssignment_1 : ( ruleVariableName ) ;
    public final void rule__BooleanDeclaration__IdentifierAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4263:1: ( ( ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4264:1: ( ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4264:1: ( ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4265:1: ruleVariableName
            {
             before(grammarAccess.getBooleanDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleVariableName_in_rule__BooleanDeclaration__IdentifierAssignment_18690);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getBooleanDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanDeclaration__IdentifierAssignment_1"


    // $ANTLR start "rule__BooleanTypeExpression__BoolAssignAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4274:1: rule__BooleanTypeExpression__BoolAssignAssignment_0 : ( ruleBooleanAssignment ) ;
    public final void rule__BooleanTypeExpression__BoolAssignAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4278:1: ( ( ruleBooleanAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4279:1: ( ruleBooleanAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4279:1: ( ruleBooleanAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4280:1: ruleBooleanAssignment
            {
             before(grammarAccess.getBooleanTypeExpressionAccess().getBoolAssignBooleanAssignmentParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleBooleanAssignment_in_rule__BooleanTypeExpression__BoolAssignAssignment_08721);
            ruleBooleanAssignment();

            state._fsp--;

             after(grammarAccess.getBooleanTypeExpressionAccess().getBoolAssignBooleanAssignmentParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanTypeExpression__BoolAssignAssignment_0"


    // $ANTLR start "rule__BooleanTypeExpression__BoolOpAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4289:1: rule__BooleanTypeExpression__BoolOpAssignment_1 : ( ruleBooleanOperation ) ;
    public final void rule__BooleanTypeExpression__BoolOpAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4293:1: ( ( ruleBooleanOperation ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4294:1: ( ruleBooleanOperation )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4294:1: ( ruleBooleanOperation )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4295:1: ruleBooleanOperation
            {
             before(grammarAccess.getBooleanTypeExpressionAccess().getBoolOpBooleanOperationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleBooleanOperation_in_rule__BooleanTypeExpression__BoolOpAssignment_18752);
            ruleBooleanOperation();

            state._fsp--;

             after(grammarAccess.getBooleanTypeExpressionAccess().getBoolOpBooleanOperationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanTypeExpression__BoolOpAssignment_1"


    // $ANTLR start "rule__BooleanTypeExpression__IntToBoolAssignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4304:1: rule__BooleanTypeExpression__IntToBoolAssignment_2 : ( ruleIntegerToBool ) ;
    public final void rule__BooleanTypeExpression__IntToBoolAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4308:1: ( ( ruleIntegerToBool ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4309:1: ( ruleIntegerToBool )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4309:1: ( ruleIntegerToBool )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4310:1: ruleIntegerToBool
            {
             before(grammarAccess.getBooleanTypeExpressionAccess().getIntToBoolIntegerToBoolParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleIntegerToBool_in_rule__BooleanTypeExpression__IntToBoolAssignment_28783);
            ruleIntegerToBool();

            state._fsp--;

             after(grammarAccess.getBooleanTypeExpressionAccess().getIntToBoolIntegerToBoolParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanTypeExpression__IntToBoolAssignment_2"


    // $ANTLR start "rule__BooleanVarVar__IdentifierAssignment_0_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4319:1: rule__BooleanVarVar__IdentifierAssignment_0_0 : ( ruleVariableName ) ;
    public final void rule__BooleanVarVar__IdentifierAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4323:1: ( ( ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4324:1: ( ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4324:1: ( ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4325:1: ruleVariableName
            {
             before(grammarAccess.getBooleanVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0_0()); 
            pushFollow(FOLLOW_ruleVariableName_in_rule__BooleanVarVar__IdentifierAssignment_0_08814);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getBooleanVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__IdentifierAssignment_0_0"


    // $ANTLR start "rule__BooleanVarVar__Value1Assignment_0_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4334:1: rule__BooleanVarVar__Value1Assignment_0_2 : ( ruleBooleanOperation ) ;
    public final void rule__BooleanVarVar__Value1Assignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4338:1: ( ( ruleBooleanOperation ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4339:1: ( ruleBooleanOperation )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4339:1: ( ruleBooleanOperation )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4340:1: ruleBooleanOperation
            {
             before(grammarAccess.getBooleanVarVarAccess().getValue1BooleanOperationParserRuleCall_0_2_0()); 
            pushFollow(FOLLOW_ruleBooleanOperation_in_rule__BooleanVarVar__Value1Assignment_0_28845);
            ruleBooleanOperation();

            state._fsp--;

             after(grammarAccess.getBooleanVarVarAccess().getValue1BooleanOperationParserRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__Value1Assignment_0_2"


    // $ANTLR start "rule__BooleanVarVar__Value2Assignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4349:1: rule__BooleanVarVar__Value2Assignment_1 : ( ruleIntegerToBool ) ;
    public final void rule__BooleanVarVar__Value2Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4353:1: ( ( ruleIntegerToBool ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4354:1: ( ruleIntegerToBool )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4354:1: ( ruleIntegerToBool )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4355:1: ruleIntegerToBool
            {
             before(grammarAccess.getBooleanVarVarAccess().getValue2IntegerToBoolParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleIntegerToBool_in_rule__BooleanVarVar__Value2Assignment_18876);
            ruleIntegerToBool();

            state._fsp--;

             after(grammarAccess.getBooleanVarVarAccess().getValue2IntegerToBoolParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanVarVar__Value2Assignment_1"


    // $ANTLR start "rule__BooleanAssignment__ValueAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4364:1: rule__BooleanAssignment__ValueAssignment : ( ( rule__BooleanAssignment__ValueAlternatives_0 ) ) ;
    public final void rule__BooleanAssignment__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4368:1: ( ( ( rule__BooleanAssignment__ValueAlternatives_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4369:1: ( ( rule__BooleanAssignment__ValueAlternatives_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4369:1: ( ( rule__BooleanAssignment__ValueAlternatives_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4370:1: ( rule__BooleanAssignment__ValueAlternatives_0 )
            {
             before(grammarAccess.getBooleanAssignmentAccess().getValueAlternatives_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4371:1: ( rule__BooleanAssignment__ValueAlternatives_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4371:2: rule__BooleanAssignment__ValueAlternatives_0
            {
            pushFollow(FOLLOW_rule__BooleanAssignment__ValueAlternatives_0_in_rule__BooleanAssignment__ValueAssignment8907);
            rule__BooleanAssignment__ValueAlternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanAssignmentAccess().getValueAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAssignment__ValueAssignment"


    // $ANTLR start "rule__BooleanOperation__Operand1Assignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4380:1: rule__BooleanOperation__Operand1Assignment_0 : ( ruleBooleanAssignment ) ;
    public final void rule__BooleanOperation__Operand1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4384:1: ( ( ruleBooleanAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4385:1: ( ruleBooleanAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4385:1: ( ruleBooleanAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4386:1: ruleBooleanAssignment
            {
             before(grammarAccess.getBooleanOperationAccess().getOperand1BooleanAssignmentParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleBooleanAssignment_in_rule__BooleanOperation__Operand1Assignment_08940);
            ruleBooleanAssignment();

            state._fsp--;

             after(grammarAccess.getBooleanOperationAccess().getOperand1BooleanAssignmentParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanOperation__Operand1Assignment_0"


    // $ANTLR start "rule__BooleanOperation__OperatorAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4395:1: rule__BooleanOperation__OperatorAssignment_1 : ( ruleBOOLOPERATOR ) ;
    public final void rule__BooleanOperation__OperatorAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4399:1: ( ( ruleBOOLOPERATOR ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4400:1: ( ruleBOOLOPERATOR )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4400:1: ( ruleBOOLOPERATOR )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4401:1: ruleBOOLOPERATOR
            {
             before(grammarAccess.getBooleanOperationAccess().getOperatorBOOLOPERATORParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleBOOLOPERATOR_in_rule__BooleanOperation__OperatorAssignment_18971);
            ruleBOOLOPERATOR();

            state._fsp--;

             after(grammarAccess.getBooleanOperationAccess().getOperatorBOOLOPERATORParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanOperation__OperatorAssignment_1"


    // $ANTLR start "rule__BooleanOperation__Operand2Assignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4410:1: rule__BooleanOperation__Operand2Assignment_2 : ( ruleBooleanAssignment ) ;
    public final void rule__BooleanOperation__Operand2Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4414:1: ( ( ruleBooleanAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4415:1: ( ruleBooleanAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4415:1: ( ruleBooleanAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4416:1: ruleBooleanAssignment
            {
             before(grammarAccess.getBooleanOperationAccess().getOperand2BooleanAssignmentParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleBooleanAssignment_in_rule__BooleanOperation__Operand2Assignment_29002);
            ruleBooleanAssignment();

            state._fsp--;

             after(grammarAccess.getBooleanOperationAccess().getOperand2BooleanAssignmentParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanOperation__Operand2Assignment_2"


    // $ANTLR start "rule__IntegerToBool__Operand1Assignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4425:1: rule__IntegerToBool__Operand1Assignment_0 : ( ruleIntegerTypeExpression ) ;
    public final void rule__IntegerToBool__Operand1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4429:1: ( ( ruleIntegerTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4430:1: ( ruleIntegerTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4430:1: ( ruleIntegerTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4431:1: ruleIntegerTypeExpression
            {
             before(grammarAccess.getIntegerToBoolAccess().getOperand1IntegerTypeExpressionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleIntegerTypeExpression_in_rule__IntegerToBool__Operand1Assignment_09033);
            ruleIntegerTypeExpression();

            state._fsp--;

             after(grammarAccess.getIntegerToBoolAccess().getOperand1IntegerTypeExpressionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerToBool__Operand1Assignment_0"


    // $ANTLR start "rule__IntegerToBool__OperatorAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4440:1: rule__IntegerToBool__OperatorAssignment_1 : ( ruleSPECIALINTOPERATOR ) ;
    public final void rule__IntegerToBool__OperatorAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4444:1: ( ( ruleSPECIALINTOPERATOR ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4445:1: ( ruleSPECIALINTOPERATOR )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4445:1: ( ruleSPECIALINTOPERATOR )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4446:1: ruleSPECIALINTOPERATOR
            {
             before(grammarAccess.getIntegerToBoolAccess().getOperatorSPECIALINTOPERATOREnumRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleSPECIALINTOPERATOR_in_rule__IntegerToBool__OperatorAssignment_19064);
            ruleSPECIALINTOPERATOR();

            state._fsp--;

             after(grammarAccess.getIntegerToBoolAccess().getOperatorSPECIALINTOPERATOREnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerToBool__OperatorAssignment_1"


    // $ANTLR start "rule__IntegerToBool__Operand2Assignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4455:1: rule__IntegerToBool__Operand2Assignment_2 : ( ruleIntegerTypeExpression ) ;
    public final void rule__IntegerToBool__Operand2Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4459:1: ( ( ruleIntegerTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4460:1: ( ruleIntegerTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4460:1: ( ruleIntegerTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4461:1: ruleIntegerTypeExpression
            {
             before(grammarAccess.getIntegerToBoolAccess().getOperand2IntegerTypeExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleIntegerTypeExpression_in_rule__IntegerToBool__Operand2Assignment_29095);
            ruleIntegerTypeExpression();

            state._fsp--;

             after(grammarAccess.getIntegerToBoolAccess().getOperand2IntegerTypeExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerToBool__Operand2Assignment_2"


    // $ANTLR start "rule__IntegerDeclarationAss__AssignAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4470:1: rule__IntegerDeclarationAss__AssignAssignment_0 : ( ruleIntegerDeclaration ) ;
    public final void rule__IntegerDeclarationAss__AssignAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4474:1: ( ( ruleIntegerDeclaration ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4475:1: ( ruleIntegerDeclaration )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4475:1: ( ruleIntegerDeclaration )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4476:1: ruleIntegerDeclaration
            {
             before(grammarAccess.getIntegerDeclarationAssAccess().getAssignIntegerDeclarationParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleIntegerDeclaration_in_rule__IntegerDeclarationAss__AssignAssignment_09126);
            ruleIntegerDeclaration();

            state._fsp--;

             after(grammarAccess.getIntegerDeclarationAssAccess().getAssignIntegerDeclarationParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__AssignAssignment_0"


    // $ANTLR start "rule__IntegerDeclarationAss__ValueAssignment_1_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4485:1: rule__IntegerDeclarationAss__ValueAssignment_1_1 : ( ruleIntegerTypeExpression ) ;
    public final void rule__IntegerDeclarationAss__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4489:1: ( ( ruleIntegerTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4490:1: ( ruleIntegerTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4490:1: ( ruleIntegerTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4491:1: ruleIntegerTypeExpression
            {
             before(grammarAccess.getIntegerDeclarationAssAccess().getValueIntegerTypeExpressionParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleIntegerTypeExpression_in_rule__IntegerDeclarationAss__ValueAssignment_1_19157);
            ruleIntegerTypeExpression();

            state._fsp--;

             after(grammarAccess.getIntegerDeclarationAssAccess().getValueIntegerTypeExpressionParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclarationAss__ValueAssignment_1_1"


    // $ANTLR start "rule__IntegerDeclaration__IdentifierAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4500:1: rule__IntegerDeclaration__IdentifierAssignment_1 : ( ruleVariableName ) ;
    public final void rule__IntegerDeclaration__IdentifierAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4504:1: ( ( ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4505:1: ( ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4505:1: ( ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4506:1: ruleVariableName
            {
             before(grammarAccess.getIntegerDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleVariableName_in_rule__IntegerDeclaration__IdentifierAssignment_19188);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getIntegerDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerDeclaration__IdentifierAssignment_1"


    // $ANTLR start "rule__IntegerVarVar__IdentifierAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4515:1: rule__IntegerVarVar__IdentifierAssignment_0 : ( ruleVariableName ) ;
    public final void rule__IntegerVarVar__IdentifierAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4519:1: ( ( ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4520:1: ( ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4520:1: ( ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4521:1: ruleVariableName
            {
             before(grammarAccess.getIntegerVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleVariableName_in_rule__IntegerVarVar__IdentifierAssignment_09219);
            ruleVariableName();

            state._fsp--;

             after(grammarAccess.getIntegerVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerVarVar__IdentifierAssignment_0"


    // $ANTLR start "rule__IntegerVarVar__ValueAssignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4530:1: rule__IntegerVarVar__ValueAssignment_2 : ( ruleIntegerTypeExpression ) ;
    public final void rule__IntegerVarVar__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4534:1: ( ( ruleIntegerTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4535:1: ( ruleIntegerTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4535:1: ( ruleIntegerTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4536:1: ruleIntegerTypeExpression
            {
             before(grammarAccess.getIntegerVarVarAccess().getValueIntegerTypeExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleIntegerTypeExpression_in_rule__IntegerVarVar__ValueAssignment_29250);
            ruleIntegerTypeExpression();

            state._fsp--;

             after(grammarAccess.getIntegerVarVarAccess().getValueIntegerTypeExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerVarVar__ValueAssignment_2"


    // $ANTLR start "rule__IntegerTypeExpression__IntegerAssignAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4545:1: rule__IntegerTypeExpression__IntegerAssignAssignment_0 : ( ruleIntegerAssignment ) ;
    public final void rule__IntegerTypeExpression__IntegerAssignAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4549:1: ( ( ruleIntegerAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4550:1: ( ruleIntegerAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4550:1: ( ruleIntegerAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4551:1: ruleIntegerAssignment
            {
             before(grammarAccess.getIntegerTypeExpressionAccess().getIntegerAssignIntegerAssignmentParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleIntegerAssignment_in_rule__IntegerTypeExpression__IntegerAssignAssignment_09281);
            ruleIntegerAssignment();

            state._fsp--;

             after(grammarAccess.getIntegerTypeExpressionAccess().getIntegerAssignIntegerAssignmentParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerTypeExpression__IntegerAssignAssignment_0"


    // $ANTLR start "rule__IntegerTypeExpression__IntegerOpAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4560:1: rule__IntegerTypeExpression__IntegerOpAssignment_1 : ( ruleIntegerOperation ) ;
    public final void rule__IntegerTypeExpression__IntegerOpAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4564:1: ( ( ruleIntegerOperation ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4565:1: ( ruleIntegerOperation )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4565:1: ( ruleIntegerOperation )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4566:1: ruleIntegerOperation
            {
             before(grammarAccess.getIntegerTypeExpressionAccess().getIntegerOpIntegerOperationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleIntegerOperation_in_rule__IntegerTypeExpression__IntegerOpAssignment_19312);
            ruleIntegerOperation();

            state._fsp--;

             after(grammarAccess.getIntegerTypeExpressionAccess().getIntegerOpIntegerOperationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerTypeExpression__IntegerOpAssignment_1"


    // $ANTLR start "rule__IntegerAssignment__ValueAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4575:1: rule__IntegerAssignment__ValueAssignment : ( ( rule__IntegerAssignment__ValueAlternatives_0 ) ) ;
    public final void rule__IntegerAssignment__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4579:1: ( ( ( rule__IntegerAssignment__ValueAlternatives_0 ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4580:1: ( ( rule__IntegerAssignment__ValueAlternatives_0 ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4580:1: ( ( rule__IntegerAssignment__ValueAlternatives_0 ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4581:1: ( rule__IntegerAssignment__ValueAlternatives_0 )
            {
             before(grammarAccess.getIntegerAssignmentAccess().getValueAlternatives_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4582:1: ( rule__IntegerAssignment__ValueAlternatives_0 )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4582:2: rule__IntegerAssignment__ValueAlternatives_0
            {
            pushFollow(FOLLOW_rule__IntegerAssignment__ValueAlternatives_0_in_rule__IntegerAssignment__ValueAssignment9343);
            rule__IntegerAssignment__ValueAlternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerAssignmentAccess().getValueAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerAssignment__ValueAssignment"


    // $ANTLR start "rule__IntegerOperation__Operand1Assignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4591:1: rule__IntegerOperation__Operand1Assignment_0 : ( ruleIntegerAssignment ) ;
    public final void rule__IntegerOperation__Operand1Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4595:1: ( ( ruleIntegerAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4596:1: ( ruleIntegerAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4596:1: ( ruleIntegerAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4597:1: ruleIntegerAssignment
            {
             before(grammarAccess.getIntegerOperationAccess().getOperand1IntegerAssignmentParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleIntegerAssignment_in_rule__IntegerOperation__Operand1Assignment_09376);
            ruleIntegerAssignment();

            state._fsp--;

             after(grammarAccess.getIntegerOperationAccess().getOperand1IntegerAssignmentParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerOperation__Operand1Assignment_0"


    // $ANTLR start "rule__IntegerOperation__OperatorAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4606:1: rule__IntegerOperation__OperatorAssignment_1 : ( ruleOPERATOR ) ;
    public final void rule__IntegerOperation__OperatorAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4610:1: ( ( ruleOPERATOR ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4611:1: ( ruleOPERATOR )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4611:1: ( ruleOPERATOR )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4612:1: ruleOPERATOR
            {
             before(grammarAccess.getIntegerOperationAccess().getOperatorOPERATOREnumRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleOPERATOR_in_rule__IntegerOperation__OperatorAssignment_19407);
            ruleOPERATOR();

            state._fsp--;

             after(grammarAccess.getIntegerOperationAccess().getOperatorOPERATOREnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerOperation__OperatorAssignment_1"


    // $ANTLR start "rule__IntegerOperation__Operand2Assignment_2"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4621:1: rule__IntegerOperation__Operand2Assignment_2 : ( ruleIntegerAssignment ) ;
    public final void rule__IntegerOperation__Operand2Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4625:1: ( ( ruleIntegerAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4626:1: ( ruleIntegerAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4626:1: ( ruleIntegerAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4627:1: ruleIntegerAssignment
            {
             before(grammarAccess.getIntegerOperationAccess().getOperand2IntegerAssignmentParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleIntegerAssignment_in_rule__IntegerOperation__Operand2Assignment_29438);
            ruleIntegerAssignment();

            state._fsp--;

             after(grammarAccess.getIntegerOperationAccess().getOperand2IntegerAssignmentParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerOperation__Operand2Assignment_2"


    // $ANTLR start "rule__CallFunction__OperationNameAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4636:1: rule__CallFunction__OperationNameAssignment_0 : ( ruleFunctionName ) ;
    public final void rule__CallFunction__OperationNameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4640:1: ( ( ruleFunctionName ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4641:1: ( ruleFunctionName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4641:1: ( ruleFunctionName )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4642:1: ruleFunctionName
            {
             before(grammarAccess.getCallFunctionAccess().getOperationNameFunctionNameParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleFunctionName_in_rule__CallFunction__OperationNameAssignment_09469);
            ruleFunctionName();

            state._fsp--;

             after(grammarAccess.getCallFunctionAccess().getOperationNameFunctionNameParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CallFunction__OperationNameAssignment_0"


    // $ANTLR start "rule__FunctionName__ValueAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4651:1: rule__FunctionName__ValueAssignment : ( RULE_ID ) ;
    public final void rule__FunctionName__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4655:1: ( ( RULE_ID ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4656:1: ( RULE_ID )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4656:1: ( RULE_ID )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4657:1: RULE_ID
            {
             before(grammarAccess.getFunctionNameAccess().getValueIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__FunctionName__ValueAssignment9500); 
             after(grammarAccess.getFunctionNameAccess().getValueIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionName__ValueAssignment"


    // $ANTLR start "rule__BOOLOPERATOR__ANDAssignment_0"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4666:1: rule__BOOLOPERATOR__ANDAssignment_0 : ( ( 'ir' ) ) ;
    public final void rule__BOOLOPERATOR__ANDAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4670:1: ( ( ( 'ir' ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4671:1: ( ( 'ir' ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4671:1: ( ( 'ir' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4672:1: ( 'ir' )
            {
             before(grammarAccess.getBOOLOPERATORAccess().getANDIrKeyword_0_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4673:1: ( 'ir' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4674:1: 'ir'
            {
             before(grammarAccess.getBOOLOPERATORAccess().getANDIrKeyword_0_0()); 
            match(input,41,FOLLOW_41_in_rule__BOOLOPERATOR__ANDAssignment_09536); 
             after(grammarAccess.getBOOLOPERATORAccess().getANDIrKeyword_0_0()); 

            }

             after(grammarAccess.getBOOLOPERATORAccess().getANDIrKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLOPERATOR__ANDAssignment_0"


    // $ANTLR start "rule__BOOLOPERATOR__ORAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4689:1: rule__BOOLOPERATOR__ORAssignment_1 : ( ( 'arba' ) ) ;
    public final void rule__BOOLOPERATOR__ORAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4693:1: ( ( ( 'arba' ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4694:1: ( ( 'arba' ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4694:1: ( ( 'arba' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4695:1: ( 'arba' )
            {
             before(grammarAccess.getBOOLOPERATORAccess().getORArbaKeyword_1_0()); 
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4696:1: ( 'arba' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4697:1: 'arba'
            {
             before(grammarAccess.getBOOLOPERATORAccess().getORArbaKeyword_1_0()); 
            match(input,42,FOLLOW_42_in_rule__BOOLOPERATOR__ORAssignment_19580); 
             after(grammarAccess.getBOOLOPERATORAccess().getORArbaKeyword_1_0()); 

            }

             after(grammarAccess.getBOOLOPERATORAccess().getORArbaKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BOOLOPERATOR__ORAssignment_1"


    // $ANTLR start "rule__Function__NameAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4712:1: rule__Function__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Function__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4716:1: ( ( RULE_ID ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4717:1: ( RULE_ID )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4717:1: ( RULE_ID )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4718:1: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Function__NameAssignment_19619); 
             after(grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__NameAssignment_1"


    // $ANTLR start "rule__Function__FeaturesAssignment_5"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4727:1: rule__Function__FeaturesAssignment_5 : ( ruleType ) ;
    public final void rule__Function__FeaturesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4731:1: ( ( ruleType ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4732:1: ( ruleType )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4732:1: ( ruleType )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4733:1: ruleType
            {
             before(grammarAccess.getFunctionAccess().getFeaturesTypeParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleType_in_rule__Function__FeaturesAssignment_59650);
            ruleType();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getFeaturesTypeParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__FeaturesAssignment_5"


    // $ANTLR start "rule__Import__ImportedNamespaceAssignment_1"
    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4742:1: rule__Import__ImportedNamespaceAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__Import__ImportedNamespaceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4746:1: ( ( ruleQualifiedNameWithWildcard ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4747:1: ( ruleQualifiedNameWithWildcard )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4747:1: ( ruleQualifiedNameWithWildcard )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4748:1: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_rule__Import__ImportedNamespaceAssignment_19681);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportedNamespaceAssignment_1"

    // Delegated rules


    protected DFA2 dfa2 = new DFA2(this);
    static final String DFA2_eotS =
        "\21\uffff";
    static final String DFA2_eofS =
        "\20\uffff\1\15";
    static final String DFA2_minS =
        "\1\5\1\uffff\1\7\10\uffff\1\21\1\4\1\uffff\1\7\1\uffff\1\5";
    static final String DFA2_maxS =
        "\1\50\1\uffff\1\7\10\uffff\1\37\1\43\1\uffff\1\7\1\uffff\1\52";
    static final String DFA2_acceptS =
        "\1\uffff\1\1\1\uffff\1\3\1\4\1\5\1\7\1\10\1\11\1\12\1\13\2\uffff"+
        "\1\2\1\uffff\1\6\1\uffff";
    static final String DFA2_specialS =
        "\21\uffff}>";
    static final String[] DFA2_transitionS = {
            "\1\4\1\uffff\1\11\22\uffff\1\6\2\uffff\1\7\2\uffff\1\5\1\3"+
            "\1\1\1\2\1\uffff\1\12\2\uffff\1\10",
            "",
            "\1\13",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\7\4\7\uffff\1\14",
            "\1\17\1\15\1\4\34\uffff\1\16",
            "",
            "\1\20",
            "",
            "\1\15\1\uffff\1\15\11\uffff\4\15\5\uffff\1\15\1\uffff\2\15"+
            "\2\uffff\4\15\1\uffff\1\15\2\uffff\1\15\2\4"
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "994:1: rule__Type__Alternatives : ( ( ( rule__Type__IntegerAssignment_0 ) ) | ( ( rule__Type__IntegervarvarAssignment_1 ) ) | ( ( rule__Type__BooleanAssignment_2 ) ) | ( ( rule__Type__BooleanvarvarAssignment_3 ) ) | ( ( rule__Type__CharacterAssignment_4 ) ) | ( ( rule__Type__CharactervarvarAssignment_5 ) ) | ( ( rule__Type__WhileAssignment_6 ) ) | ( ( rule__Type__IfAssignment_7 ) ) | ( ( rule__Type__PrintAssignment_8 ) ) | ( ( rule__Type__FunctionAssignment_9 ) ) | ( ruleImport ) );";
        }
    }
 

    public static final BitSet FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDomainmodel68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Domainmodel__ElementsAssignment_in_ruleDomainmodel94 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_ruleType_in_entryRuleType122 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleType129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__Alternatives_in_ruleType155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrintL_in_entryRulePrintL182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrintL189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrintL__Group__0_in_rulePrintL215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_While_in_entryRuleStatement_While242 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatement_While249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__0_in_ruleStatement_While275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_statment_in_entryRuleIf_statment302 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIf_statment309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__Group__0_in_ruleIf_statment335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElse_in_entryRuleElse362 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleElse369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Else__Group__0_in_ruleElse395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterDeclarationAss_in_entryRuleCharacterDeclarationAss422 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterDeclarationAss429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__Group__0_in_ruleCharacterDeclarationAss455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterDeclaration_in_entryRuleCharacterDeclaration482 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterDeclaration489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclaration__Group__0_in_ruleCharacterDeclaration515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterTypeExpression_in_entryRuleCharacterTypeExpression542 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterTypeExpression549 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterTypeExpression__CharAssignAssignment_in_ruleCharacterTypeExpression575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterAssignment_in_entryRuleCharacterAssignment602 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterAssignment609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterAssignment__ValueAssignment_in_ruleCharacterAssignment635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterVarVar_in_entryRuleCharacterVarVar662 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterVarVar669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterVarVar__Group__0_in_ruleCharacterVarVar695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanDeclarationAss_in_entryRuleBooleanDeclarationAss722 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanDeclarationAss729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__Group__0_in_ruleBooleanDeclarationAss755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanDeclaration_in_entryRuleBooleanDeclaration782 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanDeclaration789 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclaration__Group__0_in_ruleBooleanDeclaration815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanTypeExpression_in_entryRuleBooleanTypeExpression842 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanTypeExpression849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanTypeExpression__Alternatives_in_ruleBooleanTypeExpression875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanVarVar_in_entryRuleBooleanVarVar902 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanVarVar909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__Alternatives_in_ruleBooleanVarVar935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanAssignment_in_entryRuleBooleanAssignment962 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanAssignment969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanAssignment__ValueAssignment_in_ruleBooleanAssignment995 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanOperation_in_entryRuleBooleanOperation1022 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanOperation1029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanOperation__Group__0_in_ruleBooleanOperation1055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerToBool_in_entryRuleIntegerToBool1082 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerToBool1089 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerToBool__Group__0_in_ruleIntegerToBool1115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerDeclarationAss_in_entryRuleIntegerDeclarationAss1142 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerDeclarationAss1149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__Group__0_in_ruleIntegerDeclarationAss1175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerDeclaration_in_entryRuleIntegerDeclaration1202 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerDeclaration1209 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclaration__Group__0_in_ruleIntegerDeclaration1235 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerVarVar_in_entryRuleIntegerVarVar1262 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerVarVar1269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerVarVar__Group__0_in_ruleIntegerVarVar1295 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_entryRuleIntegerTypeExpression1322 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerTypeExpression1329 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerTypeExpression__Alternatives_in_ruleIntegerTypeExpression1355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerAssignment_in_entryRuleIntegerAssignment1382 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerAssignment1389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerAssignment__ValueAssignment_in_ruleIntegerAssignment1415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerOperation_in_entryRuleIntegerOperation1442 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerOperation1449 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerOperation__Group__0_in_ruleIntegerOperation1475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_entryRuleVariableName1502 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariableName1509 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VariableName__Group__0_in_ruleVariableName1535 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCallFunction_in_entryRuleCallFunction1562 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCallFunction1569 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CallFunction__Group__0_in_ruleCallFunction1595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunctionName_in_entryRuleFunctionName1622 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunctionName1629 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FunctionName__ValueAssignment_in_ruleFunctionName1655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBOOLOPERATOR_in_entryRuleBOOLOPERATOR1682 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBOOLOPERATOR1689 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BOOLOPERATOR__Alternatives_in_ruleBOOLOPERATOR1715 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunction_in_entryRuleFunction1742 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunction1749 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__0_in_ruleFunction1775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImport_in_entryRuleImport1802 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImport1809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Import__Group__0_in_ruleImport1835 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard1862 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard1869 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group__0_in_ruleQualifiedNameWithWildcard1895 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName1922 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName1929 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName1955 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OPERATOR__Alternatives_in_ruleOPERATOR1992 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SPECIALINTOPERATOR__Alternatives_in_ruleSPECIALINTOPERATOR2028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__IntegerAssignment_0_in_rule__Type__Alternatives2063 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__IntegervarvarAssignment_1_in_rule__Type__Alternatives2081 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__BooleanAssignment_2_in_rule__Type__Alternatives2099 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__BooleanvarvarAssignment_3_in_rule__Type__Alternatives2117 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__CharacterAssignment_4_in_rule__Type__Alternatives2135 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__CharactervarvarAssignment_5_in_rule__Type__Alternatives2153 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__WhileAssignment_6_in_rule__Type__Alternatives2171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__IfAssignment_7_in_rule__Type__Alternatives2189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__PrintAssignment_8_in_rule__Type__Alternatives2207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__FunctionAssignment_9_in_rule__Type__Alternatives2225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImport_in_rule__Type__Alternatives2243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CHARACTERS_in_rule__PrintL__ValueAlternatives_2_02275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INTEGERS_in_rule__PrintL__ValueAlternatives_2_02292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_BOOLVAL_in_rule__PrintL__ValueAlternatives_2_02309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__PrintL__ValueAlternatives_2_02326 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CHARACTERS_in_rule__CharacterAssignment__ValueAlternatives_02358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__CharacterAssignment__ValueAlternatives_02375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanTypeExpression__BoolAssignAssignment_0_in_rule__BooleanTypeExpression__Alternatives2407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanTypeExpression__BoolOpAssignment_1_in_rule__BooleanTypeExpression__Alternatives2425 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanTypeExpression__IntToBoolAssignment_2_in_rule__BooleanTypeExpression__Alternatives2443 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__Group_0__0_in_rule__BooleanVarVar__Alternatives2476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__Value2Assignment_1_in_rule__BooleanVarVar__Alternatives2494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_BOOLVAL_in_rule__BooleanAssignment__ValueAlternatives_02527 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__BooleanAssignment__ValueAlternatives_02544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerTypeExpression__IntegerAssignAssignment_0_in_rule__IntegerTypeExpression__Alternatives2576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerTypeExpression__IntegerOpAssignment_1_in_rule__IntegerTypeExpression__Alternatives2594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INTEGERS_in_rule__IntegerAssignment__ValueAlternatives_02627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__IntegerAssignment__ValueAlternatives_02644 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BOOLOPERATOR__ANDAssignment_0_in_rule__BOOLOPERATOR__Alternatives2676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BOOLOPERATOR__ORAssignment_1_in_rule__BOOLOPERATOR__Alternatives2694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__OPERATOR__Alternatives2728 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__OPERATOR__Alternatives2749 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__OPERATOR__Alternatives2770 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__OPERATOR__Alternatives2791 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__SPECIALINTOPERATOR__Alternatives2827 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__SPECIALINTOPERATOR__Alternatives2848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__SPECIALINTOPERATOR__Alternatives2869 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrintL__Group__0__Impl_in_rule__PrintL__Group__02902 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__PrintL__Group__1_in_rule__PrintL__Group__02905 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrintL__PrintAssignment_0_in_rule__PrintL__Group__0__Impl2932 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrintL__Group__1__Impl_in_rule__PrintL__Group__12962 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__PrintL__Group__2_in_rule__PrintL__Group__12965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__PrintL__Group__1__Impl2993 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrintL__Group__2__Impl_in_rule__PrintL__Group__23024 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__PrintL__Group__3_in_rule__PrintL__Group__23027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrintL__ValueAssignment_2_in_rule__PrintL__Group__2__Impl3054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrintL__Group__3__Impl_in_rule__PrintL__Group__33084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__PrintL__Group__3__Impl3112 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__0__Impl_in_rule__Statement_While__Group__03151 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__1_in_rule__Statement_While__Group__03154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__Statement_While__Group__0__Impl3182 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__1__Impl_in_rule__Statement_While__Group__13213 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__2_in_rule__Statement_While__Group__13216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__Statement_While__Group__1__Impl3244 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__2__Impl_in_rule__Statement_While__Group__23275 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__3_in_rule__Statement_While__Group__23278 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement_While__CondOneAssignment_2_in_rule__Statement_While__Group__2__Impl3305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__3__Impl_in_rule__Statement_While__Group__33335 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__4_in_rule__Statement_While__Group__33338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__Statement_While__Group__3__Impl3366 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__4__Impl_in_rule__Statement_While__Group__43397 = new BitSet(new long[]{0x0000012F340000F0L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__5_in_rule__Statement_While__Group__43400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__Statement_While__Group__4__Impl3428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__5__Impl_in_rule__Statement_While__Group__53459 = new BitSet(new long[]{0x0000012F340000F0L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__6_in_rule__Statement_While__Group__53462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Statement_While__OperationsAssignment_5_in_rule__Statement_While__Group__5__Impl3489 = new BitSet(new long[]{0x0000012F240000F2L});
    public static final BitSet FOLLOW_rule__Statement_While__Group__6__Impl_in_rule__Statement_While__Group__63520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__Statement_While__Group__6__Impl3548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__Group__0__Impl_in_rule__If_statment__Group__03593 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__If_statment__Group__1_in_rule__If_statment__Group__03596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__If_statment__Group__0__Impl3624 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__Group__1__Impl_in_rule__If_statment__Group__13655 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__If_statment__Group__2_in_rule__If_statment__Group__13658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__If_statment__Group__1__Impl3686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__Group__2__Impl_in_rule__If_statment__Group__23717 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__If_statment__Group__3_in_rule__If_statment__Group__23720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__CondOneAssignment_2_in_rule__If_statment__Group__2__Impl3747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__Group__3__Impl_in_rule__If_statment__Group__33777 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__If_statment__Group__4_in_rule__If_statment__Group__33780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__If_statment__Group__3__Impl3808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__Group__4__Impl_in_rule__If_statment__Group__43839 = new BitSet(new long[]{0x0000012F340000F0L});
    public static final BitSet FOLLOW_rule__If_statment__Group__5_in_rule__If_statment__Group__43842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__If_statment__Group__4__Impl3870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__Group__5__Impl_in_rule__If_statment__Group__53901 = new BitSet(new long[]{0x0000012F340000F0L});
    public static final BitSet FOLLOW_rule__If_statment__Group__6_in_rule__If_statment__Group__53904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__OperationsAssignment_5_in_rule__If_statment__Group__5__Impl3931 = new BitSet(new long[]{0x0000012F240000F2L});
    public static final BitSet FOLLOW_rule__If_statment__Group__6__Impl_in_rule__If_statment__Group__63962 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__If_statment__Group__7_in_rule__If_statment__Group__63965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__If_statment__Group__6__Impl3993 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__Group__7__Impl_in_rule__If_statment__Group__74024 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If_statment__ElseAssignment_7_in_rule__If_statment__Group__7__Impl4051 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Else__Group__0__Impl_in_rule__Else__Group__04098 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__Else__Group__1_in_rule__Else__Group__04101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Else__Group__0__Impl4129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Else__Group__1__Impl_in_rule__Else__Group__14160 = new BitSet(new long[]{0x0000012F340000F0L});
    public static final BitSet FOLLOW_rule__Else__Group__2_in_rule__Else__Group__14163 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__Else__Group__1__Impl4191 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Else__Group__2__Impl_in_rule__Else__Group__24222 = new BitSet(new long[]{0x0000012F340000F0L});
    public static final BitSet FOLLOW_rule__Else__Group__3_in_rule__Else__Group__24225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Else__OperationsAssignment_2_in_rule__Else__Group__2__Impl4252 = new BitSet(new long[]{0x0000012F240000F2L});
    public static final BitSet FOLLOW_rule__Else__Group__3__Impl_in_rule__Else__Group__34283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__Else__Group__3__Impl4311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__Group__0__Impl_in_rule__CharacterDeclarationAss__Group__04350 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__Group__1_in_rule__CharacterDeclarationAss__Group__04353 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__AssignAssignment_0_in_rule__CharacterDeclarationAss__Group__0__Impl4380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__Group__1__Impl_in_rule__CharacterDeclarationAss__Group__14410 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__Group_1__0_in_rule__CharacterDeclarationAss__Group__1__Impl4437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__Group_1__0__Impl_in_rule__CharacterDeclarationAss__Group_1__04472 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__Group_1__1_in_rule__CharacterDeclarationAss__Group_1__04475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__CharacterDeclarationAss__Group_1__0__Impl4503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__Group_1__1__Impl_in_rule__CharacterDeclarationAss__Group_1__14534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclarationAss__ValueAssignment_1_1_in_rule__CharacterDeclarationAss__Group_1__1__Impl4561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclaration__Group__0__Impl_in_rule__CharacterDeclaration__Group__04595 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__CharacterDeclaration__Group__1_in_rule__CharacterDeclaration__Group__04598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__CharacterDeclaration__Group__0__Impl4626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclaration__Group__1__Impl_in_rule__CharacterDeclaration__Group__14657 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterDeclaration__IdentifierAssignment_1_in_rule__CharacterDeclaration__Group__1__Impl4684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterVarVar__Group__0__Impl_in_rule__CharacterVarVar__Group__04718 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_rule__CharacterVarVar__Group__1_in_rule__CharacterVarVar__Group__04721 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterVarVar__IdentifierAssignment_0_in_rule__CharacterVarVar__Group__0__Impl4748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterVarVar__Group__1__Impl_in_rule__CharacterVarVar__Group__14778 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__CharacterVarVar__Group__2_in_rule__CharacterVarVar__Group__14781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__CharacterVarVar__Group__1__Impl4809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterVarVar__Group__2__Impl_in_rule__CharacterVarVar__Group__24840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterVarVar__ValueAssignment_2_in_rule__CharacterVarVar__Group__2__Impl4867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__Group__0__Impl_in_rule__BooleanDeclarationAss__Group__04903 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__Group__1_in_rule__BooleanDeclarationAss__Group__04906 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__AssignAssignment_0_in_rule__BooleanDeclarationAss__Group__0__Impl4933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__Group__1__Impl_in_rule__BooleanDeclarationAss__Group__14963 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__Group_1__0_in_rule__BooleanDeclarationAss__Group__1__Impl4990 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__Group_1__0__Impl_in_rule__BooleanDeclarationAss__Group_1__05025 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__Group_1__1_in_rule__BooleanDeclarationAss__Group_1__05028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__BooleanDeclarationAss__Group_1__0__Impl5056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__Group_1__1__Impl_in_rule__BooleanDeclarationAss__Group_1__15087 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclarationAss__ValueAssignment_1_1_in_rule__BooleanDeclarationAss__Group_1__1__Impl5114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclaration__Group__0__Impl_in_rule__BooleanDeclaration__Group__05148 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__BooleanDeclaration__Group__1_in_rule__BooleanDeclaration__Group__05151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__BooleanDeclaration__Group__0__Impl5179 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclaration__Group__1__Impl_in_rule__BooleanDeclaration__Group__15210 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanDeclaration__IdentifierAssignment_1_in_rule__BooleanDeclaration__Group__1__Impl5237 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__Group_0__0__Impl_in_rule__BooleanVarVar__Group_0__05271 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__Group_0__1_in_rule__BooleanVarVar__Group_0__05274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__IdentifierAssignment_0_0_in_rule__BooleanVarVar__Group_0__0__Impl5301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__Group_0__1__Impl_in_rule__BooleanVarVar__Group_0__15331 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__Group_0__2_in_rule__BooleanVarVar__Group_0__15334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__BooleanVarVar__Group_0__1__Impl5362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__Group_0__2__Impl_in_rule__BooleanVarVar__Group_0__25393 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanVarVar__Value1Assignment_0_2_in_rule__BooleanVarVar__Group_0__2__Impl5420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanOperation__Group__0__Impl_in_rule__BooleanOperation__Group__05456 = new BitSet(new long[]{0x0000060000000000L});
    public static final BitSet FOLLOW_rule__BooleanOperation__Group__1_in_rule__BooleanOperation__Group__05459 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanOperation__Operand1Assignment_0_in_rule__BooleanOperation__Group__0__Impl5486 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanOperation__Group__1__Impl_in_rule__BooleanOperation__Group__15516 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__BooleanOperation__Group__2_in_rule__BooleanOperation__Group__15519 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanOperation__OperatorAssignment_1_in_rule__BooleanOperation__Group__1__Impl5546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanOperation__Group__2__Impl_in_rule__BooleanOperation__Group__25576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanOperation__Operand2Assignment_2_in_rule__BooleanOperation__Group__2__Impl5603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerToBool__Group__0__Impl_in_rule__IntegerToBool__Group__05639 = new BitSet(new long[]{0x0000000000E00000L});
    public static final BitSet FOLLOW_rule__IntegerToBool__Group__1_in_rule__IntegerToBool__Group__05642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerToBool__Operand1Assignment_0_in_rule__IntegerToBool__Group__0__Impl5669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerToBool__Group__1__Impl_in_rule__IntegerToBool__Group__15699 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__IntegerToBool__Group__2_in_rule__IntegerToBool__Group__15702 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerToBool__OperatorAssignment_1_in_rule__IntegerToBool__Group__1__Impl5729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerToBool__Group__2__Impl_in_rule__IntegerToBool__Group__25759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerToBool__Operand2Assignment_2_in_rule__IntegerToBool__Group__2__Impl5786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__Group__0__Impl_in_rule__IntegerDeclarationAss__Group__05822 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__Group__1_in_rule__IntegerDeclarationAss__Group__05825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__AssignAssignment_0_in_rule__IntegerDeclarationAss__Group__0__Impl5852 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__Group__1__Impl_in_rule__IntegerDeclarationAss__Group__15882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__Group_1__0_in_rule__IntegerDeclarationAss__Group__1__Impl5909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__Group_1__0__Impl_in_rule__IntegerDeclarationAss__Group_1__05944 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__Group_1__1_in_rule__IntegerDeclarationAss__Group_1__05947 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__IntegerDeclarationAss__Group_1__0__Impl5975 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__Group_1__1__Impl_in_rule__IntegerDeclarationAss__Group_1__16006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclarationAss__ValueAssignment_1_1_in_rule__IntegerDeclarationAss__Group_1__1__Impl6033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclaration__Group__0__Impl_in_rule__IntegerDeclaration__Group__06067 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__IntegerDeclaration__Group__1_in_rule__IntegerDeclaration__Group__06070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__IntegerDeclaration__Group__0__Impl6098 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclaration__Group__1__Impl_in_rule__IntegerDeclaration__Group__16129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerDeclaration__IdentifierAssignment_1_in_rule__IntegerDeclaration__Group__1__Impl6156 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerVarVar__Group__0__Impl_in_rule__IntegerVarVar__Group__06190 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_rule__IntegerVarVar__Group__1_in_rule__IntegerVarVar__Group__06193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerVarVar__IdentifierAssignment_0_in_rule__IntegerVarVar__Group__0__Impl6220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerVarVar__Group__1__Impl_in_rule__IntegerVarVar__Group__16250 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__IntegerVarVar__Group__2_in_rule__IntegerVarVar__Group__16253 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__IntegerVarVar__Group__1__Impl6281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerVarVar__Group__2__Impl_in_rule__IntegerVarVar__Group__26312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerVarVar__ValueAssignment_2_in_rule__IntegerVarVar__Group__2__Impl6339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerOperation__Group__0__Impl_in_rule__IntegerOperation__Group__06375 = new BitSet(new long[]{0x00000000001E0000L});
    public static final BitSet FOLLOW_rule__IntegerOperation__Group__1_in_rule__IntegerOperation__Group__06378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerOperation__Operand1Assignment_0_in_rule__IntegerOperation__Group__0__Impl6405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerOperation__Group__1__Impl_in_rule__IntegerOperation__Group__16435 = new BitSet(new long[]{0x0000000800000070L});
    public static final BitSet FOLLOW_rule__IntegerOperation__Group__2_in_rule__IntegerOperation__Group__16438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerOperation__OperatorAssignment_1_in_rule__IntegerOperation__Group__1__Impl6465 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerOperation__Group__2__Impl_in_rule__IntegerOperation__Group__26495 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerOperation__Operand2Assignment_2_in_rule__IntegerOperation__Group__2__Impl6522 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VariableName__Group__0__Impl_in_rule__VariableName__Group__06558 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_rule__VariableName__Group__1_in_rule__VariableName__Group__06561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rule__VariableName__Group__0__Impl6589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VariableName__Group__1__Impl_in_rule__VariableName__Group__16620 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__VariableName__Group__1__Impl6647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CallFunction__Group__0__Impl_in_rule__CallFunction__Group__06680 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__CallFunction__Group__1_in_rule__CallFunction__Group__06683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CallFunction__OperationNameAssignment_0_in_rule__CallFunction__Group__0__Impl6710 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CallFunction__Group__1__Impl_in_rule__CallFunction__Group__16740 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__CallFunction__Group__2_in_rule__CallFunction__Group__16743 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__CallFunction__Group__1__Impl6771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CallFunction__Group__2__Impl_in_rule__CallFunction__Group__26802 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__CallFunction__Group__2__Impl6830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__0__Impl_in_rule__Function__Group__06867 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_rule__Function__Group__1_in_rule__Function__Group__06870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__Function__Group__0__Impl6898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__1__Impl_in_rule__Function__Group__16929 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__Function__Group__2_in_rule__Function__Group__16932 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__NameAssignment_1_in_rule__Function__Group__1__Impl6959 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__2__Impl_in_rule__Function__Group__26989 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__Function__Group__3_in_rule__Function__Group__26992 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__Function__Group__2__Impl7020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__3__Impl_in_rule__Function__Group__37051 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__Function__Group__4_in_rule__Function__Group__37054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__Function__Group__3__Impl7082 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__4__Impl_in_rule__Function__Group__47113 = new BitSet(new long[]{0x0000012F340000F0L});
    public static final BitSet FOLLOW_rule__Function__Group__5_in_rule__Function__Group__47116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__Function__Group__4__Impl7144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__5__Impl_in_rule__Function__Group__57175 = new BitSet(new long[]{0x0000012F340000F0L});
    public static final BitSet FOLLOW_rule__Function__Group__6_in_rule__Function__Group__57178 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__FeaturesAssignment_5_in_rule__Function__Group__5__Impl7205 = new BitSet(new long[]{0x0000012F240000F2L});
    public static final BitSet FOLLOW_rule__Function__Group__6__Impl_in_rule__Function__Group__67236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__Function__Group__6__Impl7264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Import__Group__0__Impl_in_rule__Import__Group__07309 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_rule__Import__Group__1_in_rule__Import__Group__07312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__Import__Group__0__Impl7340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Import__Group__1__Impl_in_rule__Import__Group__17371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Import__ImportedNamespaceAssignment_1_in_rule__Import__Group__1__Impl7398 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group__0__Impl_in_rule__QualifiedNameWithWildcard__Group__07432 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group__1_in_rule__QualifiedNameWithWildcard__Group__07435 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_rule__QualifiedNameWithWildcard__Group__0__Impl7462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedNameWithWildcard__Group__1__Impl_in_rule__QualifiedNameWithWildcard__Group__17491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__QualifiedNameWithWildcard__Group__1__Impl7520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__07557 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__07560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__QualifiedName__Group__0__Impl7587 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__17616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__0_in_rule__QualifiedName__Group__1__Impl7643 = new BitSet(new long[]{0x0000008000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__0__Impl_in_rule__QualifiedName__Group_1__07678 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__1_in_rule__QualifiedName__Group_1__07681 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_rule__QualifiedName__Group_1__0__Impl7709 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_1__1__Impl_in_rule__QualifiedName__Group_1__17740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__QualifiedName__Group_1__1__Impl7767 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunction_in_rule__Domainmodel__ElementsAssignment7805 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerDeclarationAss_in_rule__Type__IntegerAssignment_07836 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerVarVar_in_rule__Type__IntegervarvarAssignment_17867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanDeclarationAss_in_rule__Type__BooleanAssignment_27898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanVarVar_in_rule__Type__BooleanvarvarAssignment_37929 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterDeclarationAss_in_rule__Type__CharacterAssignment_47960 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterVarVar_in_rule__Type__CharactervarvarAssignment_57991 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_While_in_rule__Type__WhileAssignment_68022 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_statment_in_rule__Type__IfAssignment_78053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrintL_in_rule__Type__PrintAssignment_88084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCallFunction_in_rule__Type__FunctionAssignment_98115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_rule__PrintL__PrintAssignment_08151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrintL__ValueAlternatives_2_0_in_rule__PrintL__ValueAssignment_28190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanTypeExpression_in_rule__Statement_While__CondOneAssignment_28223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__Statement_While__OperationsAssignment_58254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanTypeExpression_in_rule__If_statment__CondOneAssignment_28285 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__If_statment__OperationsAssignment_58316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElse_in_rule__If_statment__ElseAssignment_78347 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__Else__OperationsAssignment_28378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterDeclaration_in_rule__CharacterDeclarationAss__AssignAssignment_08409 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterTypeExpression_in_rule__CharacterDeclarationAss__ValueAssignment_1_18440 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__CharacterDeclaration__IdentifierAssignment_18471 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterAssignment_in_rule__CharacterTypeExpression__CharAssignAssignment8502 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CharacterAssignment__ValueAlternatives_0_in_rule__CharacterAssignment__ValueAssignment8533 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__CharacterVarVar__IdentifierAssignment_08566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CHARACTERS_in_rule__CharacterVarVar__ValueAssignment_28597 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanDeclaration_in_rule__BooleanDeclarationAss__AssignAssignment_08628 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanTypeExpression_in_rule__BooleanDeclarationAss__ValueAssignment_1_18659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__BooleanDeclaration__IdentifierAssignment_18690 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanAssignment_in_rule__BooleanTypeExpression__BoolAssignAssignment_08721 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanOperation_in_rule__BooleanTypeExpression__BoolOpAssignment_18752 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerToBool_in_rule__BooleanTypeExpression__IntToBoolAssignment_28783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__BooleanVarVar__IdentifierAssignment_0_08814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanOperation_in_rule__BooleanVarVar__Value1Assignment_0_28845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerToBool_in_rule__BooleanVarVar__Value2Assignment_18876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BooleanAssignment__ValueAlternatives_0_in_rule__BooleanAssignment__ValueAssignment8907 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanAssignment_in_rule__BooleanOperation__Operand1Assignment_08940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBOOLOPERATOR_in_rule__BooleanOperation__OperatorAssignment_18971 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanAssignment_in_rule__BooleanOperation__Operand2Assignment_29002 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_rule__IntegerToBool__Operand1Assignment_09033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSPECIALINTOPERATOR_in_rule__IntegerToBool__OperatorAssignment_19064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_rule__IntegerToBool__Operand2Assignment_29095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerDeclaration_in_rule__IntegerDeclarationAss__AssignAssignment_09126 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_rule__IntegerDeclarationAss__ValueAssignment_1_19157 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__IntegerDeclaration__IdentifierAssignment_19188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_rule__IntegerVarVar__IdentifierAssignment_09219 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_rule__IntegerVarVar__ValueAssignment_29250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerAssignment_in_rule__IntegerTypeExpression__IntegerAssignAssignment_09281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerOperation_in_rule__IntegerTypeExpression__IntegerOpAssignment_19312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntegerAssignment__ValueAlternatives_0_in_rule__IntegerAssignment__ValueAssignment9343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerAssignment_in_rule__IntegerOperation__Operand1Assignment_09376 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOPERATOR_in_rule__IntegerOperation__OperatorAssignment_19407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerAssignment_in_rule__IntegerOperation__Operand2Assignment_29438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunctionName_in_rule__CallFunction__OperationNameAssignment_09469 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__FunctionName__ValueAssignment9500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_rule__BOOLOPERATOR__ANDAssignment_09536 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_rule__BOOLOPERATOR__ORAssignment_19580 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Function__NameAssignment_19619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__Function__FeaturesAssignment_59650 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_rule__Import__ImportedNamespaceAssignment_19681 = new BitSet(new long[]{0x0000000000000002L});

}