package org.xtext.Ceur.Ceur.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCeurLexer extends Lexer {
    public static final int RULE_STRING=12;
    public static final int RULE_SL_COMMENT=14;
    public static final int T__19=19;
    public static final int T__37=37;
    public static final int RULE_NUMBERS=10;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int RULE_BOOLVAL=6;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=7;
    public static final int RULE_WS=15;
    public static final int RULE_ZERO=9;
    public static final int RULE_DIGIT=8;
    public static final int RULE_ANY_OTHER=16;
    public static final int RULE_INTEGERS=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=11;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=13;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int RULE_CHARACTERS=4;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalCeurLexer() {;} 
    public InternalCeurLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalCeurLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g"; }

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:11:7: ( '+' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:11:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:12:7: ( '-' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:12:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:13:7: ( '*' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:13:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:14:7: ( '/' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:14:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:15:7: ( '==' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:15:9: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:16:7: ( '<' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:16:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:17:7: ( '>' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:17:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:18:7: ( '(' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:18:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:19:7: ( ')' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:19:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:20:7: ( 'kol' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:20:9: 'kol'
            {
            match("kol"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:21:7: ( '{' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:21:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:22:7: ( '}' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:22:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:23:7: ( 'jei' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:23:9: 'jei'
            {
            match("jei"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:24:7: ( 'kitaip' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:24:9: 'kitaip'
            {
            match("kitaip"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:25:7: ( '=' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:25:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:26:7: ( 'char' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:26:9: 'char'
            {
            match("char"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:27:7: ( 'bool' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:27:9: 'bool'
            {
            match("bool"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:28:7: ( 'int' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:28:9: 'int'
            {
            match("int"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:29:7: ( '\\u20AC' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:29:9: '\\u20AC'
            {
            match('\u20AC'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:30:7: ( 'void' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:30:9: 'void'
            {
            match("void"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:31:7: ( 'import' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:31:9: 'import'
            {
            match("import"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:32:7: ( '.*' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:32:9: '.*'
            {
            match(".*"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:33:7: ( '.' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:33:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:34:7: ( 'spausdinti' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:34:9: 'spausdinti'
            {
            match("spausdinti"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:35:7: ( 'ir' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:35:9: 'ir'
            {
            match("ir"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:36:7: ( 'arba' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:36:9: 'arba'
            {
            match("arba"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "RULE_BOOLVAL"
    public final void mRULE_BOOLVAL() throws RecognitionException {
        try {
            int _type = RULE_BOOLVAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4758:14: ( ( 'tiesa' | 'melas' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4758:16: ( 'tiesa' | 'melas' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4758:16: ( 'tiesa' | 'melas' )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='t') ) {
                alt1=1;
            }
            else if ( (LA1_0=='m') ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4758:17: 'tiesa'
                    {
                    match("tiesa"); 


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4758:25: 'melas'
                    {
                    match("melas"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_BOOLVAL"

    // $ANTLR start "RULE_CHARACTERS"
    public final void mRULE_CHARACTERS() throws RecognitionException {
        try {
            int _type = RULE_CHARACTERS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4760:17: ( '\\'' 'a' .. 'z' '\\'' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4760:19: '\\'' 'a' .. 'z' '\\''
            {
            match('\''); 
            matchRange('a','z'); 
            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CHARACTERS"

    // $ANTLR start "RULE_INTEGERS"
    public final void mRULE_INTEGERS() throws RecognitionException {
        try {
            int _type = RULE_INTEGERS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4762:15: ( ( ( '-' )? RULE_DIGIT ( RULE_DIGIT | RULE_ZERO )* | RULE_ZERO ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4762:17: ( ( '-' )? RULE_DIGIT ( RULE_DIGIT | RULE_ZERO )* | RULE_ZERO )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4762:17: ( ( '-' )? RULE_DIGIT ( RULE_DIGIT | RULE_ZERO )* | RULE_ZERO )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='-'||(LA4_0>='1' && LA4_0<='9')) ) {
                alt4=1;
            }
            else if ( (LA4_0=='0') ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4762:18: ( '-' )? RULE_DIGIT ( RULE_DIGIT | RULE_ZERO )*
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4762:18: ( '-' )?
                    int alt2=2;
                    int LA2_0 = input.LA(1);

                    if ( (LA2_0=='-') ) {
                        alt2=1;
                    }
                    switch (alt2) {
                        case 1 :
                            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4762:18: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }

                    mRULE_DIGIT(); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4762:34: ( RULE_DIGIT | RULE_ZERO )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:
                    	    {
                    	    if ( (input.LA(1)>='0' && input.LA(1)<='9') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4762:58: RULE_ZERO
                    {
                    mRULE_ZERO(); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INTEGERS"

    // $ANTLR start "RULE_DIGIT"
    public final void mRULE_DIGIT() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4764:21: ( RULE_NUMBERS )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4764:23: RULE_NUMBERS
            {
            mRULE_NUMBERS(); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIGIT"

    // $ANTLR start "RULE_ZERO"
    public final void mRULE_ZERO() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4766:20: ( '0' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4766:22: '0'
            {
            match('0'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_ZERO"

    // $ANTLR start "RULE_NUMBERS"
    public final void mRULE_NUMBERS() throws RecognitionException {
        try {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4768:23: ( '1' .. '9' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4768:25: '1' .. '9'
            {
            matchRange('1','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_NUMBERS"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4770:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4770:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4770:11: ( '^' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='^') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4770:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4770:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')||(LA6_0>='A' && LA6_0<='Z')||LA6_0=='_'||(LA6_0>='a' && LA6_0<='z')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4772:10: ( ( '0' .. '9' )+ )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4772:12: ( '0' .. '9' )+
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4772:12: ( '0' .. '9' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='0' && LA7_0<='9')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4772:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\"') ) {
                alt10=1;
            }
            else if ( (LA10_0=='\'') ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop8:
                    do {
                        int alt8=3;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0=='\\') ) {
                            alt8=1;
                        }
                        else if ( ((LA8_0>='\u0000' && LA8_0<='!')||(LA8_0>='#' && LA8_0<='[')||(LA8_0>=']' && LA8_0<='\uFFFF')) ) {
                            alt8=2;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop9:
                    do {
                        int alt9=3;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0=='\\') ) {
                            alt9=1;
                        }
                        else if ( ((LA9_0>='\u0000' && LA9_0<='&')||(LA9_0>='(' && LA9_0<='[')||(LA9_0>=']' && LA9_0<='\uFFFF')) ) {
                            alt9=2;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4774:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4776:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4776:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4776:24: ( options {greedy=false; } : . )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0=='*') ) {
                    int LA11_1 = input.LA(2);

                    if ( (LA11_1=='/') ) {
                        alt11=2;
                    }
                    else if ( ((LA11_1>='\u0000' && LA11_1<='.')||(LA11_1>='0' && LA11_1<='\uFFFF')) ) {
                        alt11=1;
                    }


                }
                else if ( ((LA11_0>='\u0000' && LA11_0<=')')||(LA11_0>='+' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4776:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4778:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4778:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4778:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='\u0000' && LA12_0<='\t')||(LA12_0>='\u000B' && LA12_0<='\f')||(LA12_0>='\u000E' && LA12_0<='\uFFFF')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4778:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4778:40: ( ( '\\r' )? '\\n' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='\n'||LA14_0=='\r') ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4778:41: ( '\\r' )? '\\n'
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4778:41: ( '\\r' )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0=='\r') ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4778:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4780:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4780:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4780:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='\t' && LA15_0<='\n')||LA15_0=='\r'||LA15_0==' ') ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4782:16: ( . )
            // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:4782:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:8: ( T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | RULE_BOOLVAL | RULE_CHARACTERS | RULE_INTEGERS | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt16=36;
        alt16 = dfa16.predict(input);
        switch (alt16) {
            case 1 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:10: T__17
                {
                mT__17(); 

                }
                break;
            case 2 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:16: T__18
                {
                mT__18(); 

                }
                break;
            case 3 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:22: T__19
                {
                mT__19(); 

                }
                break;
            case 4 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:28: T__20
                {
                mT__20(); 

                }
                break;
            case 5 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:34: T__21
                {
                mT__21(); 

                }
                break;
            case 6 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:40: T__22
                {
                mT__22(); 

                }
                break;
            case 7 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:46: T__23
                {
                mT__23(); 

                }
                break;
            case 8 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:52: T__24
                {
                mT__24(); 

                }
                break;
            case 9 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:58: T__25
                {
                mT__25(); 

                }
                break;
            case 10 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:64: T__26
                {
                mT__26(); 

                }
                break;
            case 11 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:70: T__27
                {
                mT__27(); 

                }
                break;
            case 12 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:76: T__28
                {
                mT__28(); 

                }
                break;
            case 13 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:82: T__29
                {
                mT__29(); 

                }
                break;
            case 14 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:88: T__30
                {
                mT__30(); 

                }
                break;
            case 15 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:94: T__31
                {
                mT__31(); 

                }
                break;
            case 16 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:100: T__32
                {
                mT__32(); 

                }
                break;
            case 17 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:106: T__33
                {
                mT__33(); 

                }
                break;
            case 18 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:112: T__34
                {
                mT__34(); 

                }
                break;
            case 19 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:118: T__35
                {
                mT__35(); 

                }
                break;
            case 20 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:124: T__36
                {
                mT__36(); 

                }
                break;
            case 21 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:130: T__37
                {
                mT__37(); 

                }
                break;
            case 22 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:136: T__38
                {
                mT__38(); 

                }
                break;
            case 23 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:142: T__39
                {
                mT__39(); 

                }
                break;
            case 24 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:148: T__40
                {
                mT__40(); 

                }
                break;
            case 25 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:154: T__41
                {
                mT__41(); 

                }
                break;
            case 26 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:160: T__42
                {
                mT__42(); 

                }
                break;
            case 27 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:166: RULE_BOOLVAL
                {
                mRULE_BOOLVAL(); 

                }
                break;
            case 28 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:179: RULE_CHARACTERS
                {
                mRULE_CHARACTERS(); 

                }
                break;
            case 29 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:195: RULE_INTEGERS
                {
                mRULE_INTEGERS(); 

                }
                break;
            case 30 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:209: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 31 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:217: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 32 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:226: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 33 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:238: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 34 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:254: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 35 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:270: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 36 :
                // ../org.xtext.Ceur.Ceur.Ceur.ui/src-gen/org/xtext/Ceur/Ceur/ui/contentassist/antlr/internal/InternalCeur.g:1:278: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA16 dfa16 = new DFA16(this);
    static final String DFA16_eotS =
        "\2\uffff\1\41\1\uffff\1\46\1\50\4\uffff\1\57\2\uffff\4\57\1\uffff"+
        "\1\57\1\73\4\57\1\37\2\42\1\37\1\uffff\1\37\17\uffff\2\57\3\uffff"+
        "\5\57\1\114\1\uffff\1\57\2\uffff\4\57\2\uffff\1\42\2\uffff\1\123"+
        "\1\57\1\125\2\57\1\130\1\57\1\uffff\5\57\2\uffff\1\57\1\uffff\1"+
        "\141\1\142\1\uffff\1\57\1\144\1\57\1\146\2\57\1\uffff\1\57\2\uffff"+
        "\1\57\1\uffff\1\57\1\uffff\2\154\1\155\1\156\1\57\3\uffff\3\57\1"+
        "\163\1\uffff";
    static final String DFA16_eofS =
        "\164\uffff";
    static final String DFA16_minS =
        "\1\0\1\uffff\1\61\1\uffff\1\52\1\75\4\uffff\1\151\2\uffff\1\145"+
        "\1\150\1\157\1\155\1\uffff\1\157\1\52\1\160\1\162\1\151\1\145\1"+
        "\0\2\60\1\101\1\uffff\1\0\17\uffff\1\154\1\164\3\uffff\1\151\1\141"+
        "\1\157\1\164\1\160\1\60\1\uffff\1\151\2\uffff\1\141\1\142\1\145"+
        "\1\154\1\0\1\uffff\1\60\2\uffff\1\60\1\141\1\60\1\162\1\154\1\60"+
        "\1\157\1\uffff\1\144\1\165\1\141\1\163\1\141\2\uffff\1\151\1\uffff"+
        "\2\60\1\uffff\1\162\1\60\1\163\1\60\1\141\1\163\1\uffff\1\160\2"+
        "\uffff\1\164\1\uffff\1\144\1\uffff\4\60\1\151\3\uffff\1\156\1\164"+
        "\1\151\1\60\1\uffff";
    static final String DFA16_maxS =
        "\1\uffff\1\uffff\1\71\1\uffff\1\57\1\75\4\uffff\1\157\2\uffff\1"+
        "\145\1\150\1\157\1\162\1\uffff\1\157\1\52\1\160\1\162\1\151\1\145"+
        "\1\uffff\2\71\1\172\1\uffff\1\uffff\17\uffff\1\154\1\164\3\uffff"+
        "\1\151\1\141\1\157\1\164\1\160\1\172\1\uffff\1\151\2\uffff\1\141"+
        "\1\142\1\145\1\154\1\uffff\1\uffff\1\71\2\uffff\1\172\1\141\1\172"+
        "\1\162\1\154\1\172\1\157\1\uffff\1\144\1\165\1\141\1\163\1\141\2"+
        "\uffff\1\151\1\uffff\2\172\1\uffff\1\162\1\172\1\163\1\172\1\141"+
        "\1\163\1\uffff\1\160\2\uffff\1\164\1\uffff\1\144\1\uffff\4\172\1"+
        "\151\3\uffff\1\156\1\164\1\151\1\172\1\uffff";
    static final String DFA16_acceptS =
        "\1\uffff\1\1\1\uffff\1\3\2\uffff\1\6\1\7\1\10\1\11\1\uffff\1\13"+
        "\1\14\4\uffff\1\23\12\uffff\1\36\1\uffff\1\43\1\44\1\1\1\2\1\35"+
        "\1\3\1\41\1\42\1\4\1\5\1\17\1\6\1\7\1\10\1\11\2\uffff\1\36\1\13"+
        "\1\14\6\uffff\1\23\1\uffff\1\26\1\27\5\uffff\1\40\1\uffff\1\37\1"+
        "\43\7\uffff\1\31\5\uffff\1\34\1\12\1\uffff\1\15\2\uffff\1\22\6\uffff"+
        "\1\34\1\uffff\1\20\1\21\1\uffff\1\24\1\uffff\1\32\5\uffff\1\33\1"+
        "\16\1\25\4\uffff\1\30";
    static final String DFA16_specialS =
        "\1\1\27\uffff\1\2\4\uffff\1\3\42\uffff\1\0\63\uffff}>";
    static final String[] DFA16_transitionS = {
            "\11\37\2\36\2\37\1\36\22\37\1\36\1\37\1\35\4\37\1\30\1\10\1"+
            "\11\1\3\1\1\1\37\1\2\1\23\1\4\1\32\11\31\2\37\1\6\1\5\1\7\2"+
            "\37\32\34\3\37\1\33\1\34\1\37\1\25\1\17\1\16\5\34\1\20\1\15"+
            "\1\12\1\34\1\27\5\34\1\24\1\26\1\34\1\22\4\34\1\13\1\37\1\14"+
            "\u202e\37\1\21\udf53\37",
            "",
            "\11\42",
            "",
            "\1\44\4\uffff\1\45",
            "\1\47",
            "",
            "",
            "",
            "",
            "\1\56\5\uffff\1\55",
            "",
            "",
            "\1\62",
            "\1\63",
            "\1\64",
            "\1\66\1\65\3\uffff\1\67",
            "",
            "\1\71",
            "\1\72",
            "\1\74",
            "\1\75",
            "\1\76",
            "\1\77",
            "\141\101\32\100\uff85\101",
            "\12\102",
            "\12\103",
            "\32\57\4\uffff\1\57\1\uffff\32\57",
            "",
            "\0\101",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\105",
            "\1\106",
            "",
            "",
            "",
            "\1\107",
            "\1\110",
            "\1\111",
            "\1\112",
            "\1\113",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "",
            "\1\115",
            "",
            "",
            "\1\116",
            "\1\117",
            "\1\120",
            "\1\121",
            "\47\101\1\122\uffd8\101",
            "",
            "\12\102",
            "",
            "",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\124",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\126",
            "\1\127",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\131",
            "",
            "\1\132",
            "\1\133",
            "\1\134",
            "\1\135",
            "\1\136",
            "",
            "",
            "\1\140",
            "",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "",
            "\1\143",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\145",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\147",
            "\1\150",
            "",
            "\1\151",
            "",
            "",
            "\1\152",
            "",
            "\1\153",
            "",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            "\1\157",
            "",
            "",
            "",
            "\1\160",
            "\1\161",
            "\1\162",
            "\12\57\7\uffff\32\57\4\uffff\1\57\1\uffff\32\57",
            ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | RULE_BOOLVAL | RULE_CHARACTERS | RULE_INTEGERS | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA16_64 = input.LA(1);

                        s = -1;
                        if ( (LA16_64=='\'') ) {s = 82;}

                        else if ( ((LA16_64>='\u0000' && LA16_64<='&')||(LA16_64>='(' && LA16_64<='\uFFFF')) ) {s = 65;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA16_0 = input.LA(1);

                        s = -1;
                        if ( (LA16_0=='+') ) {s = 1;}

                        else if ( (LA16_0=='-') ) {s = 2;}

                        else if ( (LA16_0=='*') ) {s = 3;}

                        else if ( (LA16_0=='/') ) {s = 4;}

                        else if ( (LA16_0=='=') ) {s = 5;}

                        else if ( (LA16_0=='<') ) {s = 6;}

                        else if ( (LA16_0=='>') ) {s = 7;}

                        else if ( (LA16_0=='(') ) {s = 8;}

                        else if ( (LA16_0==')') ) {s = 9;}

                        else if ( (LA16_0=='k') ) {s = 10;}

                        else if ( (LA16_0=='{') ) {s = 11;}

                        else if ( (LA16_0=='}') ) {s = 12;}

                        else if ( (LA16_0=='j') ) {s = 13;}

                        else if ( (LA16_0=='c') ) {s = 14;}

                        else if ( (LA16_0=='b') ) {s = 15;}

                        else if ( (LA16_0=='i') ) {s = 16;}

                        else if ( (LA16_0=='\u20AC') ) {s = 17;}

                        else if ( (LA16_0=='v') ) {s = 18;}

                        else if ( (LA16_0=='.') ) {s = 19;}

                        else if ( (LA16_0=='s') ) {s = 20;}

                        else if ( (LA16_0=='a') ) {s = 21;}

                        else if ( (LA16_0=='t') ) {s = 22;}

                        else if ( (LA16_0=='m') ) {s = 23;}

                        else if ( (LA16_0=='\'') ) {s = 24;}

                        else if ( ((LA16_0>='1' && LA16_0<='9')) ) {s = 25;}

                        else if ( (LA16_0=='0') ) {s = 26;}

                        else if ( (LA16_0=='^') ) {s = 27;}

                        else if ( ((LA16_0>='A' && LA16_0<='Z')||LA16_0=='_'||(LA16_0>='d' && LA16_0<='h')||LA16_0=='l'||(LA16_0>='n' && LA16_0<='r')||LA16_0=='u'||(LA16_0>='w' && LA16_0<='z')) ) {s = 28;}

                        else if ( (LA16_0=='\"') ) {s = 29;}

                        else if ( ((LA16_0>='\t' && LA16_0<='\n')||LA16_0=='\r'||LA16_0==' ') ) {s = 30;}

                        else if ( ((LA16_0>='\u0000' && LA16_0<='\b')||(LA16_0>='\u000B' && LA16_0<='\f')||(LA16_0>='\u000E' && LA16_0<='\u001F')||LA16_0=='!'||(LA16_0>='#' && LA16_0<='&')||LA16_0==','||(LA16_0>=':' && LA16_0<=';')||(LA16_0>='?' && LA16_0<='@')||(LA16_0>='[' && LA16_0<=']')||LA16_0=='`'||LA16_0=='|'||(LA16_0>='~' && LA16_0<='\u20AB')||(LA16_0>='\u20AD' && LA16_0<='\uFFFF')) ) {s = 31;}

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA16_24 = input.LA(1);

                        s = -1;
                        if ( ((LA16_24>='a' && LA16_24<='z')) ) {s = 64;}

                        else if ( ((LA16_24>='\u0000' && LA16_24<='`')||(LA16_24>='{' && LA16_24<='\uFFFF')) ) {s = 65;}

                        else s = 31;

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA16_29 = input.LA(1);

                        s = -1;
                        if ( ((LA16_29>='\u0000' && LA16_29<='\uFFFF')) ) {s = 65;}

                        else s = 31;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 16, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}