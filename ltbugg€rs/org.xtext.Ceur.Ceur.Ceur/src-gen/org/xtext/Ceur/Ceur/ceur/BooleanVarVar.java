/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Var Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getValue1 <em>Value1</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getValue2 <em>Value2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanVarVar()
 * @model
 * @generated
 */
public interface BooleanVarVar extends EObject
{
  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see #setIdentifier(String)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanVarVar_Identifier()
   * @model
   * @generated
   */
  String getIdentifier();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getIdentifier <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Identifier</em>' attribute.
   * @see #getIdentifier()
   * @generated
   */
  void setIdentifier(String value);

  /**
   * Returns the value of the '<em><b>Value1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value1</em>' containment reference.
   * @see #setValue1(BooleanOperation)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanVarVar_Value1()
   * @model containment="true"
   * @generated
   */
  BooleanOperation getValue1();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getValue1 <em>Value1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value1</em>' containment reference.
   * @see #getValue1()
   * @generated
   */
  void setValue1(BooleanOperation value);

  /**
   * Returns the value of the '<em><b>Value2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value2</em>' containment reference.
   * @see #setValue2(IntegerToBool)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanVarVar_Value2()
   * @model containment="true"
   * @generated
   */
  IntegerToBool getValue2();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getValue2 <em>Value2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value2</em>' containment reference.
   * @see #getValue2()
   * @generated
   */
  void setValue2(IntegerToBool value);

} // BooleanVarVar
