/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Else</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Else#getOperations <em>Operations</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getElse()
 * @model
 * @generated
 */
public interface Else extends EObject
{
  /**
   * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.Type}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operations</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getElse_Operations()
   * @model containment="true"
   * @generated
   */
  EList<Type> getOperations();

} // Else
