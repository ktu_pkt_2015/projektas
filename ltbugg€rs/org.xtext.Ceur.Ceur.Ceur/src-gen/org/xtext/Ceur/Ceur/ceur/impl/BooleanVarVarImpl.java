/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.Ceur.Ceur.ceur.BooleanOperation;
import org.xtext.Ceur.Ceur.ceur.BooleanVarVar;
import org.xtext.Ceur.Ceur.ceur.CeurPackage;
import org.xtext.Ceur.Ceur.ceur.IntegerToBool;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Var Var</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanVarVarImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanVarVarImpl#getValue1 <em>Value1</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanVarVarImpl#getValue2 <em>Value2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BooleanVarVarImpl extends MinimalEObjectImpl.Container implements BooleanVarVar
{
  /**
   * The default value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected static final String IDENTIFIER_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected String identifier = IDENTIFIER_EDEFAULT;

  /**
   * The cached value of the '{@link #getValue1() <em>Value1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue1()
   * @generated
   * @ordered
   */
  protected BooleanOperation value1;

  /**
   * The cached value of the '{@link #getValue2() <em>Value2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue2()
   * @generated
   * @ordered
   */
  protected IntegerToBool value2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BooleanVarVarImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CeurPackage.Literals.BOOLEAN_VAR_VAR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIdentifier()
  {
    return identifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIdentifier(String newIdentifier)
  {
    String oldIdentifier = identifier;
    identifier = newIdentifier;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_VAR_VAR__IDENTIFIER, oldIdentifier, identifier));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanOperation getValue1()
  {
    return value1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetValue1(BooleanOperation newValue1, NotificationChain msgs)
  {
    BooleanOperation oldValue1 = value1;
    value1 = newValue1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_VAR_VAR__VALUE1, oldValue1, newValue1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue1(BooleanOperation newValue1)
  {
    if (newValue1 != value1)
    {
      NotificationChain msgs = null;
      if (value1 != null)
        msgs = ((InternalEObject)value1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_VAR_VAR__VALUE1, null, msgs);
      if (newValue1 != null)
        msgs = ((InternalEObject)newValue1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_VAR_VAR__VALUE1, null, msgs);
      msgs = basicSetValue1(newValue1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_VAR_VAR__VALUE1, newValue1, newValue1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerToBool getValue2()
  {
    return value2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetValue2(IntegerToBool newValue2, NotificationChain msgs)
  {
    IntegerToBool oldValue2 = value2;
    value2 = newValue2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_VAR_VAR__VALUE2, oldValue2, newValue2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue2(IntegerToBool newValue2)
  {
    if (newValue2 != value2)
    {
      NotificationChain msgs = null;
      if (value2 != null)
        msgs = ((InternalEObject)value2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_VAR_VAR__VALUE2, null, msgs);
      if (newValue2 != null)
        msgs = ((InternalEObject)newValue2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_VAR_VAR__VALUE2, null, msgs);
      msgs = basicSetValue2(newValue2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_VAR_VAR__VALUE2, newValue2, newValue2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE1:
        return basicSetValue1(null, msgs);
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE2:
        return basicSetValue2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_VAR_VAR__IDENTIFIER:
        return getIdentifier();
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE1:
        return getValue1();
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE2:
        return getValue2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_VAR_VAR__IDENTIFIER:
        setIdentifier((String)newValue);
        return;
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE1:
        setValue1((BooleanOperation)newValue);
        return;
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE2:
        setValue2((IntegerToBool)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_VAR_VAR__IDENTIFIER:
        setIdentifier(IDENTIFIER_EDEFAULT);
        return;
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE1:
        setValue1((BooleanOperation)null);
        return;
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE2:
        setValue2((IntegerToBool)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_VAR_VAR__IDENTIFIER:
        return IDENTIFIER_EDEFAULT == null ? identifier != null : !IDENTIFIER_EDEFAULT.equals(identifier);
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE1:
        return value1 != null;
      case CeurPackage.BOOLEAN_VAR_VAR__VALUE2:
        return value2 != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (identifier: ");
    result.append(identifier);
    result.append(')');
    return result.toString();
  }

} //BooleanVarVarImpl
