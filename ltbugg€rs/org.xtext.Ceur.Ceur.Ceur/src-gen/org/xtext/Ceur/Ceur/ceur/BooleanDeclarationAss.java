/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Declaration Ass</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss#getAssign <em>Assign</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanDeclarationAss()
 * @model
 * @generated
 */
public interface BooleanDeclarationAss extends EObject
{
  /**
   * Returns the value of the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Assign</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Assign</em>' containment reference.
   * @see #setAssign(BooleanDeclaration)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanDeclarationAss_Assign()
   * @model containment="true"
   * @generated
   */
  BooleanDeclaration getAssign();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss#getAssign <em>Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Assign</em>' containment reference.
   * @see #getAssign()
   * @generated
   */
  void setAssign(BooleanDeclaration value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(BooleanTypeExpression)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanDeclarationAss_Value()
   * @model containment="true"
   * @generated
   */
  BooleanTypeExpression getValue();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(BooleanTypeExpression value);

} // BooleanDeclarationAss
