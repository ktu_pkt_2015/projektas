/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression;
import org.xtext.Ceur.Ceur.ceur.CeurPackage;
import org.xtext.Ceur.Ceur.ceur.Statement_While;
import org.xtext.Ceur.Ceur.ceur.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Statement While</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.Statement_WhileImpl#getCondOne <em>Cond One</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.Statement_WhileImpl#getOperations <em>Operations</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Statement_WhileImpl extends MinimalEObjectImpl.Container implements Statement_While
{
  /**
   * The cached value of the '{@link #getCondOne() <em>Cond One</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCondOne()
   * @generated
   * @ordered
   */
  protected BooleanTypeExpression condOne;

  /**
   * The cached value of the '{@link #getOperations() <em>Operations</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperations()
   * @generated
   * @ordered
   */
  protected EList<Type> operations;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Statement_WhileImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CeurPackage.Literals.STATEMENT_WHILE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanTypeExpression getCondOne()
  {
    return condOne;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCondOne(BooleanTypeExpression newCondOne, NotificationChain msgs)
  {
    BooleanTypeExpression oldCondOne = condOne;
    condOne = newCondOne;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.STATEMENT_WHILE__COND_ONE, oldCondOne, newCondOne);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCondOne(BooleanTypeExpression newCondOne)
  {
    if (newCondOne != condOne)
    {
      NotificationChain msgs = null;
      if (condOne != null)
        msgs = ((InternalEObject)condOne).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.STATEMENT_WHILE__COND_ONE, null, msgs);
      if (newCondOne != null)
        msgs = ((InternalEObject)newCondOne).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.STATEMENT_WHILE__COND_ONE, null, msgs);
      msgs = basicSetCondOne(newCondOne, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.STATEMENT_WHILE__COND_ONE, newCondOne, newCondOne));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Type> getOperations()
  {
    if (operations == null)
    {
      operations = new EObjectContainmentEList<Type>(Type.class, this, CeurPackage.STATEMENT_WHILE__OPERATIONS);
    }
    return operations;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CeurPackage.STATEMENT_WHILE__COND_ONE:
        return basicSetCondOne(null, msgs);
      case CeurPackage.STATEMENT_WHILE__OPERATIONS:
        return ((InternalEList<?>)getOperations()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CeurPackage.STATEMENT_WHILE__COND_ONE:
        return getCondOne();
      case CeurPackage.STATEMENT_WHILE__OPERATIONS:
        return getOperations();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CeurPackage.STATEMENT_WHILE__COND_ONE:
        setCondOne((BooleanTypeExpression)newValue);
        return;
      case CeurPackage.STATEMENT_WHILE__OPERATIONS:
        getOperations().clear();
        getOperations().addAll((Collection<? extends Type>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.STATEMENT_WHILE__COND_ONE:
        setCondOne((BooleanTypeExpression)null);
        return;
      case CeurPackage.STATEMENT_WHILE__OPERATIONS:
        getOperations().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.STATEMENT_WHILE__COND_ONE:
        return condOne != null;
      case CeurPackage.STATEMENT_WHILE__OPERATIONS:
        return operations != null && !operations.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //Statement_WhileImpl
