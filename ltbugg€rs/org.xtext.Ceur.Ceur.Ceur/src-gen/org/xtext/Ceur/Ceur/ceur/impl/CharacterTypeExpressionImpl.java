/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.Ceur.Ceur.ceur.CeurPackage;
import org.xtext.Ceur.Ceur.ceur.CharacterAssignment;
import org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Character Type Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterTypeExpressionImpl#getCharAssign <em>Char Assign</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CharacterTypeExpressionImpl extends MinimalEObjectImpl.Container implements CharacterTypeExpression
{
  /**
   * The cached value of the '{@link #getCharAssign() <em>Char Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCharAssign()
   * @generated
   * @ordered
   */
  protected CharacterAssignment charAssign;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CharacterTypeExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CeurPackage.Literals.CHARACTER_TYPE_EXPRESSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CharacterAssignment getCharAssign()
  {
    return charAssign;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCharAssign(CharacterAssignment newCharAssign, NotificationChain msgs)
  {
    CharacterAssignment oldCharAssign = charAssign;
    charAssign = newCharAssign;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN, oldCharAssign, newCharAssign);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCharAssign(CharacterAssignment newCharAssign)
  {
    if (newCharAssign != charAssign)
    {
      NotificationChain msgs = null;
      if (charAssign != null)
        msgs = ((InternalEObject)charAssign).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN, null, msgs);
      if (newCharAssign != null)
        msgs = ((InternalEObject)newCharAssign).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN, null, msgs);
      msgs = basicSetCharAssign(newCharAssign, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN, newCharAssign, newCharAssign));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CeurPackage.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN:
        return basicSetCharAssign(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CeurPackage.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN:
        return getCharAssign();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CeurPackage.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN:
        setCharAssign((CharacterAssignment)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN:
        setCharAssign((CharacterAssignment)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN:
        return charAssign != null;
    }
    return super.eIsSet(featureID);
  }

} //CharacterTypeExpressionImpl
