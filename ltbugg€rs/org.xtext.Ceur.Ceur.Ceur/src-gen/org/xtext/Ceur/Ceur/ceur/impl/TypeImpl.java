/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss;
import org.xtext.Ceur.Ceur.ceur.BooleanVarVar;
import org.xtext.Ceur.Ceur.ceur.CallFunction;
import org.xtext.Ceur.Ceur.ceur.CeurPackage;
import org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss;
import org.xtext.Ceur.Ceur.ceur.CharacterVarVar;
import org.xtext.Ceur.Ceur.ceur.If_statment;
import org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss;
import org.xtext.Ceur.Ceur.ceur.IntegerVarVar;
import org.xtext.Ceur.Ceur.ceur.PrintL;
import org.xtext.Ceur.Ceur.ceur.Statement_While;
import org.xtext.Ceur.Ceur.ceur.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getInteger <em>Integer</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getIntegervarvar <em>Integervarvar</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getBoolean <em>Boolean</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getBooleanvarvar <em>Booleanvarvar</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getCharacter <em>Character</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getCharactervarvar <em>Charactervarvar</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getWhile <em>While</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getIf <em>If</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getPrint <em>Print</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl#getFunction <em>Function</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TypeImpl extends MinimalEObjectImpl.Container implements Type
{
  /**
   * The cached value of the '{@link #getInteger() <em>Integer</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInteger()
   * @generated
   * @ordered
   */
  protected EList<IntegerDeclarationAss> integer;

  /**
   * The cached value of the '{@link #getIntegervarvar() <em>Integervarvar</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntegervarvar()
   * @generated
   * @ordered
   */
  protected EList<IntegerVarVar> integervarvar;

  /**
   * The cached value of the '{@link #getBoolean() <em>Boolean</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBoolean()
   * @generated
   * @ordered
   */
  protected EList<BooleanDeclarationAss> boolean_;

  /**
   * The cached value of the '{@link #getBooleanvarvar() <em>Booleanvarvar</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBooleanvarvar()
   * @generated
   * @ordered
   */
  protected EList<BooleanVarVar> booleanvarvar;

  /**
   * The cached value of the '{@link #getCharacter() <em>Character</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCharacter()
   * @generated
   * @ordered
   */
  protected EList<CharacterDeclarationAss> character;

  /**
   * The cached value of the '{@link #getCharactervarvar() <em>Charactervarvar</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCharactervarvar()
   * @generated
   * @ordered
   */
  protected EList<CharacterVarVar> charactervarvar;

  /**
   * The cached value of the '{@link #getWhile() <em>While</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWhile()
   * @generated
   * @ordered
   */
  protected EList<Statement_While> while_;

  /**
   * The cached value of the '{@link #getIf() <em>If</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIf()
   * @generated
   * @ordered
   */
  protected EList<If_statment> if_;

  /**
   * The cached value of the '{@link #getPrint() <em>Print</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrint()
   * @generated
   * @ordered
   */
  protected EList<PrintL> print;

  /**
   * The cached value of the '{@link #getFunction() <em>Function</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunction()
   * @generated
   * @ordered
   */
  protected EList<CallFunction> function;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CeurPackage.Literals.TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<IntegerDeclarationAss> getInteger()
  {
    if (integer == null)
    {
      integer = new EObjectContainmentEList<IntegerDeclarationAss>(IntegerDeclarationAss.class, this, CeurPackage.TYPE__INTEGER);
    }
    return integer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<IntegerVarVar> getIntegervarvar()
  {
    if (integervarvar == null)
    {
      integervarvar = new EObjectContainmentEList<IntegerVarVar>(IntegerVarVar.class, this, CeurPackage.TYPE__INTEGERVARVAR);
    }
    return integervarvar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<BooleanDeclarationAss> getBoolean()
  {
    if (boolean_ == null)
    {
      boolean_ = new EObjectContainmentEList<BooleanDeclarationAss>(BooleanDeclarationAss.class, this, CeurPackage.TYPE__BOOLEAN);
    }
    return boolean_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<BooleanVarVar> getBooleanvarvar()
  {
    if (booleanvarvar == null)
    {
      booleanvarvar = new EObjectContainmentEList<BooleanVarVar>(BooleanVarVar.class, this, CeurPackage.TYPE__BOOLEANVARVAR);
    }
    return booleanvarvar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<CharacterDeclarationAss> getCharacter()
  {
    if (character == null)
    {
      character = new EObjectContainmentEList<CharacterDeclarationAss>(CharacterDeclarationAss.class, this, CeurPackage.TYPE__CHARACTER);
    }
    return character;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<CharacterVarVar> getCharactervarvar()
  {
    if (charactervarvar == null)
    {
      charactervarvar = new EObjectContainmentEList<CharacterVarVar>(CharacterVarVar.class, this, CeurPackage.TYPE__CHARACTERVARVAR);
    }
    return charactervarvar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Statement_While> getWhile()
  {
    if (while_ == null)
    {
      while_ = new EObjectContainmentEList<Statement_While>(Statement_While.class, this, CeurPackage.TYPE__WHILE);
    }
    return while_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<If_statment> getIf()
  {
    if (if_ == null)
    {
      if_ = new EObjectContainmentEList<If_statment>(If_statment.class, this, CeurPackage.TYPE__IF);
    }
    return if_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PrintL> getPrint()
  {
    if (print == null)
    {
      print = new EObjectContainmentEList<PrintL>(PrintL.class, this, CeurPackage.TYPE__PRINT);
    }
    return print;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<CallFunction> getFunction()
  {
    if (function == null)
    {
      function = new EObjectContainmentEList<CallFunction>(CallFunction.class, this, CeurPackage.TYPE__FUNCTION);
    }
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CeurPackage.TYPE__INTEGER:
        return ((InternalEList<?>)getInteger()).basicRemove(otherEnd, msgs);
      case CeurPackage.TYPE__INTEGERVARVAR:
        return ((InternalEList<?>)getIntegervarvar()).basicRemove(otherEnd, msgs);
      case CeurPackage.TYPE__BOOLEAN:
        return ((InternalEList<?>)getBoolean()).basicRemove(otherEnd, msgs);
      case CeurPackage.TYPE__BOOLEANVARVAR:
        return ((InternalEList<?>)getBooleanvarvar()).basicRemove(otherEnd, msgs);
      case CeurPackage.TYPE__CHARACTER:
        return ((InternalEList<?>)getCharacter()).basicRemove(otherEnd, msgs);
      case CeurPackage.TYPE__CHARACTERVARVAR:
        return ((InternalEList<?>)getCharactervarvar()).basicRemove(otherEnd, msgs);
      case CeurPackage.TYPE__WHILE:
        return ((InternalEList<?>)getWhile()).basicRemove(otherEnd, msgs);
      case CeurPackage.TYPE__IF:
        return ((InternalEList<?>)getIf()).basicRemove(otherEnd, msgs);
      case CeurPackage.TYPE__PRINT:
        return ((InternalEList<?>)getPrint()).basicRemove(otherEnd, msgs);
      case CeurPackage.TYPE__FUNCTION:
        return ((InternalEList<?>)getFunction()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CeurPackage.TYPE__INTEGER:
        return getInteger();
      case CeurPackage.TYPE__INTEGERVARVAR:
        return getIntegervarvar();
      case CeurPackage.TYPE__BOOLEAN:
        return getBoolean();
      case CeurPackage.TYPE__BOOLEANVARVAR:
        return getBooleanvarvar();
      case CeurPackage.TYPE__CHARACTER:
        return getCharacter();
      case CeurPackage.TYPE__CHARACTERVARVAR:
        return getCharactervarvar();
      case CeurPackage.TYPE__WHILE:
        return getWhile();
      case CeurPackage.TYPE__IF:
        return getIf();
      case CeurPackage.TYPE__PRINT:
        return getPrint();
      case CeurPackage.TYPE__FUNCTION:
        return getFunction();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CeurPackage.TYPE__INTEGER:
        getInteger().clear();
        getInteger().addAll((Collection<? extends IntegerDeclarationAss>)newValue);
        return;
      case CeurPackage.TYPE__INTEGERVARVAR:
        getIntegervarvar().clear();
        getIntegervarvar().addAll((Collection<? extends IntegerVarVar>)newValue);
        return;
      case CeurPackage.TYPE__BOOLEAN:
        getBoolean().clear();
        getBoolean().addAll((Collection<? extends BooleanDeclarationAss>)newValue);
        return;
      case CeurPackage.TYPE__BOOLEANVARVAR:
        getBooleanvarvar().clear();
        getBooleanvarvar().addAll((Collection<? extends BooleanVarVar>)newValue);
        return;
      case CeurPackage.TYPE__CHARACTER:
        getCharacter().clear();
        getCharacter().addAll((Collection<? extends CharacterDeclarationAss>)newValue);
        return;
      case CeurPackage.TYPE__CHARACTERVARVAR:
        getCharactervarvar().clear();
        getCharactervarvar().addAll((Collection<? extends CharacterVarVar>)newValue);
        return;
      case CeurPackage.TYPE__WHILE:
        getWhile().clear();
        getWhile().addAll((Collection<? extends Statement_While>)newValue);
        return;
      case CeurPackage.TYPE__IF:
        getIf().clear();
        getIf().addAll((Collection<? extends If_statment>)newValue);
        return;
      case CeurPackage.TYPE__PRINT:
        getPrint().clear();
        getPrint().addAll((Collection<? extends PrintL>)newValue);
        return;
      case CeurPackage.TYPE__FUNCTION:
        getFunction().clear();
        getFunction().addAll((Collection<? extends CallFunction>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.TYPE__INTEGER:
        getInteger().clear();
        return;
      case CeurPackage.TYPE__INTEGERVARVAR:
        getIntegervarvar().clear();
        return;
      case CeurPackage.TYPE__BOOLEAN:
        getBoolean().clear();
        return;
      case CeurPackage.TYPE__BOOLEANVARVAR:
        getBooleanvarvar().clear();
        return;
      case CeurPackage.TYPE__CHARACTER:
        getCharacter().clear();
        return;
      case CeurPackage.TYPE__CHARACTERVARVAR:
        getCharactervarvar().clear();
        return;
      case CeurPackage.TYPE__WHILE:
        getWhile().clear();
        return;
      case CeurPackage.TYPE__IF:
        getIf().clear();
        return;
      case CeurPackage.TYPE__PRINT:
        getPrint().clear();
        return;
      case CeurPackage.TYPE__FUNCTION:
        getFunction().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.TYPE__INTEGER:
        return integer != null && !integer.isEmpty();
      case CeurPackage.TYPE__INTEGERVARVAR:
        return integervarvar != null && !integervarvar.isEmpty();
      case CeurPackage.TYPE__BOOLEAN:
        return boolean_ != null && !boolean_.isEmpty();
      case CeurPackage.TYPE__BOOLEANVARVAR:
        return booleanvarvar != null && !booleanvarvar.isEmpty();
      case CeurPackage.TYPE__CHARACTER:
        return character != null && !character.isEmpty();
      case CeurPackage.TYPE__CHARACTERVARVAR:
        return charactervarvar != null && !charactervarvar.isEmpty();
      case CeurPackage.TYPE__WHILE:
        return while_ != null && !while_.isEmpty();
      case CeurPackage.TYPE__IF:
        return if_ != null && !if_.isEmpty();
      case CeurPackage.TYPE__PRINT:
        return print != null && !print.isEmpty();
      case CeurPackage.TYPE__FUNCTION:
        return function != null && !function.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //TypeImpl
