/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer To Bool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperand1 <em>Operand1</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperator <em>Operator</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperand2 <em>Operand2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerToBool()
 * @model
 * @generated
 */
public interface IntegerToBool extends EObject
{
  /**
   * Returns the value of the '<em><b>Operand1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operand1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operand1</em>' containment reference.
   * @see #setOperand1(IntegerTypeExpression)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerToBool_Operand1()
   * @model containment="true"
   * @generated
   */
  IntegerTypeExpression getOperand1();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperand1 <em>Operand1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Operand1</em>' containment reference.
   * @see #getOperand1()
   * @generated
   */
  void setOperand1(IntegerTypeExpression value);

  /**
   * Returns the value of the '<em><b>Operator</b></em>' attribute.
   * The literals are from the enumeration {@link org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operator</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operator</em>' attribute.
   * @see org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR
   * @see #setOperator(SPECIALINTOPERATOR)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerToBool_Operator()
   * @model
   * @generated
   */
  SPECIALINTOPERATOR getOperator();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperator <em>Operator</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Operator</em>' attribute.
   * @see org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR
   * @see #getOperator()
   * @generated
   */
  void setOperator(SPECIALINTOPERATOR value);

  /**
   * Returns the value of the '<em><b>Operand2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operand2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operand2</em>' containment reference.
   * @see #setOperand2(IntegerTypeExpression)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerToBool_Operand2()
   * @model containment="true"
   * @generated
   */
  IntegerTypeExpression getOperand2();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperand2 <em>Operand2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Operand2</em>' containment reference.
   * @see #getOperand2()
   * @generated
   */
  void setOperand2(IntegerTypeExpression value);

} // IntegerToBool
