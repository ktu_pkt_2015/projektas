/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Var Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.IntegerVarVar#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.IntegerVarVar#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerVarVar()
 * @model
 * @generated
 */
public interface IntegerVarVar extends EObject
{
  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see #setIdentifier(String)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerVarVar_Identifier()
   * @model
   * @generated
   */
  String getIdentifier();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.IntegerVarVar#getIdentifier <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Identifier</em>' attribute.
   * @see #getIdentifier()
   * @generated
   */
  void setIdentifier(String value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(IntegerTypeExpression)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerVarVar_Value()
   * @model containment="true"
   * @generated
   */
  IntegerTypeExpression getValue();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.IntegerVarVar#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(IntegerTypeExpression value);

} // IntegerVarVar
