/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement While</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Statement_While#getCondOne <em>Cond One</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Statement_While#getOperations <em>Operations</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getStatement_While()
 * @model
 * @generated
 */
public interface Statement_While extends EObject
{
  /**
   * Returns the value of the '<em><b>Cond One</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cond One</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cond One</em>' containment reference.
   * @see #setCondOne(BooleanTypeExpression)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getStatement_While_CondOne()
   * @model containment="true"
   * @generated
   */
  BooleanTypeExpression getCondOne();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.Statement_While#getCondOne <em>Cond One</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cond One</em>' containment reference.
   * @see #getCondOne()
   * @generated
   */
  void setCondOne(BooleanTypeExpression value);

  /**
   * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.Type}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operations</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getStatement_While_Operations()
   * @model containment="true"
   * @generated
   */
  EList<Type> getOperations();

} // Statement_While
