/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperand1 <em>Operand1</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperator <em>Operator</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperand2 <em>Operand2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanOperation()
 * @model
 * @generated
 */
public interface BooleanOperation extends EObject
{
  /**
   * Returns the value of the '<em><b>Operand1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operand1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operand1</em>' containment reference.
   * @see #setOperand1(BooleanAssignment)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanOperation_Operand1()
   * @model containment="true"
   * @generated
   */
  BooleanAssignment getOperand1();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperand1 <em>Operand1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Operand1</em>' containment reference.
   * @see #getOperand1()
   * @generated
   */
  void setOperand1(BooleanAssignment value);

  /**
   * Returns the value of the '<em><b>Operator</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operator</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operator</em>' containment reference.
   * @see #setOperator(BOOLOPERATOR)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanOperation_Operator()
   * @model containment="true"
   * @generated
   */
  BOOLOPERATOR getOperator();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperator <em>Operator</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Operator</em>' containment reference.
   * @see #getOperator()
   * @generated
   */
  void setOperator(BOOLOPERATOR value);

  /**
   * Returns the value of the '<em><b>Operand2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operand2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operand2</em>' containment reference.
   * @see #setOperand2(BooleanAssignment)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanOperation_Operand2()
   * @model containment="true"
   * @generated
   */
  BooleanAssignment getOperand2();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperand2 <em>Operand2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Operand2</em>' containment reference.
   * @see #getOperand2()
   * @generated
   */
  void setOperand2(BooleanAssignment value);

} // BooleanOperation
