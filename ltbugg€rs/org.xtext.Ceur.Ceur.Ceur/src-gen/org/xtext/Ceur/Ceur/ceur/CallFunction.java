/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.CallFunction#getOperationName <em>Operation Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getCallFunction()
 * @model
 * @generated
 */
public interface CallFunction extends EObject
{
  /**
   * Returns the value of the '<em><b>Operation Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operation Name</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operation Name</em>' containment reference.
   * @see #setOperationName(FunctionName)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getCallFunction_OperationName()
   * @model containment="true"
   * @generated
   */
  FunctionName getOperationName();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.CallFunction#getOperationName <em>Operation Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Operation Name</em>' containment reference.
   * @see #getOperationName()
   * @generated
   */
  void setOperationName(FunctionName value);

} // CallFunction
