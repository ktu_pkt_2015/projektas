/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.Ceur.Ceur.ceur.BooleanAssignment;
import org.xtext.Ceur.Ceur.ceur.BooleanOperation;
import org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression;
import org.xtext.Ceur.Ceur.ceur.CeurPackage;
import org.xtext.Ceur.Ceur.ceur.IntegerToBool;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Type Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanTypeExpressionImpl#getBoolAssign <em>Bool Assign</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanTypeExpressionImpl#getBoolOp <em>Bool Op</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanTypeExpressionImpl#getIntToBool <em>Int To Bool</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BooleanTypeExpressionImpl extends MinimalEObjectImpl.Container implements BooleanTypeExpression
{
  /**
   * The cached value of the '{@link #getBoolAssign() <em>Bool Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBoolAssign()
   * @generated
   * @ordered
   */
  protected BooleanAssignment boolAssign;

  /**
   * The cached value of the '{@link #getBoolOp() <em>Bool Op</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBoolOp()
   * @generated
   * @ordered
   */
  protected BooleanOperation boolOp;

  /**
   * The cached value of the '{@link #getIntToBool() <em>Int To Bool</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntToBool()
   * @generated
   * @ordered
   */
  protected IntegerToBool intToBool;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BooleanTypeExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CeurPackage.Literals.BOOLEAN_TYPE_EXPRESSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanAssignment getBoolAssign()
  {
    return boolAssign;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBoolAssign(BooleanAssignment newBoolAssign, NotificationChain msgs)
  {
    BooleanAssignment oldBoolAssign = boolAssign;
    boolAssign = newBoolAssign;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN, oldBoolAssign, newBoolAssign);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBoolAssign(BooleanAssignment newBoolAssign)
  {
    if (newBoolAssign != boolAssign)
    {
      NotificationChain msgs = null;
      if (boolAssign != null)
        msgs = ((InternalEObject)boolAssign).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN, null, msgs);
      if (newBoolAssign != null)
        msgs = ((InternalEObject)newBoolAssign).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN, null, msgs);
      msgs = basicSetBoolAssign(newBoolAssign, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN, newBoolAssign, newBoolAssign));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanOperation getBoolOp()
  {
    return boolOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBoolOp(BooleanOperation newBoolOp, NotificationChain msgs)
  {
    BooleanOperation oldBoolOp = boolOp;
    boolOp = newBoolOp;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_OP, oldBoolOp, newBoolOp);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBoolOp(BooleanOperation newBoolOp)
  {
    if (newBoolOp != boolOp)
    {
      NotificationChain msgs = null;
      if (boolOp != null)
        msgs = ((InternalEObject)boolOp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_OP, null, msgs);
      if (newBoolOp != null)
        msgs = ((InternalEObject)newBoolOp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_OP, null, msgs);
      msgs = basicSetBoolOp(newBoolOp, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_OP, newBoolOp, newBoolOp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerToBool getIntToBool()
  {
    return intToBool;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIntToBool(IntegerToBool newIntToBool, NotificationChain msgs)
  {
    IntegerToBool oldIntToBool = intToBool;
    intToBool = newIntToBool;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL, oldIntToBool, newIntToBool);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIntToBool(IntegerToBool newIntToBool)
  {
    if (newIntToBool != intToBool)
    {
      NotificationChain msgs = null;
      if (intToBool != null)
        msgs = ((InternalEObject)intToBool).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL, null, msgs);
      if (newIntToBool != null)
        msgs = ((InternalEObject)newIntToBool).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL, null, msgs);
      msgs = basicSetIntToBool(newIntToBool, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL, newIntToBool, newIntToBool));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN:
        return basicSetBoolAssign(null, msgs);
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_OP:
        return basicSetBoolOp(null, msgs);
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL:
        return basicSetIntToBool(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN:
        return getBoolAssign();
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_OP:
        return getBoolOp();
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL:
        return getIntToBool();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN:
        setBoolAssign((BooleanAssignment)newValue);
        return;
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_OP:
        setBoolOp((BooleanOperation)newValue);
        return;
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL:
        setIntToBool((IntegerToBool)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN:
        setBoolAssign((BooleanAssignment)null);
        return;
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_OP:
        setBoolOp((BooleanOperation)null);
        return;
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL:
        setIntToBool((IntegerToBool)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN:
        return boolAssign != null;
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__BOOL_OP:
        return boolOp != null;
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL:
        return intToBool != null;
    }
    return super.eIsSet(featureID);
  }

} //BooleanTypeExpressionImpl
