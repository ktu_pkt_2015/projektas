/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.xtext.Ceur.Ceur.ceur.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CeurFactoryImpl extends EFactoryImpl implements CeurFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static CeurFactory init()
  {
    try
    {
      CeurFactory theCeurFactory = (CeurFactory)EPackage.Registry.INSTANCE.getEFactory(CeurPackage.eNS_URI);
      if (theCeurFactory != null)
      {
        return theCeurFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new CeurFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CeurFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case CeurPackage.DOMAINMODEL: return createDomainmodel();
      case CeurPackage.TYPE: return createType();
      case CeurPackage.PRINT_L: return createPrintL();
      case CeurPackage.STATEMENT_WHILE: return createStatement_While();
      case CeurPackage.IF_STATMENT: return createIf_statment();
      case CeurPackage.ELSE: return createElse();
      case CeurPackage.CHARACTER_DECLARATION_ASS: return createCharacterDeclarationAss();
      case CeurPackage.CHARACTER_DECLARATION: return createCharacterDeclaration();
      case CeurPackage.CHARACTER_TYPE_EXPRESSION: return createCharacterTypeExpression();
      case CeurPackage.CHARACTER_ASSIGNMENT: return createCharacterAssignment();
      case CeurPackage.CHARACTER_VAR_VAR: return createCharacterVarVar();
      case CeurPackage.BOOLEAN_DECLARATION_ASS: return createBooleanDeclarationAss();
      case CeurPackage.BOOLEAN_DECLARATION: return createBooleanDeclaration();
      case CeurPackage.BOOLEAN_TYPE_EXPRESSION: return createBooleanTypeExpression();
      case CeurPackage.BOOLEAN_VAR_VAR: return createBooleanVarVar();
      case CeurPackage.BOOLEAN_ASSIGNMENT: return createBooleanAssignment();
      case CeurPackage.BOOLEAN_OPERATION: return createBooleanOperation();
      case CeurPackage.INTEGER_TO_BOOL: return createIntegerToBool();
      case CeurPackage.INTEGER_DECLARATION_ASS: return createIntegerDeclarationAss();
      case CeurPackage.INTEGER_DECLARATION: return createIntegerDeclaration();
      case CeurPackage.INTEGER_VAR_VAR: return createIntegerVarVar();
      case CeurPackage.INTEGER_TYPE_EXPRESSION: return createIntegerTypeExpression();
      case CeurPackage.INTEGER_ASSIGNMENT: return createIntegerAssignment();
      case CeurPackage.INTEGER_OPERATION: return createIntegerOperation();
      case CeurPackage.CALL_FUNCTION: return createCallFunction();
      case CeurPackage.FUNCTION_NAME: return createFunctionName();
      case CeurPackage.BOOLOPERATOR: return createBOOLOPERATOR();
      case CeurPackage.FUNCTION: return createFunction();
      case CeurPackage.IMPORT: return createImport();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case CeurPackage.OPERATOR:
        return createOPERATORFromString(eDataType, initialValue);
      case CeurPackage.SPECIALINTOPERATOR:
        return createSPECIALINTOPERATORFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case CeurPackage.OPERATOR:
        return convertOPERATORToString(eDataType, instanceValue);
      case CeurPackage.SPECIALINTOPERATOR:
        return convertSPECIALINTOPERATORToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Domainmodel createDomainmodel()
  {
    DomainmodelImpl domainmodel = new DomainmodelImpl();
    return domainmodel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PrintL createPrintL()
  {
    PrintLImpl printL = new PrintLImpl();
    return printL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement_While createStatement_While()
  {
    Statement_WhileImpl statement_While = new Statement_WhileImpl();
    return statement_While;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public If_statment createIf_statment()
  {
    If_statmentImpl if_statment = new If_statmentImpl();
    return if_statment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Else createElse()
  {
    ElseImpl else_ = new ElseImpl();
    return else_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CharacterDeclarationAss createCharacterDeclarationAss()
  {
    CharacterDeclarationAssImpl characterDeclarationAss = new CharacterDeclarationAssImpl();
    return characterDeclarationAss;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CharacterDeclaration createCharacterDeclaration()
  {
    CharacterDeclarationImpl characterDeclaration = new CharacterDeclarationImpl();
    return characterDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CharacterTypeExpression createCharacterTypeExpression()
  {
    CharacterTypeExpressionImpl characterTypeExpression = new CharacterTypeExpressionImpl();
    return characterTypeExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CharacterAssignment createCharacterAssignment()
  {
    CharacterAssignmentImpl characterAssignment = new CharacterAssignmentImpl();
    return characterAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CharacterVarVar createCharacterVarVar()
  {
    CharacterVarVarImpl characterVarVar = new CharacterVarVarImpl();
    return characterVarVar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanDeclarationAss createBooleanDeclarationAss()
  {
    BooleanDeclarationAssImpl booleanDeclarationAss = new BooleanDeclarationAssImpl();
    return booleanDeclarationAss;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanDeclaration createBooleanDeclaration()
  {
    BooleanDeclarationImpl booleanDeclaration = new BooleanDeclarationImpl();
    return booleanDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanTypeExpression createBooleanTypeExpression()
  {
    BooleanTypeExpressionImpl booleanTypeExpression = new BooleanTypeExpressionImpl();
    return booleanTypeExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanVarVar createBooleanVarVar()
  {
    BooleanVarVarImpl booleanVarVar = new BooleanVarVarImpl();
    return booleanVarVar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanAssignment createBooleanAssignment()
  {
    BooleanAssignmentImpl booleanAssignment = new BooleanAssignmentImpl();
    return booleanAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanOperation createBooleanOperation()
  {
    BooleanOperationImpl booleanOperation = new BooleanOperationImpl();
    return booleanOperation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerToBool createIntegerToBool()
  {
    IntegerToBoolImpl integerToBool = new IntegerToBoolImpl();
    return integerToBool;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerDeclarationAss createIntegerDeclarationAss()
  {
    IntegerDeclarationAssImpl integerDeclarationAss = new IntegerDeclarationAssImpl();
    return integerDeclarationAss;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerDeclaration createIntegerDeclaration()
  {
    IntegerDeclarationImpl integerDeclaration = new IntegerDeclarationImpl();
    return integerDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerVarVar createIntegerVarVar()
  {
    IntegerVarVarImpl integerVarVar = new IntegerVarVarImpl();
    return integerVarVar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerTypeExpression createIntegerTypeExpression()
  {
    IntegerTypeExpressionImpl integerTypeExpression = new IntegerTypeExpressionImpl();
    return integerTypeExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerAssignment createIntegerAssignment()
  {
    IntegerAssignmentImpl integerAssignment = new IntegerAssignmentImpl();
    return integerAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerOperation createIntegerOperation()
  {
    IntegerOperationImpl integerOperation = new IntegerOperationImpl();
    return integerOperation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallFunction createCallFunction()
  {
    CallFunctionImpl callFunction = new CallFunctionImpl();
    return callFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionName createFunctionName()
  {
    FunctionNameImpl functionName = new FunctionNameImpl();
    return functionName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BOOLOPERATOR createBOOLOPERATOR()
  {
    BOOLOPERATORImpl booloperator = new BOOLOPERATORImpl();
    return booloperator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Function createFunction()
  {
    FunctionImpl function = new FunctionImpl();
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Import createImport()
  {
    ImportImpl import_ = new ImportImpl();
    return import_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OPERATOR createOPERATORFromString(EDataType eDataType, String initialValue)
  {
    OPERATOR result = OPERATOR.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertOPERATORToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SPECIALINTOPERATOR createSPECIALINTOPERATORFromString(EDataType eDataType, String initialValue)
  {
    SPECIALINTOPERATOR result = SPECIALINTOPERATOR.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSPECIALINTOPERATORToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CeurPackage getCeurPackage()
  {
    return (CeurPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static CeurPackage getPackage()
  {
    return CeurPackage.eINSTANCE;
  }

} //CeurFactoryImpl
