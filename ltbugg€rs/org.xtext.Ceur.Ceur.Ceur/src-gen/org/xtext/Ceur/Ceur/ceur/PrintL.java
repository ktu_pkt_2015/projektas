/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Print L</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.PrintL#getPrint <em>Print</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.PrintL#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getPrintL()
 * @model
 * @generated
 */
public interface PrintL extends EObject
{
  /**
   * Returns the value of the '<em><b>Print</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Print</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Print</em>' attribute.
   * @see #setPrint(String)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getPrintL_Print()
   * @model
   * @generated
   */
  String getPrint();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.PrintL#getPrint <em>Print</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Print</em>' attribute.
   * @see #getPrint()
   * @generated
   */
  void setPrint(String value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getPrintL_Value()
   * @model
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.PrintL#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

} // PrintL
