/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.Ceur.Ceur.ceur.CeurFactory
 * @model kind="package"
 * @generated
 */
public interface CeurPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "ceur";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/Ceur/Ceur/Ceur";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "ceur";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  CeurPackage eINSTANCE = org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.DomainmodelImpl <em>Domainmodel</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.DomainmodelImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getDomainmodel()
   * @generated
   */
  int DOMAINMODEL = 0;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOMAINMODEL__ELEMENTS = 0;

  /**
   * The number of structural features of the '<em>Domainmodel</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOMAINMODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.TypeImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getType()
   * @generated
   */
  int TYPE = 1;

  /**
   * The feature id for the '<em><b>Integer</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__INTEGER = 0;

  /**
   * The feature id for the '<em><b>Integervarvar</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__INTEGERVARVAR = 1;

  /**
   * The feature id for the '<em><b>Boolean</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__BOOLEAN = 2;

  /**
   * The feature id for the '<em><b>Booleanvarvar</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__BOOLEANVARVAR = 3;

  /**
   * The feature id for the '<em><b>Character</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__CHARACTER = 4;

  /**
   * The feature id for the '<em><b>Charactervarvar</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__CHARACTERVARVAR = 5;

  /**
   * The feature id for the '<em><b>While</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__WHILE = 6;

  /**
   * The feature id for the '<em><b>If</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__IF = 7;

  /**
   * The feature id for the '<em><b>Print</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__PRINT = 8;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__FUNCTION = 9;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = 10;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.PrintLImpl <em>Print L</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.PrintLImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getPrintL()
   * @generated
   */
  int PRINT_L = 2;

  /**
   * The feature id for the '<em><b>Print</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_L__PRINT = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_L__VALUE = 1;

  /**
   * The number of structural features of the '<em>Print L</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRINT_L_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.Statement_WhileImpl <em>Statement While</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.Statement_WhileImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getStatement_While()
   * @generated
   */
  int STATEMENT_WHILE = 3;

  /**
   * The feature id for the '<em><b>Cond One</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_WHILE__COND_ONE = 0;

  /**
   * The feature id for the '<em><b>Operations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_WHILE__OPERATIONS = 1;

  /**
   * The number of structural features of the '<em>Statement While</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_WHILE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.If_statmentImpl <em>If statment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.If_statmentImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIf_statment()
   * @generated
   */
  int IF_STATMENT = 4;

  /**
   * The feature id for the '<em><b>Cond One</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATMENT__COND_ONE = 0;

  /**
   * The feature id for the '<em><b>Operations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATMENT__OPERATIONS = 1;

  /**
   * The feature id for the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATMENT__ELSE = 2;

  /**
   * The number of structural features of the '<em>If statment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.ElseImpl <em>Else</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.ElseImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getElse()
   * @generated
   */
  int ELSE = 5;

  /**
   * The feature id for the '<em><b>Operations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE__OPERATIONS = 0;

  /**
   * The number of structural features of the '<em>Else</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELSE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterDeclarationAssImpl <em>Character Declaration Ass</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterDeclarationAssImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterDeclarationAss()
   * @generated
   */
  int CHARACTER_DECLARATION_ASS = 6;

  /**
   * The feature id for the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_DECLARATION_ASS__ASSIGN = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_DECLARATION_ASS__VALUE = 1;

  /**
   * The number of structural features of the '<em>Character Declaration Ass</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_DECLARATION_ASS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterDeclarationImpl <em>Character Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterDeclarationImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterDeclaration()
   * @generated
   */
  int CHARACTER_DECLARATION = 7;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_DECLARATION__IDENTIFIER = 0;

  /**
   * The number of structural features of the '<em>Character Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_DECLARATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterTypeExpressionImpl <em>Character Type Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterTypeExpressionImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterTypeExpression()
   * @generated
   */
  int CHARACTER_TYPE_EXPRESSION = 8;

  /**
   * The feature id for the '<em><b>Char Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN = 0;

  /**
   * The number of structural features of the '<em>Character Type Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_TYPE_EXPRESSION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterAssignmentImpl <em>Character Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterAssignmentImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterAssignment()
   * @generated
   */
  int CHARACTER_ASSIGNMENT = 9;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_ASSIGNMENT__VALUE = 0;

  /**
   * The number of structural features of the '<em>Character Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_ASSIGNMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterVarVarImpl <em>Character Var Var</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterVarVarImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterVarVar()
   * @generated
   */
  int CHARACTER_VAR_VAR = 10;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_VAR_VAR__IDENTIFIER = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_VAR_VAR__VALUE = 1;

  /**
   * The number of structural features of the '<em>Character Var Var</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHARACTER_VAR_VAR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanDeclarationAssImpl <em>Boolean Declaration Ass</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanDeclarationAssImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanDeclarationAss()
   * @generated
   */
  int BOOLEAN_DECLARATION_ASS = 11;

  /**
   * The feature id for the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_DECLARATION_ASS__ASSIGN = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_DECLARATION_ASS__VALUE = 1;

  /**
   * The number of structural features of the '<em>Boolean Declaration Ass</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_DECLARATION_ASS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanDeclarationImpl <em>Boolean Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanDeclarationImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanDeclaration()
   * @generated
   */
  int BOOLEAN_DECLARATION = 12;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_DECLARATION__IDENTIFIER = 0;

  /**
   * The number of structural features of the '<em>Boolean Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_DECLARATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanTypeExpressionImpl <em>Boolean Type Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanTypeExpressionImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanTypeExpression()
   * @generated
   */
  int BOOLEAN_TYPE_EXPRESSION = 13;

  /**
   * The feature id for the '<em><b>Bool Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN = 0;

  /**
   * The feature id for the '<em><b>Bool Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_TYPE_EXPRESSION__BOOL_OP = 1;

  /**
   * The feature id for the '<em><b>Int To Bool</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL = 2;

  /**
   * The number of structural features of the '<em>Boolean Type Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_TYPE_EXPRESSION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanVarVarImpl <em>Boolean Var Var</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanVarVarImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanVarVar()
   * @generated
   */
  int BOOLEAN_VAR_VAR = 14;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_VAR_VAR__IDENTIFIER = 0;

  /**
   * The feature id for the '<em><b>Value1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_VAR_VAR__VALUE1 = 1;

  /**
   * The feature id for the '<em><b>Value2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_VAR_VAR__VALUE2 = 2;

  /**
   * The number of structural features of the '<em>Boolean Var Var</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_VAR_VAR_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanAssignmentImpl <em>Boolean Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanAssignmentImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanAssignment()
   * @generated
   */
  int BOOLEAN_ASSIGNMENT = 15;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_ASSIGNMENT__VALUE = 0;

  /**
   * The number of structural features of the '<em>Boolean Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_ASSIGNMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanOperationImpl <em>Boolean Operation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanOperationImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanOperation()
   * @generated
   */
  int BOOLEAN_OPERATION = 16;

  /**
   * The feature id for the '<em><b>Operand1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_OPERATION__OPERAND1 = 0;

  /**
   * The feature id for the '<em><b>Operator</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_OPERATION__OPERATOR = 1;

  /**
   * The feature id for the '<em><b>Operand2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_OPERATION__OPERAND2 = 2;

  /**
   * The number of structural features of the '<em>Boolean Operation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_OPERATION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerToBoolImpl <em>Integer To Bool</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerToBoolImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerToBool()
   * @generated
   */
  int INTEGER_TO_BOOL = 17;

  /**
   * The feature id for the '<em><b>Operand1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TO_BOOL__OPERAND1 = 0;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TO_BOOL__OPERATOR = 1;

  /**
   * The feature id for the '<em><b>Operand2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TO_BOOL__OPERAND2 = 2;

  /**
   * The number of structural features of the '<em>Integer To Bool</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TO_BOOL_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerDeclarationAssImpl <em>Integer Declaration Ass</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerDeclarationAssImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerDeclarationAss()
   * @generated
   */
  int INTEGER_DECLARATION_ASS = 18;

  /**
   * The feature id for the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_DECLARATION_ASS__ASSIGN = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_DECLARATION_ASS__VALUE = 1;

  /**
   * The number of structural features of the '<em>Integer Declaration Ass</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_DECLARATION_ASS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerDeclarationImpl <em>Integer Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerDeclarationImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerDeclaration()
   * @generated
   */
  int INTEGER_DECLARATION = 19;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_DECLARATION__IDENTIFIER = 0;

  /**
   * The number of structural features of the '<em>Integer Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_DECLARATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerVarVarImpl <em>Integer Var Var</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerVarVarImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerVarVar()
   * @generated
   */
  int INTEGER_VAR_VAR = 20;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_VAR_VAR__IDENTIFIER = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_VAR_VAR__VALUE = 1;

  /**
   * The number of structural features of the '<em>Integer Var Var</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_VAR_VAR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerTypeExpressionImpl <em>Integer Type Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerTypeExpressionImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerTypeExpression()
   * @generated
   */
  int INTEGER_TYPE_EXPRESSION = 21;

  /**
   * The feature id for the '<em><b>Integer Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN = 0;

  /**
   * The feature id for the '<em><b>Integer Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE_EXPRESSION__INTEGER_OP = 1;

  /**
   * The number of structural features of the '<em>Integer Type Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE_EXPRESSION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerAssignmentImpl <em>Integer Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerAssignmentImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerAssignment()
   * @generated
   */
  int INTEGER_ASSIGNMENT = 22;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_ASSIGNMENT__VALUE = 0;

  /**
   * The number of structural features of the '<em>Integer Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_ASSIGNMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerOperationImpl <em>Integer Operation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerOperationImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerOperation()
   * @generated
   */
  int INTEGER_OPERATION = 23;

  /**
   * The feature id for the '<em><b>Operand1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_OPERATION__OPERAND1 = 0;

  /**
   * The feature id for the '<em><b>Operator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_OPERATION__OPERATOR = 1;

  /**
   * The feature id for the '<em><b>Operand2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_OPERATION__OPERAND2 = 2;

  /**
   * The number of structural features of the '<em>Integer Operation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_OPERATION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CallFunctionImpl <em>Call Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.CallFunctionImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCallFunction()
   * @generated
   */
  int CALL_FUNCTION = 24;

  /**
   * The feature id for the '<em><b>Operation Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_FUNCTION__OPERATION_NAME = 0;

  /**
   * The number of structural features of the '<em>Call Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_FUNCTION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.FunctionNameImpl <em>Function Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.FunctionNameImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getFunctionName()
   * @generated
   */
  int FUNCTION_NAME = 25;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_NAME__VALUE = 0;

  /**
   * The number of structural features of the '<em>Function Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_NAME_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BOOLOPERATORImpl <em>BOOLOPERATOR</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.BOOLOPERATORImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBOOLOPERATOR()
   * @generated
   */
  int BOOLOPERATOR = 26;

  /**
   * The feature id for the '<em><b>AND</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLOPERATOR__AND = 0;

  /**
   * The feature id for the '<em><b>OR</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLOPERATOR__OR = 1;

  /**
   * The number of structural features of the '<em>BOOLOPERATOR</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLOPERATOR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.FunctionImpl <em>Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.FunctionImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getFunction()
   * @generated
   */
  int FUNCTION = 27;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__NAME = 0;

  /**
   * The feature id for the '<em><b>Features</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__FEATURES = 1;

  /**
   * The number of structural features of the '<em>Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.impl.ImportImpl <em>Import</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.impl.ImportImpl
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getImport()
   * @generated
   */
  int IMPORT = 28;

  /**
   * The feature id for the '<em><b>Integer</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__INTEGER = TYPE__INTEGER;

  /**
   * The feature id for the '<em><b>Integervarvar</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__INTEGERVARVAR = TYPE__INTEGERVARVAR;

  /**
   * The feature id for the '<em><b>Boolean</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__BOOLEAN = TYPE__BOOLEAN;

  /**
   * The feature id for the '<em><b>Booleanvarvar</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__BOOLEANVARVAR = TYPE__BOOLEANVARVAR;

  /**
   * The feature id for the '<em><b>Character</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__CHARACTER = TYPE__CHARACTER;

  /**
   * The feature id for the '<em><b>Charactervarvar</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__CHARACTERVARVAR = TYPE__CHARACTERVARVAR;

  /**
   * The feature id for the '<em><b>While</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__WHILE = TYPE__WHILE;

  /**
   * The feature id for the '<em><b>If</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__IF = TYPE__IF;

  /**
   * The feature id for the '<em><b>Print</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__PRINT = TYPE__PRINT;

  /**
   * The feature id for the '<em><b>Function</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__FUNCTION = TYPE__FUNCTION;

  /**
   * The feature id for the '<em><b>Imported Namespace</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT__IMPORTED_NAMESPACE = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Import</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.OPERATOR <em>OPERATOR</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.OPERATOR
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getOPERATOR()
   * @generated
   */
  int OPERATOR = 29;

  /**
   * The meta object id for the '{@link org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR <em>SPECIALINTOPERATOR</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR
   * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getSPECIALINTOPERATOR()
   * @generated
   */
  int SPECIALINTOPERATOR = 30;


  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.Domainmodel <em>Domainmodel</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Domainmodel</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Domainmodel
   * @generated
   */
  EClass getDomainmodel();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Domainmodel#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Domainmodel#getElements()
   * @see #getDomainmodel()
   * @generated
   */
  EReference getDomainmodel_Elements();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getInteger <em>Integer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Integer</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getInteger()
   * @see #getType()
   * @generated
   */
  EReference getType_Integer();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getIntegervarvar <em>Integervarvar</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Integervarvar</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getIntegervarvar()
   * @see #getType()
   * @generated
   */
  EReference getType_Integervarvar();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getBoolean <em>Boolean</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Boolean</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getBoolean()
   * @see #getType()
   * @generated
   */
  EReference getType_Boolean();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getBooleanvarvar <em>Booleanvarvar</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Booleanvarvar</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getBooleanvarvar()
   * @see #getType()
   * @generated
   */
  EReference getType_Booleanvarvar();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getCharacter <em>Character</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Character</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getCharacter()
   * @see #getType()
   * @generated
   */
  EReference getType_Character();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getCharactervarvar <em>Charactervarvar</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Charactervarvar</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getCharactervarvar()
   * @see #getType()
   * @generated
   */
  EReference getType_Charactervarvar();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getWhile <em>While</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>While</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getWhile()
   * @see #getType()
   * @generated
   */
  EReference getType_While();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getIf <em>If</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>If</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getIf()
   * @see #getType()
   * @generated
   */
  EReference getType_If();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getPrint <em>Print</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Print</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getPrint()
   * @see #getType()
   * @generated
   */
  EReference getType_Print();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Type#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Function</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Type#getFunction()
   * @see #getType()
   * @generated
   */
  EReference getType_Function();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.PrintL <em>Print L</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Print L</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.PrintL
   * @generated
   */
  EClass getPrintL();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.PrintL#getPrint <em>Print</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Print</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.PrintL#getPrint()
   * @see #getPrintL()
   * @generated
   */
  EAttribute getPrintL_Print();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.PrintL#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.PrintL#getValue()
   * @see #getPrintL()
   * @generated
   */
  EAttribute getPrintL_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.Statement_While <em>Statement While</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement While</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Statement_While
   * @generated
   */
  EClass getStatement_While();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.Statement_While#getCondOne <em>Cond One</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cond One</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Statement_While#getCondOne()
   * @see #getStatement_While()
   * @generated
   */
  EReference getStatement_While_CondOne();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Statement_While#getOperations <em>Operations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Operations</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Statement_While#getOperations()
   * @see #getStatement_While()
   * @generated
   */
  EReference getStatement_While_Operations();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.If_statment <em>If statment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If statment</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.If_statment
   * @generated
   */
  EClass getIf_statment();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.If_statment#getCondOne <em>Cond One</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cond One</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.If_statment#getCondOne()
   * @see #getIf_statment()
   * @generated
   */
  EReference getIf_statment_CondOne();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.If_statment#getOperations <em>Operations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Operations</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.If_statment#getOperations()
   * @see #getIf_statment()
   * @generated
   */
  EReference getIf_statment_Operations();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.If_statment#getElse <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Else</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.If_statment#getElse()
   * @see #getIf_statment()
   * @generated
   */
  EReference getIf_statment_Else();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.Else <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Else</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Else
   * @generated
   */
  EClass getElse();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Else#getOperations <em>Operations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Operations</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Else#getOperations()
   * @see #getElse()
   * @generated
   */
  EReference getElse_Operations();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss <em>Character Declaration Ass</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Character Declaration Ass</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss
   * @generated
   */
  EClass getCharacterDeclarationAss();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss#getAssign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Assign</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss#getAssign()
   * @see #getCharacterDeclarationAss()
   * @generated
   */
  EReference getCharacterDeclarationAss_Assign();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss#getValue()
   * @see #getCharacterDeclarationAss()
   * @generated
   */
  EReference getCharacterDeclarationAss_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.CharacterDeclaration <em>Character Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Character Declaration</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterDeclaration
   * @generated
   */
  EClass getCharacterDeclaration();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.CharacterDeclaration#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterDeclaration#getIdentifier()
   * @see #getCharacterDeclaration()
   * @generated
   */
  EAttribute getCharacterDeclaration_Identifier();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression <em>Character Type Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Character Type Expression</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression
   * @generated
   */
  EClass getCharacterTypeExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression#getCharAssign <em>Char Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Char Assign</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression#getCharAssign()
   * @see #getCharacterTypeExpression()
   * @generated
   */
  EReference getCharacterTypeExpression_CharAssign();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.CharacterAssignment <em>Character Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Character Assignment</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterAssignment
   * @generated
   */
  EClass getCharacterAssignment();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.CharacterAssignment#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterAssignment#getValue()
   * @see #getCharacterAssignment()
   * @generated
   */
  EAttribute getCharacterAssignment_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.CharacterVarVar <em>Character Var Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Character Var Var</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterVarVar
   * @generated
   */
  EClass getCharacterVarVar();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.CharacterVarVar#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterVarVar#getIdentifier()
   * @see #getCharacterVarVar()
   * @generated
   */
  EAttribute getCharacterVarVar_Identifier();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.CharacterVarVar#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterVarVar#getValue()
   * @see #getCharacterVarVar()
   * @generated
   */
  EAttribute getCharacterVarVar_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss <em>Boolean Declaration Ass</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Declaration Ass</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss
   * @generated
   */
  EClass getBooleanDeclarationAss();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss#getAssign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Assign</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss#getAssign()
   * @see #getBooleanDeclarationAss()
   * @generated
   */
  EReference getBooleanDeclarationAss_Assign();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss#getValue()
   * @see #getBooleanDeclarationAss()
   * @generated
   */
  EReference getBooleanDeclarationAss_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclaration <em>Boolean Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Declaration</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanDeclaration
   * @generated
   */
  EClass getBooleanDeclaration();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclaration#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanDeclaration#getIdentifier()
   * @see #getBooleanDeclaration()
   * @generated
   */
  EAttribute getBooleanDeclaration_Identifier();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression <em>Boolean Type Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Type Expression</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression
   * @generated
   */
  EClass getBooleanTypeExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getBoolAssign <em>Bool Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bool Assign</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getBoolAssign()
   * @see #getBooleanTypeExpression()
   * @generated
   */
  EReference getBooleanTypeExpression_BoolAssign();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getBoolOp <em>Bool Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Bool Op</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getBoolOp()
   * @see #getBooleanTypeExpression()
   * @generated
   */
  EReference getBooleanTypeExpression_BoolOp();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getIntToBool <em>Int To Bool</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Int To Bool</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getIntToBool()
   * @see #getBooleanTypeExpression()
   * @generated
   */
  EReference getBooleanTypeExpression_IntToBool();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar <em>Boolean Var Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Var Var</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanVarVar
   * @generated
   */
  EClass getBooleanVarVar();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getIdentifier()
   * @see #getBooleanVarVar()
   * @generated
   */
  EAttribute getBooleanVarVar_Identifier();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getValue1 <em>Value1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value1</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getValue1()
   * @see #getBooleanVarVar()
   * @generated
   */
  EReference getBooleanVarVar_Value1();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getValue2 <em>Value2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value2</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanVarVar#getValue2()
   * @see #getBooleanVarVar()
   * @generated
   */
  EReference getBooleanVarVar_Value2();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.BooleanAssignment <em>Boolean Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Assignment</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanAssignment
   * @generated
   */
  EClass getBooleanAssignment();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.BooleanAssignment#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanAssignment#getValue()
   * @see #getBooleanAssignment()
   * @generated
   */
  EAttribute getBooleanAssignment_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation <em>Boolean Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Operation</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanOperation
   * @generated
   */
  EClass getBooleanOperation();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperand1 <em>Operand1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand1</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperand1()
   * @see #getBooleanOperation()
   * @generated
   */
  EReference getBooleanOperation_Operand1();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperator <em>Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operator</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperator()
   * @see #getBooleanOperation()
   * @generated
   */
  EReference getBooleanOperation_Operator();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperand2 <em>Operand2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand2</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanOperation#getOperand2()
   * @see #getBooleanOperation()
   * @generated
   */
  EReference getBooleanOperation_Operand2();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool <em>Integer To Bool</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer To Bool</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerToBool
   * @generated
   */
  EClass getIntegerToBool();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperand1 <em>Operand1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand1</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperand1()
   * @see #getIntegerToBool()
   * @generated
   */
  EReference getIntegerToBool_Operand1();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperator <em>Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operator</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperator()
   * @see #getIntegerToBool()
   * @generated
   */
  EAttribute getIntegerToBool_Operator();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperand2 <em>Operand2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand2</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerToBool#getOperand2()
   * @see #getIntegerToBool()
   * @generated
   */
  EReference getIntegerToBool_Operand2();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss <em>Integer Declaration Ass</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Declaration Ass</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss
   * @generated
   */
  EClass getIntegerDeclarationAss();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss#getAssign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Assign</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss#getAssign()
   * @see #getIntegerDeclarationAss()
   * @generated
   */
  EReference getIntegerDeclarationAss_Assign();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss#getValue()
   * @see #getIntegerDeclarationAss()
   * @generated
   */
  EReference getIntegerDeclarationAss_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclaration <em>Integer Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Declaration</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerDeclaration
   * @generated
   */
  EClass getIntegerDeclaration();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclaration#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerDeclaration#getIdentifier()
   * @see #getIntegerDeclaration()
   * @generated
   */
  EAttribute getIntegerDeclaration_Identifier();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.IntegerVarVar <em>Integer Var Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Var Var</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerVarVar
   * @generated
   */
  EClass getIntegerVarVar();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.IntegerVarVar#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerVarVar#getIdentifier()
   * @see #getIntegerVarVar()
   * @generated
   */
  EAttribute getIntegerVarVar_Identifier();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.IntegerVarVar#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerVarVar#getValue()
   * @see #getIntegerVarVar()
   * @generated
   */
  EReference getIntegerVarVar_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression <em>Integer Type Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Type Expression</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression
   * @generated
   */
  EClass getIntegerTypeExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression#getIntegerAssign <em>Integer Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Integer Assign</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression#getIntegerAssign()
   * @see #getIntegerTypeExpression()
   * @generated
   */
  EReference getIntegerTypeExpression_IntegerAssign();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression#getIntegerOp <em>Integer Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Integer Op</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression#getIntegerOp()
   * @see #getIntegerTypeExpression()
   * @generated
   */
  EReference getIntegerTypeExpression_IntegerOp();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.IntegerAssignment <em>Integer Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Assignment</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerAssignment
   * @generated
   */
  EClass getIntegerAssignment();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.IntegerAssignment#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerAssignment#getValue()
   * @see #getIntegerAssignment()
   * @generated
   */
  EAttribute getIntegerAssignment_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.IntegerOperation <em>Integer Operation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Operation</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerOperation
   * @generated
   */
  EClass getIntegerOperation();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.IntegerOperation#getOperand1 <em>Operand1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand1</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerOperation#getOperand1()
   * @see #getIntegerOperation()
   * @generated
   */
  EReference getIntegerOperation_Operand1();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.IntegerOperation#getOperator <em>Operator</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Operator</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerOperation#getOperator()
   * @see #getIntegerOperation()
   * @generated
   */
  EAttribute getIntegerOperation_Operator();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.IntegerOperation#getOperand2 <em>Operand2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand2</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerOperation#getOperand2()
   * @see #getIntegerOperation()
   * @generated
   */
  EReference getIntegerOperation_Operand2();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.CallFunction <em>Call Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Function</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CallFunction
   * @generated
   */
  EClass getCallFunction();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.Ceur.Ceur.ceur.CallFunction#getOperationName <em>Operation Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operation Name</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.CallFunction#getOperationName()
   * @see #getCallFunction()
   * @generated
   */
  EReference getCallFunction_OperationName();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.FunctionName <em>Function Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Name</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.FunctionName
   * @generated
   */
  EClass getFunctionName();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.FunctionName#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.FunctionName#getValue()
   * @see #getFunctionName()
   * @generated
   */
  EAttribute getFunctionName_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR <em>BOOLOPERATOR</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>BOOLOPERATOR</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR
   * @generated
   */
  EClass getBOOLOPERATOR();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR#getAND <em>AND</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>AND</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR#getAND()
   * @see #getBOOLOPERATOR()
   * @generated
   */
  EAttribute getBOOLOPERATOR_AND();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR#getOR <em>OR</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>OR</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR#getOR()
   * @see #getBOOLOPERATOR()
   * @generated
   */
  EAttribute getBOOLOPERATOR_OR();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.Function <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Function
   * @generated
   */
  EClass getFunction();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.Function#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Function#getName()
   * @see #getFunction()
   * @generated
   */
  EAttribute getFunction_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.Ceur.Ceur.ceur.Function#getFeatures <em>Features</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Features</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Function#getFeatures()
   * @see #getFunction()
   * @generated
   */
  EReference getFunction_Features();

  /**
   * Returns the meta object for class '{@link org.xtext.Ceur.Ceur.ceur.Import <em>Import</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Import
   * @generated
   */
  EClass getImport();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.Ceur.Ceur.ceur.Import#getImportedNamespace <em>Imported Namespace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Imported Namespace</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.Import#getImportedNamespace()
   * @see #getImport()
   * @generated
   */
  EAttribute getImport_ImportedNamespace();

  /**
   * Returns the meta object for enum '{@link org.xtext.Ceur.Ceur.ceur.OPERATOR <em>OPERATOR</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>OPERATOR</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.OPERATOR
   * @generated
   */
  EEnum getOPERATOR();

  /**
   * Returns the meta object for enum '{@link org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR <em>SPECIALINTOPERATOR</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>SPECIALINTOPERATOR</em>'.
   * @see org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR
   * @generated
   */
  EEnum getSPECIALINTOPERATOR();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  CeurFactory getCeurFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.DomainmodelImpl <em>Domainmodel</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.DomainmodelImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getDomainmodel()
     * @generated
     */
    EClass DOMAINMODEL = eINSTANCE.getDomainmodel();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOMAINMODEL__ELEMENTS = eINSTANCE.getDomainmodel_Elements();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.TypeImpl <em>Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.TypeImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getType()
     * @generated
     */
    EClass TYPE = eINSTANCE.getType();

    /**
     * The meta object literal for the '<em><b>Integer</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__INTEGER = eINSTANCE.getType_Integer();

    /**
     * The meta object literal for the '<em><b>Integervarvar</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__INTEGERVARVAR = eINSTANCE.getType_Integervarvar();

    /**
     * The meta object literal for the '<em><b>Boolean</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__BOOLEAN = eINSTANCE.getType_Boolean();

    /**
     * The meta object literal for the '<em><b>Booleanvarvar</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__BOOLEANVARVAR = eINSTANCE.getType_Booleanvarvar();

    /**
     * The meta object literal for the '<em><b>Character</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__CHARACTER = eINSTANCE.getType_Character();

    /**
     * The meta object literal for the '<em><b>Charactervarvar</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__CHARACTERVARVAR = eINSTANCE.getType_Charactervarvar();

    /**
     * The meta object literal for the '<em><b>While</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__WHILE = eINSTANCE.getType_While();

    /**
     * The meta object literal for the '<em><b>If</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__IF = eINSTANCE.getType_If();

    /**
     * The meta object literal for the '<em><b>Print</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__PRINT = eINSTANCE.getType_Print();

    /**
     * The meta object literal for the '<em><b>Function</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE__FUNCTION = eINSTANCE.getType_Function();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.PrintLImpl <em>Print L</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.PrintLImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getPrintL()
     * @generated
     */
    EClass PRINT_L = eINSTANCE.getPrintL();

    /**
     * The meta object literal for the '<em><b>Print</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PRINT_L__PRINT = eINSTANCE.getPrintL_Print();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PRINT_L__VALUE = eINSTANCE.getPrintL_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.Statement_WhileImpl <em>Statement While</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.Statement_WhileImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getStatement_While()
     * @generated
     */
    EClass STATEMENT_WHILE = eINSTANCE.getStatement_While();

    /**
     * The meta object literal for the '<em><b>Cond One</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT_WHILE__COND_ONE = eINSTANCE.getStatement_While_CondOne();

    /**
     * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATEMENT_WHILE__OPERATIONS = eINSTANCE.getStatement_While_Operations();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.If_statmentImpl <em>If statment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.If_statmentImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIf_statment()
     * @generated
     */
    EClass IF_STATMENT = eINSTANCE.getIf_statment();

    /**
     * The meta object literal for the '<em><b>Cond One</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_STATMENT__COND_ONE = eINSTANCE.getIf_statment_CondOne();

    /**
     * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_STATMENT__OPERATIONS = eINSTANCE.getIf_statment_Operations();

    /**
     * The meta object literal for the '<em><b>Else</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_STATMENT__ELSE = eINSTANCE.getIf_statment_Else();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.ElseImpl <em>Else</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.ElseImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getElse()
     * @generated
     */
    EClass ELSE = eINSTANCE.getElse();

    /**
     * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ELSE__OPERATIONS = eINSTANCE.getElse_Operations();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterDeclarationAssImpl <em>Character Declaration Ass</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterDeclarationAssImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterDeclarationAss()
     * @generated
     */
    EClass CHARACTER_DECLARATION_ASS = eINSTANCE.getCharacterDeclarationAss();

    /**
     * The meta object literal for the '<em><b>Assign</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHARACTER_DECLARATION_ASS__ASSIGN = eINSTANCE.getCharacterDeclarationAss_Assign();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHARACTER_DECLARATION_ASS__VALUE = eINSTANCE.getCharacterDeclarationAss_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterDeclarationImpl <em>Character Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterDeclarationImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterDeclaration()
     * @generated
     */
    EClass CHARACTER_DECLARATION = eINSTANCE.getCharacterDeclaration();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHARACTER_DECLARATION__IDENTIFIER = eINSTANCE.getCharacterDeclaration_Identifier();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterTypeExpressionImpl <em>Character Type Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterTypeExpressionImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterTypeExpression()
     * @generated
     */
    EClass CHARACTER_TYPE_EXPRESSION = eINSTANCE.getCharacterTypeExpression();

    /**
     * The meta object literal for the '<em><b>Char Assign</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN = eINSTANCE.getCharacterTypeExpression_CharAssign();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterAssignmentImpl <em>Character Assignment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterAssignmentImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterAssignment()
     * @generated
     */
    EClass CHARACTER_ASSIGNMENT = eINSTANCE.getCharacterAssignment();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHARACTER_ASSIGNMENT__VALUE = eINSTANCE.getCharacterAssignment_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CharacterVarVarImpl <em>Character Var Var</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.CharacterVarVarImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCharacterVarVar()
     * @generated
     */
    EClass CHARACTER_VAR_VAR = eINSTANCE.getCharacterVarVar();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHARACTER_VAR_VAR__IDENTIFIER = eINSTANCE.getCharacterVarVar_Identifier();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHARACTER_VAR_VAR__VALUE = eINSTANCE.getCharacterVarVar_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanDeclarationAssImpl <em>Boolean Declaration Ass</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanDeclarationAssImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanDeclarationAss()
     * @generated
     */
    EClass BOOLEAN_DECLARATION_ASS = eINSTANCE.getBooleanDeclarationAss();

    /**
     * The meta object literal for the '<em><b>Assign</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_DECLARATION_ASS__ASSIGN = eINSTANCE.getBooleanDeclarationAss_Assign();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_DECLARATION_ASS__VALUE = eINSTANCE.getBooleanDeclarationAss_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanDeclarationImpl <em>Boolean Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanDeclarationImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanDeclaration()
     * @generated
     */
    EClass BOOLEAN_DECLARATION = eINSTANCE.getBooleanDeclaration();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_DECLARATION__IDENTIFIER = eINSTANCE.getBooleanDeclaration_Identifier();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanTypeExpressionImpl <em>Boolean Type Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanTypeExpressionImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanTypeExpression()
     * @generated
     */
    EClass BOOLEAN_TYPE_EXPRESSION = eINSTANCE.getBooleanTypeExpression();

    /**
     * The meta object literal for the '<em><b>Bool Assign</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN = eINSTANCE.getBooleanTypeExpression_BoolAssign();

    /**
     * The meta object literal for the '<em><b>Bool Op</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_TYPE_EXPRESSION__BOOL_OP = eINSTANCE.getBooleanTypeExpression_BoolOp();

    /**
     * The meta object literal for the '<em><b>Int To Bool</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL = eINSTANCE.getBooleanTypeExpression_IntToBool();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanVarVarImpl <em>Boolean Var Var</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanVarVarImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanVarVar()
     * @generated
     */
    EClass BOOLEAN_VAR_VAR = eINSTANCE.getBooleanVarVar();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_VAR_VAR__IDENTIFIER = eINSTANCE.getBooleanVarVar_Identifier();

    /**
     * The meta object literal for the '<em><b>Value1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_VAR_VAR__VALUE1 = eINSTANCE.getBooleanVarVar_Value1();

    /**
     * The meta object literal for the '<em><b>Value2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_VAR_VAR__VALUE2 = eINSTANCE.getBooleanVarVar_Value2();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanAssignmentImpl <em>Boolean Assignment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanAssignmentImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanAssignment()
     * @generated
     */
    EClass BOOLEAN_ASSIGNMENT = eINSTANCE.getBooleanAssignment();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_ASSIGNMENT__VALUE = eINSTANCE.getBooleanAssignment_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BooleanOperationImpl <em>Boolean Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.BooleanOperationImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBooleanOperation()
     * @generated
     */
    EClass BOOLEAN_OPERATION = eINSTANCE.getBooleanOperation();

    /**
     * The meta object literal for the '<em><b>Operand1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_OPERATION__OPERAND1 = eINSTANCE.getBooleanOperation_Operand1();

    /**
     * The meta object literal for the '<em><b>Operator</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_OPERATION__OPERATOR = eINSTANCE.getBooleanOperation_Operator();

    /**
     * The meta object literal for the '<em><b>Operand2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BOOLEAN_OPERATION__OPERAND2 = eINSTANCE.getBooleanOperation_Operand2();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerToBoolImpl <em>Integer To Bool</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerToBoolImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerToBool()
     * @generated
     */
    EClass INTEGER_TO_BOOL = eINSTANCE.getIntegerToBool();

    /**
     * The meta object literal for the '<em><b>Operand1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_TO_BOOL__OPERAND1 = eINSTANCE.getIntegerToBool_Operand1();

    /**
     * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_TO_BOOL__OPERATOR = eINSTANCE.getIntegerToBool_Operator();

    /**
     * The meta object literal for the '<em><b>Operand2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_TO_BOOL__OPERAND2 = eINSTANCE.getIntegerToBool_Operand2();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerDeclarationAssImpl <em>Integer Declaration Ass</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerDeclarationAssImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerDeclarationAss()
     * @generated
     */
    EClass INTEGER_DECLARATION_ASS = eINSTANCE.getIntegerDeclarationAss();

    /**
     * The meta object literal for the '<em><b>Assign</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_DECLARATION_ASS__ASSIGN = eINSTANCE.getIntegerDeclarationAss_Assign();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_DECLARATION_ASS__VALUE = eINSTANCE.getIntegerDeclarationAss_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerDeclarationImpl <em>Integer Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerDeclarationImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerDeclaration()
     * @generated
     */
    EClass INTEGER_DECLARATION = eINSTANCE.getIntegerDeclaration();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_DECLARATION__IDENTIFIER = eINSTANCE.getIntegerDeclaration_Identifier();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerVarVarImpl <em>Integer Var Var</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerVarVarImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerVarVar()
     * @generated
     */
    EClass INTEGER_VAR_VAR = eINSTANCE.getIntegerVarVar();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_VAR_VAR__IDENTIFIER = eINSTANCE.getIntegerVarVar_Identifier();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_VAR_VAR__VALUE = eINSTANCE.getIntegerVarVar_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerTypeExpressionImpl <em>Integer Type Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerTypeExpressionImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerTypeExpression()
     * @generated
     */
    EClass INTEGER_TYPE_EXPRESSION = eINSTANCE.getIntegerTypeExpression();

    /**
     * The meta object literal for the '<em><b>Integer Assign</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN = eINSTANCE.getIntegerTypeExpression_IntegerAssign();

    /**
     * The meta object literal for the '<em><b>Integer Op</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_TYPE_EXPRESSION__INTEGER_OP = eINSTANCE.getIntegerTypeExpression_IntegerOp();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerAssignmentImpl <em>Integer Assignment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerAssignmentImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerAssignment()
     * @generated
     */
    EClass INTEGER_ASSIGNMENT = eINSTANCE.getIntegerAssignment();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_ASSIGNMENT__VALUE = eINSTANCE.getIntegerAssignment_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerOperationImpl <em>Integer Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.IntegerOperationImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getIntegerOperation()
     * @generated
     */
    EClass INTEGER_OPERATION = eINSTANCE.getIntegerOperation();

    /**
     * The meta object literal for the '<em><b>Operand1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_OPERATION__OPERAND1 = eINSTANCE.getIntegerOperation_Operand1();

    /**
     * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_OPERATION__OPERATOR = eINSTANCE.getIntegerOperation_Operator();

    /**
     * The meta object literal for the '<em><b>Operand2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_OPERATION__OPERAND2 = eINSTANCE.getIntegerOperation_Operand2();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.CallFunctionImpl <em>Call Function</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.CallFunctionImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getCallFunction()
     * @generated
     */
    EClass CALL_FUNCTION = eINSTANCE.getCallFunction();

    /**
     * The meta object literal for the '<em><b>Operation Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CALL_FUNCTION__OPERATION_NAME = eINSTANCE.getCallFunction_OperationName();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.FunctionNameImpl <em>Function Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.FunctionNameImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getFunctionName()
     * @generated
     */
    EClass FUNCTION_NAME = eINSTANCE.getFunctionName();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FUNCTION_NAME__VALUE = eINSTANCE.getFunctionName_Value();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.BOOLOPERATORImpl <em>BOOLOPERATOR</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.BOOLOPERATORImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getBOOLOPERATOR()
     * @generated
     */
    EClass BOOLOPERATOR = eINSTANCE.getBOOLOPERATOR();

    /**
     * The meta object literal for the '<em><b>AND</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLOPERATOR__AND = eINSTANCE.getBOOLOPERATOR_AND();

    /**
     * The meta object literal for the '<em><b>OR</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLOPERATOR__OR = eINSTANCE.getBOOLOPERATOR_OR();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.FunctionImpl <em>Function</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.FunctionImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getFunction()
     * @generated
     */
    EClass FUNCTION = eINSTANCE.getFunction();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FUNCTION__NAME = eINSTANCE.getFunction_Name();

    /**
     * The meta object literal for the '<em><b>Features</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION__FEATURES = eINSTANCE.getFunction_Features();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.impl.ImportImpl <em>Import</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.impl.ImportImpl
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getImport()
     * @generated
     */
    EClass IMPORT = eINSTANCE.getImport();

    /**
     * The meta object literal for the '<em><b>Imported Namespace</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IMPORT__IMPORTED_NAMESPACE = eINSTANCE.getImport_ImportedNamespace();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.OPERATOR <em>OPERATOR</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.OPERATOR
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getOPERATOR()
     * @generated
     */
    EEnum OPERATOR = eINSTANCE.getOPERATOR();

    /**
     * The meta object literal for the '{@link org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR <em>SPECIALINTOPERATOR</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR
     * @see org.xtext.Ceur.Ceur.ceur.impl.CeurPackageImpl#getSPECIALINTOPERATOR()
     * @generated
     */
    EEnum SPECIALINTOPERATOR = eINSTANCE.getSPECIALINTOPERATOR();

  }

} //CeurPackage
