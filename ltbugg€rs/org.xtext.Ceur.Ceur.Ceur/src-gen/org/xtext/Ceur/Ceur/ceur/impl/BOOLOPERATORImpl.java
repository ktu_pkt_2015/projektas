/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR;
import org.xtext.Ceur.Ceur.ceur.CeurPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BOOLOPERATOR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.BOOLOPERATORImpl#getAND <em>AND</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.BOOLOPERATORImpl#getOR <em>OR</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BOOLOPERATORImpl extends MinimalEObjectImpl.Container implements BOOLOPERATOR
{
  /**
   * The default value of the '{@link #getAND() <em>AND</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAND()
   * @generated
   * @ordered
   */
  protected static final String AND_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAND() <em>AND</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAND()
   * @generated
   * @ordered
   */
  protected String and = AND_EDEFAULT;

  /**
   * The default value of the '{@link #getOR() <em>OR</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOR()
   * @generated
   * @ordered
   */
  protected static final String OR_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOR() <em>OR</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOR()
   * @generated
   * @ordered
   */
  protected String or = OR_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BOOLOPERATORImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CeurPackage.Literals.BOOLOPERATOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAND()
  {
    return and;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAND(String newAND)
  {
    String oldAND = and;
    and = newAND;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLOPERATOR__AND, oldAND, and));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOR()
  {
    return or;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOR(String newOR)
  {
    String oldOR = or;
    or = newOR;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.BOOLOPERATOR__OR, oldOR, or));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLOPERATOR__AND:
        return getAND();
      case CeurPackage.BOOLOPERATOR__OR:
        return getOR();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLOPERATOR__AND:
        setAND((String)newValue);
        return;
      case CeurPackage.BOOLOPERATOR__OR:
        setOR((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLOPERATOR__AND:
        setAND(AND_EDEFAULT);
        return;
      case CeurPackage.BOOLOPERATOR__OR:
        setOR(OR_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.BOOLOPERATOR__AND:
        return AND_EDEFAULT == null ? and != null : !AND_EDEFAULT.equals(and);
      case CeurPackage.BOOLOPERATOR__OR:
        return OR_EDEFAULT == null ? or != null : !OR_EDEFAULT.equals(or);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (AND: ");
    result.append(and);
    result.append(", OR: ");
    result.append(or);
    result.append(')');
    return result.toString();
  }

} //BOOLOPERATORImpl
