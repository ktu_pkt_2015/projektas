/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.xtext.Ceur.Ceur.ceur.BooleanAssignment;
import org.xtext.Ceur.Ceur.ceur.BooleanDeclaration;
import org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss;
import org.xtext.Ceur.Ceur.ceur.BooleanOperation;
import org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression;
import org.xtext.Ceur.Ceur.ceur.BooleanVarVar;
import org.xtext.Ceur.Ceur.ceur.CallFunction;
import org.xtext.Ceur.Ceur.ceur.CeurFactory;
import org.xtext.Ceur.Ceur.ceur.CeurPackage;
import org.xtext.Ceur.Ceur.ceur.CharacterAssignment;
import org.xtext.Ceur.Ceur.ceur.CharacterDeclaration;
import org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss;
import org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression;
import org.xtext.Ceur.Ceur.ceur.CharacterVarVar;
import org.xtext.Ceur.Ceur.ceur.Domainmodel;
import org.xtext.Ceur.Ceur.ceur.Else;
import org.xtext.Ceur.Ceur.ceur.Function;
import org.xtext.Ceur.Ceur.ceur.FunctionName;
import org.xtext.Ceur.Ceur.ceur.If_statment;
import org.xtext.Ceur.Ceur.ceur.Import;
import org.xtext.Ceur.Ceur.ceur.IntegerAssignment;
import org.xtext.Ceur.Ceur.ceur.IntegerDeclaration;
import org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss;
import org.xtext.Ceur.Ceur.ceur.IntegerOperation;
import org.xtext.Ceur.Ceur.ceur.IntegerToBool;
import org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression;
import org.xtext.Ceur.Ceur.ceur.IntegerVarVar;
import org.xtext.Ceur.Ceur.ceur.PrintL;
import org.xtext.Ceur.Ceur.ceur.Statement_While;
import org.xtext.Ceur.Ceur.ceur.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CeurPackageImpl extends EPackageImpl implements CeurPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass domainmodelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass printLEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statement_WhileEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass if_statmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass elseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass characterDeclarationAssEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass characterDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass characterTypeExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass characterAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass characterVarVarEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanDeclarationAssEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanTypeExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanVarVarEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanOperationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerToBoolEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerDeclarationAssEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerVarVarEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerTypeExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerOperationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callFunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionNameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booloperatorEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum operatorEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum specialintoperatorEEnum = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private CeurPackageImpl()
  {
    super(eNS_URI, CeurFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link CeurPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static CeurPackage init()
  {
    if (isInited) return (CeurPackage)EPackage.Registry.INSTANCE.getEPackage(CeurPackage.eNS_URI);

    // Obtain or create and register package
    CeurPackageImpl theCeurPackage = (CeurPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CeurPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CeurPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theCeurPackage.createPackageContents();

    // Initialize created meta-data
    theCeurPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theCeurPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(CeurPackage.eNS_URI, theCeurPackage);
    return theCeurPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDomainmodel()
  {
    return domainmodelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDomainmodel_Elements()
  {
    return (EReference)domainmodelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getType()
  {
    return typeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Integer()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Integervarvar()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Boolean()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Booleanvarvar()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Character()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Charactervarvar()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_While()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_If()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Print()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Function()
  {
    return (EReference)typeEClass.getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPrintL()
  {
    return printLEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPrintL_Print()
  {
    return (EAttribute)printLEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPrintL_Value()
  {
    return (EAttribute)printLEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatement_While()
  {
    return statement_WhileEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatement_While_CondOne()
  {
    return (EReference)statement_WhileEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatement_While_Operations()
  {
    return (EReference)statement_WhileEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIf_statment()
  {
    return if_statmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIf_statment_CondOne()
  {
    return (EReference)if_statmentEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIf_statment_Operations()
  {
    return (EReference)if_statmentEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIf_statment_Else()
  {
    return (EReference)if_statmentEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getElse()
  {
    return elseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getElse_Operations()
  {
    return (EReference)elseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCharacterDeclarationAss()
  {
    return characterDeclarationAssEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCharacterDeclarationAss_Assign()
  {
    return (EReference)characterDeclarationAssEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCharacterDeclarationAss_Value()
  {
    return (EReference)characterDeclarationAssEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCharacterDeclaration()
  {
    return characterDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCharacterDeclaration_Identifier()
  {
    return (EAttribute)characterDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCharacterTypeExpression()
  {
    return characterTypeExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCharacterTypeExpression_CharAssign()
  {
    return (EReference)characterTypeExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCharacterAssignment()
  {
    return characterAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCharacterAssignment_Value()
  {
    return (EAttribute)characterAssignmentEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCharacterVarVar()
  {
    return characterVarVarEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCharacterVarVar_Identifier()
  {
    return (EAttribute)characterVarVarEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCharacterVarVar_Value()
  {
    return (EAttribute)characterVarVarEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanDeclarationAss()
  {
    return booleanDeclarationAssEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanDeclarationAss_Assign()
  {
    return (EReference)booleanDeclarationAssEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanDeclarationAss_Value()
  {
    return (EReference)booleanDeclarationAssEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanDeclaration()
  {
    return booleanDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanDeclaration_Identifier()
  {
    return (EAttribute)booleanDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanTypeExpression()
  {
    return booleanTypeExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanTypeExpression_BoolAssign()
  {
    return (EReference)booleanTypeExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanTypeExpression_BoolOp()
  {
    return (EReference)booleanTypeExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanTypeExpression_IntToBool()
  {
    return (EReference)booleanTypeExpressionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanVarVar()
  {
    return booleanVarVarEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanVarVar_Identifier()
  {
    return (EAttribute)booleanVarVarEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanVarVar_Value1()
  {
    return (EReference)booleanVarVarEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanVarVar_Value2()
  {
    return (EReference)booleanVarVarEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanAssignment()
  {
    return booleanAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanAssignment_Value()
  {
    return (EAttribute)booleanAssignmentEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanOperation()
  {
    return booleanOperationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanOperation_Operand1()
  {
    return (EReference)booleanOperationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanOperation_Operator()
  {
    return (EReference)booleanOperationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBooleanOperation_Operand2()
  {
    return (EReference)booleanOperationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerToBool()
  {
    return integerToBoolEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntegerToBool_Operand1()
  {
    return (EReference)integerToBoolEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntegerToBool_Operator()
  {
    return (EAttribute)integerToBoolEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntegerToBool_Operand2()
  {
    return (EReference)integerToBoolEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerDeclarationAss()
  {
    return integerDeclarationAssEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntegerDeclarationAss_Assign()
  {
    return (EReference)integerDeclarationAssEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntegerDeclarationAss_Value()
  {
    return (EReference)integerDeclarationAssEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerDeclaration()
  {
    return integerDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntegerDeclaration_Identifier()
  {
    return (EAttribute)integerDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerVarVar()
  {
    return integerVarVarEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntegerVarVar_Identifier()
  {
    return (EAttribute)integerVarVarEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntegerVarVar_Value()
  {
    return (EReference)integerVarVarEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerTypeExpression()
  {
    return integerTypeExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntegerTypeExpression_IntegerAssign()
  {
    return (EReference)integerTypeExpressionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntegerTypeExpression_IntegerOp()
  {
    return (EReference)integerTypeExpressionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerAssignment()
  {
    return integerAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntegerAssignment_Value()
  {
    return (EAttribute)integerAssignmentEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerOperation()
  {
    return integerOperationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntegerOperation_Operand1()
  {
    return (EReference)integerOperationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntegerOperation_Operator()
  {
    return (EAttribute)integerOperationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIntegerOperation_Operand2()
  {
    return (EReference)integerOperationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallFunction()
  {
    return callFunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallFunction_OperationName()
  {
    return (EReference)callFunctionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionName()
  {
    return functionNameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunctionName_Value()
  {
    return (EAttribute)functionNameEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBOOLOPERATOR()
  {
    return booloperatorEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBOOLOPERATOR_AND()
  {
    return (EAttribute)booloperatorEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBOOLOPERATOR_OR()
  {
    return (EAttribute)booloperatorEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunction()
  {
    return functionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunction_Name()
  {
    return (EAttribute)functionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunction_Features()
  {
    return (EReference)functionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImport()
  {
    return importEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getImport_ImportedNamespace()
  {
    return (EAttribute)importEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getOPERATOR()
  {
    return operatorEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getSPECIALINTOPERATOR()
  {
    return specialintoperatorEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CeurFactory getCeurFactory()
  {
    return (CeurFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    domainmodelEClass = createEClass(DOMAINMODEL);
    createEReference(domainmodelEClass, DOMAINMODEL__ELEMENTS);

    typeEClass = createEClass(TYPE);
    createEReference(typeEClass, TYPE__INTEGER);
    createEReference(typeEClass, TYPE__INTEGERVARVAR);
    createEReference(typeEClass, TYPE__BOOLEAN);
    createEReference(typeEClass, TYPE__BOOLEANVARVAR);
    createEReference(typeEClass, TYPE__CHARACTER);
    createEReference(typeEClass, TYPE__CHARACTERVARVAR);
    createEReference(typeEClass, TYPE__WHILE);
    createEReference(typeEClass, TYPE__IF);
    createEReference(typeEClass, TYPE__PRINT);
    createEReference(typeEClass, TYPE__FUNCTION);

    printLEClass = createEClass(PRINT_L);
    createEAttribute(printLEClass, PRINT_L__PRINT);
    createEAttribute(printLEClass, PRINT_L__VALUE);

    statement_WhileEClass = createEClass(STATEMENT_WHILE);
    createEReference(statement_WhileEClass, STATEMENT_WHILE__COND_ONE);
    createEReference(statement_WhileEClass, STATEMENT_WHILE__OPERATIONS);

    if_statmentEClass = createEClass(IF_STATMENT);
    createEReference(if_statmentEClass, IF_STATMENT__COND_ONE);
    createEReference(if_statmentEClass, IF_STATMENT__OPERATIONS);
    createEReference(if_statmentEClass, IF_STATMENT__ELSE);

    elseEClass = createEClass(ELSE);
    createEReference(elseEClass, ELSE__OPERATIONS);

    characterDeclarationAssEClass = createEClass(CHARACTER_DECLARATION_ASS);
    createEReference(characterDeclarationAssEClass, CHARACTER_DECLARATION_ASS__ASSIGN);
    createEReference(characterDeclarationAssEClass, CHARACTER_DECLARATION_ASS__VALUE);

    characterDeclarationEClass = createEClass(CHARACTER_DECLARATION);
    createEAttribute(characterDeclarationEClass, CHARACTER_DECLARATION__IDENTIFIER);

    characterTypeExpressionEClass = createEClass(CHARACTER_TYPE_EXPRESSION);
    createEReference(characterTypeExpressionEClass, CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN);

    characterAssignmentEClass = createEClass(CHARACTER_ASSIGNMENT);
    createEAttribute(characterAssignmentEClass, CHARACTER_ASSIGNMENT__VALUE);

    characterVarVarEClass = createEClass(CHARACTER_VAR_VAR);
    createEAttribute(characterVarVarEClass, CHARACTER_VAR_VAR__IDENTIFIER);
    createEAttribute(characterVarVarEClass, CHARACTER_VAR_VAR__VALUE);

    booleanDeclarationAssEClass = createEClass(BOOLEAN_DECLARATION_ASS);
    createEReference(booleanDeclarationAssEClass, BOOLEAN_DECLARATION_ASS__ASSIGN);
    createEReference(booleanDeclarationAssEClass, BOOLEAN_DECLARATION_ASS__VALUE);

    booleanDeclarationEClass = createEClass(BOOLEAN_DECLARATION);
    createEAttribute(booleanDeclarationEClass, BOOLEAN_DECLARATION__IDENTIFIER);

    booleanTypeExpressionEClass = createEClass(BOOLEAN_TYPE_EXPRESSION);
    createEReference(booleanTypeExpressionEClass, BOOLEAN_TYPE_EXPRESSION__BOOL_ASSIGN);
    createEReference(booleanTypeExpressionEClass, BOOLEAN_TYPE_EXPRESSION__BOOL_OP);
    createEReference(booleanTypeExpressionEClass, BOOLEAN_TYPE_EXPRESSION__INT_TO_BOOL);

    booleanVarVarEClass = createEClass(BOOLEAN_VAR_VAR);
    createEAttribute(booleanVarVarEClass, BOOLEAN_VAR_VAR__IDENTIFIER);
    createEReference(booleanVarVarEClass, BOOLEAN_VAR_VAR__VALUE1);
    createEReference(booleanVarVarEClass, BOOLEAN_VAR_VAR__VALUE2);

    booleanAssignmentEClass = createEClass(BOOLEAN_ASSIGNMENT);
    createEAttribute(booleanAssignmentEClass, BOOLEAN_ASSIGNMENT__VALUE);

    booleanOperationEClass = createEClass(BOOLEAN_OPERATION);
    createEReference(booleanOperationEClass, BOOLEAN_OPERATION__OPERAND1);
    createEReference(booleanOperationEClass, BOOLEAN_OPERATION__OPERATOR);
    createEReference(booleanOperationEClass, BOOLEAN_OPERATION__OPERAND2);

    integerToBoolEClass = createEClass(INTEGER_TO_BOOL);
    createEReference(integerToBoolEClass, INTEGER_TO_BOOL__OPERAND1);
    createEAttribute(integerToBoolEClass, INTEGER_TO_BOOL__OPERATOR);
    createEReference(integerToBoolEClass, INTEGER_TO_BOOL__OPERAND2);

    integerDeclarationAssEClass = createEClass(INTEGER_DECLARATION_ASS);
    createEReference(integerDeclarationAssEClass, INTEGER_DECLARATION_ASS__ASSIGN);
    createEReference(integerDeclarationAssEClass, INTEGER_DECLARATION_ASS__VALUE);

    integerDeclarationEClass = createEClass(INTEGER_DECLARATION);
    createEAttribute(integerDeclarationEClass, INTEGER_DECLARATION__IDENTIFIER);

    integerVarVarEClass = createEClass(INTEGER_VAR_VAR);
    createEAttribute(integerVarVarEClass, INTEGER_VAR_VAR__IDENTIFIER);
    createEReference(integerVarVarEClass, INTEGER_VAR_VAR__VALUE);

    integerTypeExpressionEClass = createEClass(INTEGER_TYPE_EXPRESSION);
    createEReference(integerTypeExpressionEClass, INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN);
    createEReference(integerTypeExpressionEClass, INTEGER_TYPE_EXPRESSION__INTEGER_OP);

    integerAssignmentEClass = createEClass(INTEGER_ASSIGNMENT);
    createEAttribute(integerAssignmentEClass, INTEGER_ASSIGNMENT__VALUE);

    integerOperationEClass = createEClass(INTEGER_OPERATION);
    createEReference(integerOperationEClass, INTEGER_OPERATION__OPERAND1);
    createEAttribute(integerOperationEClass, INTEGER_OPERATION__OPERATOR);
    createEReference(integerOperationEClass, INTEGER_OPERATION__OPERAND2);

    callFunctionEClass = createEClass(CALL_FUNCTION);
    createEReference(callFunctionEClass, CALL_FUNCTION__OPERATION_NAME);

    functionNameEClass = createEClass(FUNCTION_NAME);
    createEAttribute(functionNameEClass, FUNCTION_NAME__VALUE);

    booloperatorEClass = createEClass(BOOLOPERATOR);
    createEAttribute(booloperatorEClass, BOOLOPERATOR__AND);
    createEAttribute(booloperatorEClass, BOOLOPERATOR__OR);

    functionEClass = createEClass(FUNCTION);
    createEAttribute(functionEClass, FUNCTION__NAME);
    createEReference(functionEClass, FUNCTION__FEATURES);

    importEClass = createEClass(IMPORT);
    createEAttribute(importEClass, IMPORT__IMPORTED_NAMESPACE);

    // Create enums
    operatorEEnum = createEEnum(OPERATOR);
    specialintoperatorEEnum = createEEnum(SPECIALINTOPERATOR);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    importEClass.getESuperTypes().add(this.getType());

    // Initialize classes and features; add operations and parameters
    initEClass(domainmodelEClass, Domainmodel.class, "Domainmodel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDomainmodel_Elements(), this.getFunction(), null, "elements", null, 0, -1, Domainmodel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(typeEClass, Type.class, "Type", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getType_Integer(), this.getIntegerDeclarationAss(), null, "integer", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getType_Integervarvar(), this.getIntegerVarVar(), null, "integervarvar", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getType_Boolean(), this.getBooleanDeclarationAss(), null, "boolean", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getType_Booleanvarvar(), this.getBooleanVarVar(), null, "booleanvarvar", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getType_Character(), this.getCharacterDeclarationAss(), null, "character", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getType_Charactervarvar(), this.getCharacterVarVar(), null, "charactervarvar", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getType_While(), this.getStatement_While(), null, "while", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getType_If(), this.getIf_statment(), null, "if", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getType_Print(), this.getPrintL(), null, "print", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getType_Function(), this.getCallFunction(), null, "function", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(printLEClass, PrintL.class, "PrintL", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getPrintL_Print(), ecorePackage.getEString(), "print", null, 0, 1, PrintL.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPrintL_Value(), ecorePackage.getEString(), "value", null, 0, 1, PrintL.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(statement_WhileEClass, Statement_While.class, "Statement_While", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getStatement_While_CondOne(), this.getBooleanTypeExpression(), null, "condOne", null, 0, 1, Statement_While.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getStatement_While_Operations(), this.getType(), null, "operations", null, 0, -1, Statement_While.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(if_statmentEClass, If_statment.class, "If_statment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIf_statment_CondOne(), this.getBooleanTypeExpression(), null, "condOne", null, 0, 1, If_statment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIf_statment_Operations(), this.getType(), null, "operations", null, 0, -1, If_statment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIf_statment_Else(), this.getElse(), null, "else", null, 0, 1, If_statment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(elseEClass, Else.class, "Else", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getElse_Operations(), this.getType(), null, "operations", null, 0, -1, Else.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(characterDeclarationAssEClass, CharacterDeclarationAss.class, "CharacterDeclarationAss", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCharacterDeclarationAss_Assign(), this.getCharacterDeclaration(), null, "assign", null, 0, 1, CharacterDeclarationAss.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCharacterDeclarationAss_Value(), this.getCharacterTypeExpression(), null, "value", null, 0, 1, CharacterDeclarationAss.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(characterDeclarationEClass, CharacterDeclaration.class, "CharacterDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCharacterDeclaration_Identifier(), ecorePackage.getEString(), "identifier", null, 0, 1, CharacterDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(characterTypeExpressionEClass, CharacterTypeExpression.class, "CharacterTypeExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCharacterTypeExpression_CharAssign(), this.getCharacterAssignment(), null, "charAssign", null, 0, 1, CharacterTypeExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(characterAssignmentEClass, CharacterAssignment.class, "CharacterAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCharacterAssignment_Value(), ecorePackage.getEString(), "value", null, 0, 1, CharacterAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(characterVarVarEClass, CharacterVarVar.class, "CharacterVarVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCharacterVarVar_Identifier(), ecorePackage.getEString(), "identifier", null, 0, 1, CharacterVarVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getCharacterVarVar_Value(), ecorePackage.getEString(), "value", null, 0, 1, CharacterVarVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanDeclarationAssEClass, BooleanDeclarationAss.class, "BooleanDeclarationAss", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBooleanDeclarationAss_Assign(), this.getBooleanDeclaration(), null, "assign", null, 0, 1, BooleanDeclarationAss.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanDeclarationAss_Value(), this.getBooleanTypeExpression(), null, "value", null, 0, 1, BooleanDeclarationAss.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanDeclarationEClass, BooleanDeclaration.class, "BooleanDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBooleanDeclaration_Identifier(), ecorePackage.getEString(), "identifier", null, 0, 1, BooleanDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanTypeExpressionEClass, BooleanTypeExpression.class, "BooleanTypeExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBooleanTypeExpression_BoolAssign(), this.getBooleanAssignment(), null, "boolAssign", null, 0, 1, BooleanTypeExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanTypeExpression_BoolOp(), this.getBooleanOperation(), null, "boolOp", null, 0, 1, BooleanTypeExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanTypeExpression_IntToBool(), this.getIntegerToBool(), null, "intToBool", null, 0, 1, BooleanTypeExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanVarVarEClass, BooleanVarVar.class, "BooleanVarVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBooleanVarVar_Identifier(), ecorePackage.getEString(), "identifier", null, 0, 1, BooleanVarVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanVarVar_Value1(), this.getBooleanOperation(), null, "value1", null, 0, 1, BooleanVarVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanVarVar_Value2(), this.getIntegerToBool(), null, "value2", null, 0, 1, BooleanVarVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanAssignmentEClass, BooleanAssignment.class, "BooleanAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBooleanAssignment_Value(), ecorePackage.getEString(), "value", null, 0, 1, BooleanAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanOperationEClass, BooleanOperation.class, "BooleanOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getBooleanOperation_Operand1(), this.getBooleanAssignment(), null, "operand1", null, 0, 1, BooleanOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanOperation_Operator(), this.getBOOLOPERATOR(), null, "operator", null, 0, 1, BooleanOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getBooleanOperation_Operand2(), this.getBooleanAssignment(), null, "operand2", null, 0, 1, BooleanOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerToBoolEClass, IntegerToBool.class, "IntegerToBool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIntegerToBool_Operand1(), this.getIntegerTypeExpression(), null, "operand1", null, 0, 1, IntegerToBool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getIntegerToBool_Operator(), this.getSPECIALINTOPERATOR(), "operator", null, 0, 1, IntegerToBool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIntegerToBool_Operand2(), this.getIntegerTypeExpression(), null, "operand2", null, 0, 1, IntegerToBool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerDeclarationAssEClass, IntegerDeclarationAss.class, "IntegerDeclarationAss", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIntegerDeclarationAss_Assign(), this.getIntegerDeclaration(), null, "assign", null, 0, 1, IntegerDeclarationAss.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIntegerDeclarationAss_Value(), this.getIntegerTypeExpression(), null, "value", null, 0, 1, IntegerDeclarationAss.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerDeclarationEClass, IntegerDeclaration.class, "IntegerDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIntegerDeclaration_Identifier(), ecorePackage.getEString(), "identifier", null, 0, 1, IntegerDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerVarVarEClass, IntegerVarVar.class, "IntegerVarVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIntegerVarVar_Identifier(), ecorePackage.getEString(), "identifier", null, 0, 1, IntegerVarVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIntegerVarVar_Value(), this.getIntegerTypeExpression(), null, "value", null, 0, 1, IntegerVarVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerTypeExpressionEClass, IntegerTypeExpression.class, "IntegerTypeExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIntegerTypeExpression_IntegerAssign(), this.getIntegerAssignment(), null, "integerAssign", null, 0, 1, IntegerTypeExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIntegerTypeExpression_IntegerOp(), this.getIntegerOperation(), null, "integerOp", null, 0, 1, IntegerTypeExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerAssignmentEClass, IntegerAssignment.class, "IntegerAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIntegerAssignment_Value(), ecorePackage.getEString(), "value", null, 0, 1, IntegerAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerOperationEClass, IntegerOperation.class, "IntegerOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIntegerOperation_Operand1(), this.getIntegerAssignment(), null, "operand1", null, 0, 1, IntegerOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getIntegerOperation_Operator(), this.getOPERATOR(), "operator", null, 0, 1, IntegerOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIntegerOperation_Operand2(), this.getIntegerAssignment(), null, "operand2", null, 0, 1, IntegerOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(callFunctionEClass, CallFunction.class, "CallFunction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCallFunction_OperationName(), this.getFunctionName(), null, "operationName", null, 0, 1, CallFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(functionNameEClass, FunctionName.class, "FunctionName", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFunctionName_Value(), ecorePackage.getEString(), "value", null, 0, 1, FunctionName.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booloperatorEClass, org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR.class, "BOOLOPERATOR", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBOOLOPERATOR_AND(), ecorePackage.getEString(), "AND", null, 0, 1, org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getBOOLOPERATOR_OR(), ecorePackage.getEString(), "OR", null, 0, 1, org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(functionEClass, Function.class, "Function", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFunction_Name(), ecorePackage.getEString(), "name", null, 0, 1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFunction_Features(), this.getType(), null, "features", null, 0, -1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(importEClass, Import.class, "Import", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getImport_ImportedNamespace(), ecorePackage.getEString(), "importedNamespace", null, 0, 1, Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Initialize enums and add enum literals
    initEEnum(operatorEEnum, org.xtext.Ceur.Ceur.ceur.OPERATOR.class, "OPERATOR");
    addEEnumLiteral(operatorEEnum, org.xtext.Ceur.Ceur.ceur.OPERATOR.ADDITION);
    addEEnumLiteral(operatorEEnum, org.xtext.Ceur.Ceur.ceur.OPERATOR.SUBRACTION);
    addEEnumLiteral(operatorEEnum, org.xtext.Ceur.Ceur.ceur.OPERATOR.MULTIPLY);
    addEEnumLiteral(operatorEEnum, org.xtext.Ceur.Ceur.ceur.OPERATOR.DIVISION);

    initEEnum(specialintoperatorEEnum, org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR.class, "SPECIALINTOPERATOR");
    addEEnumLiteral(specialintoperatorEEnum, org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR.EQUALS);
    addEEnumLiteral(specialintoperatorEEnum, org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR.LESS);
    addEEnumLiteral(specialintoperatorEEnum, org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR.MORE);

    // Create resource
    createResource(eNS_URI);
  }

} //CeurPackageImpl
