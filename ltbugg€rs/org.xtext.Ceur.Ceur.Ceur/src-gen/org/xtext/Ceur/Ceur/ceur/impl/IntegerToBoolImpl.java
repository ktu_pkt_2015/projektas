/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.Ceur.Ceur.ceur.CeurPackage;
import org.xtext.Ceur.Ceur.ceur.IntegerToBool;
import org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression;
import org.xtext.Ceur.Ceur.ceur.SPECIALINTOPERATOR;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Integer To Bool</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerToBoolImpl#getOperand1 <em>Operand1</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerToBoolImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerToBoolImpl#getOperand2 <em>Operand2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IntegerToBoolImpl extends MinimalEObjectImpl.Container implements IntegerToBool
{
  /**
   * The cached value of the '{@link #getOperand1() <em>Operand1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperand1()
   * @generated
   * @ordered
   */
  protected IntegerTypeExpression operand1;

  /**
   * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperator()
   * @generated
   * @ordered
   */
  protected static final SPECIALINTOPERATOR OPERATOR_EDEFAULT = SPECIALINTOPERATOR.EQUALS;

  /**
   * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperator()
   * @generated
   * @ordered
   */
  protected SPECIALINTOPERATOR operator = OPERATOR_EDEFAULT;

  /**
   * The cached value of the '{@link #getOperand2() <em>Operand2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOperand2()
   * @generated
   * @ordered
   */
  protected IntegerTypeExpression operand2;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IntegerToBoolImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CeurPackage.Literals.INTEGER_TO_BOOL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerTypeExpression getOperand1()
  {
    return operand1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOperand1(IntegerTypeExpression newOperand1, NotificationChain msgs)
  {
    IntegerTypeExpression oldOperand1 = operand1;
    operand1 = newOperand1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.INTEGER_TO_BOOL__OPERAND1, oldOperand1, newOperand1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOperand1(IntegerTypeExpression newOperand1)
  {
    if (newOperand1 != operand1)
    {
      NotificationChain msgs = null;
      if (operand1 != null)
        msgs = ((InternalEObject)operand1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.INTEGER_TO_BOOL__OPERAND1, null, msgs);
      if (newOperand1 != null)
        msgs = ((InternalEObject)newOperand1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.INTEGER_TO_BOOL__OPERAND1, null, msgs);
      msgs = basicSetOperand1(newOperand1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.INTEGER_TO_BOOL__OPERAND1, newOperand1, newOperand1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SPECIALINTOPERATOR getOperator()
  {
    return operator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOperator(SPECIALINTOPERATOR newOperator)
  {
    SPECIALINTOPERATOR oldOperator = operator;
    operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.INTEGER_TO_BOOL__OPERATOR, oldOperator, operator));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerTypeExpression getOperand2()
  {
    return operand2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOperand2(IntegerTypeExpression newOperand2, NotificationChain msgs)
  {
    IntegerTypeExpression oldOperand2 = operand2;
    operand2 = newOperand2;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.INTEGER_TO_BOOL__OPERAND2, oldOperand2, newOperand2);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOperand2(IntegerTypeExpression newOperand2)
  {
    if (newOperand2 != operand2)
    {
      NotificationChain msgs = null;
      if (operand2 != null)
        msgs = ((InternalEObject)operand2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.INTEGER_TO_BOOL__OPERAND2, null, msgs);
      if (newOperand2 != null)
        msgs = ((InternalEObject)newOperand2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.INTEGER_TO_BOOL__OPERAND2, null, msgs);
      msgs = basicSetOperand2(newOperand2, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.INTEGER_TO_BOOL__OPERAND2, newOperand2, newOperand2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TO_BOOL__OPERAND1:
        return basicSetOperand1(null, msgs);
      case CeurPackage.INTEGER_TO_BOOL__OPERAND2:
        return basicSetOperand2(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TO_BOOL__OPERAND1:
        return getOperand1();
      case CeurPackage.INTEGER_TO_BOOL__OPERATOR:
        return getOperator();
      case CeurPackage.INTEGER_TO_BOOL__OPERAND2:
        return getOperand2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TO_BOOL__OPERAND1:
        setOperand1((IntegerTypeExpression)newValue);
        return;
      case CeurPackage.INTEGER_TO_BOOL__OPERATOR:
        setOperator((SPECIALINTOPERATOR)newValue);
        return;
      case CeurPackage.INTEGER_TO_BOOL__OPERAND2:
        setOperand2((IntegerTypeExpression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TO_BOOL__OPERAND1:
        setOperand1((IntegerTypeExpression)null);
        return;
      case CeurPackage.INTEGER_TO_BOOL__OPERATOR:
        setOperator(OPERATOR_EDEFAULT);
        return;
      case CeurPackage.INTEGER_TO_BOOL__OPERAND2:
        setOperand2((IntegerTypeExpression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TO_BOOL__OPERAND1:
        return operand1 != null;
      case CeurPackage.INTEGER_TO_BOOL__OPERATOR:
        return operator != OPERATOR_EDEFAULT;
      case CeurPackage.INTEGER_TO_BOOL__OPERAND2:
        return operand2 != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (operator: ");
    result.append(operator);
    result.append(')');
    return result.toString();
  }

} //IntegerToBoolImpl
