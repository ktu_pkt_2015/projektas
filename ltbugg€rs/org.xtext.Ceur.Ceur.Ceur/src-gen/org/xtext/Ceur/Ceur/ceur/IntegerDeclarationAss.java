/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Declaration Ass</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss#getAssign <em>Assign</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerDeclarationAss()
 * @model
 * @generated
 */
public interface IntegerDeclarationAss extends EObject
{
  /**
   * Returns the value of the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Assign</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Assign</em>' containment reference.
   * @see #setAssign(IntegerDeclaration)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerDeclarationAss_Assign()
   * @model containment="true"
   * @generated
   */
  IntegerDeclaration getAssign();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss#getAssign <em>Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Assign</em>' containment reference.
   * @see #getAssign()
   * @generated
   */
  void setAssign(IntegerDeclaration value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(IntegerTypeExpression)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerDeclarationAss_Value()
   * @model containment="true"
   * @generated
   */
  IntegerTypeExpression getValue();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(IntegerTypeExpression value);

} // IntegerDeclarationAss
