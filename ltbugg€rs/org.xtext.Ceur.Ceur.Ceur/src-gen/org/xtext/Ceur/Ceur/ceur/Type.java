/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getInteger <em>Integer</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getIntegervarvar <em>Integervarvar</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getBoolean <em>Boolean</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getBooleanvarvar <em>Booleanvarvar</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getCharacter <em>Character</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getCharactervarvar <em>Charactervarvar</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getWhile <em>While</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getIf <em>If</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getPrint <em>Print</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.Type#getFunction <em>Function</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType()
 * @model
 * @generated
 */
public interface Type extends EObject
{
  /**
   * Returns the value of the '<em><b>Integer</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Integer</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Integer</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_Integer()
   * @model containment="true"
   * @generated
   */
  EList<IntegerDeclarationAss> getInteger();

  /**
   * Returns the value of the '<em><b>Integervarvar</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.IntegerVarVar}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Integervarvar</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Integervarvar</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_Integervarvar()
   * @model containment="true"
   * @generated
   */
  EList<IntegerVarVar> getIntegervarvar();

  /**
   * Returns the value of the '<em><b>Boolean</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Boolean</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Boolean</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_Boolean()
   * @model containment="true"
   * @generated
   */
  EList<BooleanDeclarationAss> getBoolean();

  /**
   * Returns the value of the '<em><b>Booleanvarvar</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Booleanvarvar</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Booleanvarvar</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_Booleanvarvar()
   * @model containment="true"
   * @generated
   */
  EList<BooleanVarVar> getBooleanvarvar();

  /**
   * Returns the value of the '<em><b>Character</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Character</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Character</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_Character()
   * @model containment="true"
   * @generated
   */
  EList<CharacterDeclarationAss> getCharacter();

  /**
   * Returns the value of the '<em><b>Charactervarvar</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.CharacterVarVar}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Charactervarvar</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Charactervarvar</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_Charactervarvar()
   * @model containment="true"
   * @generated
   */
  EList<CharacterVarVar> getCharactervarvar();

  /**
   * Returns the value of the '<em><b>While</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.Statement_While}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>While</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>While</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_While()
   * @model containment="true"
   * @generated
   */
  EList<Statement_While> getWhile();

  /**
   * Returns the value of the '<em><b>If</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.If_statment}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>If</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>If</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_If()
   * @model containment="true"
   * @generated
   */
  EList<If_statment> getIf();

  /**
   * Returns the value of the '<em><b>Print</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.PrintL}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Print</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Print</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_Print()
   * @model containment="true"
   * @generated
   */
  EList<PrintL> getPrint();

  /**
   * Returns the value of the '<em><b>Function</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.Ceur.Ceur.ceur.CallFunction}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' containment reference list.
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getType_Function()
   * @model containment="true"
   * @generated
   */
  EList<CallFunction> getFunction();

} // Type
