/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Character Type Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression#getCharAssign <em>Char Assign</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getCharacterTypeExpression()
 * @model
 * @generated
 */
public interface CharacterTypeExpression extends EObject
{
  /**
   * Returns the value of the '<em><b>Char Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Char Assign</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Char Assign</em>' containment reference.
   * @see #setCharAssign(CharacterAssignment)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getCharacterTypeExpression_CharAssign()
   * @model containment="true"
   * @generated
   */
  CharacterAssignment getCharAssign();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression#getCharAssign <em>Char Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Char Assign</em>' containment reference.
   * @see #getCharAssign()
   * @generated
   */
  void setCharAssign(CharacterAssignment value);

} // CharacterTypeExpression
