/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Type Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getBoolAssign <em>Bool Assign</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getBoolOp <em>Bool Op</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getIntToBool <em>Int To Bool</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanTypeExpression()
 * @model
 * @generated
 */
public interface BooleanTypeExpression extends EObject
{
  /**
   * Returns the value of the '<em><b>Bool Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bool Assign</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bool Assign</em>' containment reference.
   * @see #setBoolAssign(BooleanAssignment)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanTypeExpression_BoolAssign()
   * @model containment="true"
   * @generated
   */
  BooleanAssignment getBoolAssign();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getBoolAssign <em>Bool Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bool Assign</em>' containment reference.
   * @see #getBoolAssign()
   * @generated
   */
  void setBoolAssign(BooleanAssignment value);

  /**
   * Returns the value of the '<em><b>Bool Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bool Op</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bool Op</em>' containment reference.
   * @see #setBoolOp(BooleanOperation)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanTypeExpression_BoolOp()
   * @model containment="true"
   * @generated
   */
  BooleanOperation getBoolOp();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getBoolOp <em>Bool Op</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bool Op</em>' containment reference.
   * @see #getBoolOp()
   * @generated
   */
  void setBoolOp(BooleanOperation value);

  /**
   * Returns the value of the '<em><b>Int To Bool</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Int To Bool</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Int To Bool</em>' containment reference.
   * @see #setIntToBool(IntegerToBool)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getBooleanTypeExpression_IntToBool()
   * @model containment="true"
   * @generated
   */
  IntegerToBool getIntToBool();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression#getIntToBool <em>Int To Bool</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Int To Bool</em>' containment reference.
   * @see #getIntToBool()
   * @generated
   */
  void setIntToBool(IntegerToBool value);

} // BooleanTypeExpression
