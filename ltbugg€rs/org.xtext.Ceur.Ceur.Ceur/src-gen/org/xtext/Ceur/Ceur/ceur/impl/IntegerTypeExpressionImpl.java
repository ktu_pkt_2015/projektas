/**
 */
package org.xtext.Ceur.Ceur.ceur.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.Ceur.Ceur.ceur.CeurPackage;
import org.xtext.Ceur.Ceur.ceur.IntegerAssignment;
import org.xtext.Ceur.Ceur.ceur.IntegerOperation;
import org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Integer Type Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerTypeExpressionImpl#getIntegerAssign <em>Integer Assign</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.impl.IntegerTypeExpressionImpl#getIntegerOp <em>Integer Op</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IntegerTypeExpressionImpl extends MinimalEObjectImpl.Container implements IntegerTypeExpression
{
  /**
   * The cached value of the '{@link #getIntegerAssign() <em>Integer Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntegerAssign()
   * @generated
   * @ordered
   */
  protected IntegerAssignment integerAssign;

  /**
   * The cached value of the '{@link #getIntegerOp() <em>Integer Op</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntegerOp()
   * @generated
   * @ordered
   */
  protected IntegerOperation integerOp;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IntegerTypeExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CeurPackage.Literals.INTEGER_TYPE_EXPRESSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerAssignment getIntegerAssign()
  {
    return integerAssign;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIntegerAssign(IntegerAssignment newIntegerAssign, NotificationChain msgs)
  {
    IntegerAssignment oldIntegerAssign = integerAssign;
    integerAssign = newIntegerAssign;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN, oldIntegerAssign, newIntegerAssign);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIntegerAssign(IntegerAssignment newIntegerAssign)
  {
    if (newIntegerAssign != integerAssign)
    {
      NotificationChain msgs = null;
      if (integerAssign != null)
        msgs = ((InternalEObject)integerAssign).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN, null, msgs);
      if (newIntegerAssign != null)
        msgs = ((InternalEObject)newIntegerAssign).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN, null, msgs);
      msgs = basicSetIntegerAssign(newIntegerAssign, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN, newIntegerAssign, newIntegerAssign));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerOperation getIntegerOp()
  {
    return integerOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIntegerOp(IntegerOperation newIntegerOp, NotificationChain msgs)
  {
    IntegerOperation oldIntegerOp = integerOp;
    integerOp = newIntegerOp;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_OP, oldIntegerOp, newIntegerOp);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIntegerOp(IntegerOperation newIntegerOp)
  {
    if (newIntegerOp != integerOp)
    {
      NotificationChain msgs = null;
      if (integerOp != null)
        msgs = ((InternalEObject)integerOp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_OP, null, msgs);
      if (newIntegerOp != null)
        msgs = ((InternalEObject)newIntegerOp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_OP, null, msgs);
      msgs = basicSetIntegerOp(newIntegerOp, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_OP, newIntegerOp, newIntegerOp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN:
        return basicSetIntegerAssign(null, msgs);
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_OP:
        return basicSetIntegerOp(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN:
        return getIntegerAssign();
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_OP:
        return getIntegerOp();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN:
        setIntegerAssign((IntegerAssignment)newValue);
        return;
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_OP:
        setIntegerOp((IntegerOperation)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN:
        setIntegerAssign((IntegerAssignment)null);
        return;
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_OP:
        setIntegerOp((IntegerOperation)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_ASSIGN:
        return integerAssign != null;
      case CeurPackage.INTEGER_TYPE_EXPRESSION__INTEGER_OP:
        return integerOp != null;
    }
    return super.eIsSet(featureID);
  }

} //IntegerTypeExpressionImpl
