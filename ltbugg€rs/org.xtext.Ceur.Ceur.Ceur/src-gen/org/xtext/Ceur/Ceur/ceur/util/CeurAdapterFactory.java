/**
 */
package org.xtext.Ceur.Ceur.ceur.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.xtext.Ceur.Ceur.ceur.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage
 * @generated
 */
public class CeurAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static CeurPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CeurAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = CeurPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CeurSwitch<Adapter> modelSwitch =
    new CeurSwitch<Adapter>()
    {
      @Override
      public Adapter caseDomainmodel(Domainmodel object)
      {
        return createDomainmodelAdapter();
      }
      @Override
      public Adapter caseType(Type object)
      {
        return createTypeAdapter();
      }
      @Override
      public Adapter casePrintL(PrintL object)
      {
        return createPrintLAdapter();
      }
      @Override
      public Adapter caseStatement_While(Statement_While object)
      {
        return createStatement_WhileAdapter();
      }
      @Override
      public Adapter caseIf_statment(If_statment object)
      {
        return createIf_statmentAdapter();
      }
      @Override
      public Adapter caseElse(Else object)
      {
        return createElseAdapter();
      }
      @Override
      public Adapter caseCharacterDeclarationAss(CharacterDeclarationAss object)
      {
        return createCharacterDeclarationAssAdapter();
      }
      @Override
      public Adapter caseCharacterDeclaration(CharacterDeclaration object)
      {
        return createCharacterDeclarationAdapter();
      }
      @Override
      public Adapter caseCharacterTypeExpression(CharacterTypeExpression object)
      {
        return createCharacterTypeExpressionAdapter();
      }
      @Override
      public Adapter caseCharacterAssignment(CharacterAssignment object)
      {
        return createCharacterAssignmentAdapter();
      }
      @Override
      public Adapter caseCharacterVarVar(CharacterVarVar object)
      {
        return createCharacterVarVarAdapter();
      }
      @Override
      public Adapter caseBooleanDeclarationAss(BooleanDeclarationAss object)
      {
        return createBooleanDeclarationAssAdapter();
      }
      @Override
      public Adapter caseBooleanDeclaration(BooleanDeclaration object)
      {
        return createBooleanDeclarationAdapter();
      }
      @Override
      public Adapter caseBooleanTypeExpression(BooleanTypeExpression object)
      {
        return createBooleanTypeExpressionAdapter();
      }
      @Override
      public Adapter caseBooleanVarVar(BooleanVarVar object)
      {
        return createBooleanVarVarAdapter();
      }
      @Override
      public Adapter caseBooleanAssignment(BooleanAssignment object)
      {
        return createBooleanAssignmentAdapter();
      }
      @Override
      public Adapter caseBooleanOperation(BooleanOperation object)
      {
        return createBooleanOperationAdapter();
      }
      @Override
      public Adapter caseIntegerToBool(IntegerToBool object)
      {
        return createIntegerToBoolAdapter();
      }
      @Override
      public Adapter caseIntegerDeclarationAss(IntegerDeclarationAss object)
      {
        return createIntegerDeclarationAssAdapter();
      }
      @Override
      public Adapter caseIntegerDeclaration(IntegerDeclaration object)
      {
        return createIntegerDeclarationAdapter();
      }
      @Override
      public Adapter caseIntegerVarVar(IntegerVarVar object)
      {
        return createIntegerVarVarAdapter();
      }
      @Override
      public Adapter caseIntegerTypeExpression(IntegerTypeExpression object)
      {
        return createIntegerTypeExpressionAdapter();
      }
      @Override
      public Adapter caseIntegerAssignment(IntegerAssignment object)
      {
        return createIntegerAssignmentAdapter();
      }
      @Override
      public Adapter caseIntegerOperation(IntegerOperation object)
      {
        return createIntegerOperationAdapter();
      }
      @Override
      public Adapter caseCallFunction(CallFunction object)
      {
        return createCallFunctionAdapter();
      }
      @Override
      public Adapter caseFunctionName(FunctionName object)
      {
        return createFunctionNameAdapter();
      }
      @Override
      public Adapter caseBOOLOPERATOR(BOOLOPERATOR object)
      {
        return createBOOLOPERATORAdapter();
      }
      @Override
      public Adapter caseFunction(Function object)
      {
        return createFunctionAdapter();
      }
      @Override
      public Adapter caseImport(Import object)
      {
        return createImportAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.Domainmodel <em>Domainmodel</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.Domainmodel
   * @generated
   */
  public Adapter createDomainmodelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.Type
   * @generated
   */
  public Adapter createTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.PrintL <em>Print L</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.PrintL
   * @generated
   */
  public Adapter createPrintLAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.Statement_While <em>Statement While</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.Statement_While
   * @generated
   */
  public Adapter createStatement_WhileAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.If_statment <em>If statment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.If_statment
   * @generated
   */
  public Adapter createIf_statmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.Else <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.Else
   * @generated
   */
  public Adapter createElseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss <em>Character Declaration Ass</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss
   * @generated
   */
  public Adapter createCharacterDeclarationAssAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.CharacterDeclaration <em>Character Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterDeclaration
   * @generated
   */
  public Adapter createCharacterDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression <em>Character Type Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression
   * @generated
   */
  public Adapter createCharacterTypeExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.CharacterAssignment <em>Character Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterAssignment
   * @generated
   */
  public Adapter createCharacterAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.CharacterVarVar <em>Character Var Var</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.CharacterVarVar
   * @generated
   */
  public Adapter createCharacterVarVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss <em>Boolean Declaration Ass</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss
   * @generated
   */
  public Adapter createBooleanDeclarationAssAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.BooleanDeclaration <em>Boolean Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanDeclaration
   * @generated
   */
  public Adapter createBooleanDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression <em>Boolean Type Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression
   * @generated
   */
  public Adapter createBooleanTypeExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.BooleanVarVar <em>Boolean Var Var</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanVarVar
   * @generated
   */
  public Adapter createBooleanVarVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.BooleanAssignment <em>Boolean Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanAssignment
   * @generated
   */
  public Adapter createBooleanAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.BooleanOperation <em>Boolean Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.BooleanOperation
   * @generated
   */
  public Adapter createBooleanOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.IntegerToBool <em>Integer To Bool</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerToBool
   * @generated
   */
  public Adapter createIntegerToBoolAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss <em>Integer Declaration Ass</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss
   * @generated
   */
  public Adapter createIntegerDeclarationAssAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.IntegerDeclaration <em>Integer Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerDeclaration
   * @generated
   */
  public Adapter createIntegerDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.IntegerVarVar <em>Integer Var Var</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerVarVar
   * @generated
   */
  public Adapter createIntegerVarVarAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression <em>Integer Type Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression
   * @generated
   */
  public Adapter createIntegerTypeExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.IntegerAssignment <em>Integer Assignment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerAssignment
   * @generated
   */
  public Adapter createIntegerAssignmentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.IntegerOperation <em>Integer Operation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.IntegerOperation
   * @generated
   */
  public Adapter createIntegerOperationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.CallFunction <em>Call Function</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.CallFunction
   * @generated
   */
  public Adapter createCallFunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.FunctionName <em>Function Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.FunctionName
   * @generated
   */
  public Adapter createFunctionNameAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR <em>BOOLOPERATOR</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR
   * @generated
   */
  public Adapter createBOOLOPERATORAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.Function <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.Function
   * @generated
   */
  public Adapter createFunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.xtext.Ceur.Ceur.ceur.Import <em>Import</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.xtext.Ceur.Ceur.ceur.Import
   * @generated
   */
  public Adapter createImportAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //CeurAdapterFactory
