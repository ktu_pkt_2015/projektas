/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Type Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression#getIntegerAssign <em>Integer Assign</em>}</li>
 *   <li>{@link org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression#getIntegerOp <em>Integer Op</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerTypeExpression()
 * @model
 * @generated
 */
public interface IntegerTypeExpression extends EObject
{
  /**
   * Returns the value of the '<em><b>Integer Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Integer Assign</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Integer Assign</em>' containment reference.
   * @see #setIntegerAssign(IntegerAssignment)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerTypeExpression_IntegerAssign()
   * @model containment="true"
   * @generated
   */
  IntegerAssignment getIntegerAssign();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression#getIntegerAssign <em>Integer Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Integer Assign</em>' containment reference.
   * @see #getIntegerAssign()
   * @generated
   */
  void setIntegerAssign(IntegerAssignment value);

  /**
   * Returns the value of the '<em><b>Integer Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Integer Op</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Integer Op</em>' containment reference.
   * @see #setIntegerOp(IntegerOperation)
   * @see org.xtext.Ceur.Ceur.ceur.CeurPackage#getIntegerTypeExpression_IntegerOp()
   * @model containment="true"
   * @generated
   */
  IntegerOperation getIntegerOp();

  /**
   * Sets the value of the '{@link org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression#getIntegerOp <em>Integer Op</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Integer Op</em>' containment reference.
   * @see #getIntegerOp()
   * @generated
   */
  void setIntegerOp(IntegerOperation value);

} // IntegerTypeExpression
