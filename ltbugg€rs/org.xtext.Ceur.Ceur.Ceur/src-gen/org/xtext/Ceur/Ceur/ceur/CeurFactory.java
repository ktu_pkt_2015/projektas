/**
 */
package org.xtext.Ceur.Ceur.ceur;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.xtext.Ceur.Ceur.ceur.CeurPackage
 * @generated
 */
public interface CeurFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  CeurFactory eINSTANCE = org.xtext.Ceur.Ceur.ceur.impl.CeurFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Domainmodel</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Domainmodel</em>'.
   * @generated
   */
  Domainmodel createDomainmodel();

  /**
   * Returns a new object of class '<em>Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type</em>'.
   * @generated
   */
  Type createType();

  /**
   * Returns a new object of class '<em>Print L</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Print L</em>'.
   * @generated
   */
  PrintL createPrintL();

  /**
   * Returns a new object of class '<em>Statement While</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement While</em>'.
   * @generated
   */
  Statement_While createStatement_While();

  /**
   * Returns a new object of class '<em>If statment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>If statment</em>'.
   * @generated
   */
  If_statment createIf_statment();

  /**
   * Returns a new object of class '<em>Else</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Else</em>'.
   * @generated
   */
  Else createElse();

  /**
   * Returns a new object of class '<em>Character Declaration Ass</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Character Declaration Ass</em>'.
   * @generated
   */
  CharacterDeclarationAss createCharacterDeclarationAss();

  /**
   * Returns a new object of class '<em>Character Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Character Declaration</em>'.
   * @generated
   */
  CharacterDeclaration createCharacterDeclaration();

  /**
   * Returns a new object of class '<em>Character Type Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Character Type Expression</em>'.
   * @generated
   */
  CharacterTypeExpression createCharacterTypeExpression();

  /**
   * Returns a new object of class '<em>Character Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Character Assignment</em>'.
   * @generated
   */
  CharacterAssignment createCharacterAssignment();

  /**
   * Returns a new object of class '<em>Character Var Var</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Character Var Var</em>'.
   * @generated
   */
  CharacterVarVar createCharacterVarVar();

  /**
   * Returns a new object of class '<em>Boolean Declaration Ass</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Declaration Ass</em>'.
   * @generated
   */
  BooleanDeclarationAss createBooleanDeclarationAss();

  /**
   * Returns a new object of class '<em>Boolean Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Declaration</em>'.
   * @generated
   */
  BooleanDeclaration createBooleanDeclaration();

  /**
   * Returns a new object of class '<em>Boolean Type Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Type Expression</em>'.
   * @generated
   */
  BooleanTypeExpression createBooleanTypeExpression();

  /**
   * Returns a new object of class '<em>Boolean Var Var</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Var Var</em>'.
   * @generated
   */
  BooleanVarVar createBooleanVarVar();

  /**
   * Returns a new object of class '<em>Boolean Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Assignment</em>'.
   * @generated
   */
  BooleanAssignment createBooleanAssignment();

  /**
   * Returns a new object of class '<em>Boolean Operation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Operation</em>'.
   * @generated
   */
  BooleanOperation createBooleanOperation();

  /**
   * Returns a new object of class '<em>Integer To Bool</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer To Bool</em>'.
   * @generated
   */
  IntegerToBool createIntegerToBool();

  /**
   * Returns a new object of class '<em>Integer Declaration Ass</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Declaration Ass</em>'.
   * @generated
   */
  IntegerDeclarationAss createIntegerDeclarationAss();

  /**
   * Returns a new object of class '<em>Integer Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Declaration</em>'.
   * @generated
   */
  IntegerDeclaration createIntegerDeclaration();

  /**
   * Returns a new object of class '<em>Integer Var Var</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Var Var</em>'.
   * @generated
   */
  IntegerVarVar createIntegerVarVar();

  /**
   * Returns a new object of class '<em>Integer Type Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Type Expression</em>'.
   * @generated
   */
  IntegerTypeExpression createIntegerTypeExpression();

  /**
   * Returns a new object of class '<em>Integer Assignment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Assignment</em>'.
   * @generated
   */
  IntegerAssignment createIntegerAssignment();

  /**
   * Returns a new object of class '<em>Integer Operation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Operation</em>'.
   * @generated
   */
  IntegerOperation createIntegerOperation();

  /**
   * Returns a new object of class '<em>Call Function</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Function</em>'.
   * @generated
   */
  CallFunction createCallFunction();

  /**
   * Returns a new object of class '<em>Function Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function Name</em>'.
   * @generated
   */
  FunctionName createFunctionName();

  /**
   * Returns a new object of class '<em>BOOLOPERATOR</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>BOOLOPERATOR</em>'.
   * @generated
   */
  BOOLOPERATOR createBOOLOPERATOR();

  /**
   * Returns a new object of class '<em>Function</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Function</em>'.
   * @generated
   */
  Function createFunction();

  /**
   * Returns a new object of class '<em>Import</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import</em>'.
   * @generated
   */
  Import createImport();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  CeurPackage getCeurPackage();

} //CeurFactory
