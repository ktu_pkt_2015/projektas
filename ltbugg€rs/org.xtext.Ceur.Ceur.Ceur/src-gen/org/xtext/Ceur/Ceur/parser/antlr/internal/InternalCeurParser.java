package org.xtext.Ceur.Ceur.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.Ceur.Ceur.services.CeurGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCeurParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_CHARACTERS", "RULE_INTEGERS", "RULE_BOOLVAL", "RULE_ID", "RULE_DIGIT", "RULE_ZERO", "RULE_NUMBERS", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'spausdinti'", "'('", "')'", "'kol'", "'{'", "'}'", "'jei'", "'kitaip'", "'='", "'char'", "'bool'", "'int'", "'\\u20AC'", "'ir'", "'arba'", "'void'", "'import'", "'.*'", "'.'", "'+'", "'-'", "'*'", "'/'", "'=='", "'<'", "'>'"
    };
    public static final int RULE_STRING=12;
    public static final int RULE_SL_COMMENT=14;
    public static final int T__19=19;
    public static final int T__37=37;
    public static final int RULE_NUMBERS=10;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int RULE_BOOLVAL=6;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=7;
    public static final int RULE_WS=15;
    public static final int RULE_ZERO=9;
    public static final int RULE_DIGIT=8;
    public static final int RULE_ANY_OTHER=16;
    public static final int RULE_INTEGERS=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=11;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=13;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int RULE_CHARACTERS=4;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__42=42;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalCeurParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCeurParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCeurParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g"; }



     	private CeurGrammarAccess grammarAccess;
     	
        public InternalCeurParser(TokenStream input, CeurGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Domainmodel";	
       	}
       	
       	@Override
       	protected CeurGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleDomainmodel"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:68:1: entryRuleDomainmodel returns [EObject current=null] : iv_ruleDomainmodel= ruleDomainmodel EOF ;
    public final EObject entryRuleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainmodel = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:69:2: (iv_ruleDomainmodel= ruleDomainmodel EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:70:2: iv_ruleDomainmodel= ruleDomainmodel EOF
            {
             newCompositeNode(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel75);
            iv_ruleDomainmodel=ruleDomainmodel();

            state._fsp--;

             current =iv_ruleDomainmodel; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDomainmodel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:77:1: ruleDomainmodel returns [EObject current=null] : ( (lv_elements_0_0= ruleFunction ) )* ;
    public final EObject ruleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject lv_elements_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:80:28: ( ( (lv_elements_0_0= ruleFunction ) )* )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:81:1: ( (lv_elements_0_0= ruleFunction ) )*
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:81:1: ( (lv_elements_0_0= ruleFunction ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==32) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:82:1: (lv_elements_0_0= ruleFunction )
            	    {
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:82:1: (lv_elements_0_0= ruleFunction )
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:83:3: lv_elements_0_0= ruleFunction
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDomainmodelAccess().getElementsFunctionParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleFunction_in_ruleDomainmodel130);
            	    lv_elements_0_0=ruleFunction();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDomainmodelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"elements",
            	            		lv_elements_0_0, 
            	            		"Function");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleType"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:107:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:108:2: (iv_ruleType= ruleType EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:109:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_ruleType_in_entryRuleType166);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleType176); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:116:1: ruleType returns [EObject current=null] : ( ( (lv_integer_0_0= ruleIntegerDeclarationAss ) ) | ( (lv_integervarvar_1_0= ruleIntegerVarVar ) ) | ( (lv_boolean_2_0= ruleBooleanDeclarationAss ) ) | ( (lv_booleanvarvar_3_0= ruleBooleanVarVar ) ) | ( (lv_character_4_0= ruleCharacterDeclarationAss ) ) | ( (lv_charactervarvar_5_0= ruleCharacterVarVar ) ) | ( (lv_while_6_0= ruleStatement_While ) ) | ( (lv_if_7_0= ruleIf_statment ) ) | ( (lv_print_8_0= rulePrintL ) ) | ( (lv_function_9_0= ruleCallFunction ) ) | this_Import_10= ruleImport ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject lv_integer_0_0 = null;

        EObject lv_integervarvar_1_0 = null;

        EObject lv_boolean_2_0 = null;

        EObject lv_booleanvarvar_3_0 = null;

        EObject lv_character_4_0 = null;

        EObject lv_charactervarvar_5_0 = null;

        EObject lv_while_6_0 = null;

        EObject lv_if_7_0 = null;

        EObject lv_print_8_0 = null;

        EObject lv_function_9_0 = null;

        EObject this_Import_10 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:119:28: ( ( ( (lv_integer_0_0= ruleIntegerDeclarationAss ) ) | ( (lv_integervarvar_1_0= ruleIntegerVarVar ) ) | ( (lv_boolean_2_0= ruleBooleanDeclarationAss ) ) | ( (lv_booleanvarvar_3_0= ruleBooleanVarVar ) ) | ( (lv_character_4_0= ruleCharacterDeclarationAss ) ) | ( (lv_charactervarvar_5_0= ruleCharacterVarVar ) ) | ( (lv_while_6_0= ruleStatement_While ) ) | ( (lv_if_7_0= ruleIf_statment ) ) | ( (lv_print_8_0= rulePrintL ) ) | ( (lv_function_9_0= ruleCallFunction ) ) | this_Import_10= ruleImport ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:120:1: ( ( (lv_integer_0_0= ruleIntegerDeclarationAss ) ) | ( (lv_integervarvar_1_0= ruleIntegerVarVar ) ) | ( (lv_boolean_2_0= ruleBooleanDeclarationAss ) ) | ( (lv_booleanvarvar_3_0= ruleBooleanVarVar ) ) | ( (lv_character_4_0= ruleCharacterDeclarationAss ) ) | ( (lv_charactervarvar_5_0= ruleCharacterVarVar ) ) | ( (lv_while_6_0= ruleStatement_While ) ) | ( (lv_if_7_0= ruleIf_statment ) ) | ( (lv_print_8_0= rulePrintL ) ) | ( (lv_function_9_0= ruleCallFunction ) ) | this_Import_10= ruleImport )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:120:1: ( ( (lv_integer_0_0= ruleIntegerDeclarationAss ) ) | ( (lv_integervarvar_1_0= ruleIntegerVarVar ) ) | ( (lv_boolean_2_0= ruleBooleanDeclarationAss ) ) | ( (lv_booleanvarvar_3_0= ruleBooleanVarVar ) ) | ( (lv_character_4_0= ruleCharacterDeclarationAss ) ) | ( (lv_charactervarvar_5_0= ruleCharacterVarVar ) ) | ( (lv_while_6_0= ruleStatement_While ) ) | ( (lv_if_7_0= ruleIf_statment ) ) | ( (lv_print_8_0= rulePrintL ) ) | ( (lv_function_9_0= ruleCallFunction ) ) | this_Import_10= ruleImport )
            int alt2=11;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:120:2: ( (lv_integer_0_0= ruleIntegerDeclarationAss ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:120:2: ( (lv_integer_0_0= ruleIntegerDeclarationAss ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:121:1: (lv_integer_0_0= ruleIntegerDeclarationAss )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:121:1: (lv_integer_0_0= ruleIntegerDeclarationAss )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:122:3: lv_integer_0_0= ruleIntegerDeclarationAss
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getIntegerIntegerDeclarationAssParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleIntegerDeclarationAss_in_ruleType222);
                    lv_integer_0_0=ruleIntegerDeclarationAss();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"integer",
                            		lv_integer_0_0, 
                            		"IntegerDeclarationAss");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:139:6: ( (lv_integervarvar_1_0= ruleIntegerVarVar ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:139:6: ( (lv_integervarvar_1_0= ruleIntegerVarVar ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:140:1: (lv_integervarvar_1_0= ruleIntegerVarVar )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:140:1: (lv_integervarvar_1_0= ruleIntegerVarVar )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:141:3: lv_integervarvar_1_0= ruleIntegerVarVar
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getIntegervarvarIntegerVarVarParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleIntegerVarVar_in_ruleType249);
                    lv_integervarvar_1_0=ruleIntegerVarVar();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"integervarvar",
                            		lv_integervarvar_1_0, 
                            		"IntegerVarVar");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:158:6: ( (lv_boolean_2_0= ruleBooleanDeclarationAss ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:158:6: ( (lv_boolean_2_0= ruleBooleanDeclarationAss ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:159:1: (lv_boolean_2_0= ruleBooleanDeclarationAss )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:159:1: (lv_boolean_2_0= ruleBooleanDeclarationAss )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:160:3: lv_boolean_2_0= ruleBooleanDeclarationAss
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getBooleanBooleanDeclarationAssParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleBooleanDeclarationAss_in_ruleType276);
                    lv_boolean_2_0=ruleBooleanDeclarationAss();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"boolean",
                            		lv_boolean_2_0, 
                            		"BooleanDeclarationAss");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:177:6: ( (lv_booleanvarvar_3_0= ruleBooleanVarVar ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:177:6: ( (lv_booleanvarvar_3_0= ruleBooleanVarVar ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:178:1: (lv_booleanvarvar_3_0= ruleBooleanVarVar )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:178:1: (lv_booleanvarvar_3_0= ruleBooleanVarVar )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:179:3: lv_booleanvarvar_3_0= ruleBooleanVarVar
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getBooleanvarvarBooleanVarVarParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleBooleanVarVar_in_ruleType303);
                    lv_booleanvarvar_3_0=ruleBooleanVarVar();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"booleanvarvar",
                            		lv_booleanvarvar_3_0, 
                            		"BooleanVarVar");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:196:6: ( (lv_character_4_0= ruleCharacterDeclarationAss ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:196:6: ( (lv_character_4_0= ruleCharacterDeclarationAss ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:197:1: (lv_character_4_0= ruleCharacterDeclarationAss )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:197:1: (lv_character_4_0= ruleCharacterDeclarationAss )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:198:3: lv_character_4_0= ruleCharacterDeclarationAss
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getCharacterCharacterDeclarationAssParserRuleCall_4_0()); 
                    	    
                    pushFollow(FOLLOW_ruleCharacterDeclarationAss_in_ruleType330);
                    lv_character_4_0=ruleCharacterDeclarationAss();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"character",
                            		lv_character_4_0, 
                            		"CharacterDeclarationAss");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 6 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:215:6: ( (lv_charactervarvar_5_0= ruleCharacterVarVar ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:215:6: ( (lv_charactervarvar_5_0= ruleCharacterVarVar ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:216:1: (lv_charactervarvar_5_0= ruleCharacterVarVar )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:216:1: (lv_charactervarvar_5_0= ruleCharacterVarVar )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:217:3: lv_charactervarvar_5_0= ruleCharacterVarVar
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getCharactervarvarCharacterVarVarParserRuleCall_5_0()); 
                    	    
                    pushFollow(FOLLOW_ruleCharacterVarVar_in_ruleType357);
                    lv_charactervarvar_5_0=ruleCharacterVarVar();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"charactervarvar",
                            		lv_charactervarvar_5_0, 
                            		"CharacterVarVar");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 7 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:234:6: ( (lv_while_6_0= ruleStatement_While ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:234:6: ( (lv_while_6_0= ruleStatement_While ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:235:1: (lv_while_6_0= ruleStatement_While )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:235:1: (lv_while_6_0= ruleStatement_While )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:236:3: lv_while_6_0= ruleStatement_While
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getWhileStatement_WhileParserRuleCall_6_0()); 
                    	    
                    pushFollow(FOLLOW_ruleStatement_While_in_ruleType384);
                    lv_while_6_0=ruleStatement_While();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"while",
                            		lv_while_6_0, 
                            		"Statement_While");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 8 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:253:6: ( (lv_if_7_0= ruleIf_statment ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:253:6: ( (lv_if_7_0= ruleIf_statment ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:254:1: (lv_if_7_0= ruleIf_statment )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:254:1: (lv_if_7_0= ruleIf_statment )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:255:3: lv_if_7_0= ruleIf_statment
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getIfIf_statmentParserRuleCall_7_0()); 
                    	    
                    pushFollow(FOLLOW_ruleIf_statment_in_ruleType411);
                    lv_if_7_0=ruleIf_statment();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"if",
                            		lv_if_7_0, 
                            		"If_statment");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 9 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:272:6: ( (lv_print_8_0= rulePrintL ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:272:6: ( (lv_print_8_0= rulePrintL ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:273:1: (lv_print_8_0= rulePrintL )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:273:1: (lv_print_8_0= rulePrintL )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:274:3: lv_print_8_0= rulePrintL
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getPrintPrintLParserRuleCall_8_0()); 
                    	    
                    pushFollow(FOLLOW_rulePrintL_in_ruleType438);
                    lv_print_8_0=rulePrintL();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"print",
                            		lv_print_8_0, 
                            		"PrintL");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 10 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:291:6: ( (lv_function_9_0= ruleCallFunction ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:291:6: ( (lv_function_9_0= ruleCallFunction ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:292:1: (lv_function_9_0= ruleCallFunction )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:292:1: (lv_function_9_0= ruleCallFunction )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:293:3: lv_function_9_0= ruleCallFunction
                    {
                     
                    	        newCompositeNode(grammarAccess.getTypeAccess().getFunctionCallFunctionParserRuleCall_9_0()); 
                    	    
                    pushFollow(FOLLOW_ruleCallFunction_in_ruleType465);
                    lv_function_9_0=ruleCallFunction();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTypeRule());
                    	        }
                           		add(
                           			current, 
                           			"function",
                            		lv_function_9_0, 
                            		"CallFunction");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 11 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:311:5: this_Import_10= ruleImport
                    {
                     
                            newCompositeNode(grammarAccess.getTypeAccess().getImportParserRuleCall_10()); 
                        
                    pushFollow(FOLLOW_ruleImport_in_ruleType493);
                    this_Import_10=ruleImport();

                    state._fsp--;

                     
                            current = this_Import_10; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRulePrintL"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:327:1: entryRulePrintL returns [EObject current=null] : iv_rulePrintL= rulePrintL EOF ;
    public final EObject entryRulePrintL() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrintL = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:328:2: (iv_rulePrintL= rulePrintL EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:329:2: iv_rulePrintL= rulePrintL EOF
            {
             newCompositeNode(grammarAccess.getPrintLRule()); 
            pushFollow(FOLLOW_rulePrintL_in_entryRulePrintL528);
            iv_rulePrintL=rulePrintL();

            state._fsp--;

             current =iv_rulePrintL; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrintL538); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrintL"


    // $ANTLR start "rulePrintL"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:336:1: rulePrintL returns [EObject current=null] : ( ( (lv_print_0_0= 'spausdinti' ) ) otherlv_1= '(' ( ( (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName ) ) ) otherlv_3= ')' ) ;
    public final EObject rulePrintL() throws RecognitionException {
        EObject current = null;

        Token lv_print_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_1=null;
        Token lv_value_2_2=null;
        Token lv_value_2_3=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_value_2_4 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:339:28: ( ( ( (lv_print_0_0= 'spausdinti' ) ) otherlv_1= '(' ( ( (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName ) ) ) otherlv_3= ')' ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:340:1: ( ( (lv_print_0_0= 'spausdinti' ) ) otherlv_1= '(' ( ( (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName ) ) ) otherlv_3= ')' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:340:1: ( ( (lv_print_0_0= 'spausdinti' ) ) otherlv_1= '(' ( ( (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName ) ) ) otherlv_3= ')' )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:340:2: ( (lv_print_0_0= 'spausdinti' ) ) otherlv_1= '(' ( ( (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName ) ) ) otherlv_3= ')'
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:340:2: ( (lv_print_0_0= 'spausdinti' ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:341:1: (lv_print_0_0= 'spausdinti' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:341:1: (lv_print_0_0= 'spausdinti' )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:342:3: lv_print_0_0= 'spausdinti'
            {
            lv_print_0_0=(Token)match(input,17,FOLLOW_17_in_rulePrintL581); 

                    newLeafNode(lv_print_0_0, grammarAccess.getPrintLAccess().getPrintSpausdintiKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPrintLRule());
            	        }
                   		setWithLastConsumed(current, "print", lv_print_0_0, "spausdinti");
            	    

            }


            }

            otherlv_1=(Token)match(input,18,FOLLOW_18_in_rulePrintL606); 

                	newLeafNode(otherlv_1, grammarAccess.getPrintLAccess().getLeftParenthesisKeyword_1());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:359:1: ( ( (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:360:1: ( (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:360:1: ( (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:361:1: (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:361:1: (lv_value_2_1= RULE_CHARACTERS | lv_value_2_2= RULE_INTEGERS | lv_value_2_3= RULE_BOOLVAL | lv_value_2_4= ruleVariableName )
            int alt3=4;
            switch ( input.LA(1) ) {
            case RULE_CHARACTERS:
                {
                alt3=1;
                }
                break;
            case RULE_INTEGERS:
                {
                alt3=2;
                }
                break;
            case RULE_BOOLVAL:
                {
                alt3=3;
                }
                break;
            case 29:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:362:3: lv_value_2_1= RULE_CHARACTERS
                    {
                    lv_value_2_1=(Token)match(input,RULE_CHARACTERS,FOLLOW_RULE_CHARACTERS_in_rulePrintL625); 

                    			newLeafNode(lv_value_2_1, grammarAccess.getPrintLAccess().getValueCHARACTERSTerminalRuleCall_2_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPrintLRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_2_1, 
                            		"CHARACTERS");
                    	    

                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:377:8: lv_value_2_2= RULE_INTEGERS
                    {
                    lv_value_2_2=(Token)match(input,RULE_INTEGERS,FOLLOW_RULE_INTEGERS_in_rulePrintL645); 

                    			newLeafNode(lv_value_2_2, grammarAccess.getPrintLAccess().getValueINTEGERSTerminalRuleCall_2_0_1()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPrintLRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_2_2, 
                            		"INTEGERS");
                    	    

                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:392:8: lv_value_2_3= RULE_BOOLVAL
                    {
                    lv_value_2_3=(Token)match(input,RULE_BOOLVAL,FOLLOW_RULE_BOOLVAL_in_rulePrintL665); 

                    			newLeafNode(lv_value_2_3, grammarAccess.getPrintLAccess().getValueBOOLVALTerminalRuleCall_2_0_2()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPrintLRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_2_3, 
                            		"BOOLVAL");
                    	    

                    }
                    break;
                case 4 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:407:8: lv_value_2_4= ruleVariableName
                    {
                     
                    	        newCompositeNode(grammarAccess.getPrintLAccess().getValueVariableNameParserRuleCall_2_0_3()); 
                    	    
                    pushFollow(FOLLOW_ruleVariableName_in_rulePrintL689);
                    lv_value_2_4=ruleVariableName();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPrintLRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_2_4, 
                            		"VariableName");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }

            otherlv_3=(Token)match(input,19,FOLLOW_19_in_rulePrintL704); 

                	newLeafNode(otherlv_3, grammarAccess.getPrintLAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrintL"


    // $ANTLR start "entryRuleStatement_While"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:437:1: entryRuleStatement_While returns [EObject current=null] : iv_ruleStatement_While= ruleStatement_While EOF ;
    public final EObject entryRuleStatement_While() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement_While = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:438:2: (iv_ruleStatement_While= ruleStatement_While EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:439:2: iv_ruleStatement_While= ruleStatement_While EOF
            {
             newCompositeNode(grammarAccess.getStatement_WhileRule()); 
            pushFollow(FOLLOW_ruleStatement_While_in_entryRuleStatement_While740);
            iv_ruleStatement_While=ruleStatement_While();

            state._fsp--;

             current =iv_ruleStatement_While; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatement_While750); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement_While"


    // $ANTLR start "ruleStatement_While"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:446:1: ruleStatement_While returns [EObject current=null] : (otherlv_0= 'kol' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}' ) ;
    public final EObject ruleStatement_While() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_condOne_2_0 = null;

        EObject lv_operations_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:449:28: ( (otherlv_0= 'kol' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}' ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:450:1: (otherlv_0= 'kol' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:450:1: (otherlv_0= 'kol' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}' )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:450:3: otherlv_0= 'kol' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleStatement_While787); 

                	newLeafNode(otherlv_0, grammarAccess.getStatement_WhileAccess().getKolKeyword_0());
                
            otherlv_1=(Token)match(input,18,FOLLOW_18_in_ruleStatement_While799); 

                	newLeafNode(otherlv_1, grammarAccess.getStatement_WhileAccess().getLeftParenthesisKeyword_1());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:458:1: ( (lv_condOne_2_0= ruleBooleanTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:459:1: (lv_condOne_2_0= ruleBooleanTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:459:1: (lv_condOne_2_0= ruleBooleanTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:460:3: lv_condOne_2_0= ruleBooleanTypeExpression
            {
             
            	        newCompositeNode(grammarAccess.getStatement_WhileAccess().getCondOneBooleanTypeExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleBooleanTypeExpression_in_ruleStatement_While820);
            lv_condOne_2_0=ruleBooleanTypeExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getStatement_WhileRule());
            	        }
                   		set(
                   			current, 
                   			"condOne",
                    		lv_condOne_2_0, 
                    		"BooleanTypeExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,19,FOLLOW_19_in_ruleStatement_While832); 

                	newLeafNode(otherlv_3, grammarAccess.getStatement_WhileAccess().getRightParenthesisKeyword_3());
                
            otherlv_4=(Token)match(input,21,FOLLOW_21_in_ruleStatement_While844); 

                	newLeafNode(otherlv_4, grammarAccess.getStatement_WhileAccess().getLeftCurlyBracketKeyword_4());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:484:1: ( (lv_operations_5_0= ruleType ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_INTEGERS||LA4_0==RULE_ID||LA4_0==17||LA4_0==20||LA4_0==23||(LA4_0>=26 && LA4_0<=29)||LA4_0==33) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:485:1: (lv_operations_5_0= ruleType )
            	    {
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:485:1: (lv_operations_5_0= ruleType )
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:486:3: lv_operations_5_0= ruleType
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStatement_WhileAccess().getOperationsTypeParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleType_in_ruleStatement_While865);
            	    lv_operations_5_0=ruleType();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStatement_WhileRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"operations",
            	            		lv_operations_5_0, 
            	            		"Type");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_6=(Token)match(input,22,FOLLOW_22_in_ruleStatement_While878); 

                	newLeafNode(otherlv_6, grammarAccess.getStatement_WhileAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement_While"


    // $ANTLR start "entryRuleIf_statment"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:514:1: entryRuleIf_statment returns [EObject current=null] : iv_ruleIf_statment= ruleIf_statment EOF ;
    public final EObject entryRuleIf_statment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIf_statment = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:515:2: (iv_ruleIf_statment= ruleIf_statment EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:516:2: iv_ruleIf_statment= ruleIf_statment EOF
            {
             newCompositeNode(grammarAccess.getIf_statmentRule()); 
            pushFollow(FOLLOW_ruleIf_statment_in_entryRuleIf_statment914);
            iv_ruleIf_statment=ruleIf_statment();

            state._fsp--;

             current =iv_ruleIf_statment; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIf_statment924); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIf_statment"


    // $ANTLR start "ruleIf_statment"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:523:1: ruleIf_statment returns [EObject current=null] : (otherlv_0= 'jei' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}' ( (lv_else_7_0= ruleElse ) )? ) ;
    public final EObject ruleIf_statment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_condOne_2_0 = null;

        EObject lv_operations_5_0 = null;

        EObject lv_else_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:526:28: ( (otherlv_0= 'jei' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}' ( (lv_else_7_0= ruleElse ) )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:527:1: (otherlv_0= 'jei' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}' ( (lv_else_7_0= ruleElse ) )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:527:1: (otherlv_0= 'jei' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}' ( (lv_else_7_0= ruleElse ) )? )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:527:3: otherlv_0= 'jei' otherlv_1= '(' ( (lv_condOne_2_0= ruleBooleanTypeExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_operations_5_0= ruleType ) )* otherlv_6= '}' ( (lv_else_7_0= ruleElse ) )?
            {
            otherlv_0=(Token)match(input,23,FOLLOW_23_in_ruleIf_statment961); 

                	newLeafNode(otherlv_0, grammarAccess.getIf_statmentAccess().getJeiKeyword_0());
                
            otherlv_1=(Token)match(input,18,FOLLOW_18_in_ruleIf_statment973); 

                	newLeafNode(otherlv_1, grammarAccess.getIf_statmentAccess().getLeftParenthesisKeyword_1());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:535:1: ( (lv_condOne_2_0= ruleBooleanTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:536:1: (lv_condOne_2_0= ruleBooleanTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:536:1: (lv_condOne_2_0= ruleBooleanTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:537:3: lv_condOne_2_0= ruleBooleanTypeExpression
            {
             
            	        newCompositeNode(grammarAccess.getIf_statmentAccess().getCondOneBooleanTypeExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleBooleanTypeExpression_in_ruleIf_statment994);
            lv_condOne_2_0=ruleBooleanTypeExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIf_statmentRule());
            	        }
                   		set(
                   			current, 
                   			"condOne",
                    		lv_condOne_2_0, 
                    		"BooleanTypeExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,19,FOLLOW_19_in_ruleIf_statment1006); 

                	newLeafNode(otherlv_3, grammarAccess.getIf_statmentAccess().getRightParenthesisKeyword_3());
                
            otherlv_4=(Token)match(input,21,FOLLOW_21_in_ruleIf_statment1018); 

                	newLeafNode(otherlv_4, grammarAccess.getIf_statmentAccess().getLeftCurlyBracketKeyword_4());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:561:1: ( (lv_operations_5_0= ruleType ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_INTEGERS||LA5_0==RULE_ID||LA5_0==17||LA5_0==20||LA5_0==23||(LA5_0>=26 && LA5_0<=29)||LA5_0==33) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:562:1: (lv_operations_5_0= ruleType )
            	    {
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:562:1: (lv_operations_5_0= ruleType )
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:563:3: lv_operations_5_0= ruleType
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getIf_statmentAccess().getOperationsTypeParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleType_in_ruleIf_statment1039);
            	    lv_operations_5_0=ruleType();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getIf_statmentRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"operations",
            	            		lv_operations_5_0, 
            	            		"Type");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_6=(Token)match(input,22,FOLLOW_22_in_ruleIf_statment1052); 

                	newLeafNode(otherlv_6, grammarAccess.getIf_statmentAccess().getRightCurlyBracketKeyword_6());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:583:1: ( (lv_else_7_0= ruleElse ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==24) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:584:1: (lv_else_7_0= ruleElse )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:584:1: (lv_else_7_0= ruleElse )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:585:3: lv_else_7_0= ruleElse
                    {
                     
                    	        newCompositeNode(grammarAccess.getIf_statmentAccess().getElseElseParserRuleCall_7_0()); 
                    	    
                    pushFollow(FOLLOW_ruleElse_in_ruleIf_statment1073);
                    lv_else_7_0=ruleElse();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIf_statmentRule());
                    	        }
                           		set(
                           			current, 
                           			"else",
                            		lv_else_7_0, 
                            		"Else");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIf_statment"


    // $ANTLR start "entryRuleElse"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:609:1: entryRuleElse returns [EObject current=null] : iv_ruleElse= ruleElse EOF ;
    public final EObject entryRuleElse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElse = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:610:2: (iv_ruleElse= ruleElse EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:611:2: iv_ruleElse= ruleElse EOF
            {
             newCompositeNode(grammarAccess.getElseRule()); 
            pushFollow(FOLLOW_ruleElse_in_entryRuleElse1110);
            iv_ruleElse=ruleElse();

            state._fsp--;

             current =iv_ruleElse; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleElse1120); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElse"


    // $ANTLR start "ruleElse"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:618:1: ruleElse returns [EObject current=null] : (otherlv_0= 'kitaip' otherlv_1= '{' ( (lv_operations_2_0= ruleType ) )* otherlv_3= '}' ) ;
    public final EObject ruleElse() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_operations_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:621:28: ( (otherlv_0= 'kitaip' otherlv_1= '{' ( (lv_operations_2_0= ruleType ) )* otherlv_3= '}' ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:622:1: (otherlv_0= 'kitaip' otherlv_1= '{' ( (lv_operations_2_0= ruleType ) )* otherlv_3= '}' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:622:1: (otherlv_0= 'kitaip' otherlv_1= '{' ( (lv_operations_2_0= ruleType ) )* otherlv_3= '}' )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:622:3: otherlv_0= 'kitaip' otherlv_1= '{' ( (lv_operations_2_0= ruleType ) )* otherlv_3= '}'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleElse1157); 

                	newLeafNode(otherlv_0, grammarAccess.getElseAccess().getKitaipKeyword_0());
                
            otherlv_1=(Token)match(input,21,FOLLOW_21_in_ruleElse1169); 

                	newLeafNode(otherlv_1, grammarAccess.getElseAccess().getLeftCurlyBracketKeyword_1());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:630:1: ( (lv_operations_2_0= ruleType ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_INTEGERS||LA7_0==RULE_ID||LA7_0==17||LA7_0==20||LA7_0==23||(LA7_0>=26 && LA7_0<=29)||LA7_0==33) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:631:1: (lv_operations_2_0= ruleType )
            	    {
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:631:1: (lv_operations_2_0= ruleType )
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:632:3: lv_operations_2_0= ruleType
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getElseAccess().getOperationsTypeParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleType_in_ruleElse1190);
            	    lv_operations_2_0=ruleType();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getElseRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"operations",
            	            		lv_operations_2_0, 
            	            		"Type");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_3=(Token)match(input,22,FOLLOW_22_in_ruleElse1203); 

                	newLeafNode(otherlv_3, grammarAccess.getElseAccess().getRightCurlyBracketKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElse"


    // $ANTLR start "entryRuleCharacterDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:660:1: entryRuleCharacterDeclarationAss returns [EObject current=null] : iv_ruleCharacterDeclarationAss= ruleCharacterDeclarationAss EOF ;
    public final EObject entryRuleCharacterDeclarationAss() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCharacterDeclarationAss = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:661:2: (iv_ruleCharacterDeclarationAss= ruleCharacterDeclarationAss EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:662:2: iv_ruleCharacterDeclarationAss= ruleCharacterDeclarationAss EOF
            {
             newCompositeNode(grammarAccess.getCharacterDeclarationAssRule()); 
            pushFollow(FOLLOW_ruleCharacterDeclarationAss_in_entryRuleCharacterDeclarationAss1239);
            iv_ruleCharacterDeclarationAss=ruleCharacterDeclarationAss();

            state._fsp--;

             current =iv_ruleCharacterDeclarationAss; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterDeclarationAss1249); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCharacterDeclarationAss"


    // $ANTLR start "ruleCharacterDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:669:1: ruleCharacterDeclarationAss returns [EObject current=null] : ( ( (lv_assign_0_0= ruleCharacterDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleCharacterTypeExpression ) ) )? ) ;
    public final EObject ruleCharacterDeclarationAss() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_assign_0_0 = null;

        EObject lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:672:28: ( ( ( (lv_assign_0_0= ruleCharacterDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleCharacterTypeExpression ) ) )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:673:1: ( ( (lv_assign_0_0= ruleCharacterDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleCharacterTypeExpression ) ) )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:673:1: ( ( (lv_assign_0_0= ruleCharacterDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleCharacterTypeExpression ) ) )? )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:673:2: ( (lv_assign_0_0= ruleCharacterDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleCharacterTypeExpression ) ) )?
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:673:2: ( (lv_assign_0_0= ruleCharacterDeclaration ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:674:1: (lv_assign_0_0= ruleCharacterDeclaration )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:674:1: (lv_assign_0_0= ruleCharacterDeclaration )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:675:3: lv_assign_0_0= ruleCharacterDeclaration
            {
             
            	        newCompositeNode(grammarAccess.getCharacterDeclarationAssAccess().getAssignCharacterDeclarationParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleCharacterDeclaration_in_ruleCharacterDeclarationAss1295);
            lv_assign_0_0=ruleCharacterDeclaration();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCharacterDeclarationAssRule());
            	        }
                   		set(
                   			current, 
                   			"assign",
                    		lv_assign_0_0, 
                    		"CharacterDeclaration");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:691:2: (otherlv_1= '=' ( (lv_value_2_0= ruleCharacterTypeExpression ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==25) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:691:4: otherlv_1= '=' ( (lv_value_2_0= ruleCharacterTypeExpression ) )
                    {
                    otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleCharacterDeclarationAss1308); 

                        	newLeafNode(otherlv_1, grammarAccess.getCharacterDeclarationAssAccess().getEqualsSignKeyword_1_0());
                        
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:695:1: ( (lv_value_2_0= ruleCharacterTypeExpression ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:696:1: (lv_value_2_0= ruleCharacterTypeExpression )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:696:1: (lv_value_2_0= ruleCharacterTypeExpression )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:697:3: lv_value_2_0= ruleCharacterTypeExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getCharacterDeclarationAssAccess().getValueCharacterTypeExpressionParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleCharacterTypeExpression_in_ruleCharacterDeclarationAss1329);
                    lv_value_2_0=ruleCharacterTypeExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCharacterDeclarationAssRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_2_0, 
                            		"CharacterTypeExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCharacterDeclarationAss"


    // $ANTLR start "entryRuleCharacterDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:721:1: entryRuleCharacterDeclaration returns [EObject current=null] : iv_ruleCharacterDeclaration= ruleCharacterDeclaration EOF ;
    public final EObject entryRuleCharacterDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCharacterDeclaration = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:722:2: (iv_ruleCharacterDeclaration= ruleCharacterDeclaration EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:723:2: iv_ruleCharacterDeclaration= ruleCharacterDeclaration EOF
            {
             newCompositeNode(grammarAccess.getCharacterDeclarationRule()); 
            pushFollow(FOLLOW_ruleCharacterDeclaration_in_entryRuleCharacterDeclaration1367);
            iv_ruleCharacterDeclaration=ruleCharacterDeclaration();

            state._fsp--;

             current =iv_ruleCharacterDeclaration; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterDeclaration1377); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCharacterDeclaration"


    // $ANTLR start "ruleCharacterDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:730:1: ruleCharacterDeclaration returns [EObject current=null] : (otherlv_0= 'char' ( (lv_identifier_1_0= ruleVariableName ) ) ) ;
    public final EObject ruleCharacterDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_identifier_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:733:28: ( (otherlv_0= 'char' ( (lv_identifier_1_0= ruleVariableName ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:734:1: (otherlv_0= 'char' ( (lv_identifier_1_0= ruleVariableName ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:734:1: (otherlv_0= 'char' ( (lv_identifier_1_0= ruleVariableName ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:734:3: otherlv_0= 'char' ( (lv_identifier_1_0= ruleVariableName ) )
            {
            otherlv_0=(Token)match(input,26,FOLLOW_26_in_ruleCharacterDeclaration1414); 

                	newLeafNode(otherlv_0, grammarAccess.getCharacterDeclarationAccess().getCharKeyword_0());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:738:1: ( (lv_identifier_1_0= ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:739:1: (lv_identifier_1_0= ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:739:1: (lv_identifier_1_0= ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:740:3: lv_identifier_1_0= ruleVariableName
            {
             
            	        newCompositeNode(grammarAccess.getCharacterDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVariableName_in_ruleCharacterDeclaration1435);
            lv_identifier_1_0=ruleVariableName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCharacterDeclarationRule());
            	        }
                   		set(
                   			current, 
                   			"identifier",
                    		lv_identifier_1_0, 
                    		"VariableName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCharacterDeclaration"


    // $ANTLR start "entryRuleCharacterTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:764:1: entryRuleCharacterTypeExpression returns [EObject current=null] : iv_ruleCharacterTypeExpression= ruleCharacterTypeExpression EOF ;
    public final EObject entryRuleCharacterTypeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCharacterTypeExpression = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:765:2: (iv_ruleCharacterTypeExpression= ruleCharacterTypeExpression EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:766:2: iv_ruleCharacterTypeExpression= ruleCharacterTypeExpression EOF
            {
             newCompositeNode(grammarAccess.getCharacterTypeExpressionRule()); 
            pushFollow(FOLLOW_ruleCharacterTypeExpression_in_entryRuleCharacterTypeExpression1471);
            iv_ruleCharacterTypeExpression=ruleCharacterTypeExpression();

            state._fsp--;

             current =iv_ruleCharacterTypeExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterTypeExpression1481); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCharacterTypeExpression"


    // $ANTLR start "ruleCharacterTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:773:1: ruleCharacterTypeExpression returns [EObject current=null] : ( (lv_charAssign_0_0= ruleCharacterAssignment ) ) ;
    public final EObject ruleCharacterTypeExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_charAssign_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:776:28: ( ( (lv_charAssign_0_0= ruleCharacterAssignment ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:777:1: ( (lv_charAssign_0_0= ruleCharacterAssignment ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:777:1: ( (lv_charAssign_0_0= ruleCharacterAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:778:1: (lv_charAssign_0_0= ruleCharacterAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:778:1: (lv_charAssign_0_0= ruleCharacterAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:779:3: lv_charAssign_0_0= ruleCharacterAssignment
            {
             
            	        newCompositeNode(grammarAccess.getCharacterTypeExpressionAccess().getCharAssignCharacterAssignmentParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_ruleCharacterAssignment_in_ruleCharacterTypeExpression1526);
            lv_charAssign_0_0=ruleCharacterAssignment();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCharacterTypeExpressionRule());
            	        }
                   		set(
                   			current, 
                   			"charAssign",
                    		lv_charAssign_0_0, 
                    		"CharacterAssignment");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCharacterTypeExpression"


    // $ANTLR start "entryRuleCharacterAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:803:1: entryRuleCharacterAssignment returns [EObject current=null] : iv_ruleCharacterAssignment= ruleCharacterAssignment EOF ;
    public final EObject entryRuleCharacterAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCharacterAssignment = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:804:2: (iv_ruleCharacterAssignment= ruleCharacterAssignment EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:805:2: iv_ruleCharacterAssignment= ruleCharacterAssignment EOF
            {
             newCompositeNode(grammarAccess.getCharacterAssignmentRule()); 
            pushFollow(FOLLOW_ruleCharacterAssignment_in_entryRuleCharacterAssignment1561);
            iv_ruleCharacterAssignment=ruleCharacterAssignment();

            state._fsp--;

             current =iv_ruleCharacterAssignment; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterAssignment1571); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCharacterAssignment"


    // $ANTLR start "ruleCharacterAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:812:1: ruleCharacterAssignment returns [EObject current=null] : ( ( (lv_value_0_1= RULE_CHARACTERS | lv_value_0_2= ruleVariableName ) ) ) ;
    public final EObject ruleCharacterAssignment() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        AntlrDatatypeRuleToken lv_value_0_2 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:815:28: ( ( ( (lv_value_0_1= RULE_CHARACTERS | lv_value_0_2= ruleVariableName ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:816:1: ( ( (lv_value_0_1= RULE_CHARACTERS | lv_value_0_2= ruleVariableName ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:816:1: ( ( (lv_value_0_1= RULE_CHARACTERS | lv_value_0_2= ruleVariableName ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:817:1: ( (lv_value_0_1= RULE_CHARACTERS | lv_value_0_2= ruleVariableName ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:817:1: ( (lv_value_0_1= RULE_CHARACTERS | lv_value_0_2= ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:818:1: (lv_value_0_1= RULE_CHARACTERS | lv_value_0_2= ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:818:1: (lv_value_0_1= RULE_CHARACTERS | lv_value_0_2= ruleVariableName )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_CHARACTERS) ) {
                alt9=1;
            }
            else if ( (LA9_0==29) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:819:3: lv_value_0_1= RULE_CHARACTERS
                    {
                    lv_value_0_1=(Token)match(input,RULE_CHARACTERS,FOLLOW_RULE_CHARACTERS_in_ruleCharacterAssignment1614); 

                    			newLeafNode(lv_value_0_1, grammarAccess.getCharacterAssignmentAccess().getValueCHARACTERSTerminalRuleCall_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCharacterAssignmentRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_0_1, 
                            		"CHARACTERS");
                    	    

                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:834:8: lv_value_0_2= ruleVariableName
                    {
                     
                    	        newCompositeNode(grammarAccess.getCharacterAssignmentAccess().getValueVariableNameParserRuleCall_0_1()); 
                    	    
                    pushFollow(FOLLOW_ruleVariableName_in_ruleCharacterAssignment1638);
                    lv_value_0_2=ruleVariableName();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCharacterAssignmentRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_0_2, 
                            		"VariableName");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCharacterAssignment"


    // $ANTLR start "entryRuleCharacterVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:860:1: entryRuleCharacterVarVar returns [EObject current=null] : iv_ruleCharacterVarVar= ruleCharacterVarVar EOF ;
    public final EObject entryRuleCharacterVarVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCharacterVarVar = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:861:2: (iv_ruleCharacterVarVar= ruleCharacterVarVar EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:862:2: iv_ruleCharacterVarVar= ruleCharacterVarVar EOF
            {
             newCompositeNode(grammarAccess.getCharacterVarVarRule()); 
            pushFollow(FOLLOW_ruleCharacterVarVar_in_entryRuleCharacterVarVar1676);
            iv_ruleCharacterVarVar=ruleCharacterVarVar();

            state._fsp--;

             current =iv_ruleCharacterVarVar; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCharacterVarVar1686); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCharacterVarVar"


    // $ANTLR start "ruleCharacterVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:869:1: ruleCharacterVarVar returns [EObject current=null] : ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_CHARACTERS ) ) ) ;
    public final EObject ruleCharacterVarVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_value_2_0=null;
        AntlrDatatypeRuleToken lv_identifier_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:872:28: ( ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_CHARACTERS ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:873:1: ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_CHARACTERS ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:873:1: ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_CHARACTERS ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:873:2: ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_CHARACTERS ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:873:2: ( (lv_identifier_0_0= ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:874:1: (lv_identifier_0_0= ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:874:1: (lv_identifier_0_0= ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:875:3: lv_identifier_0_0= ruleVariableName
            {
             
            	        newCompositeNode(grammarAccess.getCharacterVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleVariableName_in_ruleCharacterVarVar1732);
            lv_identifier_0_0=ruleVariableName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCharacterVarVarRule());
            	        }
                   		set(
                   			current, 
                   			"identifier",
                    		lv_identifier_0_0, 
                    		"VariableName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleCharacterVarVar1744); 

                	newLeafNode(otherlv_1, grammarAccess.getCharacterVarVarAccess().getEqualsSignKeyword_1());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:895:1: ( (lv_value_2_0= RULE_CHARACTERS ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:896:1: (lv_value_2_0= RULE_CHARACTERS )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:896:1: (lv_value_2_0= RULE_CHARACTERS )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:897:3: lv_value_2_0= RULE_CHARACTERS
            {
            lv_value_2_0=(Token)match(input,RULE_CHARACTERS,FOLLOW_RULE_CHARACTERS_in_ruleCharacterVarVar1761); 

            			newLeafNode(lv_value_2_0, grammarAccess.getCharacterVarVarAccess().getValueCHARACTERSTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCharacterVarVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_2_0, 
                    		"CHARACTERS");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCharacterVarVar"


    // $ANTLR start "entryRuleBooleanDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:921:1: entryRuleBooleanDeclarationAss returns [EObject current=null] : iv_ruleBooleanDeclarationAss= ruleBooleanDeclarationAss EOF ;
    public final EObject entryRuleBooleanDeclarationAss() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanDeclarationAss = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:922:2: (iv_ruleBooleanDeclarationAss= ruleBooleanDeclarationAss EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:923:2: iv_ruleBooleanDeclarationAss= ruleBooleanDeclarationAss EOF
            {
             newCompositeNode(grammarAccess.getBooleanDeclarationAssRule()); 
            pushFollow(FOLLOW_ruleBooleanDeclarationAss_in_entryRuleBooleanDeclarationAss1802);
            iv_ruleBooleanDeclarationAss=ruleBooleanDeclarationAss();

            state._fsp--;

             current =iv_ruleBooleanDeclarationAss; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanDeclarationAss1812); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanDeclarationAss"


    // $ANTLR start "ruleBooleanDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:930:1: ruleBooleanDeclarationAss returns [EObject current=null] : ( ( (lv_assign_0_0= ruleBooleanDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleBooleanTypeExpression ) ) )? ) ;
    public final EObject ruleBooleanDeclarationAss() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_assign_0_0 = null;

        EObject lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:933:28: ( ( ( (lv_assign_0_0= ruleBooleanDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleBooleanTypeExpression ) ) )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:934:1: ( ( (lv_assign_0_0= ruleBooleanDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleBooleanTypeExpression ) ) )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:934:1: ( ( (lv_assign_0_0= ruleBooleanDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleBooleanTypeExpression ) ) )? )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:934:2: ( (lv_assign_0_0= ruleBooleanDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleBooleanTypeExpression ) ) )?
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:934:2: ( (lv_assign_0_0= ruleBooleanDeclaration ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:935:1: (lv_assign_0_0= ruleBooleanDeclaration )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:935:1: (lv_assign_0_0= ruleBooleanDeclaration )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:936:3: lv_assign_0_0= ruleBooleanDeclaration
            {
             
            	        newCompositeNode(grammarAccess.getBooleanDeclarationAssAccess().getAssignBooleanDeclarationParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleBooleanDeclaration_in_ruleBooleanDeclarationAss1858);
            lv_assign_0_0=ruleBooleanDeclaration();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanDeclarationAssRule());
            	        }
                   		set(
                   			current, 
                   			"assign",
                    		lv_assign_0_0, 
                    		"BooleanDeclaration");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:952:2: (otherlv_1= '=' ( (lv_value_2_0= ruleBooleanTypeExpression ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==25) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:952:4: otherlv_1= '=' ( (lv_value_2_0= ruleBooleanTypeExpression ) )
                    {
                    otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleBooleanDeclarationAss1871); 

                        	newLeafNode(otherlv_1, grammarAccess.getBooleanDeclarationAssAccess().getEqualsSignKeyword_1_0());
                        
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:956:1: ( (lv_value_2_0= ruleBooleanTypeExpression ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:957:1: (lv_value_2_0= ruleBooleanTypeExpression )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:957:1: (lv_value_2_0= ruleBooleanTypeExpression )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:958:3: lv_value_2_0= ruleBooleanTypeExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanDeclarationAssAccess().getValueBooleanTypeExpressionParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleBooleanTypeExpression_in_ruleBooleanDeclarationAss1892);
                    lv_value_2_0=ruleBooleanTypeExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanDeclarationAssRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_2_0, 
                            		"BooleanTypeExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanDeclarationAss"


    // $ANTLR start "entryRuleBooleanDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:982:1: entryRuleBooleanDeclaration returns [EObject current=null] : iv_ruleBooleanDeclaration= ruleBooleanDeclaration EOF ;
    public final EObject entryRuleBooleanDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanDeclaration = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:983:2: (iv_ruleBooleanDeclaration= ruleBooleanDeclaration EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:984:2: iv_ruleBooleanDeclaration= ruleBooleanDeclaration EOF
            {
             newCompositeNode(grammarAccess.getBooleanDeclarationRule()); 
            pushFollow(FOLLOW_ruleBooleanDeclaration_in_entryRuleBooleanDeclaration1930);
            iv_ruleBooleanDeclaration=ruleBooleanDeclaration();

            state._fsp--;

             current =iv_ruleBooleanDeclaration; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanDeclaration1940); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanDeclaration"


    // $ANTLR start "ruleBooleanDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:991:1: ruleBooleanDeclaration returns [EObject current=null] : (otherlv_0= 'bool' ( (lv_identifier_1_0= ruleVariableName ) ) ) ;
    public final EObject ruleBooleanDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_identifier_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:994:28: ( (otherlv_0= 'bool' ( (lv_identifier_1_0= ruleVariableName ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:995:1: (otherlv_0= 'bool' ( (lv_identifier_1_0= ruleVariableName ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:995:1: (otherlv_0= 'bool' ( (lv_identifier_1_0= ruleVariableName ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:995:3: otherlv_0= 'bool' ( (lv_identifier_1_0= ruleVariableName ) )
            {
            otherlv_0=(Token)match(input,27,FOLLOW_27_in_ruleBooleanDeclaration1977); 

                	newLeafNode(otherlv_0, grammarAccess.getBooleanDeclarationAccess().getBoolKeyword_0());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:999:1: ( (lv_identifier_1_0= ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1000:1: (lv_identifier_1_0= ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1000:1: (lv_identifier_1_0= ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1001:3: lv_identifier_1_0= ruleVariableName
            {
             
            	        newCompositeNode(grammarAccess.getBooleanDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVariableName_in_ruleBooleanDeclaration1998);
            lv_identifier_1_0=ruleVariableName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanDeclarationRule());
            	        }
                   		set(
                   			current, 
                   			"identifier",
                    		lv_identifier_1_0, 
                    		"VariableName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanDeclaration"


    // $ANTLR start "entryRuleBooleanTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1025:1: entryRuleBooleanTypeExpression returns [EObject current=null] : iv_ruleBooleanTypeExpression= ruleBooleanTypeExpression EOF ;
    public final EObject entryRuleBooleanTypeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanTypeExpression = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1026:2: (iv_ruleBooleanTypeExpression= ruleBooleanTypeExpression EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1027:2: iv_ruleBooleanTypeExpression= ruleBooleanTypeExpression EOF
            {
             newCompositeNode(grammarAccess.getBooleanTypeExpressionRule()); 
            pushFollow(FOLLOW_ruleBooleanTypeExpression_in_entryRuleBooleanTypeExpression2034);
            iv_ruleBooleanTypeExpression=ruleBooleanTypeExpression();

            state._fsp--;

             current =iv_ruleBooleanTypeExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanTypeExpression2044); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanTypeExpression"


    // $ANTLR start "ruleBooleanTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1034:1: ruleBooleanTypeExpression returns [EObject current=null] : ( ( (lv_boolAssign_0_0= ruleBooleanAssignment ) ) | ( (lv_boolOp_1_0= ruleBooleanOperation ) ) | ( (lv_intToBool_2_0= ruleIntegerToBool ) ) ) ;
    public final EObject ruleBooleanTypeExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_boolAssign_0_0 = null;

        EObject lv_boolOp_1_0 = null;

        EObject lv_intToBool_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1037:28: ( ( ( (lv_boolAssign_0_0= ruleBooleanAssignment ) ) | ( (lv_boolOp_1_0= ruleBooleanOperation ) ) | ( (lv_intToBool_2_0= ruleIntegerToBool ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1038:1: ( ( (lv_boolAssign_0_0= ruleBooleanAssignment ) ) | ( (lv_boolOp_1_0= ruleBooleanOperation ) ) | ( (lv_intToBool_2_0= ruleIntegerToBool ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1038:1: ( ( (lv_boolAssign_0_0= ruleBooleanAssignment ) ) | ( (lv_boolOp_1_0= ruleBooleanOperation ) ) | ( (lv_intToBool_2_0= ruleIntegerToBool ) ) )
            int alt11=3;
            switch ( input.LA(1) ) {
            case RULE_BOOLVAL:
                {
                int LA11_1 = input.LA(2);

                if ( ((LA11_1>=30 && LA11_1<=31)) ) {
                    alt11=2;
                }
                else if ( (LA11_1==EOF||LA11_1==RULE_INTEGERS||LA11_1==RULE_ID||LA11_1==17||(LA11_1>=19 && LA11_1<=20)||(LA11_1>=22 && LA11_1<=23)||(LA11_1>=26 && LA11_1<=29)||LA11_1==33) ) {
                    alt11=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 1, input);

                    throw nvae;
                }
                }
                break;
            case 29:
                {
                int LA11_2 = input.LA(2);

                if ( (LA11_2==RULE_ID) ) {
                    switch ( input.LA(3) ) {
                    case EOF:
                    case RULE_INTEGERS:
                    case RULE_ID:
                    case 17:
                    case 19:
                    case 20:
                    case 22:
                    case 23:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 33:
                        {
                        alt11=1;
                        }
                        break;
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                        {
                        alt11=3;
                        }
                        break;
                    case 30:
                    case 31:
                        {
                        alt11=2;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 11, 6, input);

                        throw nvae;
                    }

                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INTEGERS:
                {
                alt11=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1038:2: ( (lv_boolAssign_0_0= ruleBooleanAssignment ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1038:2: ( (lv_boolAssign_0_0= ruleBooleanAssignment ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1039:1: (lv_boolAssign_0_0= ruleBooleanAssignment )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1039:1: (lv_boolAssign_0_0= ruleBooleanAssignment )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1040:3: lv_boolAssign_0_0= ruleBooleanAssignment
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanTypeExpressionAccess().getBoolAssignBooleanAssignmentParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleBooleanAssignment_in_ruleBooleanTypeExpression2090);
                    lv_boolAssign_0_0=ruleBooleanAssignment();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanTypeExpressionRule());
                    	        }
                           		set(
                           			current, 
                           			"boolAssign",
                            		lv_boolAssign_0_0, 
                            		"BooleanAssignment");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1057:6: ( (lv_boolOp_1_0= ruleBooleanOperation ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1057:6: ( (lv_boolOp_1_0= ruleBooleanOperation ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1058:1: (lv_boolOp_1_0= ruleBooleanOperation )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1058:1: (lv_boolOp_1_0= ruleBooleanOperation )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1059:3: lv_boolOp_1_0= ruleBooleanOperation
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanTypeExpressionAccess().getBoolOpBooleanOperationParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleBooleanOperation_in_ruleBooleanTypeExpression2117);
                    lv_boolOp_1_0=ruleBooleanOperation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanTypeExpressionRule());
                    	        }
                           		set(
                           			current, 
                           			"boolOp",
                            		lv_boolOp_1_0, 
                            		"BooleanOperation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1076:6: ( (lv_intToBool_2_0= ruleIntegerToBool ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1076:6: ( (lv_intToBool_2_0= ruleIntegerToBool ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1077:1: (lv_intToBool_2_0= ruleIntegerToBool )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1077:1: (lv_intToBool_2_0= ruleIntegerToBool )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1078:3: lv_intToBool_2_0= ruleIntegerToBool
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanTypeExpressionAccess().getIntToBoolIntegerToBoolParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleIntegerToBool_in_ruleBooleanTypeExpression2144);
                    lv_intToBool_2_0=ruleIntegerToBool();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanTypeExpressionRule());
                    	        }
                           		set(
                           			current, 
                           			"intToBool",
                            		lv_intToBool_2_0, 
                            		"IntegerToBool");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanTypeExpression"


    // $ANTLR start "entryRuleBooleanVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1102:1: entryRuleBooleanVarVar returns [EObject current=null] : iv_ruleBooleanVarVar= ruleBooleanVarVar EOF ;
    public final EObject entryRuleBooleanVarVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanVarVar = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1103:2: (iv_ruleBooleanVarVar= ruleBooleanVarVar EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1104:2: iv_ruleBooleanVarVar= ruleBooleanVarVar EOF
            {
             newCompositeNode(grammarAccess.getBooleanVarVarRule()); 
            pushFollow(FOLLOW_ruleBooleanVarVar_in_entryRuleBooleanVarVar2180);
            iv_ruleBooleanVarVar=ruleBooleanVarVar();

            state._fsp--;

             current =iv_ruleBooleanVarVar; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanVarVar2190); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanVarVar"


    // $ANTLR start "ruleBooleanVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1111:1: ruleBooleanVarVar returns [EObject current=null] : ( ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value1_2_0= ruleBooleanOperation ) ) ) | ( (lv_value2_3_0= ruleIntegerToBool ) ) ) ;
    public final EObject ruleBooleanVarVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_identifier_0_0 = null;

        EObject lv_value1_2_0 = null;

        EObject lv_value2_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1114:28: ( ( ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value1_2_0= ruleBooleanOperation ) ) ) | ( (lv_value2_3_0= ruleIntegerToBool ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1115:1: ( ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value1_2_0= ruleBooleanOperation ) ) ) | ( (lv_value2_3_0= ruleIntegerToBool ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1115:1: ( ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value1_2_0= ruleBooleanOperation ) ) ) | ( (lv_value2_3_0= ruleIntegerToBool ) ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==29) ) {
                int LA12_1 = input.LA(2);

                if ( (LA12_1==RULE_ID) ) {
                    int LA12_3 = input.LA(3);

                    if ( (LA12_3==25) ) {
                        alt12=1;
                    }
                    else if ( ((LA12_3>=36 && LA12_3<=42)) ) {
                        alt12=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 3, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 12, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA12_0==RULE_INTEGERS) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1115:2: ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value1_2_0= ruleBooleanOperation ) ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1115:2: ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value1_2_0= ruleBooleanOperation ) ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1115:3: ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value1_2_0= ruleBooleanOperation ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1115:3: ( (lv_identifier_0_0= ruleVariableName ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1116:1: (lv_identifier_0_0= ruleVariableName )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1116:1: (lv_identifier_0_0= ruleVariableName )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1117:3: lv_identifier_0_0= ruleVariableName
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleVariableName_in_ruleBooleanVarVar2237);
                    lv_identifier_0_0=ruleVariableName();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanVarVarRule());
                    	        }
                           		set(
                           			current, 
                           			"identifier",
                            		lv_identifier_0_0, 
                            		"VariableName");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleBooleanVarVar2249); 

                        	newLeafNode(otherlv_1, grammarAccess.getBooleanVarVarAccess().getEqualsSignKeyword_0_1());
                        
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1137:1: ( (lv_value1_2_0= ruleBooleanOperation ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1138:1: (lv_value1_2_0= ruleBooleanOperation )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1138:1: (lv_value1_2_0= ruleBooleanOperation )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1139:3: lv_value1_2_0= ruleBooleanOperation
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanVarVarAccess().getValue1BooleanOperationParserRuleCall_0_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleBooleanOperation_in_ruleBooleanVarVar2270);
                    lv_value1_2_0=ruleBooleanOperation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanVarVarRule());
                    	        }
                           		set(
                           			current, 
                           			"value1",
                            		lv_value1_2_0, 
                            		"BooleanOperation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1156:6: ( (lv_value2_3_0= ruleIntegerToBool ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1156:6: ( (lv_value2_3_0= ruleIntegerToBool ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1157:1: (lv_value2_3_0= ruleIntegerToBool )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1157:1: (lv_value2_3_0= ruleIntegerToBool )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1158:3: lv_value2_3_0= ruleIntegerToBool
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanVarVarAccess().getValue2IntegerToBoolParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleIntegerToBool_in_ruleBooleanVarVar2298);
                    lv_value2_3_0=ruleIntegerToBool();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanVarVarRule());
                    	        }
                           		set(
                           			current, 
                           			"value2",
                            		lv_value2_3_0, 
                            		"IntegerToBool");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanVarVar"


    // $ANTLR start "entryRuleBooleanAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1182:1: entryRuleBooleanAssignment returns [EObject current=null] : iv_ruleBooleanAssignment= ruleBooleanAssignment EOF ;
    public final EObject entryRuleBooleanAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanAssignment = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1183:2: (iv_ruleBooleanAssignment= ruleBooleanAssignment EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1184:2: iv_ruleBooleanAssignment= ruleBooleanAssignment EOF
            {
             newCompositeNode(grammarAccess.getBooleanAssignmentRule()); 
            pushFollow(FOLLOW_ruleBooleanAssignment_in_entryRuleBooleanAssignment2334);
            iv_ruleBooleanAssignment=ruleBooleanAssignment();

            state._fsp--;

             current =iv_ruleBooleanAssignment; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanAssignment2344); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanAssignment"


    // $ANTLR start "ruleBooleanAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1191:1: ruleBooleanAssignment returns [EObject current=null] : ( ( (lv_value_0_1= RULE_BOOLVAL | lv_value_0_2= ruleVariableName ) ) ) ;
    public final EObject ruleBooleanAssignment() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        AntlrDatatypeRuleToken lv_value_0_2 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1194:28: ( ( ( (lv_value_0_1= RULE_BOOLVAL | lv_value_0_2= ruleVariableName ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1195:1: ( ( (lv_value_0_1= RULE_BOOLVAL | lv_value_0_2= ruleVariableName ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1195:1: ( ( (lv_value_0_1= RULE_BOOLVAL | lv_value_0_2= ruleVariableName ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1196:1: ( (lv_value_0_1= RULE_BOOLVAL | lv_value_0_2= ruleVariableName ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1196:1: ( (lv_value_0_1= RULE_BOOLVAL | lv_value_0_2= ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1197:1: (lv_value_0_1= RULE_BOOLVAL | lv_value_0_2= ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1197:1: (lv_value_0_1= RULE_BOOLVAL | lv_value_0_2= ruleVariableName )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_BOOLVAL) ) {
                alt13=1;
            }
            else if ( (LA13_0==29) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1198:3: lv_value_0_1= RULE_BOOLVAL
                    {
                    lv_value_0_1=(Token)match(input,RULE_BOOLVAL,FOLLOW_RULE_BOOLVAL_in_ruleBooleanAssignment2387); 

                    			newLeafNode(lv_value_0_1, grammarAccess.getBooleanAssignmentAccess().getValueBOOLVALTerminalRuleCall_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBooleanAssignmentRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_0_1, 
                            		"BOOLVAL");
                    	    

                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1213:8: lv_value_0_2= ruleVariableName
                    {
                     
                    	        newCompositeNode(grammarAccess.getBooleanAssignmentAccess().getValueVariableNameParserRuleCall_0_1()); 
                    	    
                    pushFollow(FOLLOW_ruleVariableName_in_ruleBooleanAssignment2411);
                    lv_value_0_2=ruleVariableName();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getBooleanAssignmentRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_0_2, 
                            		"VariableName");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanAssignment"


    // $ANTLR start "entryRuleBooleanOperation"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1239:1: entryRuleBooleanOperation returns [EObject current=null] : iv_ruleBooleanOperation= ruleBooleanOperation EOF ;
    public final EObject entryRuleBooleanOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanOperation = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1240:2: (iv_ruleBooleanOperation= ruleBooleanOperation EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1241:2: iv_ruleBooleanOperation= ruleBooleanOperation EOF
            {
             newCompositeNode(grammarAccess.getBooleanOperationRule()); 
            pushFollow(FOLLOW_ruleBooleanOperation_in_entryRuleBooleanOperation2449);
            iv_ruleBooleanOperation=ruleBooleanOperation();

            state._fsp--;

             current =iv_ruleBooleanOperation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanOperation2459); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanOperation"


    // $ANTLR start "ruleBooleanOperation"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1248:1: ruleBooleanOperation returns [EObject current=null] : ( ( (lv_operand1_0_0= ruleBooleanAssignment ) ) ( (lv_operator_1_0= ruleBOOLOPERATOR ) ) ( (lv_operand2_2_0= ruleBooleanAssignment ) ) ) ;
    public final EObject ruleBooleanOperation() throws RecognitionException {
        EObject current = null;

        EObject lv_operand1_0_0 = null;

        EObject lv_operator_1_0 = null;

        EObject lv_operand2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1251:28: ( ( ( (lv_operand1_0_0= ruleBooleanAssignment ) ) ( (lv_operator_1_0= ruleBOOLOPERATOR ) ) ( (lv_operand2_2_0= ruleBooleanAssignment ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1252:1: ( ( (lv_operand1_0_0= ruleBooleanAssignment ) ) ( (lv_operator_1_0= ruleBOOLOPERATOR ) ) ( (lv_operand2_2_0= ruleBooleanAssignment ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1252:1: ( ( (lv_operand1_0_0= ruleBooleanAssignment ) ) ( (lv_operator_1_0= ruleBOOLOPERATOR ) ) ( (lv_operand2_2_0= ruleBooleanAssignment ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1252:2: ( (lv_operand1_0_0= ruleBooleanAssignment ) ) ( (lv_operator_1_0= ruleBOOLOPERATOR ) ) ( (lv_operand2_2_0= ruleBooleanAssignment ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1252:2: ( (lv_operand1_0_0= ruleBooleanAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1253:1: (lv_operand1_0_0= ruleBooleanAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1253:1: (lv_operand1_0_0= ruleBooleanAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1254:3: lv_operand1_0_0= ruleBooleanAssignment
            {
             
            	        newCompositeNode(grammarAccess.getBooleanOperationAccess().getOperand1BooleanAssignmentParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleBooleanAssignment_in_ruleBooleanOperation2505);
            lv_operand1_0_0=ruleBooleanAssignment();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanOperationRule());
            	        }
                   		set(
                   			current, 
                   			"operand1",
                    		lv_operand1_0_0, 
                    		"BooleanAssignment");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1270:2: ( (lv_operator_1_0= ruleBOOLOPERATOR ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1271:1: (lv_operator_1_0= ruleBOOLOPERATOR )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1271:1: (lv_operator_1_0= ruleBOOLOPERATOR )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1272:3: lv_operator_1_0= ruleBOOLOPERATOR
            {
             
            	        newCompositeNode(grammarAccess.getBooleanOperationAccess().getOperatorBOOLOPERATORParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleBOOLOPERATOR_in_ruleBooleanOperation2526);
            lv_operator_1_0=ruleBOOLOPERATOR();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanOperationRule());
            	        }
                   		set(
                   			current, 
                   			"operator",
                    		lv_operator_1_0, 
                    		"BOOLOPERATOR");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1288:2: ( (lv_operand2_2_0= ruleBooleanAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1289:1: (lv_operand2_2_0= ruleBooleanAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1289:1: (lv_operand2_2_0= ruleBooleanAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1290:3: lv_operand2_2_0= ruleBooleanAssignment
            {
             
            	        newCompositeNode(grammarAccess.getBooleanOperationAccess().getOperand2BooleanAssignmentParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleBooleanAssignment_in_ruleBooleanOperation2547);
            lv_operand2_2_0=ruleBooleanAssignment();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanOperationRule());
            	        }
                   		set(
                   			current, 
                   			"operand2",
                    		lv_operand2_2_0, 
                    		"BooleanAssignment");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanOperation"


    // $ANTLR start "entryRuleIntegerToBool"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1314:1: entryRuleIntegerToBool returns [EObject current=null] : iv_ruleIntegerToBool= ruleIntegerToBool EOF ;
    public final EObject entryRuleIntegerToBool() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerToBool = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1315:2: (iv_ruleIntegerToBool= ruleIntegerToBool EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1316:2: iv_ruleIntegerToBool= ruleIntegerToBool EOF
            {
             newCompositeNode(grammarAccess.getIntegerToBoolRule()); 
            pushFollow(FOLLOW_ruleIntegerToBool_in_entryRuleIntegerToBool2583);
            iv_ruleIntegerToBool=ruleIntegerToBool();

            state._fsp--;

             current =iv_ruleIntegerToBool; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerToBool2593); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerToBool"


    // $ANTLR start "ruleIntegerToBool"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1323:1: ruleIntegerToBool returns [EObject current=null] : ( ( (lv_operand1_0_0= ruleIntegerTypeExpression ) ) ( (lv_operator_1_0= ruleSPECIALINTOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerTypeExpression ) ) ) ;
    public final EObject ruleIntegerToBool() throws RecognitionException {
        EObject current = null;

        EObject lv_operand1_0_0 = null;

        Enumerator lv_operator_1_0 = null;

        EObject lv_operand2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1326:28: ( ( ( (lv_operand1_0_0= ruleIntegerTypeExpression ) ) ( (lv_operator_1_0= ruleSPECIALINTOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerTypeExpression ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1327:1: ( ( (lv_operand1_0_0= ruleIntegerTypeExpression ) ) ( (lv_operator_1_0= ruleSPECIALINTOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerTypeExpression ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1327:1: ( ( (lv_operand1_0_0= ruleIntegerTypeExpression ) ) ( (lv_operator_1_0= ruleSPECIALINTOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerTypeExpression ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1327:2: ( (lv_operand1_0_0= ruleIntegerTypeExpression ) ) ( (lv_operator_1_0= ruleSPECIALINTOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerTypeExpression ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1327:2: ( (lv_operand1_0_0= ruleIntegerTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1328:1: (lv_operand1_0_0= ruleIntegerTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1328:1: (lv_operand1_0_0= ruleIntegerTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1329:3: lv_operand1_0_0= ruleIntegerTypeExpression
            {
             
            	        newCompositeNode(grammarAccess.getIntegerToBoolAccess().getOperand1IntegerTypeExpressionParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleIntegerTypeExpression_in_ruleIntegerToBool2639);
            lv_operand1_0_0=ruleIntegerTypeExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerToBoolRule());
            	        }
                   		set(
                   			current, 
                   			"operand1",
                    		lv_operand1_0_0, 
                    		"IntegerTypeExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1345:2: ( (lv_operator_1_0= ruleSPECIALINTOPERATOR ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1346:1: (lv_operator_1_0= ruleSPECIALINTOPERATOR )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1346:1: (lv_operator_1_0= ruleSPECIALINTOPERATOR )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1347:3: lv_operator_1_0= ruleSPECIALINTOPERATOR
            {
             
            	        newCompositeNode(grammarAccess.getIntegerToBoolAccess().getOperatorSPECIALINTOPERATOREnumRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleSPECIALINTOPERATOR_in_ruleIntegerToBool2660);
            lv_operator_1_0=ruleSPECIALINTOPERATOR();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerToBoolRule());
            	        }
                   		set(
                   			current, 
                   			"operator",
                    		lv_operator_1_0, 
                    		"SPECIALINTOPERATOR");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1363:2: ( (lv_operand2_2_0= ruleIntegerTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1364:1: (lv_operand2_2_0= ruleIntegerTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1364:1: (lv_operand2_2_0= ruleIntegerTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1365:3: lv_operand2_2_0= ruleIntegerTypeExpression
            {
             
            	        newCompositeNode(grammarAccess.getIntegerToBoolAccess().getOperand2IntegerTypeExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleIntegerTypeExpression_in_ruleIntegerToBool2681);
            lv_operand2_2_0=ruleIntegerTypeExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerToBoolRule());
            	        }
                   		set(
                   			current, 
                   			"operand2",
                    		lv_operand2_2_0, 
                    		"IntegerTypeExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerToBool"


    // $ANTLR start "entryRuleIntegerDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1389:1: entryRuleIntegerDeclarationAss returns [EObject current=null] : iv_ruleIntegerDeclarationAss= ruleIntegerDeclarationAss EOF ;
    public final EObject entryRuleIntegerDeclarationAss() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerDeclarationAss = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1390:2: (iv_ruleIntegerDeclarationAss= ruleIntegerDeclarationAss EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1391:2: iv_ruleIntegerDeclarationAss= ruleIntegerDeclarationAss EOF
            {
             newCompositeNode(grammarAccess.getIntegerDeclarationAssRule()); 
            pushFollow(FOLLOW_ruleIntegerDeclarationAss_in_entryRuleIntegerDeclarationAss2717);
            iv_ruleIntegerDeclarationAss=ruleIntegerDeclarationAss();

            state._fsp--;

             current =iv_ruleIntegerDeclarationAss; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerDeclarationAss2727); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerDeclarationAss"


    // $ANTLR start "ruleIntegerDeclarationAss"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1398:1: ruleIntegerDeclarationAss returns [EObject current=null] : ( ( (lv_assign_0_0= ruleIntegerDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) )? ) ;
    public final EObject ruleIntegerDeclarationAss() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_assign_0_0 = null;

        EObject lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1401:28: ( ( ( (lv_assign_0_0= ruleIntegerDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1402:1: ( ( (lv_assign_0_0= ruleIntegerDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1402:1: ( ( (lv_assign_0_0= ruleIntegerDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) )? )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1402:2: ( (lv_assign_0_0= ruleIntegerDeclaration ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) )?
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1402:2: ( (lv_assign_0_0= ruleIntegerDeclaration ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1403:1: (lv_assign_0_0= ruleIntegerDeclaration )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1403:1: (lv_assign_0_0= ruleIntegerDeclaration )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1404:3: lv_assign_0_0= ruleIntegerDeclaration
            {
             
            	        newCompositeNode(grammarAccess.getIntegerDeclarationAssAccess().getAssignIntegerDeclarationParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleIntegerDeclaration_in_ruleIntegerDeclarationAss2773);
            lv_assign_0_0=ruleIntegerDeclaration();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerDeclarationAssRule());
            	        }
                   		set(
                   			current, 
                   			"assign",
                    		lv_assign_0_0, 
                    		"IntegerDeclaration");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1420:2: (otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==25) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1420:4: otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) )
                    {
                    otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleIntegerDeclarationAss2786); 

                        	newLeafNode(otherlv_1, grammarAccess.getIntegerDeclarationAssAccess().getEqualsSignKeyword_1_0());
                        
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1424:1: ( (lv_value_2_0= ruleIntegerTypeExpression ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1425:1: (lv_value_2_0= ruleIntegerTypeExpression )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1425:1: (lv_value_2_0= ruleIntegerTypeExpression )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1426:3: lv_value_2_0= ruleIntegerTypeExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getIntegerDeclarationAssAccess().getValueIntegerTypeExpressionParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleIntegerTypeExpression_in_ruleIntegerDeclarationAss2807);
                    lv_value_2_0=ruleIntegerTypeExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIntegerDeclarationAssRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_2_0, 
                            		"IntegerTypeExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerDeclarationAss"


    // $ANTLR start "entryRuleIntegerDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1450:1: entryRuleIntegerDeclaration returns [EObject current=null] : iv_ruleIntegerDeclaration= ruleIntegerDeclaration EOF ;
    public final EObject entryRuleIntegerDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerDeclaration = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1451:2: (iv_ruleIntegerDeclaration= ruleIntegerDeclaration EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1452:2: iv_ruleIntegerDeclaration= ruleIntegerDeclaration EOF
            {
             newCompositeNode(grammarAccess.getIntegerDeclarationRule()); 
            pushFollow(FOLLOW_ruleIntegerDeclaration_in_entryRuleIntegerDeclaration2845);
            iv_ruleIntegerDeclaration=ruleIntegerDeclaration();

            state._fsp--;

             current =iv_ruleIntegerDeclaration; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerDeclaration2855); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerDeclaration"


    // $ANTLR start "ruleIntegerDeclaration"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1459:1: ruleIntegerDeclaration returns [EObject current=null] : (otherlv_0= 'int' ( (lv_identifier_1_0= ruleVariableName ) ) ) ;
    public final EObject ruleIntegerDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_identifier_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1462:28: ( (otherlv_0= 'int' ( (lv_identifier_1_0= ruleVariableName ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1463:1: (otherlv_0= 'int' ( (lv_identifier_1_0= ruleVariableName ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1463:1: (otherlv_0= 'int' ( (lv_identifier_1_0= ruleVariableName ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1463:3: otherlv_0= 'int' ( (lv_identifier_1_0= ruleVariableName ) )
            {
            otherlv_0=(Token)match(input,28,FOLLOW_28_in_ruleIntegerDeclaration2892); 

                	newLeafNode(otherlv_0, grammarAccess.getIntegerDeclarationAccess().getIntKeyword_0());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1467:1: ( (lv_identifier_1_0= ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1468:1: (lv_identifier_1_0= ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1468:1: (lv_identifier_1_0= ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1469:3: lv_identifier_1_0= ruleVariableName
            {
             
            	        newCompositeNode(grammarAccess.getIntegerDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVariableName_in_ruleIntegerDeclaration2913);
            lv_identifier_1_0=ruleVariableName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerDeclarationRule());
            	        }
                   		set(
                   			current, 
                   			"identifier",
                    		lv_identifier_1_0, 
                    		"VariableName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerDeclaration"


    // $ANTLR start "entryRuleIntegerVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1493:1: entryRuleIntegerVarVar returns [EObject current=null] : iv_ruleIntegerVarVar= ruleIntegerVarVar EOF ;
    public final EObject entryRuleIntegerVarVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerVarVar = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1494:2: (iv_ruleIntegerVarVar= ruleIntegerVarVar EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1495:2: iv_ruleIntegerVarVar= ruleIntegerVarVar EOF
            {
             newCompositeNode(grammarAccess.getIntegerVarVarRule()); 
            pushFollow(FOLLOW_ruleIntegerVarVar_in_entryRuleIntegerVarVar2949);
            iv_ruleIntegerVarVar=ruleIntegerVarVar();

            state._fsp--;

             current =iv_ruleIntegerVarVar; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerVarVar2959); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerVarVar"


    // $ANTLR start "ruleIntegerVarVar"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1502:1: ruleIntegerVarVar returns [EObject current=null] : ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) ) ;
    public final EObject ruleIntegerVarVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_identifier_0_0 = null;

        EObject lv_value_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1505:28: ( ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1506:1: ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1506:1: ( ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1506:2: ( (lv_identifier_0_0= ruleVariableName ) ) otherlv_1= '=' ( (lv_value_2_0= ruleIntegerTypeExpression ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1506:2: ( (lv_identifier_0_0= ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1507:1: (lv_identifier_0_0= ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1507:1: (lv_identifier_0_0= ruleVariableName )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1508:3: lv_identifier_0_0= ruleVariableName
            {
             
            	        newCompositeNode(grammarAccess.getIntegerVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleVariableName_in_ruleIntegerVarVar3005);
            lv_identifier_0_0=ruleVariableName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerVarVarRule());
            	        }
                   		set(
                   			current, 
                   			"identifier",
                    		lv_identifier_0_0, 
                    		"VariableName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleIntegerVarVar3017); 

                	newLeafNode(otherlv_1, grammarAccess.getIntegerVarVarAccess().getEqualsSignKeyword_1());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1528:1: ( (lv_value_2_0= ruleIntegerTypeExpression ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1529:1: (lv_value_2_0= ruleIntegerTypeExpression )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1529:1: (lv_value_2_0= ruleIntegerTypeExpression )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1530:3: lv_value_2_0= ruleIntegerTypeExpression
            {
             
            	        newCompositeNode(grammarAccess.getIntegerVarVarAccess().getValueIntegerTypeExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleIntegerTypeExpression_in_ruleIntegerVarVar3038);
            lv_value_2_0=ruleIntegerTypeExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerVarVarRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_2_0, 
                    		"IntegerTypeExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerVarVar"


    // $ANTLR start "entryRuleIntegerTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1554:1: entryRuleIntegerTypeExpression returns [EObject current=null] : iv_ruleIntegerTypeExpression= ruleIntegerTypeExpression EOF ;
    public final EObject entryRuleIntegerTypeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerTypeExpression = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1555:2: (iv_ruleIntegerTypeExpression= ruleIntegerTypeExpression EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1556:2: iv_ruleIntegerTypeExpression= ruleIntegerTypeExpression EOF
            {
             newCompositeNode(grammarAccess.getIntegerTypeExpressionRule()); 
            pushFollow(FOLLOW_ruleIntegerTypeExpression_in_entryRuleIntegerTypeExpression3074);
            iv_ruleIntegerTypeExpression=ruleIntegerTypeExpression();

            state._fsp--;

             current =iv_ruleIntegerTypeExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerTypeExpression3084); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerTypeExpression"


    // $ANTLR start "ruleIntegerTypeExpression"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1563:1: ruleIntegerTypeExpression returns [EObject current=null] : ( ( (lv_integerAssign_0_0= ruleIntegerAssignment ) ) | ( (lv_integerOp_1_0= ruleIntegerOperation ) ) ) ;
    public final EObject ruleIntegerTypeExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_integerAssign_0_0 = null;

        EObject lv_integerOp_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1566:28: ( ( ( (lv_integerAssign_0_0= ruleIntegerAssignment ) ) | ( (lv_integerOp_1_0= ruleIntegerOperation ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1567:1: ( ( (lv_integerAssign_0_0= ruleIntegerAssignment ) ) | ( (lv_integerOp_1_0= ruleIntegerOperation ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1567:1: ( ( (lv_integerAssign_0_0= ruleIntegerAssignment ) ) | ( (lv_integerOp_1_0= ruleIntegerOperation ) ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_INTEGERS) ) {
                int LA15_1 = input.LA(2);

                if ( ((LA15_1>=36 && LA15_1<=39)) ) {
                    alt15=2;
                }
                else if ( (LA15_1==EOF||LA15_1==RULE_INTEGERS||LA15_1==RULE_ID||LA15_1==17||(LA15_1>=19 && LA15_1<=20)||(LA15_1>=22 && LA15_1<=23)||(LA15_1>=26 && LA15_1<=29)||LA15_1==33||(LA15_1>=40 && LA15_1<=42)) ) {
                    alt15=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 15, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA15_0==29) ) {
                int LA15_2 = input.LA(2);

                if ( (LA15_2==RULE_ID) ) {
                    int LA15_5 = input.LA(3);

                    if ( (LA15_5==EOF||LA15_5==RULE_INTEGERS||LA15_5==RULE_ID||LA15_5==17||(LA15_5>=19 && LA15_5<=20)||(LA15_5>=22 && LA15_5<=23)||(LA15_5>=26 && LA15_5<=29)||LA15_5==33||(LA15_5>=40 && LA15_5<=42)) ) {
                        alt15=1;
                    }
                    else if ( ((LA15_5>=36 && LA15_5<=39)) ) {
                        alt15=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 15, 5, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 15, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1567:2: ( (lv_integerAssign_0_0= ruleIntegerAssignment ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1567:2: ( (lv_integerAssign_0_0= ruleIntegerAssignment ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1568:1: (lv_integerAssign_0_0= ruleIntegerAssignment )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1568:1: (lv_integerAssign_0_0= ruleIntegerAssignment )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1569:3: lv_integerAssign_0_0= ruleIntegerAssignment
                    {
                     
                    	        newCompositeNode(grammarAccess.getIntegerTypeExpressionAccess().getIntegerAssignIntegerAssignmentParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleIntegerAssignment_in_ruleIntegerTypeExpression3130);
                    lv_integerAssign_0_0=ruleIntegerAssignment();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIntegerTypeExpressionRule());
                    	        }
                           		set(
                           			current, 
                           			"integerAssign",
                            		lv_integerAssign_0_0, 
                            		"IntegerAssignment");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1586:6: ( (lv_integerOp_1_0= ruleIntegerOperation ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1586:6: ( (lv_integerOp_1_0= ruleIntegerOperation ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1587:1: (lv_integerOp_1_0= ruleIntegerOperation )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1587:1: (lv_integerOp_1_0= ruleIntegerOperation )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1588:3: lv_integerOp_1_0= ruleIntegerOperation
                    {
                     
                    	        newCompositeNode(grammarAccess.getIntegerTypeExpressionAccess().getIntegerOpIntegerOperationParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleIntegerOperation_in_ruleIntegerTypeExpression3157);
                    lv_integerOp_1_0=ruleIntegerOperation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIntegerTypeExpressionRule());
                    	        }
                           		set(
                           			current, 
                           			"integerOp",
                            		lv_integerOp_1_0, 
                            		"IntegerOperation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerTypeExpression"


    // $ANTLR start "entryRuleIntegerAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1612:1: entryRuleIntegerAssignment returns [EObject current=null] : iv_ruleIntegerAssignment= ruleIntegerAssignment EOF ;
    public final EObject entryRuleIntegerAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerAssignment = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1613:2: (iv_ruleIntegerAssignment= ruleIntegerAssignment EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1614:2: iv_ruleIntegerAssignment= ruleIntegerAssignment EOF
            {
             newCompositeNode(grammarAccess.getIntegerAssignmentRule()); 
            pushFollow(FOLLOW_ruleIntegerAssignment_in_entryRuleIntegerAssignment3193);
            iv_ruleIntegerAssignment=ruleIntegerAssignment();

            state._fsp--;

             current =iv_ruleIntegerAssignment; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerAssignment3203); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerAssignment"


    // $ANTLR start "ruleIntegerAssignment"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1621:1: ruleIntegerAssignment returns [EObject current=null] : ( ( (lv_value_0_1= RULE_INTEGERS | lv_value_0_2= ruleVariableName ) ) ) ;
    public final EObject ruleIntegerAssignment() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        AntlrDatatypeRuleToken lv_value_0_2 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1624:28: ( ( ( (lv_value_0_1= RULE_INTEGERS | lv_value_0_2= ruleVariableName ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1625:1: ( ( (lv_value_0_1= RULE_INTEGERS | lv_value_0_2= ruleVariableName ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1625:1: ( ( (lv_value_0_1= RULE_INTEGERS | lv_value_0_2= ruleVariableName ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1626:1: ( (lv_value_0_1= RULE_INTEGERS | lv_value_0_2= ruleVariableName ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1626:1: ( (lv_value_0_1= RULE_INTEGERS | lv_value_0_2= ruleVariableName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1627:1: (lv_value_0_1= RULE_INTEGERS | lv_value_0_2= ruleVariableName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1627:1: (lv_value_0_1= RULE_INTEGERS | lv_value_0_2= ruleVariableName )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_INTEGERS) ) {
                alt16=1;
            }
            else if ( (LA16_0==29) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1628:3: lv_value_0_1= RULE_INTEGERS
                    {
                    lv_value_0_1=(Token)match(input,RULE_INTEGERS,FOLLOW_RULE_INTEGERS_in_ruleIntegerAssignment3246); 

                    			newLeafNode(lv_value_0_1, grammarAccess.getIntegerAssignmentAccess().getValueINTEGERSTerminalRuleCall_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getIntegerAssignmentRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_0_1, 
                            		"INTEGERS");
                    	    

                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1643:8: lv_value_0_2= ruleVariableName
                    {
                     
                    	        newCompositeNode(grammarAccess.getIntegerAssignmentAccess().getValueVariableNameParserRuleCall_0_1()); 
                    	    
                    pushFollow(FOLLOW_ruleVariableName_in_ruleIntegerAssignment3270);
                    lv_value_0_2=ruleVariableName();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIntegerAssignmentRule());
                    	        }
                           		set(
                           			current, 
                           			"value",
                            		lv_value_0_2, 
                            		"VariableName");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerAssignment"


    // $ANTLR start "entryRuleIntegerOperation"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1669:1: entryRuleIntegerOperation returns [EObject current=null] : iv_ruleIntegerOperation= ruleIntegerOperation EOF ;
    public final EObject entryRuleIntegerOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerOperation = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1670:2: (iv_ruleIntegerOperation= ruleIntegerOperation EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1671:2: iv_ruleIntegerOperation= ruleIntegerOperation EOF
            {
             newCompositeNode(grammarAccess.getIntegerOperationRule()); 
            pushFollow(FOLLOW_ruleIntegerOperation_in_entryRuleIntegerOperation3308);
            iv_ruleIntegerOperation=ruleIntegerOperation();

            state._fsp--;

             current =iv_ruleIntegerOperation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerOperation3318); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerOperation"


    // $ANTLR start "ruleIntegerOperation"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1678:1: ruleIntegerOperation returns [EObject current=null] : ( ( (lv_operand1_0_0= ruleIntegerAssignment ) ) ( (lv_operator_1_0= ruleOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerAssignment ) ) ) ;
    public final EObject ruleIntegerOperation() throws RecognitionException {
        EObject current = null;

        EObject lv_operand1_0_0 = null;

        Enumerator lv_operator_1_0 = null;

        EObject lv_operand2_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1681:28: ( ( ( (lv_operand1_0_0= ruleIntegerAssignment ) ) ( (lv_operator_1_0= ruleOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerAssignment ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1682:1: ( ( (lv_operand1_0_0= ruleIntegerAssignment ) ) ( (lv_operator_1_0= ruleOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerAssignment ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1682:1: ( ( (lv_operand1_0_0= ruleIntegerAssignment ) ) ( (lv_operator_1_0= ruleOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerAssignment ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1682:2: ( (lv_operand1_0_0= ruleIntegerAssignment ) ) ( (lv_operator_1_0= ruleOPERATOR ) ) ( (lv_operand2_2_0= ruleIntegerAssignment ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1682:2: ( (lv_operand1_0_0= ruleIntegerAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1683:1: (lv_operand1_0_0= ruleIntegerAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1683:1: (lv_operand1_0_0= ruleIntegerAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1684:3: lv_operand1_0_0= ruleIntegerAssignment
            {
             
            	        newCompositeNode(grammarAccess.getIntegerOperationAccess().getOperand1IntegerAssignmentParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleIntegerAssignment_in_ruleIntegerOperation3364);
            lv_operand1_0_0=ruleIntegerAssignment();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerOperationRule());
            	        }
                   		set(
                   			current, 
                   			"operand1",
                    		lv_operand1_0_0, 
                    		"IntegerAssignment");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1700:2: ( (lv_operator_1_0= ruleOPERATOR ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1701:1: (lv_operator_1_0= ruleOPERATOR )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1701:1: (lv_operator_1_0= ruleOPERATOR )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1702:3: lv_operator_1_0= ruleOPERATOR
            {
             
            	        newCompositeNode(grammarAccess.getIntegerOperationAccess().getOperatorOPERATOREnumRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleOPERATOR_in_ruleIntegerOperation3385);
            lv_operator_1_0=ruleOPERATOR();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerOperationRule());
            	        }
                   		set(
                   			current, 
                   			"operator",
                    		lv_operator_1_0, 
                    		"OPERATOR");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1718:2: ( (lv_operand2_2_0= ruleIntegerAssignment ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1719:1: (lv_operand2_2_0= ruleIntegerAssignment )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1719:1: (lv_operand2_2_0= ruleIntegerAssignment )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1720:3: lv_operand2_2_0= ruleIntegerAssignment
            {
             
            	        newCompositeNode(grammarAccess.getIntegerOperationAccess().getOperand2IntegerAssignmentParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleIntegerAssignment_in_ruleIntegerOperation3406);
            lv_operand2_2_0=ruleIntegerAssignment();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerOperationRule());
            	        }
                   		set(
                   			current, 
                   			"operand2",
                    		lv_operand2_2_0, 
                    		"IntegerAssignment");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerOperation"


    // $ANTLR start "entryRuleVariableName"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1744:1: entryRuleVariableName returns [String current=null] : iv_ruleVariableName= ruleVariableName EOF ;
    public final String entryRuleVariableName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleVariableName = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1745:2: (iv_ruleVariableName= ruleVariableName EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1746:2: iv_ruleVariableName= ruleVariableName EOF
            {
             newCompositeNode(grammarAccess.getVariableNameRule()); 
            pushFollow(FOLLOW_ruleVariableName_in_entryRuleVariableName3443);
            iv_ruleVariableName=ruleVariableName();

            state._fsp--;

             current =iv_ruleVariableName.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariableName3454); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableName"


    // $ANTLR start "ruleVariableName"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1753:1: ruleVariableName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '\\u20AC' this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleVariableName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1756:28: ( (kw= '\\u20AC' this_ID_1= RULE_ID ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1757:1: (kw= '\\u20AC' this_ID_1= RULE_ID )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1757:1: (kw= '\\u20AC' this_ID_1= RULE_ID )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1758:2: kw= '\\u20AC' this_ID_1= RULE_ID
            {
            kw=(Token)match(input,29,FOLLOW_29_in_ruleVariableName3492); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getVariableNameAccess().getEuroSignKeyword_0()); 
                
            this_ID_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVariableName3507); 

            		current.merge(this_ID_1);
                
             
                newLeafNode(this_ID_1, grammarAccess.getVariableNameAccess().getIDTerminalRuleCall_1()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableName"


    // $ANTLR start "entryRuleCallFunction"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1778:1: entryRuleCallFunction returns [EObject current=null] : iv_ruleCallFunction= ruleCallFunction EOF ;
    public final EObject entryRuleCallFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallFunction = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1779:2: (iv_ruleCallFunction= ruleCallFunction EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1780:2: iv_ruleCallFunction= ruleCallFunction EOF
            {
             newCompositeNode(grammarAccess.getCallFunctionRule()); 
            pushFollow(FOLLOW_ruleCallFunction_in_entryRuleCallFunction3552);
            iv_ruleCallFunction=ruleCallFunction();

            state._fsp--;

             current =iv_ruleCallFunction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCallFunction3562); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallFunction"


    // $ANTLR start "ruleCallFunction"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1787:1: ruleCallFunction returns [EObject current=null] : ( ( (lv_operationName_0_0= ruleFunctionName ) ) otherlv_1= '(' otherlv_2= ')' ) ;
    public final EObject ruleCallFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_operationName_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1790:28: ( ( ( (lv_operationName_0_0= ruleFunctionName ) ) otherlv_1= '(' otherlv_2= ')' ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1791:1: ( ( (lv_operationName_0_0= ruleFunctionName ) ) otherlv_1= '(' otherlv_2= ')' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1791:1: ( ( (lv_operationName_0_0= ruleFunctionName ) ) otherlv_1= '(' otherlv_2= ')' )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1791:2: ( (lv_operationName_0_0= ruleFunctionName ) ) otherlv_1= '(' otherlv_2= ')'
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1791:2: ( (lv_operationName_0_0= ruleFunctionName ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1792:1: (lv_operationName_0_0= ruleFunctionName )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1792:1: (lv_operationName_0_0= ruleFunctionName )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1793:3: lv_operationName_0_0= ruleFunctionName
            {
             
            	        newCompositeNode(grammarAccess.getCallFunctionAccess().getOperationNameFunctionNameParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleFunctionName_in_ruleCallFunction3608);
            lv_operationName_0_0=ruleFunctionName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getCallFunctionRule());
            	        }
                   		set(
                   			current, 
                   			"operationName",
                    		lv_operationName_0_0, 
                    		"FunctionName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,18,FOLLOW_18_in_ruleCallFunction3620); 

                	newLeafNode(otherlv_1, grammarAccess.getCallFunctionAccess().getLeftParenthesisKeyword_1());
                
            otherlv_2=(Token)match(input,19,FOLLOW_19_in_ruleCallFunction3632); 

                	newLeafNode(otherlv_2, grammarAccess.getCallFunctionAccess().getRightParenthesisKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallFunction"


    // $ANTLR start "entryRuleFunctionName"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1825:1: entryRuleFunctionName returns [EObject current=null] : iv_ruleFunctionName= ruleFunctionName EOF ;
    public final EObject entryRuleFunctionName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionName = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1826:2: (iv_ruleFunctionName= ruleFunctionName EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1827:2: iv_ruleFunctionName= ruleFunctionName EOF
            {
             newCompositeNode(grammarAccess.getFunctionNameRule()); 
            pushFollow(FOLLOW_ruleFunctionName_in_entryRuleFunctionName3668);
            iv_ruleFunctionName=ruleFunctionName();

            state._fsp--;

             current =iv_ruleFunctionName; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunctionName3678); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionName"


    // $ANTLR start "ruleFunctionName"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1834:1: ruleFunctionName returns [EObject current=null] : ( (lv_value_0_0= RULE_ID ) ) ;
    public final EObject ruleFunctionName() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1837:28: ( ( (lv_value_0_0= RULE_ID ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1838:1: ( (lv_value_0_0= RULE_ID ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1838:1: ( (lv_value_0_0= RULE_ID ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1839:1: (lv_value_0_0= RULE_ID )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1839:1: (lv_value_0_0= RULE_ID )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1840:3: lv_value_0_0= RULE_ID
            {
            lv_value_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFunctionName3719); 

            			newLeafNode(lv_value_0_0, grammarAccess.getFunctionNameAccess().getValueIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFunctionNameRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionName"


    // $ANTLR start "entryRuleBOOLOPERATOR"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1864:1: entryRuleBOOLOPERATOR returns [EObject current=null] : iv_ruleBOOLOPERATOR= ruleBOOLOPERATOR EOF ;
    public final EObject entryRuleBOOLOPERATOR() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBOOLOPERATOR = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1865:2: (iv_ruleBOOLOPERATOR= ruleBOOLOPERATOR EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1866:2: iv_ruleBOOLOPERATOR= ruleBOOLOPERATOR EOF
            {
             newCompositeNode(grammarAccess.getBOOLOPERATORRule()); 
            pushFollow(FOLLOW_ruleBOOLOPERATOR_in_entryRuleBOOLOPERATOR3759);
            iv_ruleBOOLOPERATOR=ruleBOOLOPERATOR();

            state._fsp--;

             current =iv_ruleBOOLOPERATOR; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBOOLOPERATOR3769); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBOOLOPERATOR"


    // $ANTLR start "ruleBOOLOPERATOR"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1873:1: ruleBOOLOPERATOR returns [EObject current=null] : ( ( (lv_AND_0_0= 'ir' ) ) | ( (lv_OR_1_0= 'arba' ) ) ) ;
    public final EObject ruleBOOLOPERATOR() throws RecognitionException {
        EObject current = null;

        Token lv_AND_0_0=null;
        Token lv_OR_1_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1876:28: ( ( ( (lv_AND_0_0= 'ir' ) ) | ( (lv_OR_1_0= 'arba' ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1877:1: ( ( (lv_AND_0_0= 'ir' ) ) | ( (lv_OR_1_0= 'arba' ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1877:1: ( ( (lv_AND_0_0= 'ir' ) ) | ( (lv_OR_1_0= 'arba' ) ) )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==30) ) {
                alt17=1;
            }
            else if ( (LA17_0==31) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1877:2: ( (lv_AND_0_0= 'ir' ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1877:2: ( (lv_AND_0_0= 'ir' ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1878:1: (lv_AND_0_0= 'ir' )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1878:1: (lv_AND_0_0= 'ir' )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1879:3: lv_AND_0_0= 'ir'
                    {
                    lv_AND_0_0=(Token)match(input,30,FOLLOW_30_in_ruleBOOLOPERATOR3812); 

                            newLeafNode(lv_AND_0_0, grammarAccess.getBOOLOPERATORAccess().getANDIrKeyword_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBOOLOPERATORRule());
                    	        }
                           		setWithLastConsumed(current, "AND", lv_AND_0_0, "ir");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1893:6: ( (lv_OR_1_0= 'arba' ) )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1893:6: ( (lv_OR_1_0= 'arba' ) )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1894:1: (lv_OR_1_0= 'arba' )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1894:1: (lv_OR_1_0= 'arba' )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1895:3: lv_OR_1_0= 'arba'
                    {
                    lv_OR_1_0=(Token)match(input,31,FOLLOW_31_in_ruleBOOLOPERATOR3849); 

                            newLeafNode(lv_OR_1_0, grammarAccess.getBOOLOPERATORAccess().getORArbaKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBOOLOPERATORRule());
                    	        }
                           		setWithLastConsumed(current, "OR", lv_OR_1_0, "arba");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBOOLOPERATOR"


    // $ANTLR start "entryRuleFunction"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1916:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1917:2: (iv_ruleFunction= ruleFunction EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1918:2: iv_ruleFunction= ruleFunction EOF
            {
             newCompositeNode(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_ruleFunction_in_entryRuleFunction3898);
            iv_ruleFunction=ruleFunction();

            state._fsp--;

             current =iv_ruleFunction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunction3908); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1925:1: ruleFunction returns [EObject current=null] : (otherlv_0= 'void' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_features_5_0= ruleType ) )* otherlv_6= '}' ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_features_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1928:28: ( (otherlv_0= 'void' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_features_5_0= ruleType ) )* otherlv_6= '}' ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1929:1: (otherlv_0= 'void' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_features_5_0= ruleType ) )* otherlv_6= '}' )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1929:1: (otherlv_0= 'void' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_features_5_0= ruleType ) )* otherlv_6= '}' )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1929:3: otherlv_0= 'void' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= '{' ( (lv_features_5_0= ruleType ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,32,FOLLOW_32_in_ruleFunction3945); 

                	newLeafNode(otherlv_0, grammarAccess.getFunctionAccess().getVoidKeyword_0());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1933:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1934:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1934:1: (lv_name_1_0= RULE_ID )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1935:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFunction3962); 

            			newLeafNode(lv_name_1_0, grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFunctionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleFunction3979); 

                	newLeafNode(otherlv_2, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2());
                
            otherlv_3=(Token)match(input,19,FOLLOW_19_in_ruleFunction3991); 

                	newLeafNode(otherlv_3, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_3());
                
            otherlv_4=(Token)match(input,21,FOLLOW_21_in_ruleFunction4003); 

                	newLeafNode(otherlv_4, grammarAccess.getFunctionAccess().getLeftCurlyBracketKeyword_4());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1963:1: ( (lv_features_5_0= ruleType ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==RULE_INTEGERS||LA18_0==RULE_ID||LA18_0==17||LA18_0==20||LA18_0==23||(LA18_0>=26 && LA18_0<=29)||LA18_0==33) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1964:1: (lv_features_5_0= ruleType )
            	    {
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1964:1: (lv_features_5_0= ruleType )
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1965:3: lv_features_5_0= ruleType
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFunctionAccess().getFeaturesTypeParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleType_in_ruleFunction4024);
            	    lv_features_5_0=ruleType();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFunctionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"features",
            	            		lv_features_5_0, 
            	            		"Type");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            otherlv_6=(Token)match(input,22,FOLLOW_22_in_ruleFunction4037); 

                	newLeafNode(otherlv_6, grammarAccess.getFunctionAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleImport"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1993:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1994:2: (iv_ruleImport= ruleImport EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:1995:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_ruleImport_in_entryRuleImport4073);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImport4083); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2002:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2005:28: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2006:1: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2006:1: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2006:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            {
            otherlv_0=(Token)match(input,33,FOLLOW_33_in_ruleImport4120); 

                	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2010:1: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2011:1: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2011:1: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2012:3: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {
             
            	        newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_ruleImport4141);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getImportRule());
            	        }
                   		set(
                   			current, 
                   			"importedNamespace",
                    		lv_importedNamespace_1_0, 
                    		"QualifiedNameWithWildcard");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2036:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2037:2: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2038:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard4178);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard4189); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2045:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2048:28: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2049:1: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2049:1: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2050:5: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {
             
                    newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard4236);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            		current.merge(this_QualifiedName_0);
                
             
                    afterParserOrEnumRuleCall();
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2060:1: (kw= '.*' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==34) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2061:2: kw= '.*'
                    {
                    kw=(Token)match(input,34,FOLLOW_34_in_ruleQualifiedNameWithWildcard4255); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2074:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2075:2: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2076:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName4298);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedName4309); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2083:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2086:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2087:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2087:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2087:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQualifiedName4349); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
                
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2094:1: (kw= '.' this_ID_2= RULE_ID )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==35) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2095:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,35,FOLLOW_35_in_ruleQualifiedName4368); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            	        
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQualifiedName4383); 

            	    		current.merge(this_ID_2);
            	        
            	     
            	        newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "ruleOPERATOR"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2115:1: ruleOPERATOR returns [Enumerator current=null] : ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '*' ) | (enumLiteral_3= '/' ) ) ;
    public final Enumerator ruleOPERATOR() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2117:28: ( ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '*' ) | (enumLiteral_3= '/' ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2118:1: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '*' ) | (enumLiteral_3= '/' ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2118:1: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) | (enumLiteral_2= '*' ) | (enumLiteral_3= '/' ) )
            int alt21=4;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt21=1;
                }
                break;
            case 37:
                {
                alt21=2;
                }
                break;
            case 38:
                {
                alt21=3;
                }
                break;
            case 39:
                {
                alt21=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }

            switch (alt21) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2118:2: (enumLiteral_0= '+' )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2118:2: (enumLiteral_0= '+' )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2118:4: enumLiteral_0= '+'
                    {
                    enumLiteral_0=(Token)match(input,36,FOLLOW_36_in_ruleOPERATOR4444); 

                            current = grammarAccess.getOPERATORAccess().getADDITIONEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getOPERATORAccess().getADDITIONEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2124:6: (enumLiteral_1= '-' )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2124:6: (enumLiteral_1= '-' )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2124:8: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,37,FOLLOW_37_in_ruleOPERATOR4461); 

                            current = grammarAccess.getOPERATORAccess().getSUBRACTIONEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getOPERATORAccess().getSUBRACTIONEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2130:6: (enumLiteral_2= '*' )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2130:6: (enumLiteral_2= '*' )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2130:8: enumLiteral_2= '*'
                    {
                    enumLiteral_2=(Token)match(input,38,FOLLOW_38_in_ruleOPERATOR4478); 

                            current = grammarAccess.getOPERATORAccess().getMULTIPLYEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getOPERATORAccess().getMULTIPLYEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2136:6: (enumLiteral_3= '/' )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2136:6: (enumLiteral_3= '/' )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2136:8: enumLiteral_3= '/'
                    {
                    enumLiteral_3=(Token)match(input,39,FOLLOW_39_in_ruleOPERATOR4495); 

                            current = grammarAccess.getOPERATORAccess().getDIVISIONEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getOPERATORAccess().getDIVISIONEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOPERATOR"


    // $ANTLR start "ruleSPECIALINTOPERATOR"
    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2146:1: ruleSPECIALINTOPERATOR returns [Enumerator current=null] : ( (enumLiteral_0= '==' ) | (enumLiteral_1= '<' ) | (enumLiteral_2= '>' ) ) ;
    public final Enumerator ruleSPECIALINTOPERATOR() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2148:28: ( ( (enumLiteral_0= '==' ) | (enumLiteral_1= '<' ) | (enumLiteral_2= '>' ) ) )
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2149:1: ( (enumLiteral_0= '==' ) | (enumLiteral_1= '<' ) | (enumLiteral_2= '>' ) )
            {
            // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2149:1: ( (enumLiteral_0= '==' ) | (enumLiteral_1= '<' ) | (enumLiteral_2= '>' ) )
            int alt22=3;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt22=1;
                }
                break;
            case 41:
                {
                alt22=2;
                }
                break;
            case 42:
                {
                alt22=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2149:2: (enumLiteral_0= '==' )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2149:2: (enumLiteral_0= '==' )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2149:4: enumLiteral_0= '=='
                    {
                    enumLiteral_0=(Token)match(input,40,FOLLOW_40_in_ruleSPECIALINTOPERATOR4540); 

                            current = grammarAccess.getSPECIALINTOPERATORAccess().getEQUALSEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getSPECIALINTOPERATORAccess().getEQUALSEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2155:6: (enumLiteral_1= '<' )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2155:6: (enumLiteral_1= '<' )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2155:8: enumLiteral_1= '<'
                    {
                    enumLiteral_1=(Token)match(input,41,FOLLOW_41_in_ruleSPECIALINTOPERATOR4557); 

                            current = grammarAccess.getSPECIALINTOPERATORAccess().getLESSEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getSPECIALINTOPERATORAccess().getLESSEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2161:6: (enumLiteral_2= '>' )
                    {
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2161:6: (enumLiteral_2= '>' )
                    // ../org.xtext.Ceur.Ceur.Ceur/src-gen/org/xtext/Ceur/Ceur/parser/antlr/internal/InternalCeur.g:2161:8: enumLiteral_2= '>'
                    {
                    enumLiteral_2=(Token)match(input,42,FOLLOW_42_in_ruleSPECIALINTOPERATOR4574); 

                            current = grammarAccess.getSPECIALINTOPERATORAccess().getMOREEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getSPECIALINTOPERATORAccess().getMOREEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSPECIALINTOPERATOR"

    // Delegated rules


    protected DFA2 dfa2 = new DFA2(this);
    static final String DFA2_eotS =
        "\21\uffff";
    static final String DFA2_eofS =
        "\20\uffff\1\17";
    static final String DFA2_minS =
        "\1\5\1\uffff\1\7\10\uffff\1\31\1\4\1\uffff\1\7\1\uffff\1\5";
    static final String DFA2_maxS =
        "\1\41\1\uffff\1\7\10\uffff\1\52\1\35\1\uffff\1\7\1\uffff\1\47";
    static final String DFA2_acceptS =
        "\1\uffff\1\1\1\uffff\1\3\1\4\1\5\1\7\1\10\1\11\1\12\1\13\2\uffff"+
        "\1\6\1\uffff\1\2\1\uffff";
    static final String DFA2_specialS =
        "\21\uffff}>";
    static final String[] DFA2_transitionS = {
            "\1\4\1\uffff\1\11\11\uffff\1\10\2\uffff\1\6\2\uffff\1\7\2\uffff"+
            "\1\5\1\3\1\1\1\2\3\uffff\1\12",
            "",
            "\1\13",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\14\12\uffff\7\4",
            "\1\15\1\17\1\4\26\uffff\1\16",
            "",
            "\1\20",
            "",
            "\1\17\1\uffff\1\17\11\uffff\1\17\2\uffff\1\17\1\uffff\2\17"+
            "\2\uffff\4\17\2\4\1\uffff\1\17\2\uffff\4\17"
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "120:1: ( ( (lv_integer_0_0= ruleIntegerDeclarationAss ) ) | ( (lv_integervarvar_1_0= ruleIntegerVarVar ) ) | ( (lv_boolean_2_0= ruleBooleanDeclarationAss ) ) | ( (lv_booleanvarvar_3_0= ruleBooleanVarVar ) ) | ( (lv_character_4_0= ruleCharacterDeclarationAss ) ) | ( (lv_charactervarvar_5_0= ruleCharacterVarVar ) ) | ( (lv_while_6_0= ruleStatement_While ) ) | ( (lv_if_7_0= ruleIf_statment ) ) | ( (lv_print_8_0= rulePrintL ) ) | ( (lv_function_9_0= ruleCallFunction ) ) | this_Import_10= ruleImport )";
        }
    }
 

    public static final BitSet FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDomainmodel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunction_in_ruleDomainmodel130 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_ruleType_in_entryRuleType166 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleType176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerDeclarationAss_in_ruleType222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerVarVar_in_ruleType249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanDeclarationAss_in_ruleType276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanVarVar_in_ruleType303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterDeclarationAss_in_ruleType330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterVarVar_in_ruleType357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_While_in_ruleType384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_statment_in_ruleType411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrintL_in_ruleType438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCallFunction_in_ruleType465 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImport_in_ruleType493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrintL_in_entryRulePrintL528 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrintL538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rulePrintL581 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rulePrintL606 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_RULE_CHARACTERS_in_rulePrintL625 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_RULE_INTEGERS_in_rulePrintL645 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_RULE_BOOLVAL_in_rulePrintL665 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_ruleVariableName_in_rulePrintL689 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rulePrintL704 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_While_in_entryRuleStatement_While740 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatement_While750 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleStatement_While787 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleStatement_While799 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleBooleanTypeExpression_in_ruleStatement_While820 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleStatement_While832 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleStatement_While844 = new BitSet(new long[]{0x000000023CD200F0L});
    public static final BitSet FOLLOW_ruleType_in_ruleStatement_While865 = new BitSet(new long[]{0x000000023CD200F0L});
    public static final BitSet FOLLOW_22_in_ruleStatement_While878 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_statment_in_entryRuleIf_statment914 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIf_statment924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleIf_statment961 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleIf_statment973 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleBooleanTypeExpression_in_ruleIf_statment994 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleIf_statment1006 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleIf_statment1018 = new BitSet(new long[]{0x000000023CD200F0L});
    public static final BitSet FOLLOW_ruleType_in_ruleIf_statment1039 = new BitSet(new long[]{0x000000023CD200F0L});
    public static final BitSet FOLLOW_22_in_ruleIf_statment1052 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_ruleElse_in_ruleIf_statment1073 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleElse_in_entryRuleElse1110 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleElse1120 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleElse1157 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleElse1169 = new BitSet(new long[]{0x000000023CD200F0L});
    public static final BitSet FOLLOW_ruleType_in_ruleElse1190 = new BitSet(new long[]{0x000000023CD200F0L});
    public static final BitSet FOLLOW_22_in_ruleElse1203 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterDeclarationAss_in_entryRuleCharacterDeclarationAss1239 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterDeclarationAss1249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterDeclaration_in_ruleCharacterDeclarationAss1295 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_25_in_ruleCharacterDeclarationAss1308 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleCharacterTypeExpression_in_ruleCharacterDeclarationAss1329 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterDeclaration_in_entryRuleCharacterDeclaration1367 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterDeclaration1377 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_ruleCharacterDeclaration1414 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleVariableName_in_ruleCharacterDeclaration1435 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterTypeExpression_in_entryRuleCharacterTypeExpression1471 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterTypeExpression1481 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterAssignment_in_ruleCharacterTypeExpression1526 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterAssignment_in_entryRuleCharacterAssignment1561 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterAssignment1571 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_CHARACTERS_in_ruleCharacterAssignment1614 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_ruleCharacterAssignment1638 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCharacterVarVar_in_entryRuleCharacterVarVar1676 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCharacterVarVar1686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_ruleCharacterVarVar1732 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleCharacterVarVar1744 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_CHARACTERS_in_ruleCharacterVarVar1761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanDeclarationAss_in_entryRuleBooleanDeclarationAss1802 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanDeclarationAss1812 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanDeclaration_in_ruleBooleanDeclarationAss1858 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_25_in_ruleBooleanDeclarationAss1871 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleBooleanTypeExpression_in_ruleBooleanDeclarationAss1892 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanDeclaration_in_entryRuleBooleanDeclaration1930 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanDeclaration1940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleBooleanDeclaration1977 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleVariableName_in_ruleBooleanDeclaration1998 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanTypeExpression_in_entryRuleBooleanTypeExpression2034 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanTypeExpression2044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanAssignment_in_ruleBooleanTypeExpression2090 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanOperation_in_ruleBooleanTypeExpression2117 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerToBool_in_ruleBooleanTypeExpression2144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanVarVar_in_entryRuleBooleanVarVar2180 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanVarVar2190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_ruleBooleanVarVar2237 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleBooleanVarVar2249 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleBooleanOperation_in_ruleBooleanVarVar2270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerToBool_in_ruleBooleanVarVar2298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanAssignment_in_entryRuleBooleanAssignment2334 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanAssignment2344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_BOOLVAL_in_ruleBooleanAssignment2387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_ruleBooleanAssignment2411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanOperation_in_entryRuleBooleanOperation2449 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanOperation2459 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanAssignment_in_ruleBooleanOperation2505 = new BitSet(new long[]{0x00000000C0000000L});
    public static final BitSet FOLLOW_ruleBOOLOPERATOR_in_ruleBooleanOperation2526 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleBooleanAssignment_in_ruleBooleanOperation2547 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerToBool_in_entryRuleIntegerToBool2583 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerToBool2593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_ruleIntegerToBool2639 = new BitSet(new long[]{0x0000070000000000L});
    public static final BitSet FOLLOW_ruleSPECIALINTOPERATOR_in_ruleIntegerToBool2660 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_ruleIntegerToBool2681 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerDeclarationAss_in_entryRuleIntegerDeclarationAss2717 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerDeclarationAss2727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerDeclaration_in_ruleIntegerDeclarationAss2773 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_25_in_ruleIntegerDeclarationAss2786 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_ruleIntegerDeclarationAss2807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerDeclaration_in_entryRuleIntegerDeclaration2845 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerDeclaration2855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_ruleIntegerDeclaration2892 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleVariableName_in_ruleIntegerDeclaration2913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerVarVar_in_entryRuleIntegerVarVar2949 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerVarVar2959 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_ruleIntegerVarVar3005 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleIntegerVarVar3017 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_ruleIntegerVarVar3038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerTypeExpression_in_entryRuleIntegerTypeExpression3074 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerTypeExpression3084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerAssignment_in_ruleIntegerTypeExpression3130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerOperation_in_ruleIntegerTypeExpression3157 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerAssignment_in_entryRuleIntegerAssignment3193 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerAssignment3203 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INTEGERS_in_ruleIntegerAssignment3246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_ruleIntegerAssignment3270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerOperation_in_entryRuleIntegerOperation3308 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerOperation3318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerAssignment_in_ruleIntegerOperation3364 = new BitSet(new long[]{0x000000F000000000L});
    public static final BitSet FOLLOW_ruleOPERATOR_in_ruleIntegerOperation3385 = new BitSet(new long[]{0x0000000020000070L});
    public static final BitSet FOLLOW_ruleIntegerAssignment_in_ruleIntegerOperation3406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariableName_in_entryRuleVariableName3443 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariableName3454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleVariableName3492 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVariableName3507 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCallFunction_in_entryRuleCallFunction3552 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCallFunction3562 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunctionName_in_ruleCallFunction3608 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleCallFunction3620 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleCallFunction3632 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunctionName_in_entryRuleFunctionName3668 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunctionName3678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFunctionName3719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBOOLOPERATOR_in_entryRuleBOOLOPERATOR3759 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBOOLOPERATOR3769 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleBOOLOPERATOR3812 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_ruleBOOLOPERATOR3849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunction_in_entryRuleFunction3898 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunction3908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_ruleFunction3945 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFunction3962 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleFunction3979 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleFunction3991 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleFunction4003 = new BitSet(new long[]{0x000000023CD200F0L});
    public static final BitSet FOLLOW_ruleType_in_ruleFunction4024 = new BitSet(new long[]{0x000000023CD200F0L});
    public static final BitSet FOLLOW_22_in_ruleFunction4037 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImport_in_entryRuleImport4073 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImport4083 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_ruleImport4120 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_ruleImport4141 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedNameWithWildcard_in_entryRuleQualifiedNameWithWildcard4178 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedNameWithWildcard4189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleQualifiedNameWithWildcard4236 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34_in_ruleQualifiedNameWithWildcard4255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName4298 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName4309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQualifiedName4349 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_35_in_ruleQualifiedName4368 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQualifiedName4383 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_36_in_ruleOPERATOR4444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_ruleOPERATOR4461 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_ruleOPERATOR4478 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleOPERATOR4495 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_ruleSPECIALINTOPERATOR4540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_ruleSPECIALINTOPERATOR4557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruleSPECIALINTOPERATOR4574 = new BitSet(new long[]{0x0000000000000002L});

}