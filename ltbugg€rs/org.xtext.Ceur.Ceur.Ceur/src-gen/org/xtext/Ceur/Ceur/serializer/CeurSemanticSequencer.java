/*
 * generated by Xtext
 */
package org.xtext.Ceur.Ceur.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.xtext.Ceur.Ceur.ceur.BOOLOPERATOR;
import org.xtext.Ceur.Ceur.ceur.BooleanAssignment;
import org.xtext.Ceur.Ceur.ceur.BooleanDeclaration;
import org.xtext.Ceur.Ceur.ceur.BooleanDeclarationAss;
import org.xtext.Ceur.Ceur.ceur.BooleanOperation;
import org.xtext.Ceur.Ceur.ceur.BooleanTypeExpression;
import org.xtext.Ceur.Ceur.ceur.BooleanVarVar;
import org.xtext.Ceur.Ceur.ceur.CallFunction;
import org.xtext.Ceur.Ceur.ceur.CeurPackage;
import org.xtext.Ceur.Ceur.ceur.CharacterAssignment;
import org.xtext.Ceur.Ceur.ceur.CharacterDeclaration;
import org.xtext.Ceur.Ceur.ceur.CharacterDeclarationAss;
import org.xtext.Ceur.Ceur.ceur.CharacterTypeExpression;
import org.xtext.Ceur.Ceur.ceur.CharacterVarVar;
import org.xtext.Ceur.Ceur.ceur.Domainmodel;
import org.xtext.Ceur.Ceur.ceur.Else;
import org.xtext.Ceur.Ceur.ceur.Function;
import org.xtext.Ceur.Ceur.ceur.FunctionName;
import org.xtext.Ceur.Ceur.ceur.If_statment;
import org.xtext.Ceur.Ceur.ceur.Import;
import org.xtext.Ceur.Ceur.ceur.IntegerAssignment;
import org.xtext.Ceur.Ceur.ceur.IntegerDeclaration;
import org.xtext.Ceur.Ceur.ceur.IntegerDeclarationAss;
import org.xtext.Ceur.Ceur.ceur.IntegerOperation;
import org.xtext.Ceur.Ceur.ceur.IntegerToBool;
import org.xtext.Ceur.Ceur.ceur.IntegerTypeExpression;
import org.xtext.Ceur.Ceur.ceur.IntegerVarVar;
import org.xtext.Ceur.Ceur.ceur.PrintL;
import org.xtext.Ceur.Ceur.ceur.Statement_While;
import org.xtext.Ceur.Ceur.ceur.Type;
import org.xtext.Ceur.Ceur.services.CeurGrammarAccess;

@SuppressWarnings("all")
public class CeurSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private CeurGrammarAccess grammarAccess;
	
	@Override
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == CeurPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case CeurPackage.BOOLOPERATOR:
				sequence_BOOLOPERATOR(context, (BOOLOPERATOR) semanticObject); 
				return; 
			case CeurPackage.BOOLEAN_ASSIGNMENT:
				sequence_BooleanAssignment(context, (BooleanAssignment) semanticObject); 
				return; 
			case CeurPackage.BOOLEAN_DECLARATION:
				sequence_BooleanDeclaration(context, (BooleanDeclaration) semanticObject); 
				return; 
			case CeurPackage.BOOLEAN_DECLARATION_ASS:
				sequence_BooleanDeclarationAss(context, (BooleanDeclarationAss) semanticObject); 
				return; 
			case CeurPackage.BOOLEAN_OPERATION:
				sequence_BooleanOperation(context, (BooleanOperation) semanticObject); 
				return; 
			case CeurPackage.BOOLEAN_TYPE_EXPRESSION:
				sequence_BooleanTypeExpression(context, (BooleanTypeExpression) semanticObject); 
				return; 
			case CeurPackage.BOOLEAN_VAR_VAR:
				sequence_BooleanVarVar(context, (BooleanVarVar) semanticObject); 
				return; 
			case CeurPackage.CALL_FUNCTION:
				sequence_CallFunction(context, (CallFunction) semanticObject); 
				return; 
			case CeurPackage.CHARACTER_ASSIGNMENT:
				sequence_CharacterAssignment(context, (CharacterAssignment) semanticObject); 
				return; 
			case CeurPackage.CHARACTER_DECLARATION:
				sequence_CharacterDeclaration(context, (CharacterDeclaration) semanticObject); 
				return; 
			case CeurPackage.CHARACTER_DECLARATION_ASS:
				sequence_CharacterDeclarationAss(context, (CharacterDeclarationAss) semanticObject); 
				return; 
			case CeurPackage.CHARACTER_TYPE_EXPRESSION:
				sequence_CharacterTypeExpression(context, (CharacterTypeExpression) semanticObject); 
				return; 
			case CeurPackage.CHARACTER_VAR_VAR:
				sequence_CharacterVarVar(context, (CharacterVarVar) semanticObject); 
				return; 
			case CeurPackage.DOMAINMODEL:
				sequence_Domainmodel(context, (Domainmodel) semanticObject); 
				return; 
			case CeurPackage.ELSE:
				sequence_Else(context, (Else) semanticObject); 
				return; 
			case CeurPackage.FUNCTION:
				sequence_Function(context, (Function) semanticObject); 
				return; 
			case CeurPackage.FUNCTION_NAME:
				sequence_FunctionName(context, (FunctionName) semanticObject); 
				return; 
			case CeurPackage.IF_STATMENT:
				sequence_If_statment(context, (If_statment) semanticObject); 
				return; 
			case CeurPackage.IMPORT:
				sequence_Import(context, (Import) semanticObject); 
				return; 
			case CeurPackage.INTEGER_ASSIGNMENT:
				sequence_IntegerAssignment(context, (IntegerAssignment) semanticObject); 
				return; 
			case CeurPackage.INTEGER_DECLARATION:
				sequence_IntegerDeclaration(context, (IntegerDeclaration) semanticObject); 
				return; 
			case CeurPackage.INTEGER_DECLARATION_ASS:
				sequence_IntegerDeclarationAss(context, (IntegerDeclarationAss) semanticObject); 
				return; 
			case CeurPackage.INTEGER_OPERATION:
				sequence_IntegerOperation(context, (IntegerOperation) semanticObject); 
				return; 
			case CeurPackage.INTEGER_TO_BOOL:
				sequence_IntegerToBool(context, (IntegerToBool) semanticObject); 
				return; 
			case CeurPackage.INTEGER_TYPE_EXPRESSION:
				sequence_IntegerTypeExpression(context, (IntegerTypeExpression) semanticObject); 
				return; 
			case CeurPackage.INTEGER_VAR_VAR:
				sequence_IntegerVarVar(context, (IntegerVarVar) semanticObject); 
				return; 
			case CeurPackage.PRINT_L:
				sequence_PrintL(context, (PrintL) semanticObject); 
				return; 
			case CeurPackage.STATEMENT_WHILE:
				sequence_Statement_While(context, (Statement_While) semanticObject); 
				return; 
			case CeurPackage.TYPE:
				sequence_Type(context, (Type) semanticObject); 
				return; 
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (AND='ir' | OR='arba')
	 */
	protected void sequence_BOOLOPERATOR(EObject context, BOOLOPERATOR semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (value=BOOLVAL | value=VariableName)
	 */
	protected void sequence_BooleanAssignment(EObject context, BooleanAssignment semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (assign=BooleanDeclaration value=BooleanTypeExpression?)
	 */
	protected void sequence_BooleanDeclarationAss(EObject context, BooleanDeclarationAss semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     identifier=VariableName
	 */
	protected void sequence_BooleanDeclaration(EObject context, BooleanDeclaration semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.BOOLEAN_DECLARATION__IDENTIFIER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.BOOLEAN_DECLARATION__IDENTIFIER));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBooleanDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0(), semanticObject.getIdentifier());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (operand1=BooleanAssignment operator=BOOLOPERATOR operand2=BooleanAssignment)
	 */
	protected void sequence_BooleanOperation(EObject context, BooleanOperation semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.BOOLEAN_OPERATION__OPERAND1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.BOOLEAN_OPERATION__OPERAND1));
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.BOOLEAN_OPERATION__OPERATOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.BOOLEAN_OPERATION__OPERATOR));
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.BOOLEAN_OPERATION__OPERAND2) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.BOOLEAN_OPERATION__OPERAND2));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBooleanOperationAccess().getOperand1BooleanAssignmentParserRuleCall_0_0(), semanticObject.getOperand1());
		feeder.accept(grammarAccess.getBooleanOperationAccess().getOperatorBOOLOPERATORParserRuleCall_1_0(), semanticObject.getOperator());
		feeder.accept(grammarAccess.getBooleanOperationAccess().getOperand2BooleanAssignmentParserRuleCall_2_0(), semanticObject.getOperand2());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (boolAssign=BooleanAssignment | boolOp=BooleanOperation | intToBool=IntegerToBool)
	 */
	protected void sequence_BooleanTypeExpression(EObject context, BooleanTypeExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((identifier=VariableName value1=BooleanOperation) | value2=IntegerToBool)
	 */
	protected void sequence_BooleanVarVar(EObject context, BooleanVarVar semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     operationName=FunctionName
	 */
	protected void sequence_CallFunction(EObject context, CallFunction semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.CALL_FUNCTION__OPERATION_NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.CALL_FUNCTION__OPERATION_NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getCallFunctionAccess().getOperationNameFunctionNameParserRuleCall_0_0(), semanticObject.getOperationName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (value=CHARACTERS | value=VariableName)
	 */
	protected void sequence_CharacterAssignment(EObject context, CharacterAssignment semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (assign=CharacterDeclaration value=CharacterTypeExpression?)
	 */
	protected void sequence_CharacterDeclarationAss(EObject context, CharacterDeclarationAss semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     identifier=VariableName
	 */
	protected void sequence_CharacterDeclaration(EObject context, CharacterDeclaration semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.CHARACTER_DECLARATION__IDENTIFIER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.CHARACTER_DECLARATION__IDENTIFIER));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getCharacterDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0(), semanticObject.getIdentifier());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     charAssign=CharacterAssignment
	 */
	protected void sequence_CharacterTypeExpression(EObject context, CharacterTypeExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.CHARACTER_TYPE_EXPRESSION__CHAR_ASSIGN));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getCharacterTypeExpressionAccess().getCharAssignCharacterAssignmentParserRuleCall_0(), semanticObject.getCharAssign());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (identifier=VariableName value=CHARACTERS)
	 */
	protected void sequence_CharacterVarVar(EObject context, CharacterVarVar semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.CHARACTER_VAR_VAR__IDENTIFIER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.CHARACTER_VAR_VAR__IDENTIFIER));
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.CHARACTER_VAR_VAR__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.CHARACTER_VAR_VAR__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getCharacterVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0(), semanticObject.getIdentifier());
		feeder.accept(grammarAccess.getCharacterVarVarAccess().getValueCHARACTERSTerminalRuleCall_2_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     elements+=Function*
	 */
	protected void sequence_Domainmodel(EObject context, Domainmodel semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     operations+=Type*
	 */
	protected void sequence_Else(EObject context, Else semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=ID
	 */
	protected void sequence_FunctionName(EObject context, FunctionName semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.FUNCTION_NAME__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.FUNCTION_NAME__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getFunctionNameAccess().getValueIDTerminalRuleCall_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID features+=Type*)
	 */
	protected void sequence_Function(EObject context, Function semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (condOne=BooleanTypeExpression operations+=Type* else=Else?)
	 */
	protected void sequence_If_statment(EObject context, If_statment semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     importedNamespace=QualifiedNameWithWildcard
	 */
	protected void sequence_Import(EObject context, Import semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (value=INTEGERS | value=VariableName)
	 */
	protected void sequence_IntegerAssignment(EObject context, IntegerAssignment semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (assign=IntegerDeclaration value=IntegerTypeExpression?)
	 */
	protected void sequence_IntegerDeclarationAss(EObject context, IntegerDeclarationAss semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     identifier=VariableName
	 */
	protected void sequence_IntegerDeclaration(EObject context, IntegerDeclaration semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.INTEGER_DECLARATION__IDENTIFIER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.INTEGER_DECLARATION__IDENTIFIER));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIntegerDeclarationAccess().getIdentifierVariableNameParserRuleCall_1_0(), semanticObject.getIdentifier());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (operand1=IntegerAssignment operator=OPERATOR operand2=IntegerAssignment)
	 */
	protected void sequence_IntegerOperation(EObject context, IntegerOperation semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.INTEGER_OPERATION__OPERAND1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.INTEGER_OPERATION__OPERAND1));
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.INTEGER_OPERATION__OPERATOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.INTEGER_OPERATION__OPERATOR));
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.INTEGER_OPERATION__OPERAND2) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.INTEGER_OPERATION__OPERAND2));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIntegerOperationAccess().getOperand1IntegerAssignmentParserRuleCall_0_0(), semanticObject.getOperand1());
		feeder.accept(grammarAccess.getIntegerOperationAccess().getOperatorOPERATOREnumRuleCall_1_0(), semanticObject.getOperator());
		feeder.accept(grammarAccess.getIntegerOperationAccess().getOperand2IntegerAssignmentParserRuleCall_2_0(), semanticObject.getOperand2());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (operand1=IntegerTypeExpression operator=SPECIALINTOPERATOR operand2=IntegerTypeExpression)
	 */
	protected void sequence_IntegerToBool(EObject context, IntegerToBool semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.INTEGER_TO_BOOL__OPERAND1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.INTEGER_TO_BOOL__OPERAND1));
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.INTEGER_TO_BOOL__OPERATOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.INTEGER_TO_BOOL__OPERATOR));
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.INTEGER_TO_BOOL__OPERAND2) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.INTEGER_TO_BOOL__OPERAND2));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIntegerToBoolAccess().getOperand1IntegerTypeExpressionParserRuleCall_0_0(), semanticObject.getOperand1());
		feeder.accept(grammarAccess.getIntegerToBoolAccess().getOperatorSPECIALINTOPERATOREnumRuleCall_1_0(), semanticObject.getOperator());
		feeder.accept(grammarAccess.getIntegerToBoolAccess().getOperand2IntegerTypeExpressionParserRuleCall_2_0(), semanticObject.getOperand2());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (integerAssign=IntegerAssignment | integerOp=IntegerOperation)
	 */
	protected void sequence_IntegerTypeExpression(EObject context, IntegerTypeExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (identifier=VariableName value=IntegerTypeExpression)
	 */
	protected void sequence_IntegerVarVar(EObject context, IntegerVarVar semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.INTEGER_VAR_VAR__IDENTIFIER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.INTEGER_VAR_VAR__IDENTIFIER));
			if(transientValues.isValueTransient(semanticObject, CeurPackage.Literals.INTEGER_VAR_VAR__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CeurPackage.Literals.INTEGER_VAR_VAR__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIntegerVarVarAccess().getIdentifierVariableNameParserRuleCall_0_0(), semanticObject.getIdentifier());
		feeder.accept(grammarAccess.getIntegerVarVarAccess().getValueIntegerTypeExpressionParserRuleCall_2_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (print='spausdinti' (value=CHARACTERS | value=INTEGERS | value=BOOLVAL | value=VariableName))
	 */
	protected void sequence_PrintL(EObject context, PrintL semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (condOne=BooleanTypeExpression operations+=Type*)
	 */
	protected void sequence_Statement_While(EObject context, Statement_While semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         integer+=IntegerDeclarationAss | 
	 *         integervarvar+=IntegerVarVar | 
	 *         boolean+=BooleanDeclarationAss | 
	 *         booleanvarvar+=BooleanVarVar | 
	 *         character+=CharacterDeclarationAss | 
	 *         charactervarvar+=CharacterVarVar | 
	 *         while+=Statement_While | 
	 *         if+=If_statment | 
	 *         print+=PrintL | 
	 *         function+=CallFunction
	 *     )
	 */
	protected void sequence_Type(EObject context, Type semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
