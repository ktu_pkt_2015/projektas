/*
 * generated by Xtext
 */
package org.xtext.Ceur.Ceur.validation;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.ecore.EPackage;

public class AbstractCeurValidator extends org.eclipse.xtext.validation.AbstractDeclarativeValidator {

	@Override
	protected List<EPackage> getEPackages() {
	    List<EPackage> result = new ArrayList<EPackage>();
	    result.add(org.xtext.Ceur.Ceur.ceur.CeurPackage.eINSTANCE);
		return result;
	}
}
