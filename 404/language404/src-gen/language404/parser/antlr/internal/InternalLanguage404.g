/*
 * generated by Xtext
 */
grammar InternalLanguage404;

options {
	superClass=AbstractInternalAntlrParser;
	
}

@lexer::header {
package language404.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package language404.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import language404.services.Language404GrammarAccess;

}

@parser::members {

 	private Language404GrammarAccess grammarAccess;
 	
    public InternalLanguage404Parser(TokenStream input, Language404GrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }
    
    @Override
    protected String getFirstRuleName() {
    	return "IfModel";	
   	}
   	
   	@Override
   	protected Language404GrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}
}

@rulecatch { 
    catch (RecognitionException re) { 
        recover(input,re); 
        appendSkippedTokens();
    } 
}




// Entry rule entryRuleIfModel
entryRuleIfModel returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIfModelRule()); }
	 iv_ruleIfModel=ruleIfModel 
	 { $current=$iv_ruleIfModel.current; } 
	 EOF 
;

// Rule IfModel
ruleIfModel returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getIfModelAccess().getIfModelAction_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getIfModelAccess().getStatementIfStatementParserRuleCall_1_0()); 
	    }
		lv_statement_1_0=ruleIfStatement		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getIfModelRule());
	        }
       		add(
       			$current, 
       			"statement",
        		lv_statement_1_0, 
        		"IfStatement");
	        afterParserOrEnumRuleCall();
	    }

)
)*)
;





// Entry rule entryRuleIfStatement
entryRuleIfStatement returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIfStatementRule()); }
	 iv_ruleIfStatement=ruleIfStatement 
	 { $current=$iv_ruleIfStatement.current; } 
	 EOF 
;

// Rule IfStatement
ruleIfStatement returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='if' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getIfStatementAccess().getIfKeyword_0());
    }
	otherlv_1='(' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getIfStatementAccess().getLeftParenthesisKeyword_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getIfStatementAccess().getConditionIfExpressionParserRuleCall_2_0()); 
	    }
		lv_condition_2_0=ruleIfExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getIfStatementRule());
	        }
       		set(
       			$current, 
       			"condition",
        		lv_condition_2_0, 
        		"IfExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_3=')' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getIfStatementAccess().getRightParenthesisKeyword_3());
    }
	otherlv_4='then' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getIfStatementAccess().getThenKeyword_4());
    }
	otherlv_5='{' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getIfStatementAccess().getLeftCurlyBracketKeyword_5());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getIfStatementAccess().getThenIfExpressionParserRuleCall_6_0()); 
	    }
		lv_then_6_0=ruleIfExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getIfStatementRule());
	        }
       		set(
       			$current, 
       			"then",
        		lv_then_6_0, 
        		"IfExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_7='}' 
    {
    	newLeafNode(otherlv_7, grammarAccess.getIfStatementAccess().getRightCurlyBracketKeyword_7());
    }
(((	'else' 
)=>	otherlv_8='else' 
    {
    	newLeafNode(otherlv_8, grammarAccess.getIfStatementAccess().getElseKeyword_8_0());
    }
)	otherlv_9='{' 
    {
    	newLeafNode(otherlv_9, grammarAccess.getIfStatementAccess().getLeftCurlyBracketKeyword_8_1());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getIfStatementAccess().getElseIfExpressionParserRuleCall_8_2_0()); 
	    }
		lv_else_10_0=ruleIfExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getIfStatementRule());
	        }
       		set(
       			$current, 
       			"else",
        		lv_else_10_0, 
        		"IfExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_11='}' 
    {
    	newLeafNode(otherlv_11, grammarAccess.getIfStatementAccess().getRightCurlyBracketKeyword_8_3());
    }
)?)
;





// Entry rule entryRuleIfExpression
entryRuleIfExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIfExpressionRule()); }
	 iv_ruleIfExpression=ruleIfExpression 
	 { $current=$iv_ruleIfExpression.current; } 
	 EOF 
;

// Rule IfExpression
ruleIfExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getIfExpressionAccess().getIfStatementParserRuleCall_0()); 
    }
    this_IfStatement_0=ruleIfStatement
    { 
        $current = $this_IfStatement_0.current; 
        afterParserOrEnumRuleCall();
    }

    |(
(
		lv_name_1_0=RULE_ID
		{
			newLeafNode(lv_name_1_0, grammarAccess.getIfExpressionAccess().getNameIDTerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIfExpressionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_1_0, 
        		"ID");
	    }

)
))
;







// Entry rule entryRuleForStatement
entryRuleForStatement returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getForStatementRule()); }
	 iv_ruleForStatement=ruleForStatement 
	 { $current=$iv_ruleForStatement.current; } 
	 EOF 
;

// Rule ForStatement
ruleForStatement returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='for' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getForStatementAccess().getForKeyword_0());
    }
	otherlv_1='(' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getForStatementAccess().getLeftParenthesisKeyword_1());
    }

    { 
        newCompositeNode(grammarAccess.getForStatementAccess().getIntVariableParserRuleCall_2()); 
    }
    this_IntVariable_2=ruleIntVariable
    { 
        $current = $this_IntVariable_2.current; 
        afterParserOrEnumRuleCall();
    }
	otherlv_3=';' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getForStatementAccess().getSemicolonKeyword_3());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getForStatementAccess().getConditionForConditionParserRuleCall_4_0()); 
	    }
		lv_condition_4_0=ruleForCondition		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getForStatementRule());
	        }
       		set(
       			$current, 
       			"condition",
        		lv_condition_4_0, 
        		"ForCondition");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_5=';' 
    {
    	newLeafNode(otherlv_5, grammarAccess.getForStatementAccess().getSemicolonKeyword_5());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getForStatementAccess().getExpressionIncrementParserRuleCall_6_0()); 
	    }
		lv_expression_6_0=ruleIncrement		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getForStatementRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_6_0, 
        		"Increment");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_7=')' 
    {
    	newLeafNode(otherlv_7, grammarAccess.getForStatementAccess().getRightParenthesisKeyword_7());
    }
	otherlv_8='{' 
    {
    	newLeafNode(otherlv_8, grammarAccess.getForStatementAccess().getLeftCurlyBracketKeyword_8());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getForStatementAccess().getStatementForExpressionParserRuleCall_9_0()); 
	    }
		lv_statement_9_0=ruleForExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getForStatementRule());
	        }
       		set(
       			$current, 
       			"statement",
        		lv_statement_9_0, 
        		"ForExpression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_10='}' 
    {
    	newLeafNode(otherlv_10, grammarAccess.getForStatementAccess().getRightCurlyBracketKeyword_10());
    }
)
;





// Entry rule entryRuleIncrement
entryRuleIncrement returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIncrementRule()); }
	 iv_ruleIncrement=ruleIncrement 
	 { $current=$iv_ruleIncrement.current; } 
	 EOF 
;

// Rule Increment
ruleIncrement returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		lv_name_0_0=RULE_ID
		{
			newLeafNode(lv_name_0_0, grammarAccess.getIncrementAccess().getNameIDTerminalRuleCall_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIncrementRule());
	        }
       		addWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_0_0, 
        		"ID");
	    }

)
)
;





// Entry rule entryRuleForCondition
entryRuleForCondition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getForConditionRule()); }
	 iv_ruleForCondition=ruleForCondition 
	 { $current=$iv_ruleForCondition.current; } 
	 EOF 
;

// Rule ForCondition
ruleForCondition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		lv_name_0_0=RULE_ID
		{
			newLeafNode(lv_name_0_0, grammarAccess.getForConditionAccess().getNameIDTerminalRuleCall_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getForConditionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		true, 
        		"ID");
	    }

)
)
;





// Entry rule entryRuleForExpression
entryRuleForExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getForExpressionRule()); }
	 iv_ruleForExpression=ruleForExpression 
	 { $current=$iv_ruleForExpression.current; } 
	 EOF 
;

// Rule ForExpression
ruleForExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getForExpressionAccess().getForStatementParserRuleCall_0()); 
    }
    this_ForStatement_0=ruleForStatement
    { 
        $current = $this_ForStatement_0.current; 
        afterParserOrEnumRuleCall();
    }

    |(
(
		lv_name_1_0=RULE_ID
		{
			newLeafNode(lv_name_1_0, grammarAccess.getForExpressionAccess().getNameIDTerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getForExpressionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_1_0, 
        		"ID");
	    }

)
))
;





// Entry rule entryRuleIntVariable
entryRuleIntVariable returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIntVariableRule()); }
	 iv_ruleIntVariable=ruleIntVariable 
	 { $current=$iv_ruleIntVariable.current; } 
	 EOF 
;

// Rule IntVariable
ruleIntVariable returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='int' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getIntVariableAccess().getIntKeyword_0());
    }
(
(
		lv_name_1_0=RULE_INTEGER
		{
			newLeafNode(lv_name_1_0, grammarAccess.getIntVariableAccess().getNameINTEGERTerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntVariableRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_1_0, 
        		"INTEGER");
	    }

)
))
;











// Entry rule entryRuleStatement
entryRuleStatement returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getStatementRule()); }
	 iv_ruleStatement=ruleStatement 
	 { $current=$iv_ruleStatement.current; } 
	 EOF 
;

// Rule Statement
ruleStatement returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getStatementAccess().getDefinitionParserRuleCall_0()); 
    }
    this_Definition_0=ruleDefinition
    { 
        $current = $this_Definition_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getStatementAccess().getEvaluationParserRuleCall_1()); 
    }
    this_Evaluation_1=ruleEvaluation
    { 
        $current = $this_Evaluation_1.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleDefinition
entryRuleDefinition returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getDefinitionRule()); }
	 iv_ruleDefinition=ruleDefinition 
	 { $current=$iv_ruleDefinition.current; } 
	 EOF 
;

// Rule Definition
ruleDefinition returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='def' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getDefinitionAccess().getDefKeyword_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getDefinitionAccess().getReturnTypeTypeParserRuleCall_1_0()); 
	    }
		lv_returnType_1_0=ruleType		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getDefinitionRule());
	        }
       		set(
       			$current, 
       			"returnType",
        		lv_returnType_1_0, 
        		"Type");
	        afterParserOrEnumRuleCall();
	    }

)
)(
(
		lv_name_2_0=RULE_ID
		{
			newLeafNode(lv_name_2_0, grammarAccess.getDefinitionAccess().getNameIDTerminalRuleCall_2_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getDefinitionRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"name",
        		lv_name_2_0, 
        		"ID");
	    }

)
)(	otherlv_3='(' 
    {
    	newLeafNode(otherlv_3, grammarAccess.getDefinitionAccess().getLeftParenthesisKeyword_3_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getDefinitionAccess().getTypesTypeParserRuleCall_3_1_0()); 
	    }
		lv_types_4_0=ruleType		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getDefinitionRule());
	        }
       		add(
       			$current, 
       			"types",
        		lv_types_4_0, 
        		"Type");
	        afterParserOrEnumRuleCall();
	    }

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getDefinitionAccess().getArgsDeclaredParameterParserRuleCall_3_2_0()); 
	    }
		lv_args_5_0=ruleDeclaredParameter		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getDefinitionRule());
	        }
       		add(
       			$current, 
       			"args",
        		lv_args_5_0, 
        		"DeclaredParameter");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_6=',' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getDefinitionAccess().getCommaKeyword_3_3_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getDefinitionAccess().getTypesTypeParserRuleCall_3_3_1_0()); 
	    }
		lv_types_7_0=ruleType		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getDefinitionRule());
	        }
       		add(
       			$current, 
       			"types",
        		lv_types_7_0, 
        		"Type");
	        afterParserOrEnumRuleCall();
	    }

)
)(
(
		{ 
	        newCompositeNode(grammarAccess.getDefinitionAccess().getArgsDeclaredParameterParserRuleCall_3_3_2_0()); 
	    }
		lv_args_8_0=ruleDeclaredParameter		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getDefinitionRule());
	        }
       		add(
       			$current, 
       			"args",
        		lv_args_8_0, 
        		"DeclaredParameter");
	        afterParserOrEnumRuleCall();
	    }

)
))*	otherlv_9=')' 
    {
    	newLeafNode(otherlv_9, grammarAccess.getDefinitionAccess().getRightParenthesisKeyword_3_4());
    }
)?	otherlv_10=';' 
    {
    	newLeafNode(otherlv_10, grammarAccess.getDefinitionAccess().getSemicolonKeyword_4());
    }
)
;





// Entry rule entryRuleDeclaredParameter
entryRuleDeclaredParameter returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getDeclaredParameterRule()); }
	 iv_ruleDeclaredParameter=ruleDeclaredParameter 
	 { $current=$iv_ruleDeclaredParameter.current; } 
	 EOF 
;

// Rule DeclaredParameter
ruleDeclaredParameter returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		{ 
	        newCompositeNode(grammarAccess.getDeclaredParameterAccess().getNameVarNameParserRuleCall_0()); 
	    }
		lv_name_0_0=ruleVarName		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getDeclaredParameterRule());
	        }
       		set(
       			$current, 
       			"name",
        		lv_name_0_0, 
        		"VarName");
	        afterParserOrEnumRuleCall();
	    }

)
)
;





// Entry rule entryRuleType
entryRuleType returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getTypeRule()); }
	 iv_ruleType=ruleType 
	 { $current=$iv_ruleType.current; } 
	 EOF 
;

// Rule Type
ruleType returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getTypeAccess().getIntegerTypeParserRuleCall_0()); 
    }
    this_IntegerType_0=ruleIntegerType
    { 
        $current = $this_IntegerType_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getTypeAccess().getStringTypeParserRuleCall_1()); 
    }
    this_StringType_1=ruleStringType
    { 
        $current = $this_StringType_1.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleIntegerType
entryRuleIntegerType returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIntegerTypeRule()); }
	 iv_ruleIntegerType=ruleIntegerType 
	 { $current=$iv_ruleIntegerType.current; } 
	 EOF 
;

// Rule IntegerType
ruleIntegerType returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getIntegerTypeAccess().getIntegerTypeAction_0(),
            $current);
    }
)(
(
		lv_type_1_0=	'Integer' 
    {
        newLeafNode(lv_type_1_0, grammarAccess.getIntegerTypeAccess().getTypeIntegerKeyword_1_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getIntegerTypeRule());
	        }
       		setWithLastConsumed($current, "type", lv_type_1_0, "Integer");
	    }

)
))
;





// Entry rule entryRuleStringType
entryRuleStringType returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getStringTypeRule()); }
	 iv_ruleStringType=ruleStringType 
	 { $current=$iv_ruleStringType.current; } 
	 EOF 
;

// Rule StringType
ruleStringType returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getStringTypeAccess().getStringTypeAction_0(),
            $current);
    }
)(
(
		lv_type_1_0=	'String' 
    {
        newLeafNode(lv_type_1_0, grammarAccess.getStringTypeAccess().getTypeStringKeyword_1_0());
    }
 
	    {
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getStringTypeRule());
	        }
       		setWithLastConsumed($current, "type", lv_type_1_0, "String");
	    }

)
))
;





// Entry rule entryRuleEvaluation
entryRuleEvaluation returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getEvaluationRule()); }
	 iv_ruleEvaluation=ruleEvaluation 
	 { $current=$iv_ruleEvaluation.current; } 
	 EOF 
;

// Rule Evaluation
ruleEvaluation returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{ 
	        newCompositeNode(grammarAccess.getEvaluationAccess().getExpressionExpressionParserRuleCall_0_0()); 
	    }
		lv_expression_0_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getEvaluationRule());
	        }
       		set(
       			$current, 
       			"expression",
        		lv_expression_0_0, 
        		"Expression");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_1=';' 
    {
    	newLeafNode(otherlv_1, grammarAccess.getEvaluationAccess().getSemicolonKeyword_1());
    }
)
;





// Entry rule entryRuleExpression
entryRuleExpression returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getExpressionRule()); }
	 iv_ruleExpression=ruleExpression 
	 { $current=$iv_ruleExpression.current; } 
	 EOF 
;

// Rule Expression
ruleExpression returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
    { 
        newCompositeNode(grammarAccess.getExpressionAccess().getFunctionCallParserRuleCall_0()); 
    }
    this_FunctionCall_0=ruleFunctionCall
    { 
        $current = $this_FunctionCall_0.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getExpressionAccess().getConstantParserRuleCall_1()); 
    }
    this_Constant_1=ruleConstant
    { 
        $current = $this_Constant_1.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getExpressionAccess().getIntegerAtomParserRuleCall_2()); 
    }
    this_IntegerAtom_2=ruleIntegerAtom
    { 
        $current = $this_IntegerAtom_2.current; 
        afterParserOrEnumRuleCall();
    }

    |
    { 
        newCompositeNode(grammarAccess.getExpressionAccess().getStringAtomParserRuleCall_3()); 
    }
    this_StringAtom_3=ruleStringAtom
    { 
        $current = $this_StringAtom_3.current; 
        afterParserOrEnumRuleCall();
    }
)
;





// Entry rule entryRuleIntegerAtom
entryRuleIntegerAtom returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getIntegerAtomRule()); }
	 iv_ruleIntegerAtom=ruleIntegerAtom 
	 { $current=$iv_ruleIntegerAtom.current; } 
	 EOF 
;

// Rule IntegerAtom
ruleIntegerAtom returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getIntegerAtomAccess().getIntegerAtomAction_0(),
            $current);
    }
)this_INT_1=RULE_INT
    { 
    newLeafNode(this_INT_1, grammarAccess.getIntegerAtomAccess().getINTTerminalRuleCall_1()); 
    }
)
;





// Entry rule entryRuleStringAtom
entryRuleStringAtom returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getStringAtomRule()); }
	 iv_ruleStringAtom=ruleStringAtom 
	 { $current=$iv_ruleStringAtom.current; } 
	 EOF 
;

// Rule StringAtom
ruleStringAtom returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(
(
		lv_value_0_0=RULE_A_STRING
		{
			newLeafNode(lv_value_0_0, grammarAccess.getStringAtomAccess().getValueA_STRINGTerminalRuleCall_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getStringAtomRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_0_0, 
        		"A_STRING");
	    }

)
)
;





// Entry rule entryRuleFunctionCall
entryRuleFunctionCall returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getFunctionCallRule()); }
	 iv_ruleFunctionCall=ruleFunctionCall 
	 { $current=$iv_ruleFunctionCall.current; } 
	 EOF 
;

// Rule FunctionCall
ruleFunctionCall returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getFunctionCallAccess().getFunctionCallAction_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getFunctionCallAccess().getSourceDefinitionRefParserRuleCall_1_0()); 
	    }
		lv_source_1_0=ruleDefinitionRef		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFunctionCallRule());
	        }
       		set(
       			$current, 
       			"source",
        		lv_source_1_0, 
        		"DefinitionRef");
	        afterParserOrEnumRuleCall();
	    }

)
)	otherlv_2='(' 
    {
    	newLeafNode(otherlv_2, grammarAccess.getFunctionCallAccess().getLeftParenthesisKeyword_2());
    }
((
(
		{ 
	        newCompositeNode(grammarAccess.getFunctionCallAccess().getArgsExpressionParserRuleCall_3_0_0()); 
	    }
		lv_args_3_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFunctionCallRule());
	        }
       		add(
       			$current, 
       			"args",
        		lv_args_3_0, 
        		"Expression");
	        afterParserOrEnumRuleCall();
	    }

)
)(	otherlv_4=',' 
    {
    	newLeafNode(otherlv_4, grammarAccess.getFunctionCallAccess().getCommaKeyword_3_1_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getFunctionCallAccess().getArgsExpressionParserRuleCall_3_1_1_0()); 
	    }
		lv_args_5_0=ruleExpression		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getFunctionCallRule());
	        }
       		add(
       			$current, 
       			"args",
        		lv_args_5_0, 
        		"Expression");
	        afterParserOrEnumRuleCall();
	    }

)
))*)?	otherlv_6=')' 
    {
    	newLeafNode(otherlv_6, grammarAccess.getFunctionCallAccess().getRightParenthesisKeyword_4());
    }
)
;





// Entry rule entryRuleDefinitionRef
entryRuleDefinitionRef returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getDefinitionRefRule()); }
	 iv_ruleDefinitionRef=ruleDefinitionRef 
	 { $current=$iv_ruleDefinitionRef.current; } 
	 EOF 
;

// Rule DefinitionRef
ruleDefinitionRef returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getDefinitionRefAccess().getDefinitionRefAction_0(),
            $current);
    }
)(
(
		{
			if ($current==null) {
	            $current = createModelElement(grammarAccess.getDefinitionRefRule());
	        }
        }
	otherlv_1=RULE_ID
	{
		newLeafNode(otherlv_1, grammarAccess.getDefinitionRefAccess().getRefNameDefinitionCrossReference_1_0()); 
	}

)
))
;





// Entry rule entryRuleConstant
entryRuleConstant returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getConstantRule()); }
	 iv_ruleConstant=ruleConstant 
	 { $current=$iv_ruleConstant.current; } 
	 EOF 
;

// Rule Constant
ruleConstant returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
(	otherlv_0='const' 
    {
    	newLeafNode(otherlv_0, grammarAccess.getConstantAccess().getConstKeyword_0());
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getConstantAccess().getTypeTypeParserRuleCall_1_0()); 
	    }
		lv_type_1_0=ruleType		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getConstantRule());
	        }
       		set(
       			$current, 
       			"type",
        		lv_type_1_0, 
        		"Type");
	        afterParserOrEnumRuleCall();
	    }

)
)(
(
		lv_constName_2_0=RULE_CONSTNAME
		{
			newLeafNode(lv_constName_2_0, grammarAccess.getConstantAccess().getConstNameCONSTNAMETerminalRuleCall_2_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getConstantRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"constName",
        		lv_constName_2_0, 
        		"CONSTNAME");
	    }

)
))
;





// Entry rule entryRuleVarName
entryRuleVarName returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getVarNameRule()); }
	 iv_ruleVarName=ruleVarName 
	 { $current=$iv_ruleVarName.current; } 
	 EOF 
;

// Rule VarName
ruleVarName returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getVarNameAccess().getVarNameAction_0(),
            $current);
    }
)(
(
		lv_value_1_0=RULE_A_VARNAME
		{
			newLeafNode(lv_value_1_0, grammarAccess.getVarNameAccess().getValueA_VARNAMETerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getVarNameRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"value",
        		lv_value_1_0, 
        		"A_VARNAME");
	    }

)
))
;





// Rule Visibility
ruleVisibility returns [Enumerator current=null] 
    @init { enterRule(); }
    @after { leaveRule(); }:
((	enumLiteral_0='public' 
	{
        $current = grammarAccess.getVisibilityAccess().getPUBLICEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_0, grammarAccess.getVisibilityAccess().getPUBLICEnumLiteralDeclaration_0()); 
    }
)
    |(	enumLiteral_1='private' 
	{
        $current = grammarAccess.getVisibilityAccess().getPRIVATEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_1, grammarAccess.getVisibilityAccess().getPRIVATEEnumLiteralDeclaration_1()); 
    }
)
    |(	enumLiteral_2='protected' 
	{
        $current = grammarAccess.getVisibilityAccess().getPROTECTEDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_2, grammarAccess.getVisibilityAccess().getPROTECTEDEnumLiteralDeclaration_2()); 
    }
));



RULE_INTEGER : ('0'..'9')+;

RULE_STRING : ('"' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'"')))* '"'|'\'' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'\'')))* '\'');

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_A_STRING : '"' ('a'..'z')+ '"';

RULE_A_VARNAME : 'a'..'z' ('a'..'z')+;

RULE_CONSTNAME : '$' 'A'..'Z' ('a'..'z')+;

RULE_INT : ('0'..'9')+;

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;


