package language404.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalLanguage404Lexer extends Lexer {
    public static final int RULE_A_VARNAME=9;
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=14;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int RULE_SL_COMMENT=12;
    public static final int RULE_ML_COMMENT=11;
    public static final int T__30=30;
    public static final int T__19=19;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_STRING=10;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int RULE_CONSTNAME=8;
    public static final int RULE_WS=13;
    public static final int RULE_A_STRING=7;
    public static final int RULE_INTEGER=5;

    // delegates
    // delegators

    public InternalLanguage404Lexer() {;} 
    public InternalLanguage404Lexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalLanguage404Lexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g"; }

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:11:7: ( 'if' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:11:9: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:12:7: ( '(' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:12:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:13:7: ( ')' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:13:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:14:7: ( 'then' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:14:9: 'then'
            {
            match("then"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:15:7: ( '{' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:15:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:16:7: ( '}' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:16:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:17:7: ( 'else' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:17:9: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:18:7: ( 'for' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:18:9: 'for'
            {
            match("for"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:19:7: ( ';' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:19:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:20:7: ( 'int' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:20:9: 'int'
            {
            match("int"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:21:7: ( 'def' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:21:9: 'def'
            {
            match("def"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:22:7: ( ',' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:22:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:23:7: ( 'Integer' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:23:9: 'Integer'
            {
            match("Integer"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:24:7: ( 'String' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:24:9: 'String'
            {
            match("String"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:25:7: ( 'const' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:25:9: 'const'
            {
            match("const"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:26:7: ( 'public' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:26:9: 'public'
            {
            match("public"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:27:7: ( 'private' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:27:9: 'private'
            {
            match("private"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:28:7: ( 'protected' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:28:9: 'protected'
            {
            match("protected"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "RULE_INTEGER"
    public final void mRULE_INTEGER() throws RecognitionException {
        try {
            int _type = RULE_INTEGER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1352:14: ( ( '0' .. '9' )+ )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1352:16: ( '0' .. '9' )+
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1352:16: ( '0' .. '9' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='0' && LA1_0<='9')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1352:17: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INTEGER"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:13: ( ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\"') ) {
                alt4=1;
            }
            else if ( (LA4_0=='\'') ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:16: '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:20: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop2:
                    do {
                        int alt2=3;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0=='\\') ) {
                            alt2=1;
                        }
                        else if ( ((LA2_0>='\u0000' && LA2_0<='!')||(LA2_0>='#' && LA2_0<='[')||(LA2_0>=']' && LA2_0<='\uFFFF')) ) {
                            alt2=2;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:21: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:66: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:86: '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:91: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop3:
                    do {
                        int alt3=3;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0=='\\') ) {
                            alt3=1;
                        }
                        else if ( ((LA3_0>='\u0000' && LA3_0<='&')||(LA3_0>='(' && LA3_0<='[')||(LA3_0>=']' && LA3_0<='\uFFFF')) ) {
                            alt3=2;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:92: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1354:137: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1356:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1356:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1356:11: ( '^' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='^') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1356:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1356:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')||(LA6_0>='A' && LA6_0<='Z')||LA6_0=='_'||(LA6_0>='a' && LA6_0<='z')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_A_STRING"
    public final void mRULE_A_STRING() throws RecognitionException {
        try {
            int _type = RULE_A_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1358:15: ( '\"' ( 'a' .. 'z' )+ '\"' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1358:17: '\"' ( 'a' .. 'z' )+ '\"'
            {
            match('\"'); 
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1358:21: ( 'a' .. 'z' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='a' && LA7_0<='z')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1358:22: 'a' .. 'z'
            	    {
            	    matchRange('a','z'); 

            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);

            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_A_STRING"

    // $ANTLR start "RULE_A_VARNAME"
    public final void mRULE_A_VARNAME() throws RecognitionException {
        try {
            int _type = RULE_A_VARNAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1360:16: ( 'a' .. 'z' ( 'a' .. 'z' )+ )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1360:18: 'a' .. 'z' ( 'a' .. 'z' )+
            {
            matchRange('a','z'); 
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1360:27: ( 'a' .. 'z' )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='a' && LA8_0<='z')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1360:28: 'a' .. 'z'
            	    {
            	    matchRange('a','z'); 

            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_A_VARNAME"

    // $ANTLR start "RULE_CONSTNAME"
    public final void mRULE_CONSTNAME() throws RecognitionException {
        try {
            int _type = RULE_CONSTNAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1362:16: ( '$' 'A' .. 'Z' ( 'a' .. 'z' )+ )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1362:18: '$' 'A' .. 'Z' ( 'a' .. 'z' )+
            {
            match('$'); 
            matchRange('A','Z'); 
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1362:31: ( 'a' .. 'z' )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='a' && LA9_0<='z')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1362:32: 'a' .. 'z'
            	    {
            	    matchRange('a','z'); 

            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CONSTNAME"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1364:10: ( ( '0' .. '9' )+ )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1364:12: ( '0' .. '9' )+
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1364:12: ( '0' .. '9' )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>='0' && LA10_0<='9')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1364:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1366:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1366:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1366:24: ( options {greedy=false; } : . )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0=='*') ) {
                    int LA11_1 = input.LA(2);

                    if ( (LA11_1=='/') ) {
                        alt11=2;
                    }
                    else if ( ((LA11_1>='\u0000' && LA11_1<='.')||(LA11_1>='0' && LA11_1<='\uFFFF')) ) {
                        alt11=1;
                    }


                }
                else if ( ((LA11_0>='\u0000' && LA11_0<=')')||(LA11_0>='+' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1366:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1368:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1368:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1368:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='\u0000' && LA12_0<='\t')||(LA12_0>='\u000B' && LA12_0<='\f')||(LA12_0>='\u000E' && LA12_0<='\uFFFF')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1368:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1368:40: ( ( '\\r' )? '\\n' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='\n'||LA14_0=='\r') ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1368:41: ( '\\r' )? '\\n'
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1368:41: ( '\\r' )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0=='\r') ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1368:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1370:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1370:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1370:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='\t' && LA15_0<='\n')||LA15_0=='\r'||LA15_0==' ') ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1372:16: ( . )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1372:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:8: ( T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | RULE_INTEGER | RULE_STRING | RULE_ID | RULE_A_STRING | RULE_A_VARNAME | RULE_CONSTNAME | RULE_INT | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt16=29;
        alt16 = dfa16.predict(input);
        switch (alt16) {
            case 1 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:10: T__15
                {
                mT__15(); 

                }
                break;
            case 2 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:16: T__16
                {
                mT__16(); 

                }
                break;
            case 3 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:22: T__17
                {
                mT__17(); 

                }
                break;
            case 4 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:28: T__18
                {
                mT__18(); 

                }
                break;
            case 5 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:34: T__19
                {
                mT__19(); 

                }
                break;
            case 6 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:40: T__20
                {
                mT__20(); 

                }
                break;
            case 7 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:46: T__21
                {
                mT__21(); 

                }
                break;
            case 8 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:52: T__22
                {
                mT__22(); 

                }
                break;
            case 9 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:58: T__23
                {
                mT__23(); 

                }
                break;
            case 10 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:64: T__24
                {
                mT__24(); 

                }
                break;
            case 11 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:70: T__25
                {
                mT__25(); 

                }
                break;
            case 12 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:76: T__26
                {
                mT__26(); 

                }
                break;
            case 13 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:82: T__27
                {
                mT__27(); 

                }
                break;
            case 14 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:88: T__28
                {
                mT__28(); 

                }
                break;
            case 15 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:94: T__29
                {
                mT__29(); 

                }
                break;
            case 16 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:100: T__30
                {
                mT__30(); 

                }
                break;
            case 17 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:106: T__31
                {
                mT__31(); 

                }
                break;
            case 18 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:112: T__32
                {
                mT__32(); 

                }
                break;
            case 19 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:118: RULE_INTEGER
                {
                mRULE_INTEGER(); 

                }
                break;
            case 20 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:131: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 21 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:143: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 22 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:151: RULE_A_STRING
                {
                mRULE_A_STRING(); 

                }
                break;
            case 23 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:165: RULE_A_VARNAME
                {
                mRULE_A_VARNAME(); 

                }
                break;
            case 24 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:180: RULE_CONSTNAME
                {
                mRULE_CONSTNAME(); 

                }
                break;
            case 25 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:195: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 26 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:204: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 27 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:220: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 28 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:236: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 29 :
                // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1:244: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA16 dfa16 = new DFA16(this);
    static final String DFA16_eotS =
        "\1\uffff\1\35\2\uffff\1\35\2\uffff\2\35\1\uffff\1\35\1\uffff\4"+
        "\35\1\55\3\31\1\35\1\uffff\2\31\2\uffff\1\65\2\35\3\uffff\1\35\2"+
        "\uffff\2\35\1\uffff\1\35\1\uffff\5\35\1\uffff\1\55\7\uffff\1\102"+
        "\2\35\1\105\1\106\6\35\2\uffff\1\115\1\116\2\uffff\6\35\2\uffff"+
        "\2\35\1\127\4\35\1\134\1\uffff\1\135\2\35\1\140\2\uffff\1\141\1"+
        "\35\2\uffff\1\35\1\144\1\uffff";
    static final String DFA16_eofS =
        "\145\uffff";
    static final String DFA16_minS =
        "\1\0\1\141\2\uffff\1\141\2\uffff\2\141\1\uffff\1\141\1\uffff\1"+
        "\156\1\164\2\141\1\60\2\0\1\101\1\141\1\uffff\1\101\1\52\2\uffff"+
        "\1\60\2\141\3\uffff\1\141\2\uffff\2\141\1\uffff\1\141\1\uffff\1"+
        "\164\1\162\3\141\1\uffff\1\60\1\0\6\uffff\1\60\2\141\2\60\1\145"+
        "\1\151\4\141\2\uffff\2\60\2\uffff\1\147\1\156\4\141\2\uffff\1\145"+
        "\1\147\1\60\3\141\1\162\1\60\1\uffff\1\60\2\141\1\60\2\uffff\1\60"+
        "\1\141\2\uffff\1\141\1\60\1\uffff";
    static final String DFA16_maxS =
        "\1\uffff\1\172\2\uffff\1\172\2\uffff\2\172\1\uffff\1\172\1\uffff"+
        "\1\156\1\164\2\172\1\71\2\uffff\2\172\1\uffff\1\132\1\57\2\uffff"+
        "\3\172\3\uffff\1\172\2\uffff\2\172\1\uffff\1\172\1\uffff\1\164\1"+
        "\162\3\172\1\uffff\1\71\1\uffff\6\uffff\5\172\1\145\1\151\4\172"+
        "\2\uffff\2\172\2\uffff\1\147\1\156\4\172\2\uffff\1\145\1\147\4\172"+
        "\1\162\1\172\1\uffff\4\172\2\uffff\2\172\2\uffff\2\172\1\uffff";
    static final String DFA16_acceptS =
        "\2\uffff\1\2\1\3\1\uffff\1\5\1\6\2\uffff\1\11\1\uffff\1\14\11\uffff"+
        "\1\25\2\uffff\1\34\1\35\3\uffff\1\25\1\2\1\3\1\uffff\1\5\1\6\2\uffff"+
        "\1\11\1\uffff\1\14\5\uffff\1\23\2\uffff\1\24\1\30\1\32\1\33\1\34"+
        "\1\1\13\uffff\1\24\1\12\2\uffff\1\10\1\13\6\uffff\1\4\1\7\10\uffff"+
        "\1\17\4\uffff\1\16\1\20\2\uffff\1\15\1\21\2\uffff\1\22";
    static final String DFA16_specialS =
        "\1\0\20\uffff\1\3\1\1\34\uffff\1\2\65\uffff}>";
    static final String[] DFA16_transitionS = {
            "\11\31\2\30\2\31\1\30\22\31\1\30\1\31\1\21\1\31\1\26\2\31\1"+
            "\22\1\2\1\3\2\31\1\13\2\31\1\27\12\20\1\31\1\11\5\31\10\25\1"+
            "\14\11\25\1\15\7\25\3\31\1\23\1\25\1\31\2\24\1\16\1\12\1\7\1"+
            "\10\2\24\1\1\6\24\1\17\3\24\1\4\6\24\1\5\1\31\1\6\uff82\31",
            "\5\34\1\32\7\34\1\33\14\34",
            "",
            "",
            "\7\34\1\40\22\34",
            "",
            "",
            "\13\34\1\43\16\34",
            "\16\34\1\44\13\34",
            "",
            "\4\34\1\46\25\34",
            "",
            "\1\50",
            "\1\51",
            "\16\34\1\52\13\34",
            "\21\34\1\54\2\34\1\53\5\34",
            "\12\56",
            "\141\60\32\57\uff85\60",
            "\0\60",
            "\32\35\4\uffff\1\35\1\uffff\32\35",
            "\32\34",
            "",
            "\32\61",
            "\1\62\4\uffff\1\63",
            "",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            "\23\34\1\66\6\34",
            "\32\34",
            "",
            "",
            "",
            "\4\34\1\67\25\34",
            "",
            "",
            "\22\34\1\70\7\34",
            "\21\34\1\71\10\34",
            "",
            "\5\34\1\72\24\34",
            "",
            "\1\73",
            "\1\74",
            "\15\34\1\75\14\34",
            "\1\34\1\76\30\34",
            "\10\34\1\77\5\34\1\100\13\34",
            "",
            "\12\56",
            "\42\60\1\101\76\60\32\57\uff85\60",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            "\15\34\1\103\14\34",
            "\4\34\1\104\25\34",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            "\1\107",
            "\1\110",
            "\22\34\1\111\7\34",
            "\13\34\1\112\16\34",
            "\25\34\1\113\4\34",
            "\23\34\1\114\6\34",
            "",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            "",
            "",
            "\1\117",
            "\1\120",
            "\23\34\1\121\6\34",
            "\10\34\1\122\21\34",
            "\1\123\31\34",
            "\4\34\1\124\25\34",
            "",
            "",
            "\1\125",
            "\1\126",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            "\2\34\1\130\27\34",
            "\23\34\1\131\6\34",
            "\2\34\1\132\27\34",
            "\1\133",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            "\4\34\1\136\25\34",
            "\23\34\1\137\6\34",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            "\4\34\1\142\25\34",
            "",
            "",
            "\3\34\1\143\26\34",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\34",
            ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | RULE_INTEGER | RULE_STRING | RULE_ID | RULE_A_STRING | RULE_A_VARNAME | RULE_CONSTNAME | RULE_INT | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA16_0 = input.LA(1);

                        s = -1;
                        if ( (LA16_0=='i') ) {s = 1;}

                        else if ( (LA16_0=='(') ) {s = 2;}

                        else if ( (LA16_0==')') ) {s = 3;}

                        else if ( (LA16_0=='t') ) {s = 4;}

                        else if ( (LA16_0=='{') ) {s = 5;}

                        else if ( (LA16_0=='}') ) {s = 6;}

                        else if ( (LA16_0=='e') ) {s = 7;}

                        else if ( (LA16_0=='f') ) {s = 8;}

                        else if ( (LA16_0==';') ) {s = 9;}

                        else if ( (LA16_0=='d') ) {s = 10;}

                        else if ( (LA16_0==',') ) {s = 11;}

                        else if ( (LA16_0=='I') ) {s = 12;}

                        else if ( (LA16_0=='S') ) {s = 13;}

                        else if ( (LA16_0=='c') ) {s = 14;}

                        else if ( (LA16_0=='p') ) {s = 15;}

                        else if ( ((LA16_0>='0' && LA16_0<='9')) ) {s = 16;}

                        else if ( (LA16_0=='\"') ) {s = 17;}

                        else if ( (LA16_0=='\'') ) {s = 18;}

                        else if ( (LA16_0=='^') ) {s = 19;}

                        else if ( ((LA16_0>='a' && LA16_0<='b')||(LA16_0>='g' && LA16_0<='h')||(LA16_0>='j' && LA16_0<='o')||(LA16_0>='q' && LA16_0<='s')||(LA16_0>='u' && LA16_0<='z')) ) {s = 20;}

                        else if ( ((LA16_0>='A' && LA16_0<='H')||(LA16_0>='J' && LA16_0<='R')||(LA16_0>='T' && LA16_0<='Z')||LA16_0=='_') ) {s = 21;}

                        else if ( (LA16_0=='$') ) {s = 22;}

                        else if ( (LA16_0=='/') ) {s = 23;}

                        else if ( ((LA16_0>='\t' && LA16_0<='\n')||LA16_0=='\r'||LA16_0==' ') ) {s = 24;}

                        else if ( ((LA16_0>='\u0000' && LA16_0<='\b')||(LA16_0>='\u000B' && LA16_0<='\f')||(LA16_0>='\u000E' && LA16_0<='\u001F')||LA16_0=='!'||LA16_0=='#'||(LA16_0>='%' && LA16_0<='&')||(LA16_0>='*' && LA16_0<='+')||(LA16_0>='-' && LA16_0<='.')||LA16_0==':'||(LA16_0>='<' && LA16_0<='@')||(LA16_0>='[' && LA16_0<=']')||LA16_0=='`'||LA16_0=='|'||(LA16_0>='~' && LA16_0<='\uFFFF')) ) {s = 25;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA16_18 = input.LA(1);

                        s = -1;
                        if ( ((LA16_18>='\u0000' && LA16_18<='\uFFFF')) ) {s = 48;}

                        else s = 25;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA16_47 = input.LA(1);

                        s = -1;
                        if ( (LA16_47=='\"') ) {s = 65;}

                        else if ( ((LA16_47>='a' && LA16_47<='z')) ) {s = 47;}

                        else if ( ((LA16_47>='\u0000' && LA16_47<='!')||(LA16_47>='#' && LA16_47<='`')||(LA16_47>='{' && LA16_47<='\uFFFF')) ) {s = 48;}

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA16_17 = input.LA(1);

                        s = -1;
                        if ( ((LA16_17>='a' && LA16_17<='z')) ) {s = 47;}

                        else if ( ((LA16_17>='\u0000' && LA16_17<='`')||(LA16_17>='{' && LA16_17<='\uFFFF')) ) {s = 48;}

                        else s = 25;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 16, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}