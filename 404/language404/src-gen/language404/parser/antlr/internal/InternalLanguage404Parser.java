package language404.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import language404.services.Language404GrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalLanguage404Parser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INTEGER", "RULE_INT", "RULE_A_STRING", "RULE_CONSTNAME", "RULE_A_VARNAME", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'if'", "'('", "')'", "'then'", "'{'", "'}'", "'else'", "'for'", "';'", "'int'", "'def'", "','", "'Integer'", "'String'", "'const'", "'public'", "'private'", "'protected'"
    };
    public static final int RULE_A_VARNAME=9;
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=14;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=12;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=11;
    public static final int T__19=19;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=10;
    public static final int T__32=32;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int RULE_CONSTNAME=8;
    public static final int RULE_WS=13;
    public static final int RULE_A_STRING=7;
    public static final int RULE_INTEGER=5;

    // delegates
    // delegators


        public InternalLanguage404Parser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalLanguage404Parser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalLanguage404Parser.tokenNames; }
    public String getGrammarFileName() { return "../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g"; }



     	private Language404GrammarAccess grammarAccess;
     	
        public InternalLanguage404Parser(TokenStream input, Language404GrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "IfModel";	
       	}
       	
       	@Override
       	protected Language404GrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleIfModel"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:68:1: entryRuleIfModel returns [EObject current=null] : iv_ruleIfModel= ruleIfModel EOF ;
    public final EObject entryRuleIfModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfModel = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:69:2: (iv_ruleIfModel= ruleIfModel EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:70:2: iv_ruleIfModel= ruleIfModel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIfModelRule()); 
            }
            pushFollow(FOLLOW_ruleIfModel_in_entryRuleIfModel75);
            iv_ruleIfModel=ruleIfModel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIfModel; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIfModel85); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfModel"


    // $ANTLR start "ruleIfModel"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:77:1: ruleIfModel returns [EObject current=null] : ( () ( (lv_statement_1_0= ruleIfStatement ) )* ) ;
    public final EObject ruleIfModel() throws RecognitionException {
        EObject current = null;

        EObject lv_statement_1_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:80:28: ( ( () ( (lv_statement_1_0= ruleIfStatement ) )* ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:81:1: ( () ( (lv_statement_1_0= ruleIfStatement ) )* )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:81:1: ( () ( (lv_statement_1_0= ruleIfStatement ) )* )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:81:2: () ( (lv_statement_1_0= ruleIfStatement ) )*
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:81:2: ()
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:82:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getIfModelAccess().getIfModelAction_0(),
                          current);
                  
            }

            }

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:87:2: ( (lv_statement_1_0= ruleIfStatement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==15) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:88:1: (lv_statement_1_0= ruleIfStatement )
            	    {
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:88:1: (lv_statement_1_0= ruleIfStatement )
            	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:89:3: lv_statement_1_0= ruleIfStatement
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getIfModelAccess().getStatementIfStatementParserRuleCall_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleIfStatement_in_ruleIfModel140);
            	    lv_statement_1_0=ruleIfStatement();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getIfModelRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"statement",
            	              		lv_statement_1_0, 
            	              		"IfStatement");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfModel"


    // $ANTLR start "entryRuleIfStatement"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:113:1: entryRuleIfStatement returns [EObject current=null] : iv_ruleIfStatement= ruleIfStatement EOF ;
    public final EObject entryRuleIfStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfStatement = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:114:2: (iv_ruleIfStatement= ruleIfStatement EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:115:2: iv_ruleIfStatement= ruleIfStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIfStatementRule()); 
            }
            pushFollow(FOLLOW_ruleIfStatement_in_entryRuleIfStatement177);
            iv_ruleIfStatement=ruleIfStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIfStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIfStatement187); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfStatement"


    // $ANTLR start "ruleIfStatement"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:122:1: ruleIfStatement returns [EObject current=null] : (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleIfExpression ) ) otherlv_3= ')' otherlv_4= 'then' otherlv_5= '{' ( (lv_then_6_0= ruleIfExpression ) ) otherlv_7= '}' ( ( ( 'else' )=>otherlv_8= 'else' ) otherlv_9= '{' ( (lv_else_10_0= ruleIfExpression ) ) otherlv_11= '}' )? ) ;
    public final EObject ruleIfStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_condition_2_0 = null;

        EObject lv_then_6_0 = null;

        EObject lv_else_10_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:125:28: ( (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleIfExpression ) ) otherlv_3= ')' otherlv_4= 'then' otherlv_5= '{' ( (lv_then_6_0= ruleIfExpression ) ) otherlv_7= '}' ( ( ( 'else' )=>otherlv_8= 'else' ) otherlv_9= '{' ( (lv_else_10_0= ruleIfExpression ) ) otherlv_11= '}' )? ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:126:1: (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleIfExpression ) ) otherlv_3= ')' otherlv_4= 'then' otherlv_5= '{' ( (lv_then_6_0= ruleIfExpression ) ) otherlv_7= '}' ( ( ( 'else' )=>otherlv_8= 'else' ) otherlv_9= '{' ( (lv_else_10_0= ruleIfExpression ) ) otherlv_11= '}' )? )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:126:1: (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleIfExpression ) ) otherlv_3= ')' otherlv_4= 'then' otherlv_5= '{' ( (lv_then_6_0= ruleIfExpression ) ) otherlv_7= '}' ( ( ( 'else' )=>otherlv_8= 'else' ) otherlv_9= '{' ( (lv_else_10_0= ruleIfExpression ) ) otherlv_11= '}' )? )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:126:3: otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleIfExpression ) ) otherlv_3= ')' otherlv_4= 'then' otherlv_5= '{' ( (lv_then_6_0= ruleIfExpression ) ) otherlv_7= '}' ( ( ( 'else' )=>otherlv_8= 'else' ) otherlv_9= '{' ( (lv_else_10_0= ruleIfExpression ) ) otherlv_11= '}' )?
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleIfStatement224); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIfStatementAccess().getIfKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,16,FOLLOW_16_in_ruleIfStatement236); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getIfStatementAccess().getLeftParenthesisKeyword_1());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:134:1: ( (lv_condition_2_0= ruleIfExpression ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:135:1: (lv_condition_2_0= ruleIfExpression )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:135:1: (lv_condition_2_0= ruleIfExpression )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:136:3: lv_condition_2_0= ruleIfExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIfStatementAccess().getConditionIfExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleIfExpression_in_ruleIfStatement257);
            lv_condition_2_0=ruleIfExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIfStatementRule());
              	        }
                     		set(
                     			current, 
                     			"condition",
                      		lv_condition_2_0, 
                      		"IfExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,17,FOLLOW_17_in_ruleIfStatement269); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getIfStatementAccess().getRightParenthesisKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,18,FOLLOW_18_in_ruleIfStatement281); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getIfStatementAccess().getThenKeyword_4());
                  
            }
            otherlv_5=(Token)match(input,19,FOLLOW_19_in_ruleIfStatement293); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getIfStatementAccess().getLeftCurlyBracketKeyword_5());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:164:1: ( (lv_then_6_0= ruleIfExpression ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:165:1: (lv_then_6_0= ruleIfExpression )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:165:1: (lv_then_6_0= ruleIfExpression )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:166:3: lv_then_6_0= ruleIfExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIfStatementAccess().getThenIfExpressionParserRuleCall_6_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleIfExpression_in_ruleIfStatement314);
            lv_then_6_0=ruleIfExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIfStatementRule());
              	        }
                     		set(
                     			current, 
                     			"then",
                      		lv_then_6_0, 
                      		"IfExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_7=(Token)match(input,20,FOLLOW_20_in_ruleIfStatement326); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getIfStatementAccess().getRightCurlyBracketKeyword_7());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:186:1: ( ( ( 'else' )=>otherlv_8= 'else' ) otherlv_9= '{' ( (lv_else_10_0= ruleIfExpression ) ) otherlv_11= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==21) && (synpred1_InternalLanguage404())) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:186:2: ( ( 'else' )=>otherlv_8= 'else' ) otherlv_9= '{' ( (lv_else_10_0= ruleIfExpression ) ) otherlv_11= '}'
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:186:2: ( ( 'else' )=>otherlv_8= 'else' )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:186:3: ( 'else' )=>otherlv_8= 'else'
                    {
                    otherlv_8=(Token)match(input,21,FOLLOW_21_in_ruleIfStatement347); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getIfStatementAccess().getElseKeyword_8_0());
                          
                    }

                    }

                    otherlv_9=(Token)match(input,19,FOLLOW_19_in_ruleIfStatement360); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_9, grammarAccess.getIfStatementAccess().getLeftCurlyBracketKeyword_8_1());
                          
                    }
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:195:1: ( (lv_else_10_0= ruleIfExpression ) )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:196:1: (lv_else_10_0= ruleIfExpression )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:196:1: (lv_else_10_0= ruleIfExpression )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:197:3: lv_else_10_0= ruleIfExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getIfStatementAccess().getElseIfExpressionParserRuleCall_8_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleIfExpression_in_ruleIfStatement381);
                    lv_else_10_0=ruleIfExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getIfStatementRule());
                      	        }
                             		set(
                             			current, 
                             			"else",
                              		lv_else_10_0, 
                              		"IfExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_11=(Token)match(input,20,FOLLOW_20_in_ruleIfStatement393); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_11, grammarAccess.getIfStatementAccess().getRightCurlyBracketKeyword_8_3());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfStatement"


    // $ANTLR start "entryRuleIfExpression"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:225:1: entryRuleIfExpression returns [EObject current=null] : iv_ruleIfExpression= ruleIfExpression EOF ;
    public final EObject entryRuleIfExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfExpression = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:226:2: (iv_ruleIfExpression= ruleIfExpression EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:227:2: iv_ruleIfExpression= ruleIfExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIfExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleIfExpression_in_entryRuleIfExpression431);
            iv_ruleIfExpression=ruleIfExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIfExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIfExpression441); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfExpression"


    // $ANTLR start "ruleIfExpression"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:234:1: ruleIfExpression returns [EObject current=null] : (this_IfStatement_0= ruleIfStatement | ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleIfExpression() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        EObject this_IfStatement_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:237:28: ( (this_IfStatement_0= ruleIfStatement | ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:238:1: (this_IfStatement_0= ruleIfStatement | ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:238:1: (this_IfStatement_0= ruleIfStatement | ( (lv_name_1_0= RULE_ID ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:239:5: this_IfStatement_0= ruleIfStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getIfExpressionAccess().getIfStatementParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIfStatement_in_ruleIfExpression488);
                    this_IfStatement_0=ruleIfStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IfStatement_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:248:6: ( (lv_name_1_0= RULE_ID ) )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:248:6: ( (lv_name_1_0= RULE_ID ) )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:249:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:249:1: (lv_name_1_0= RULE_ID )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:250:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleIfExpression510); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_1_0, grammarAccess.getIfExpressionAccess().getNameIDTerminalRuleCall_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIfExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_1_0, 
                              		"ID");
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfExpression"


    // $ANTLR start "entryRuleForStatement"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:276:1: entryRuleForStatement returns [EObject current=null] : iv_ruleForStatement= ruleForStatement EOF ;
    public final EObject entryRuleForStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForStatement = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:277:2: (iv_ruleForStatement= ruleForStatement EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:278:2: iv_ruleForStatement= ruleForStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getForStatementRule()); 
            }
            pushFollow(FOLLOW_ruleForStatement_in_entryRuleForStatement553);
            iv_ruleForStatement=ruleForStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleForStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleForStatement563); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForStatement"


    // $ANTLR start "ruleForStatement"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:285:1: ruleForStatement returns [EObject current=null] : (otherlv_0= 'for' otherlv_1= '(' this_IntVariable_2= ruleIntVariable otherlv_3= ';' ( (lv_condition_4_0= ruleForCondition ) ) otherlv_5= ';' ( (lv_expression_6_0= ruleIncrement ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statement_9_0= ruleForExpression ) ) otherlv_10= '}' ) ;
    public final EObject ruleForStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject this_IntVariable_2 = null;

        EObject lv_condition_4_0 = null;

        EObject lv_expression_6_0 = null;

        EObject lv_statement_9_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:288:28: ( (otherlv_0= 'for' otherlv_1= '(' this_IntVariable_2= ruleIntVariable otherlv_3= ';' ( (lv_condition_4_0= ruleForCondition ) ) otherlv_5= ';' ( (lv_expression_6_0= ruleIncrement ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statement_9_0= ruleForExpression ) ) otherlv_10= '}' ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:289:1: (otherlv_0= 'for' otherlv_1= '(' this_IntVariable_2= ruleIntVariable otherlv_3= ';' ( (lv_condition_4_0= ruleForCondition ) ) otherlv_5= ';' ( (lv_expression_6_0= ruleIncrement ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statement_9_0= ruleForExpression ) ) otherlv_10= '}' )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:289:1: (otherlv_0= 'for' otherlv_1= '(' this_IntVariable_2= ruleIntVariable otherlv_3= ';' ( (lv_condition_4_0= ruleForCondition ) ) otherlv_5= ';' ( (lv_expression_6_0= ruleIncrement ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statement_9_0= ruleForExpression ) ) otherlv_10= '}' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:289:3: otherlv_0= 'for' otherlv_1= '(' this_IntVariable_2= ruleIntVariable otherlv_3= ';' ( (lv_condition_4_0= ruleForCondition ) ) otherlv_5= ';' ( (lv_expression_6_0= ruleIncrement ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statement_9_0= ruleForExpression ) ) otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleForStatement600); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getForStatementAccess().getForKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,16,FOLLOW_16_in_ruleForStatement612); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getForStatementAccess().getLeftParenthesisKeyword_1());
                  
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getForStatementAccess().getIntVariableParserRuleCall_2()); 
                  
            }
            pushFollow(FOLLOW_ruleIntVariable_in_ruleForStatement634);
            this_IntVariable_2=ruleIntVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_IntVariable_2; 
                      afterParserOrEnumRuleCall();
                  
            }
            otherlv_3=(Token)match(input,23,FOLLOW_23_in_ruleForStatement645); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getForStatementAccess().getSemicolonKeyword_3());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:310:1: ( (lv_condition_4_0= ruleForCondition ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:311:1: (lv_condition_4_0= ruleForCondition )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:311:1: (lv_condition_4_0= ruleForCondition )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:312:3: lv_condition_4_0= ruleForCondition
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getForStatementAccess().getConditionForConditionParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleForCondition_in_ruleForStatement666);
            lv_condition_4_0=ruleForCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getForStatementRule());
              	        }
                     		set(
                     			current, 
                     			"condition",
                      		lv_condition_4_0, 
                      		"ForCondition");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,23,FOLLOW_23_in_ruleForStatement678); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getForStatementAccess().getSemicolonKeyword_5());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:332:1: ( (lv_expression_6_0= ruleIncrement ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:333:1: (lv_expression_6_0= ruleIncrement )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:333:1: (lv_expression_6_0= ruleIncrement )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:334:3: lv_expression_6_0= ruleIncrement
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getForStatementAccess().getExpressionIncrementParserRuleCall_6_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleIncrement_in_ruleForStatement699);
            lv_expression_6_0=ruleIncrement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getForStatementRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_6_0, 
                      		"Increment");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_7=(Token)match(input,17,FOLLOW_17_in_ruleForStatement711); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getForStatementAccess().getRightParenthesisKeyword_7());
                  
            }
            otherlv_8=(Token)match(input,19,FOLLOW_19_in_ruleForStatement723); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getForStatementAccess().getLeftCurlyBracketKeyword_8());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:358:1: ( (lv_statement_9_0= ruleForExpression ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:359:1: (lv_statement_9_0= ruleForExpression )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:359:1: (lv_statement_9_0= ruleForExpression )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:360:3: lv_statement_9_0= ruleForExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getForStatementAccess().getStatementForExpressionParserRuleCall_9_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleForExpression_in_ruleForStatement744);
            lv_statement_9_0=ruleForExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getForStatementRule());
              	        }
                     		set(
                     			current, 
                     			"statement",
                      		lv_statement_9_0, 
                      		"ForExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_10=(Token)match(input,20,FOLLOW_20_in_ruleForStatement756); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_10, grammarAccess.getForStatementAccess().getRightCurlyBracketKeyword_10());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForStatement"


    // $ANTLR start "entryRuleIncrement"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:388:1: entryRuleIncrement returns [EObject current=null] : iv_ruleIncrement= ruleIncrement EOF ;
    public final EObject entryRuleIncrement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIncrement = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:389:2: (iv_ruleIncrement= ruleIncrement EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:390:2: iv_ruleIncrement= ruleIncrement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIncrementRule()); 
            }
            pushFollow(FOLLOW_ruleIncrement_in_entryRuleIncrement792);
            iv_ruleIncrement=ruleIncrement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIncrement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIncrement802); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIncrement"


    // $ANTLR start "ruleIncrement"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:397:1: ruleIncrement returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleIncrement() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:400:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:401:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:401:1: ( (lv_name_0_0= RULE_ID ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:402:1: (lv_name_0_0= RULE_ID )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:402:1: (lv_name_0_0= RULE_ID )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:403:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleIncrement843); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getIncrementAccess().getNameIDTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIncrementRule());
              	        }
                     		addWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIncrement"


    // $ANTLR start "entryRuleForCondition"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:427:1: entryRuleForCondition returns [EObject current=null] : iv_ruleForCondition= ruleForCondition EOF ;
    public final EObject entryRuleForCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForCondition = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:428:2: (iv_ruleForCondition= ruleForCondition EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:429:2: iv_ruleForCondition= ruleForCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getForConditionRule()); 
            }
            pushFollow(FOLLOW_ruleForCondition_in_entryRuleForCondition883);
            iv_ruleForCondition=ruleForCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleForCondition; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleForCondition893); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForCondition"


    // $ANTLR start "ruleForCondition"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:436:1: ruleForCondition returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleForCondition() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:439:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:440:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:440:1: ( (lv_name_0_0= RULE_ID ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:441:1: (lv_name_0_0= RULE_ID )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:441:1: (lv_name_0_0= RULE_ID )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:442:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleForCondition934); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getForConditionAccess().getNameIDTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getForConditionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		true, 
                      		"ID");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForCondition"


    // $ANTLR start "entryRuleForExpression"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:466:1: entryRuleForExpression returns [EObject current=null] : iv_ruleForExpression= ruleForExpression EOF ;
    public final EObject entryRuleForExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForExpression = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:467:2: (iv_ruleForExpression= ruleForExpression EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:468:2: iv_ruleForExpression= ruleForExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getForExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleForExpression_in_entryRuleForExpression974);
            iv_ruleForExpression=ruleForExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleForExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleForExpression984); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForExpression"


    // $ANTLR start "ruleForExpression"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:475:1: ruleForExpression returns [EObject current=null] : (this_ForStatement_0= ruleForStatement | ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleForExpression() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        EObject this_ForStatement_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:478:28: ( (this_ForStatement_0= ruleForStatement | ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:479:1: (this_ForStatement_0= ruleForStatement | ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:479:1: (this_ForStatement_0= ruleForStatement | ( (lv_name_1_0= RULE_ID ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==22) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_ID) ) {
                alt4=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:480:5: this_ForStatement_0= ruleForStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getForExpressionAccess().getForStatementParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleForStatement_in_ruleForExpression1031);
                    this_ForStatement_0=ruleForStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ForStatement_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:489:6: ( (lv_name_1_0= RULE_ID ) )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:489:6: ( (lv_name_1_0= RULE_ID ) )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:490:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:490:1: (lv_name_1_0= RULE_ID )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:491:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleForExpression1053); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_1_0, grammarAccess.getForExpressionAccess().getNameIDTerminalRuleCall_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getForExpressionRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_1_0, 
                              		"ID");
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForExpression"


    // $ANTLR start "entryRuleIntVariable"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:515:1: entryRuleIntVariable returns [EObject current=null] : iv_ruleIntVariable= ruleIntVariable EOF ;
    public final EObject entryRuleIntVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntVariable = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:516:2: (iv_ruleIntVariable= ruleIntVariable EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:517:2: iv_ruleIntVariable= ruleIntVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntVariableRule()); 
            }
            pushFollow(FOLLOW_ruleIntVariable_in_entryRuleIntVariable1094);
            iv_ruleIntVariable=ruleIntVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntVariable; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntVariable1104); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntVariable"


    // $ANTLR start "ruleIntVariable"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:524:1: ruleIntVariable returns [EObject current=null] : (otherlv_0= 'int' ( (lv_name_1_0= RULE_INTEGER ) ) ) ;
    public final EObject ruleIntVariable() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;

         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:527:28: ( (otherlv_0= 'int' ( (lv_name_1_0= RULE_INTEGER ) ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:528:1: (otherlv_0= 'int' ( (lv_name_1_0= RULE_INTEGER ) ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:528:1: (otherlv_0= 'int' ( (lv_name_1_0= RULE_INTEGER ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:528:3: otherlv_0= 'int' ( (lv_name_1_0= RULE_INTEGER ) )
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleIntVariable1141); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIntVariableAccess().getIntKeyword_0());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:532:1: ( (lv_name_1_0= RULE_INTEGER ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:533:1: (lv_name_1_0= RULE_INTEGER )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:533:1: (lv_name_1_0= RULE_INTEGER )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:534:3: lv_name_1_0= RULE_INTEGER
            {
            lv_name_1_0=(Token)match(input,RULE_INTEGER,FOLLOW_RULE_INTEGER_in_ruleIntVariable1158); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getIntVariableAccess().getNameINTEGERTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntVariableRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"INTEGER");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntVariable"


    // $ANTLR start "entryRuleStatement"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:564:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:565:2: (iv_ruleStatement= ruleStatement EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:566:2: iv_ruleStatement= ruleStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStatementRule()); 
            }
            pushFollow(FOLLOW_ruleStatement_in_entryRuleStatement1205);
            iv_ruleStatement=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatement1215); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:573:1: ruleStatement returns [EObject current=null] : (this_Definition_0= ruleDefinition | this_Evaluation_1= ruleEvaluation ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        EObject this_Definition_0 = null;

        EObject this_Evaluation_1 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:576:28: ( (this_Definition_0= ruleDefinition | this_Evaluation_1= ruleEvaluation ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:577:1: (this_Definition_0= ruleDefinition | this_Evaluation_1= ruleEvaluation )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:577:1: (this_Definition_0= ruleDefinition | this_Evaluation_1= ruleEvaluation )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==25) ) {
                alt5=1;
            }
            else if ( (LA5_0==RULE_ID||(LA5_0>=RULE_INT && LA5_0<=RULE_A_STRING)||LA5_0==29) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:578:5: this_Definition_0= ruleDefinition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getDefinitionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleDefinition_in_ruleStatement1262);
                    this_Definition_0=ruleDefinition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Definition_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:588:5: this_Evaluation_1= ruleEvaluation
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getEvaluationParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleEvaluation_in_ruleStatement1289);
                    this_Evaluation_1=ruleEvaluation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Evaluation_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleDefinition"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:604:1: entryRuleDefinition returns [EObject current=null] : iv_ruleDefinition= ruleDefinition EOF ;
    public final EObject entryRuleDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinition = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:605:2: (iv_ruleDefinition= ruleDefinition EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:606:2: iv_ruleDefinition= ruleDefinition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDefinitionRule()); 
            }
            pushFollow(FOLLOW_ruleDefinition_in_entryRuleDefinition1324);
            iv_ruleDefinition=ruleDefinition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDefinition; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDefinition1334); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinition"


    // $ANTLR start "ruleDefinition"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:613:1: ruleDefinition returns [EObject current=null] : (otherlv_0= 'def' ( (lv_returnType_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' ( (lv_types_4_0= ruleType ) ) ( (lv_args_5_0= ruleDeclaredParameter ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) ( (lv_args_8_0= ruleDeclaredParameter ) ) )* otherlv_9= ')' )? otherlv_10= ';' ) ;
    public final EObject ruleDefinition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_returnType_1_0 = null;

        EObject lv_types_4_0 = null;

        EObject lv_args_5_0 = null;

        EObject lv_types_7_0 = null;

        EObject lv_args_8_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:616:28: ( (otherlv_0= 'def' ( (lv_returnType_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' ( (lv_types_4_0= ruleType ) ) ( (lv_args_5_0= ruleDeclaredParameter ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) ( (lv_args_8_0= ruleDeclaredParameter ) ) )* otherlv_9= ')' )? otherlv_10= ';' ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:617:1: (otherlv_0= 'def' ( (lv_returnType_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' ( (lv_types_4_0= ruleType ) ) ( (lv_args_5_0= ruleDeclaredParameter ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) ( (lv_args_8_0= ruleDeclaredParameter ) ) )* otherlv_9= ')' )? otherlv_10= ';' )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:617:1: (otherlv_0= 'def' ( (lv_returnType_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' ( (lv_types_4_0= ruleType ) ) ( (lv_args_5_0= ruleDeclaredParameter ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) ( (lv_args_8_0= ruleDeclaredParameter ) ) )* otherlv_9= ')' )? otherlv_10= ';' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:617:3: otherlv_0= 'def' ( (lv_returnType_1_0= ruleType ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '(' ( (lv_types_4_0= ruleType ) ) ( (lv_args_5_0= ruleDeclaredParameter ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) ( (lv_args_8_0= ruleDeclaredParameter ) ) )* otherlv_9= ')' )? otherlv_10= ';'
            {
            otherlv_0=(Token)match(input,25,FOLLOW_25_in_ruleDefinition1371); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getDefinitionAccess().getDefKeyword_0());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:621:1: ( (lv_returnType_1_0= ruleType ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:622:1: (lv_returnType_1_0= ruleType )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:622:1: (lv_returnType_1_0= ruleType )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:623:3: lv_returnType_1_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getDefinitionAccess().getReturnTypeTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleType_in_ruleDefinition1392);
            lv_returnType_1_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getDefinitionRule());
              	        }
                     		set(
                     			current, 
                     			"returnType",
                      		lv_returnType_1_0, 
                      		"Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:639:2: ( (lv_name_2_0= RULE_ID ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:640:1: (lv_name_2_0= RULE_ID )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:640:1: (lv_name_2_0= RULE_ID )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:641:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDefinition1409); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getDefinitionAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getDefinitionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"ID");
              	    
            }

            }


            }

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:657:2: (otherlv_3= '(' ( (lv_types_4_0= ruleType ) ) ( (lv_args_5_0= ruleDeclaredParameter ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) ( (lv_args_8_0= ruleDeclaredParameter ) ) )* otherlv_9= ')' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==16) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:657:4: otherlv_3= '(' ( (lv_types_4_0= ruleType ) ) ( (lv_args_5_0= ruleDeclaredParameter ) ) (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) ( (lv_args_8_0= ruleDeclaredParameter ) ) )* otherlv_9= ')'
                    {
                    otherlv_3=(Token)match(input,16,FOLLOW_16_in_ruleDefinition1427); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getDefinitionAccess().getLeftParenthesisKeyword_3_0());
                          
                    }
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:661:1: ( (lv_types_4_0= ruleType ) )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:662:1: (lv_types_4_0= ruleType )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:662:1: (lv_types_4_0= ruleType )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:663:3: lv_types_4_0= ruleType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDefinitionAccess().getTypesTypeParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleType_in_ruleDefinition1448);
                    lv_types_4_0=ruleType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDefinitionRule());
                      	        }
                             		add(
                             			current, 
                             			"types",
                              		lv_types_4_0, 
                              		"Type");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:679:2: ( (lv_args_5_0= ruleDeclaredParameter ) )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:680:1: (lv_args_5_0= ruleDeclaredParameter )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:680:1: (lv_args_5_0= ruleDeclaredParameter )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:681:3: lv_args_5_0= ruleDeclaredParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDefinitionAccess().getArgsDeclaredParameterParserRuleCall_3_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleDeclaredParameter_in_ruleDefinition1469);
                    lv_args_5_0=ruleDeclaredParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDefinitionRule());
                      	        }
                             		add(
                             			current, 
                             			"args",
                              		lv_args_5_0, 
                              		"DeclaredParameter");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:697:2: (otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) ( (lv_args_8_0= ruleDeclaredParameter ) ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==26) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:697:4: otherlv_6= ',' ( (lv_types_7_0= ruleType ) ) ( (lv_args_8_0= ruleDeclaredParameter ) )
                    	    {
                    	    otherlv_6=(Token)match(input,26,FOLLOW_26_in_ruleDefinition1482); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_6, grammarAccess.getDefinitionAccess().getCommaKeyword_3_3_0());
                    	          
                    	    }
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:701:1: ( (lv_types_7_0= ruleType ) )
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:702:1: (lv_types_7_0= ruleType )
                    	    {
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:702:1: (lv_types_7_0= ruleType )
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:703:3: lv_types_7_0= ruleType
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getDefinitionAccess().getTypesTypeParserRuleCall_3_3_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleType_in_ruleDefinition1503);
                    	    lv_types_7_0=ruleType();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getDefinitionRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"types",
                    	              		lv_types_7_0, 
                    	              		"Type");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }

                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:719:2: ( (lv_args_8_0= ruleDeclaredParameter ) )
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:720:1: (lv_args_8_0= ruleDeclaredParameter )
                    	    {
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:720:1: (lv_args_8_0= ruleDeclaredParameter )
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:721:3: lv_args_8_0= ruleDeclaredParameter
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getDefinitionAccess().getArgsDeclaredParameterParserRuleCall_3_3_2_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleDeclaredParameter_in_ruleDefinition1524);
                    	    lv_args_8_0=ruleDeclaredParameter();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getDefinitionRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"args",
                    	              		lv_args_8_0, 
                    	              		"DeclaredParameter");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,17,FOLLOW_17_in_ruleDefinition1538); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_9, grammarAccess.getDefinitionAccess().getRightParenthesisKeyword_3_4());
                          
                    }

                    }
                    break;

            }

            otherlv_10=(Token)match(input,23,FOLLOW_23_in_ruleDefinition1552); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_10, grammarAccess.getDefinitionAccess().getSemicolonKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinition"


    // $ANTLR start "entryRuleDeclaredParameter"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:753:1: entryRuleDeclaredParameter returns [EObject current=null] : iv_ruleDeclaredParameter= ruleDeclaredParameter EOF ;
    public final EObject entryRuleDeclaredParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclaredParameter = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:754:2: (iv_ruleDeclaredParameter= ruleDeclaredParameter EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:755:2: iv_ruleDeclaredParameter= ruleDeclaredParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDeclaredParameterRule()); 
            }
            pushFollow(FOLLOW_ruleDeclaredParameter_in_entryRuleDeclaredParameter1588);
            iv_ruleDeclaredParameter=ruleDeclaredParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDeclaredParameter; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDeclaredParameter1598); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclaredParameter"


    // $ANTLR start "ruleDeclaredParameter"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:762:1: ruleDeclaredParameter returns [EObject current=null] : ( (lv_name_0_0= ruleVarName ) ) ;
    public final EObject ruleDeclaredParameter() throws RecognitionException {
        EObject current = null;

        EObject lv_name_0_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:765:28: ( ( (lv_name_0_0= ruleVarName ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:766:1: ( (lv_name_0_0= ruleVarName ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:766:1: ( (lv_name_0_0= ruleVarName ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:767:1: (lv_name_0_0= ruleVarName )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:767:1: (lv_name_0_0= ruleVarName )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:768:3: lv_name_0_0= ruleVarName
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getDeclaredParameterAccess().getNameVarNameParserRuleCall_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVarName_in_ruleDeclaredParameter1643);
            lv_name_0_0=ruleVarName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getDeclaredParameterRule());
              	        }
                     		set(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"VarName");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclaredParameter"


    // $ANTLR start "entryRuleType"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:792:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:793:2: (iv_ruleType= ruleType EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:794:2: iv_ruleType= ruleType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeRule()); 
            }
            pushFollow(FOLLOW_ruleType_in_entryRuleType1678);
            iv_ruleType=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleType1688); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:801:1: ruleType returns [EObject current=null] : (this_IntegerType_0= ruleIntegerType | this_StringType_1= ruleStringType ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerType_0 = null;

        EObject this_StringType_1 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:804:28: ( (this_IntegerType_0= ruleIntegerType | this_StringType_1= ruleStringType ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:805:1: (this_IntegerType_0= ruleIntegerType | this_StringType_1= ruleStringType )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:805:1: (this_IntegerType_0= ruleIntegerType | this_StringType_1= ruleStringType )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==27) ) {
                alt8=1;
            }
            else if ( (LA8_0==28) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:806:5: this_IntegerType_0= ruleIntegerType
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeAccess().getIntegerTypeParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIntegerType_in_ruleType1735);
                    this_IntegerType_0=ruleIntegerType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerType_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:816:5: this_StringType_1= ruleStringType
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeAccess().getStringTypeParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleStringType_in_ruleType1762);
                    this_StringType_1=ruleStringType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringType_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleIntegerType"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:832:1: entryRuleIntegerType returns [EObject current=null] : iv_ruleIntegerType= ruleIntegerType EOF ;
    public final EObject entryRuleIntegerType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerType = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:833:2: (iv_ruleIntegerType= ruleIntegerType EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:834:2: iv_ruleIntegerType= ruleIntegerType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerTypeRule()); 
            }
            pushFollow(FOLLOW_ruleIntegerType_in_entryRuleIntegerType1797);
            iv_ruleIntegerType=ruleIntegerType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerType1807); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerType"


    // $ANTLR start "ruleIntegerType"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:841:1: ruleIntegerType returns [EObject current=null] : ( () ( (lv_type_1_0= 'Integer' ) ) ) ;
    public final EObject ruleIntegerType() throws RecognitionException {
        EObject current = null;

        Token lv_type_1_0=null;

         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:844:28: ( ( () ( (lv_type_1_0= 'Integer' ) ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:845:1: ( () ( (lv_type_1_0= 'Integer' ) ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:845:1: ( () ( (lv_type_1_0= 'Integer' ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:845:2: () ( (lv_type_1_0= 'Integer' ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:845:2: ()
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:846:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getIntegerTypeAccess().getIntegerTypeAction_0(),
                          current);
                  
            }

            }

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:851:2: ( (lv_type_1_0= 'Integer' ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:852:1: (lv_type_1_0= 'Integer' )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:852:1: (lv_type_1_0= 'Integer' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:853:3: lv_type_1_0= 'Integer'
            {
            lv_type_1_0=(Token)match(input,27,FOLLOW_27_in_ruleIntegerType1859); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_type_1_0, grammarAccess.getIntegerTypeAccess().getTypeIntegerKeyword_1_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerTypeRule());
              	        }
                     		setWithLastConsumed(current, "type", lv_type_1_0, "Integer");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerType"


    // $ANTLR start "entryRuleStringType"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:874:1: entryRuleStringType returns [EObject current=null] : iv_ruleStringType= ruleStringType EOF ;
    public final EObject entryRuleStringType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringType = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:875:2: (iv_ruleStringType= ruleStringType EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:876:2: iv_ruleStringType= ruleStringType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringTypeRule()); 
            }
            pushFollow(FOLLOW_ruleStringType_in_entryRuleStringType1908);
            iv_ruleStringType=ruleStringType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStringType1918); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringType"


    // $ANTLR start "ruleStringType"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:883:1: ruleStringType returns [EObject current=null] : ( () ( (lv_type_1_0= 'String' ) ) ) ;
    public final EObject ruleStringType() throws RecognitionException {
        EObject current = null;

        Token lv_type_1_0=null;

         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:886:28: ( ( () ( (lv_type_1_0= 'String' ) ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:887:1: ( () ( (lv_type_1_0= 'String' ) ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:887:1: ( () ( (lv_type_1_0= 'String' ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:887:2: () ( (lv_type_1_0= 'String' ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:887:2: ()
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:888:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getStringTypeAccess().getStringTypeAction_0(),
                          current);
                  
            }

            }

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:893:2: ( (lv_type_1_0= 'String' ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:894:1: (lv_type_1_0= 'String' )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:894:1: (lv_type_1_0= 'String' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:895:3: lv_type_1_0= 'String'
            {
            lv_type_1_0=(Token)match(input,28,FOLLOW_28_in_ruleStringType1970); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_type_1_0, grammarAccess.getStringTypeAccess().getTypeStringKeyword_1_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringTypeRule());
              	        }
                     		setWithLastConsumed(current, "type", lv_type_1_0, "String");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringType"


    // $ANTLR start "entryRuleEvaluation"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:916:1: entryRuleEvaluation returns [EObject current=null] : iv_ruleEvaluation= ruleEvaluation EOF ;
    public final EObject entryRuleEvaluation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvaluation = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:917:2: (iv_ruleEvaluation= ruleEvaluation EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:918:2: iv_ruleEvaluation= ruleEvaluation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEvaluationRule()); 
            }
            pushFollow(FOLLOW_ruleEvaluation_in_entryRuleEvaluation2019);
            iv_ruleEvaluation=ruleEvaluation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEvaluation; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEvaluation2029); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvaluation"


    // $ANTLR start "ruleEvaluation"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:925:1: ruleEvaluation returns [EObject current=null] : ( ( (lv_expression_0_0= ruleExpression ) ) otherlv_1= ';' ) ;
    public final EObject ruleEvaluation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_expression_0_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:928:28: ( ( ( (lv_expression_0_0= ruleExpression ) ) otherlv_1= ';' ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:929:1: ( ( (lv_expression_0_0= ruleExpression ) ) otherlv_1= ';' )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:929:1: ( ( (lv_expression_0_0= ruleExpression ) ) otherlv_1= ';' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:929:2: ( (lv_expression_0_0= ruleExpression ) ) otherlv_1= ';'
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:929:2: ( (lv_expression_0_0= ruleExpression ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:930:1: (lv_expression_0_0= ruleExpression )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:930:1: (lv_expression_0_0= ruleExpression )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:931:3: lv_expression_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEvaluationAccess().getExpressionExpressionParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleEvaluation2075);
            lv_expression_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEvaluationRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_0_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,23,FOLLOW_23_in_ruleEvaluation2087); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEvaluationAccess().getSemicolonKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvaluation"


    // $ANTLR start "entryRuleExpression"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:959:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:960:2: (iv_ruleExpression= ruleExpression EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:961:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression2123);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression2133); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:968:1: ruleExpression returns [EObject current=null] : (this_FunctionCall_0= ruleFunctionCall | this_Constant_1= ruleConstant | this_IntegerAtom_2= ruleIntegerAtom | this_StringAtom_3= ruleStringAtom ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_FunctionCall_0 = null;

        EObject this_Constant_1 = null;

        EObject this_IntegerAtom_2 = null;

        EObject this_StringAtom_3 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:971:28: ( (this_FunctionCall_0= ruleFunctionCall | this_Constant_1= ruleConstant | this_IntegerAtom_2= ruleIntegerAtom | this_StringAtom_3= ruleStringAtom ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:972:1: (this_FunctionCall_0= ruleFunctionCall | this_Constant_1= ruleConstant | this_IntegerAtom_2= ruleIntegerAtom | this_StringAtom_3= ruleStringAtom )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:972:1: (this_FunctionCall_0= ruleFunctionCall | this_Constant_1= ruleConstant | this_IntegerAtom_2= ruleIntegerAtom | this_StringAtom_3= ruleStringAtom )
            int alt9=4;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt9=1;
                }
                break;
            case 29:
                {
                alt9=2;
                }
                break;
            case RULE_INT:
                {
                alt9=3;
                }
                break;
            case RULE_A_STRING:
                {
                alt9=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:973:5: this_FunctionCall_0= ruleFunctionCall
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAccess().getFunctionCallParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleFunctionCall_in_ruleExpression2180);
                    this_FunctionCall_0=ruleFunctionCall();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FunctionCall_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:983:5: this_Constant_1= ruleConstant
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAccess().getConstantParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleConstant_in_ruleExpression2207);
                    this_Constant_1=ruleConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Constant_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:993:5: this_IntegerAtom_2= ruleIntegerAtom
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAccess().getIntegerAtomParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIntegerAtom_in_ruleExpression2234);
                    this_IntegerAtom_2=ruleIntegerAtom();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerAtom_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1003:5: this_StringAtom_3= ruleStringAtom
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAccess().getStringAtomParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleStringAtom_in_ruleExpression2261);
                    this_StringAtom_3=ruleStringAtom();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringAtom_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleIntegerAtom"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1019:1: entryRuleIntegerAtom returns [EObject current=null] : iv_ruleIntegerAtom= ruleIntegerAtom EOF ;
    public final EObject entryRuleIntegerAtom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerAtom = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1020:2: (iv_ruleIntegerAtom= ruleIntegerAtom EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1021:2: iv_ruleIntegerAtom= ruleIntegerAtom EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerAtomRule()); 
            }
            pushFollow(FOLLOW_ruleIntegerAtom_in_entryRuleIntegerAtom2296);
            iv_ruleIntegerAtom=ruleIntegerAtom();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerAtom; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerAtom2306); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerAtom"


    // $ANTLR start "ruleIntegerAtom"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1028:1: ruleIntegerAtom returns [EObject current=null] : ( () this_INT_1= RULE_INT ) ;
    public final EObject ruleIntegerAtom() throws RecognitionException {
        EObject current = null;

        Token this_INT_1=null;

         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1031:28: ( ( () this_INT_1= RULE_INT ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1032:1: ( () this_INT_1= RULE_INT )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1032:1: ( () this_INT_1= RULE_INT )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1032:2: () this_INT_1= RULE_INT
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1032:2: ()
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1033:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getIntegerAtomAccess().getIntegerAtomAction_0(),
                          current);
                  
            }

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIntegerAtom2351); if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_INT_1, grammarAccess.getIntegerAtomAccess().getINTTerminalRuleCall_1()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerAtom"


    // $ANTLR start "entryRuleStringAtom"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1050:1: entryRuleStringAtom returns [EObject current=null] : iv_ruleStringAtom= ruleStringAtom EOF ;
    public final EObject entryRuleStringAtom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringAtom = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1051:2: (iv_ruleStringAtom= ruleStringAtom EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1052:2: iv_ruleStringAtom= ruleStringAtom EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringAtomRule()); 
            }
            pushFollow(FOLLOW_ruleStringAtom_in_entryRuleStringAtom2386);
            iv_ruleStringAtom=ruleStringAtom();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringAtom; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStringAtom2396); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringAtom"


    // $ANTLR start "ruleStringAtom"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1059:1: ruleStringAtom returns [EObject current=null] : ( (lv_value_0_0= RULE_A_STRING ) ) ;
    public final EObject ruleStringAtom() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1062:28: ( ( (lv_value_0_0= RULE_A_STRING ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1063:1: ( (lv_value_0_0= RULE_A_STRING ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1063:1: ( (lv_value_0_0= RULE_A_STRING ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1064:1: (lv_value_0_0= RULE_A_STRING )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1064:1: (lv_value_0_0= RULE_A_STRING )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1065:3: lv_value_0_0= RULE_A_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_A_STRING,FOLLOW_RULE_A_STRING_in_ruleStringAtom2437); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getStringAtomAccess().getValueA_STRINGTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringAtomRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"A_STRING");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringAtom"


    // $ANTLR start "entryRuleFunctionCall"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1089:1: entryRuleFunctionCall returns [EObject current=null] : iv_ruleFunctionCall= ruleFunctionCall EOF ;
    public final EObject entryRuleFunctionCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionCall = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1090:2: (iv_ruleFunctionCall= ruleFunctionCall EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1091:2: iv_ruleFunctionCall= ruleFunctionCall EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionCallRule()); 
            }
            pushFollow(FOLLOW_ruleFunctionCall_in_entryRuleFunctionCall2477);
            iv_ruleFunctionCall=ruleFunctionCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunctionCall; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunctionCall2487); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionCall"


    // $ANTLR start "ruleFunctionCall"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1098:1: ruleFunctionCall returns [EObject current=null] : ( () ( (lv_source_1_0= ruleDefinitionRef ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleExpression ) ) )* )? otherlv_6= ')' ) ;
    public final EObject ruleFunctionCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_source_1_0 = null;

        EObject lv_args_3_0 = null;

        EObject lv_args_5_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1101:28: ( ( () ( (lv_source_1_0= ruleDefinitionRef ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleExpression ) ) )* )? otherlv_6= ')' ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1102:1: ( () ( (lv_source_1_0= ruleDefinitionRef ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleExpression ) ) )* )? otherlv_6= ')' )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1102:1: ( () ( (lv_source_1_0= ruleDefinitionRef ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleExpression ) ) )* )? otherlv_6= ')' )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1102:2: () ( (lv_source_1_0= ruleDefinitionRef ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleExpression ) ) )* )? otherlv_6= ')'
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1102:2: ()
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1103:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getFunctionCallAccess().getFunctionCallAction_0(),
                          current);
                  
            }

            }

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1108:2: ( (lv_source_1_0= ruleDefinitionRef ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1109:1: (lv_source_1_0= ruleDefinitionRef )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1109:1: (lv_source_1_0= ruleDefinitionRef )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1110:3: lv_source_1_0= ruleDefinitionRef
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFunctionCallAccess().getSourceDefinitionRefParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleDefinitionRef_in_ruleFunctionCall2542);
            lv_source_1_0=ruleDefinitionRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFunctionCallRule());
              	        }
                     		set(
                     			current, 
                     			"source",
                      		lv_source_1_0, 
                      		"DefinitionRef");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleFunctionCall2554); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getFunctionCallAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1130:1: ( ( (lv_args_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleExpression ) ) )* )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_ID||(LA11_0>=RULE_INT && LA11_0<=RULE_A_STRING)||LA11_0==29) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1130:2: ( (lv_args_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleExpression ) ) )*
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1130:2: ( (lv_args_3_0= ruleExpression ) )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1131:1: (lv_args_3_0= ruleExpression )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1131:1: (lv_args_3_0= ruleExpression )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1132:3: lv_args_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFunctionCallAccess().getArgsExpressionParserRuleCall_3_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleFunctionCall2576);
                    lv_args_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFunctionCallRule());
                      	        }
                             		add(
                             			current, 
                             			"args",
                              		lv_args_3_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1148:2: (otherlv_4= ',' ( (lv_args_5_0= ruleExpression ) ) )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==26) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1148:4: otherlv_4= ',' ( (lv_args_5_0= ruleExpression ) )
                    	    {
                    	    otherlv_4=(Token)match(input,26,FOLLOW_26_in_ruleFunctionCall2589); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_4, grammarAccess.getFunctionCallAccess().getCommaKeyword_3_1_0());
                    	          
                    	    }
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1152:1: ( (lv_args_5_0= ruleExpression ) )
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1153:1: (lv_args_5_0= ruleExpression )
                    	    {
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1153:1: (lv_args_5_0= ruleExpression )
                    	    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1154:3: lv_args_5_0= ruleExpression
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getFunctionCallAccess().getArgsExpressionParserRuleCall_3_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleExpression_in_ruleFunctionCall2610);
                    	    lv_args_5_0=ruleExpression();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getFunctionCallRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"args",
                    	              		lv_args_5_0, 
                    	              		"Expression");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,17,FOLLOW_17_in_ruleFunctionCall2626); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getFunctionCallAccess().getRightParenthesisKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionCall"


    // $ANTLR start "entryRuleDefinitionRef"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1182:1: entryRuleDefinitionRef returns [EObject current=null] : iv_ruleDefinitionRef= ruleDefinitionRef EOF ;
    public final EObject entryRuleDefinitionRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinitionRef = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1183:2: (iv_ruleDefinitionRef= ruleDefinitionRef EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1184:2: iv_ruleDefinitionRef= ruleDefinitionRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDefinitionRefRule()); 
            }
            pushFollow(FOLLOW_ruleDefinitionRef_in_entryRuleDefinitionRef2662);
            iv_ruleDefinitionRef=ruleDefinitionRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDefinitionRef; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDefinitionRef2672); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinitionRef"


    // $ANTLR start "ruleDefinitionRef"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1191:1: ruleDefinitionRef returns [EObject current=null] : ( () ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleDefinitionRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1194:28: ( ( () ( (otherlv_1= RULE_ID ) ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1195:1: ( () ( (otherlv_1= RULE_ID ) ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1195:1: ( () ( (otherlv_1= RULE_ID ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1195:2: () ( (otherlv_1= RULE_ID ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1195:2: ()
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1196:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getDefinitionRefAccess().getDefinitionRefAction_0(),
                          current);
                  
            }

            }

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1201:2: ( (otherlv_1= RULE_ID ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1202:1: (otherlv_1= RULE_ID )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1202:1: (otherlv_1= RULE_ID )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1203:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getDefinitionRefRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDefinitionRef2726); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getDefinitionRefAccess().getRefNameDefinitionCrossReference_1_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinitionRef"


    // $ANTLR start "entryRuleConstant"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1222:1: entryRuleConstant returns [EObject current=null] : iv_ruleConstant= ruleConstant EOF ;
    public final EObject entryRuleConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstant = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1223:2: (iv_ruleConstant= ruleConstant EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1224:2: iv_ruleConstant= ruleConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstantRule()); 
            }
            pushFollow(FOLLOW_ruleConstant_in_entryRuleConstant2762);
            iv_ruleConstant=ruleConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleConstant2772); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1231:1: ruleConstant returns [EObject current=null] : (otherlv_0= 'const' ( (lv_type_1_0= ruleType ) ) ( (lv_constName_2_0= RULE_CONSTNAME ) ) ) ;
    public final EObject ruleConstant() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_constName_2_0=null;
        EObject lv_type_1_0 = null;


         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1234:28: ( (otherlv_0= 'const' ( (lv_type_1_0= ruleType ) ) ( (lv_constName_2_0= RULE_CONSTNAME ) ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1235:1: (otherlv_0= 'const' ( (lv_type_1_0= ruleType ) ) ( (lv_constName_2_0= RULE_CONSTNAME ) ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1235:1: (otherlv_0= 'const' ( (lv_type_1_0= ruleType ) ) ( (lv_constName_2_0= RULE_CONSTNAME ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1235:3: otherlv_0= 'const' ( (lv_type_1_0= ruleType ) ) ( (lv_constName_2_0= RULE_CONSTNAME ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_29_in_ruleConstant2809); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getConstantAccess().getConstKeyword_0());
                  
            }
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1239:1: ( (lv_type_1_0= ruleType ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1240:1: (lv_type_1_0= ruleType )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1240:1: (lv_type_1_0= ruleType )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1241:3: lv_type_1_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConstantAccess().getTypeTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleType_in_ruleConstant2830);
            lv_type_1_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConstantRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_1_0, 
                      		"Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1257:2: ( (lv_constName_2_0= RULE_CONSTNAME ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1258:1: (lv_constName_2_0= RULE_CONSTNAME )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1258:1: (lv_constName_2_0= RULE_CONSTNAME )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1259:3: lv_constName_2_0= RULE_CONSTNAME
            {
            lv_constName_2_0=(Token)match(input,RULE_CONSTNAME,FOLLOW_RULE_CONSTNAME_in_ruleConstant2847); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_constName_2_0, grammarAccess.getConstantAccess().getConstNameCONSTNAMETerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getConstantRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"constName",
                      		lv_constName_2_0, 
                      		"CONSTNAME");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleVarName"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1283:1: entryRuleVarName returns [EObject current=null] : iv_ruleVarName= ruleVarName EOF ;
    public final EObject entryRuleVarName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVarName = null;


        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1284:2: (iv_ruleVarName= ruleVarName EOF )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1285:2: iv_ruleVarName= ruleVarName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVarNameRule()); 
            }
            pushFollow(FOLLOW_ruleVarName_in_entryRuleVarName2888);
            iv_ruleVarName=ruleVarName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVarName; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleVarName2898); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVarName"


    // $ANTLR start "ruleVarName"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1292:1: ruleVarName returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_A_VARNAME ) ) ) ;
    public final EObject ruleVarName() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1295:28: ( ( () ( (lv_value_1_0= RULE_A_VARNAME ) ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1296:1: ( () ( (lv_value_1_0= RULE_A_VARNAME ) ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1296:1: ( () ( (lv_value_1_0= RULE_A_VARNAME ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1296:2: () ( (lv_value_1_0= RULE_A_VARNAME ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1296:2: ()
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1297:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getVarNameAccess().getVarNameAction_0(),
                          current);
                  
            }

            }

            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1302:2: ( (lv_value_1_0= RULE_A_VARNAME ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1303:1: (lv_value_1_0= RULE_A_VARNAME )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1303:1: (lv_value_1_0= RULE_A_VARNAME )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1304:3: lv_value_1_0= RULE_A_VARNAME
            {
            lv_value_1_0=(Token)match(input,RULE_A_VARNAME,FOLLOW_RULE_A_VARNAME_in_ruleVarName2949); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_1_0, grammarAccess.getVarNameAccess().getValueA_VARNAMETerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getVarNameRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"A_VARNAME");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVarName"


    // $ANTLR start "ruleVisibility"
    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1328:1: ruleVisibility returns [Enumerator current=null] : ( (enumLiteral_0= 'public' ) | (enumLiteral_1= 'private' ) | (enumLiteral_2= 'protected' ) ) ;
    public final Enumerator ruleVisibility() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1330:28: ( ( (enumLiteral_0= 'public' ) | (enumLiteral_1= 'private' ) | (enumLiteral_2= 'protected' ) ) )
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1331:1: ( (enumLiteral_0= 'public' ) | (enumLiteral_1= 'private' ) | (enumLiteral_2= 'protected' ) )
            {
            // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1331:1: ( (enumLiteral_0= 'public' ) | (enumLiteral_1= 'private' ) | (enumLiteral_2= 'protected' ) )
            int alt12=3;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt12=1;
                }
                break;
            case 31:
                {
                alt12=2;
                }
                break;
            case 32:
                {
                alt12=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1331:2: (enumLiteral_0= 'public' )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1331:2: (enumLiteral_0= 'public' )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1331:4: enumLiteral_0= 'public'
                    {
                    enumLiteral_0=(Token)match(input,30,FOLLOW_30_in_ruleVisibility3004); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getVisibilityAccess().getPUBLICEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getVisibilityAccess().getPUBLICEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1337:6: (enumLiteral_1= 'private' )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1337:6: (enumLiteral_1= 'private' )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1337:8: enumLiteral_1= 'private'
                    {
                    enumLiteral_1=(Token)match(input,31,FOLLOW_31_in_ruleVisibility3021); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getVisibilityAccess().getPRIVATEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getVisibilityAccess().getPRIVATEEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1343:6: (enumLiteral_2= 'protected' )
                    {
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1343:6: (enumLiteral_2= 'protected' )
                    // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:1343:8: enumLiteral_2= 'protected'
                    {
                    enumLiteral_2=(Token)match(input,32,FOLLOW_32_in_ruleVisibility3038); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getVisibilityAccess().getPROTECTEDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getVisibilityAccess().getPROTECTEDEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVisibility"

    // $ANTLR start synpred1_InternalLanguage404
    public final void synpred1_InternalLanguage404_fragment() throws RecognitionException {   
        // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:186:3: ( 'else' )
        // ../language404/src-gen/language404/parser/antlr/internal/InternalLanguage404.g:186:5: 'else'
        {
        match(input,21,FOLLOW_21_in_synpred1_InternalLanguage404339); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_InternalLanguage404

    // Delegated rules

    public final boolean synpred1_InternalLanguage404() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalLanguage404_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_ruleIfModel_in_entryRuleIfModel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIfModel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfStatement_in_ruleIfModel140 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_ruleIfStatement_in_entryRuleIfStatement177 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIfStatement187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleIfStatement224 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleIfStatement236 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_ruleIfExpression_in_ruleIfStatement257 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleIfStatement269 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleIfStatement281 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleIfStatement293 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_ruleIfExpression_in_ruleIfStatement314 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleIfStatement326 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_21_in_ruleIfStatement347 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleIfStatement360 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_ruleIfExpression_in_ruleIfStatement381 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleIfStatement393 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfExpression_in_entryRuleIfExpression431 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIfExpression441 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfStatement_in_ruleIfExpression488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleIfExpression510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForStatement_in_entryRuleForStatement553 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleForStatement563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleForStatement600 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleForStatement612 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_ruleIntVariable_in_ruleForStatement634 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleForStatement645 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleForCondition_in_ruleForStatement666 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleForStatement678 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleIncrement_in_ruleForStatement699 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleForStatement711 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleForStatement723 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_ruleForExpression_in_ruleForStatement744 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleForStatement756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIncrement_in_entryRuleIncrement792 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIncrement802 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleIncrement843 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForCondition_in_entryRuleForCondition883 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleForCondition893 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleForCondition934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForExpression_in_entryRuleForExpression974 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleForExpression984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForStatement_in_ruleForExpression1031 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleForExpression1053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntVariable_in_entryRuleIntVariable1094 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntVariable1104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleIntVariable1141 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INTEGER_in_ruleIntVariable1158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_entryRuleStatement1205 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatement1215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDefinition_in_ruleStatement1262 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEvaluation_in_ruleStatement1289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDefinition_in_entryRuleDefinition1324 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDefinition1334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleDefinition1371 = new BitSet(new long[]{0x0000000018000000L});
    public static final BitSet FOLLOW_ruleType_in_ruleDefinition1392 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDefinition1409 = new BitSet(new long[]{0x0000000000810000L});
    public static final BitSet FOLLOW_16_in_ruleDefinition1427 = new BitSet(new long[]{0x0000000018000000L});
    public static final BitSet FOLLOW_ruleType_in_ruleDefinition1448 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ruleDeclaredParameter_in_ruleDefinition1469 = new BitSet(new long[]{0x0000000004020000L});
    public static final BitSet FOLLOW_26_in_ruleDefinition1482 = new BitSet(new long[]{0x0000000018000000L});
    public static final BitSet FOLLOW_ruleType_in_ruleDefinition1503 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_ruleDeclaredParameter_in_ruleDefinition1524 = new BitSet(new long[]{0x0000000004020000L});
    public static final BitSet FOLLOW_17_in_ruleDefinition1538 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleDefinition1552 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclaredParameter_in_entryRuleDeclaredParameter1588 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDeclaredParameter1598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVarName_in_ruleDeclaredParameter1643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_entryRuleType1678 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleType1688 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerType_in_ruleType1735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringType_in_ruleType1762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerType_in_entryRuleIntegerType1797 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerType1807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleIntegerType1859 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringType_in_entryRuleStringType1908 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStringType1918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_ruleStringType1970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEvaluation_in_entryRuleEvaluation2019 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEvaluation2029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleEvaluation2075 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleEvaluation2087 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression2123 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression2133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunctionCall_in_ruleExpression2180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_ruleExpression2207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerAtom_in_ruleExpression2234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringAtom_in_ruleExpression2261 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerAtom_in_entryRuleIntegerAtom2296 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerAtom2306 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerAtom2351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringAtom_in_entryRuleStringAtom2386 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStringAtom2396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_A_STRING_in_ruleStringAtom2437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunctionCall_in_entryRuleFunctionCall2477 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunctionCall2487 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDefinitionRef_in_ruleFunctionCall2542 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleFunctionCall2554 = new BitSet(new long[]{0x00000000200200D0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleFunctionCall2576 = new BitSet(new long[]{0x0000000004020000L});
    public static final BitSet FOLLOW_26_in_ruleFunctionCall2589 = new BitSet(new long[]{0x00000000200000D0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleFunctionCall2610 = new BitSet(new long[]{0x0000000004020000L});
    public static final BitSet FOLLOW_17_in_ruleFunctionCall2626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDefinitionRef_in_entryRuleDefinitionRef2662 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDefinitionRef2672 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDefinitionRef2726 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_entryRuleConstant2762 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConstant2772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleConstant2809 = new BitSet(new long[]{0x0000000018000000L});
    public static final BitSet FOLLOW_ruleType_in_ruleConstant2830 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_RULE_CONSTNAME_in_ruleConstant2847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVarName_in_entryRuleVarName2888 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVarName2898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_A_VARNAME_in_ruleVarName2949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleVisibility3004 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_ruleVisibility3021 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_ruleVisibility3038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_synpred1_InternalLanguage404339 = new BitSet(new long[]{0x0000000000000002L});

}
