/**
 */
package language404.language404;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.FunctionCall#getSource <em>Source</em>}</li>
 *   <li>{@link language404.language404.FunctionCall#getArgs <em>Args</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getFunctionCall()
 * @model
 * @generated
 */
public interface FunctionCall extends Type
{
  /**
   * Returns the value of the '<em><b>Source</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Source</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Source</em>' containment reference.
   * @see #setSource(Type)
   * @see language404.language404.Language404Package#getFunctionCall_Source()
   * @model containment="true"
   * @generated
   */
  Type getSource();

  /**
   * Sets the value of the '{@link language404.language404.FunctionCall#getSource <em>Source</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Source</em>' containment reference.
   * @see #getSource()
   * @generated
   */
  void setSource(Type value);

  /**
   * Returns the value of the '<em><b>Args</b></em>' containment reference list.
   * The list contents are of type {@link language404.language404.Type}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Args</em>' containment reference list.
   * @see language404.language404.Language404Package#getFunctionCall_Args()
   * @model containment="true"
   * @generated
   */
  EList<Type> getArgs();

} // FunctionCall
