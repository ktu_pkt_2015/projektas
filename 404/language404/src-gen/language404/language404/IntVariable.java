/**
 */
package language404.language404;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.IntVariable#getCondition <em>Condition</em>}</li>
 *   <li>{@link language404.language404.IntVariable#getExpression <em>Expression</em>}</li>
 *   <li>{@link language404.language404.IntVariable#getStatement <em>Statement</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getIntVariable()
 * @model
 * @generated
 */
public interface IntVariable extends ForStatement
{
  /**
   * Returns the value of the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Condition</em>' containment reference.
   * @see #setCondition(ForCondition)
   * @see language404.language404.Language404Package#getIntVariable_Condition()
   * @model containment="true"
   * @generated
   */
  ForCondition getCondition();

  /**
   * Sets the value of the '{@link language404.language404.IntVariable#getCondition <em>Condition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Condition</em>' containment reference.
   * @see #getCondition()
   * @generated
   */
  void setCondition(ForCondition value);

  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(Increment)
   * @see language404.language404.Language404Package#getIntVariable_Expression()
   * @model containment="true"
   * @generated
   */
  Increment getExpression();

  /**
   * Sets the value of the '{@link language404.language404.IntVariable#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(Increment value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(ForExpression)
   * @see language404.language404.Language404Package#getIntVariable_Statement()
   * @model containment="true"
   * @generated
   */
  ForExpression getStatement();

  /**
   * Sets the value of the '{@link language404.language404.IntVariable#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(ForExpression value);

} // IntVariable
