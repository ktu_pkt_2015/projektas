/**
 */
package language404.language404;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Atom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see language404.language404.Language404Package#getIntegerAtom()
 * @model
 * @generated
 */
public interface IntegerAtom extends IntegerType
{
} // IntegerAtom
