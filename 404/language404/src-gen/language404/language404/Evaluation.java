/**
 */
package language404.language404;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.Evaluation#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getEvaluation()
 * @model
 * @generated
 */
public interface Evaluation extends Statement
{
  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(Type)
   * @see language404.language404.Language404Package#getEvaluation_Expression()
   * @model containment="true"
   * @generated
   */
  Type getExpression();

  /**
   * Sets the value of the '{@link language404.language404.Evaluation#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(Type value);

} // Evaluation
