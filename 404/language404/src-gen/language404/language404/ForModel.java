/**
 */
package language404.language404;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.ForModel#getStatement <em>Statement</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getForModel()
 * @model
 * @generated
 */
public interface ForModel extends EObject
{
  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference list.
   * The list contents are of type {@link language404.language404.ForStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference list.
   * @see language404.language404.Language404Package#getForModel_Statement()
   * @model containment="true"
   * @generated
   */
  EList<ForStatement> getStatement();

} // ForModel
