/**
 */
package language404.language404;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.IfStatement#getCondition <em>Condition</em>}</li>
 *   <li>{@link language404.language404.IfStatement#getThen <em>Then</em>}</li>
 *   <li>{@link language404.language404.IfStatement#getElse <em>Else</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getIfStatement()
 * @model
 * @generated
 */
public interface IfStatement extends IfExpression
{
  /**
   * Returns the value of the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Condition</em>' containment reference.
   * @see #setCondition(IfExpression)
   * @see language404.language404.Language404Package#getIfStatement_Condition()
   * @model containment="true"
   * @generated
   */
  IfExpression getCondition();

  /**
   * Sets the value of the '{@link language404.language404.IfStatement#getCondition <em>Condition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Condition</em>' containment reference.
   * @see #getCondition()
   * @generated
   */
  void setCondition(IfExpression value);

  /**
   * Returns the value of the '<em><b>Then</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Then</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Then</em>' containment reference.
   * @see #setThen(IfExpression)
   * @see language404.language404.Language404Package#getIfStatement_Then()
   * @model containment="true"
   * @generated
   */
  IfExpression getThen();

  /**
   * Sets the value of the '{@link language404.language404.IfStatement#getThen <em>Then</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Then</em>' containment reference.
   * @see #getThen()
   * @generated
   */
  void setThen(IfExpression value);

  /**
   * Returns the value of the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Else</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else</em>' containment reference.
   * @see #setElse(IfExpression)
   * @see language404.language404.Language404Package#getIfStatement_Else()
   * @model containment="true"
   * @generated
   */
  IfExpression getElse();

  /**
   * Sets the value of the '{@link language404.language404.IfStatement#getElse <em>Else</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Else</em>' containment reference.
   * @see #getElse()
   * @generated
   */
  void setElse(IfExpression value);

} // IfStatement
