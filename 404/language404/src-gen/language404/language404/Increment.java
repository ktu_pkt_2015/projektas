/**
 */
package language404.language404;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Increment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.Increment#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getIncrement()
 * @model
 * @generated
 */
public interface Increment extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute list.
   * @see language404.language404.Language404Package#getIncrement_Name()
   * @model unique="false"
   * @generated
   */
  EList<String> getName();

} // Increment
