/**
 */
package language404.language404;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.IntegerType#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getIntegerType()
 * @model
 * @generated
 */
public interface IntegerType extends Type
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see language404.language404.Language404Package#getIntegerType_Type()
   * @model
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link language404.language404.IntegerType#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

} // IntegerType
