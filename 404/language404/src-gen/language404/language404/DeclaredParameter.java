/**
 */
package language404.language404;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declared Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.DeclaredParameter#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getDeclaredParameter()
 * @model
 * @generated
 */
public interface DeclaredParameter extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' containment reference.
   * @see #setName(VarName)
   * @see language404.language404.Language404Package#getDeclaredParameter_Name()
   * @model containment="true"
   * @generated
   */
  VarName getName();

  /**
   * Sets the value of the '{@link language404.language404.DeclaredParameter#getName <em>Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' containment reference.
   * @see #getName()
   * @generated
   */
  void setName(VarName value);

} // DeclaredParameter
