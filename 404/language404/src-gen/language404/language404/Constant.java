/**
 */
package language404.language404;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.Constant#getType <em>Type</em>}</li>
 *   <li>{@link language404.language404.Constant#getConstName <em>Const Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getConstant()
 * @model
 * @generated
 */
public interface Constant extends Type
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see language404.language404.Language404Package#getConstant_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link language404.language404.Constant#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Const Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Const Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Const Name</em>' attribute.
   * @see #setConstName(String)
   * @see language404.language404.Language404Package#getConstant_ConstName()
   * @model
   * @generated
   */
  String getConstName();

  /**
   * Sets the value of the '{@link language404.language404.Constant#getConstName <em>Const Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Const Name</em>' attribute.
   * @see #getConstName()
   * @generated
   */
  void setConstName(String value);

} // Constant
