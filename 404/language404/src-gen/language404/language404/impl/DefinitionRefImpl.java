/**
 */
package language404.language404.impl;

import language404.language404.Definition;
import language404.language404.DefinitionRef;
import language404.language404.Language404Package;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definition Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link language404.language404.impl.DefinitionRefImpl#getRefName <em>Ref Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DefinitionRefImpl extends TypeImpl implements DefinitionRef
{
  /**
   * The cached value of the '{@link #getRefName() <em>Ref Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRefName()
   * @generated
   * @ordered
   */
  protected Definition refName;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DefinitionRefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Language404Package.Literals.DEFINITION_REF;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Definition getRefName()
  {
    if (refName != null && refName.eIsProxy())
    {
      InternalEObject oldRefName = (InternalEObject)refName;
      refName = (Definition)eResolveProxy(oldRefName);
      if (refName != oldRefName)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, Language404Package.DEFINITION_REF__REF_NAME, oldRefName, refName));
      }
    }
    return refName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Definition basicGetRefName()
  {
    return refName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRefName(Definition newRefName)
  {
    Definition oldRefName = refName;
    refName = newRefName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Language404Package.DEFINITION_REF__REF_NAME, oldRefName, refName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case Language404Package.DEFINITION_REF__REF_NAME:
        if (resolve) return getRefName();
        return basicGetRefName();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case Language404Package.DEFINITION_REF__REF_NAME:
        setRefName((Definition)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case Language404Package.DEFINITION_REF__REF_NAME:
        setRefName((Definition)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case Language404Package.DEFINITION_REF__REF_NAME:
        return refName != null;
    }
    return super.eIsSet(featureID);
  }

} //DefinitionRefImpl
