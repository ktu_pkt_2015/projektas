/**
 */
package language404.language404;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see language404.language404.Language404Package#getForStatement()
 * @model
 * @generated
 */
public interface ForStatement extends ForExpression
{
} // ForStatement
