/**
 */
package language404.language404;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link language404.language404.DefinitionRef#getRefName <em>Ref Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see language404.language404.Language404Package#getDefinitionRef()
 * @model
 * @generated
 */
public interface DefinitionRef extends Type
{
  /**
   * Returns the value of the '<em><b>Ref Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref Name</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref Name</em>' reference.
   * @see #setRefName(Definition)
   * @see language404.language404.Language404Package#getDefinitionRef_RefName()
   * @model
   * @generated
   */
  Definition getRefName();

  /**
   * Sets the value of the '{@link language404.language404.DefinitionRef#getRefName <em>Ref Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref Name</em>' reference.
   * @see #getRefName()
   * @generated
   */
  void setRefName(Definition value);

} // DefinitionRef
