/**
 */
package language404.language404;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see language404.language404.Language404Factory
 * @model kind="package"
 * @generated
 */
public interface Language404Package extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "language404";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.Language404.language404";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "language404";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  Language404Package eINSTANCE = language404.language404.impl.Language404PackageImpl.init();

  /**
   * The meta object id for the '{@link language404.language404.impl.IfModelImpl <em>If Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.IfModelImpl
   * @see language404.language404.impl.Language404PackageImpl#getIfModel()
   * @generated
   */
  int IF_MODEL = 0;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_MODEL__STATEMENT = 0;

  /**
   * The number of structural features of the '<em>If Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_MODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.IfExpressionImpl <em>If Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.IfExpressionImpl
   * @see language404.language404.impl.Language404PackageImpl#getIfExpression()
   * @generated
   */
  int IF_EXPRESSION = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_EXPRESSION__NAME = 0;

  /**
   * The number of structural features of the '<em>If Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_EXPRESSION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.IfStatementImpl <em>If Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.IfStatementImpl
   * @see language404.language404.impl.Language404PackageImpl#getIfStatement()
   * @generated
   */
  int IF_STATEMENT = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT__NAME = IF_EXPRESSION__NAME;

  /**
   * The feature id for the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT__CONDITION = IF_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Then</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT__THEN = IF_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT__ELSE = IF_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>If Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_STATEMENT_FEATURE_COUNT = IF_EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link language404.language404.impl.ForModelImpl <em>For Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.ForModelImpl
   * @see language404.language404.impl.Language404PackageImpl#getForModel()
   * @generated
   */
  int FOR_MODEL = 3;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_MODEL__STATEMENT = 0;

  /**
   * The number of structural features of the '<em>For Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_MODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.ForExpressionImpl <em>For Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.ForExpressionImpl
   * @see language404.language404.impl.Language404PackageImpl#getForExpression()
   * @generated
   */
  int FOR_EXPRESSION = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_EXPRESSION__NAME = 0;

  /**
   * The number of structural features of the '<em>For Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_EXPRESSION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.ForStatementImpl <em>For Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.ForStatementImpl
   * @see language404.language404.impl.Language404PackageImpl#getForStatement()
   * @generated
   */
  int FOR_STATEMENT = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT__NAME = FOR_EXPRESSION__NAME;

  /**
   * The number of structural features of the '<em>For Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_STATEMENT_FEATURE_COUNT = FOR_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link language404.language404.impl.IncrementImpl <em>Increment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.IncrementImpl
   * @see language404.language404.impl.Language404PackageImpl#getIncrement()
   * @generated
   */
  int INCREMENT = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCREMENT__NAME = 0;

  /**
   * The number of structural features of the '<em>Increment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INCREMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.ForConditionImpl <em>For Condition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.ForConditionImpl
   * @see language404.language404.impl.Language404PackageImpl#getForCondition()
   * @generated
   */
  int FOR_CONDITION = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_CONDITION__NAME = 0;

  /**
   * The number of structural features of the '<em>For Condition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_CONDITION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.IntVariableImpl <em>Int Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.IntVariableImpl
   * @see language404.language404.impl.Language404PackageImpl#getIntVariable()
   * @generated
   */
  int INT_VARIABLE = 8;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_VARIABLE__NAME = FOR_STATEMENT__NAME;

  /**
   * The feature id for the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_VARIABLE__CONDITION = FOR_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_VARIABLE__EXPRESSION = FOR_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_VARIABLE__STATEMENT = FOR_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Int Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_VARIABLE_FEATURE_COUNT = FOR_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link language404.language404.impl.StringVariableImpl <em>String Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.StringVariableImpl
   * @see language404.language404.impl.Language404PackageImpl#getStringVariable()
   * @generated
   */
  int STRING_VARIABLE = 9;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_VARIABLE__NAME = 0;

  /**
   * The number of structural features of the '<em>String Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_VARIABLE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.ModifierImpl <em>Modifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.ModifierImpl
   * @see language404.language404.impl.Language404PackageImpl#getModifier()
   * @generated
   */
  int MODIFIER = 10;

  /**
   * The feature id for the '<em><b>Static</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIER__STATIC = 0;

  /**
   * The feature id for the '<em><b>Final</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIER__FINAL = 1;

  /**
   * The feature id for the '<em><b>Visibility</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIER__VISIBILITY = 2;

  /**
   * The number of structural features of the '<em>Modifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFIER_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link language404.language404.impl.ModuleImpl <em>Module</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.ModuleImpl
   * @see language404.language404.impl.Language404PackageImpl#getModule()
   * @generated
   */
  int MODULE = 11;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE__STATEMENTS = 0;

  /**
   * The number of structural features of the '<em>Module</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.StatementImpl <em>Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.StatementImpl
   * @see language404.language404.impl.Language404PackageImpl#getStatement()
   * @generated
   */
  int STATEMENT = 12;

  /**
   * The number of structural features of the '<em>Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link language404.language404.impl.DefinitionImpl <em>Definition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.DefinitionImpl
   * @see language404.language404.impl.Language404PackageImpl#getDefinition()
   * @generated
   */
  int DEFINITION = 13;

  /**
   * The feature id for the '<em><b>Return Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION__RETURN_TYPE = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION__NAME = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION__TYPES = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Args</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION__ARGS = STATEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Definition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link language404.language404.impl.DeclaredParameterImpl <em>Declared Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.DeclaredParameterImpl
   * @see language404.language404.impl.Language404PackageImpl#getDeclaredParameter()
   * @generated
   */
  int DECLARED_PARAMETER = 14;

  /**
   * The feature id for the '<em><b>Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARED_PARAMETER__NAME = 0;

  /**
   * The number of structural features of the '<em>Declared Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARED_PARAMETER_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.TypeImpl
   * @see language404.language404.impl.Language404PackageImpl#getType()
   * @generated
   */
  int TYPE = 15;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link language404.language404.impl.IntegerTypeImpl <em>Integer Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.IntegerTypeImpl
   * @see language404.language404.impl.Language404PackageImpl#getIntegerType()
   * @generated
   */
  int INTEGER_TYPE = 16;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE__TYPE = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Integer Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.StringTypeImpl <em>String Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.StringTypeImpl
   * @see language404.language404.impl.Language404PackageImpl#getStringType()
   * @generated
   */
  int STRING_TYPE = 17;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_TYPE__TYPE = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_TYPE__VALUE = TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>String Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link language404.language404.impl.EvaluationImpl <em>Evaluation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.EvaluationImpl
   * @see language404.language404.impl.Language404PackageImpl#getEvaluation()
   * @generated
   */
  int EVALUATION = 18;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVALUATION__EXPRESSION = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Evaluation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVALUATION_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.ConstantImpl <em>Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.ConstantImpl
   * @see language404.language404.impl.Language404PackageImpl#getConstant()
   * @generated
   */
  int CONSTANT = 19;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__TYPE = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Const Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__CONST_NAME = TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link language404.language404.impl.VarNameImpl <em>Var Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.VarNameImpl
   * @see language404.language404.impl.Language404PackageImpl#getVarName()
   * @generated
   */
  int VAR_NAME = 20;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_NAME__VALUE = 0;

  /**
   * The number of structural features of the '<em>Var Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_NAME_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link language404.language404.impl.IntegerAtomImpl <em>Integer Atom</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.IntegerAtomImpl
   * @see language404.language404.impl.Language404PackageImpl#getIntegerAtom()
   * @generated
   */
  int INTEGER_ATOM = 21;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_ATOM__TYPE = INTEGER_TYPE__TYPE;

  /**
   * The number of structural features of the '<em>Integer Atom</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_ATOM_FEATURE_COUNT = INTEGER_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link language404.language404.impl.FunctionCallImpl <em>Function Call</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.FunctionCallImpl
   * @see language404.language404.impl.Language404PackageImpl#getFunctionCall()
   * @generated
   */
  int FUNCTION_CALL = 22;

  /**
   * The feature id for the '<em><b>Source</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_CALL__SOURCE = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Args</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_CALL__ARGS = TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Function Call</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_CALL_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link language404.language404.impl.DefinitionRefImpl <em>Definition Ref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.impl.DefinitionRefImpl
   * @see language404.language404.impl.Language404PackageImpl#getDefinitionRef()
   * @generated
   */
  int DEFINITION_REF = 23;

  /**
   * The feature id for the '<em><b>Ref Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION_REF__REF_NAME = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Definition Ref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION_REF_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link language404.language404.Visibility <em>Visibility</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see language404.language404.Visibility
   * @see language404.language404.impl.Language404PackageImpl#getVisibility()
   * @generated
   */
  int VISIBILITY = 24;


  /**
   * Returns the meta object for class '{@link language404.language404.IfModel <em>If Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If Model</em>'.
   * @see language404.language404.IfModel
   * @generated
   */
  EClass getIfModel();

  /**
   * Returns the meta object for the containment reference list '{@link language404.language404.IfModel#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statement</em>'.
   * @see language404.language404.IfModel#getStatement()
   * @see #getIfModel()
   * @generated
   */
  EReference getIfModel_Statement();

  /**
   * Returns the meta object for class '{@link language404.language404.IfStatement <em>If Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If Statement</em>'.
   * @see language404.language404.IfStatement
   * @generated
   */
  EClass getIfStatement();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.IfStatement#getCondition <em>Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Condition</em>'.
   * @see language404.language404.IfStatement#getCondition()
   * @see #getIfStatement()
   * @generated
   */
  EReference getIfStatement_Condition();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.IfStatement#getThen <em>Then</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Then</em>'.
   * @see language404.language404.IfStatement#getThen()
   * @see #getIfStatement()
   * @generated
   */
  EReference getIfStatement_Then();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.IfStatement#getElse <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Else</em>'.
   * @see language404.language404.IfStatement#getElse()
   * @see #getIfStatement()
   * @generated
   */
  EReference getIfStatement_Else();

  /**
   * Returns the meta object for class '{@link language404.language404.IfExpression <em>If Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If Expression</em>'.
   * @see language404.language404.IfExpression
   * @generated
   */
  EClass getIfExpression();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.IfExpression#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see language404.language404.IfExpression#getName()
   * @see #getIfExpression()
   * @generated
   */
  EAttribute getIfExpression_Name();

  /**
   * Returns the meta object for class '{@link language404.language404.ForModel <em>For Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>For Model</em>'.
   * @see language404.language404.ForModel
   * @generated
   */
  EClass getForModel();

  /**
   * Returns the meta object for the containment reference list '{@link language404.language404.ForModel#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statement</em>'.
   * @see language404.language404.ForModel#getStatement()
   * @see #getForModel()
   * @generated
   */
  EReference getForModel_Statement();

  /**
   * Returns the meta object for class '{@link language404.language404.ForStatement <em>For Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>For Statement</em>'.
   * @see language404.language404.ForStatement
   * @generated
   */
  EClass getForStatement();

  /**
   * Returns the meta object for class '{@link language404.language404.Increment <em>Increment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Increment</em>'.
   * @see language404.language404.Increment
   * @generated
   */
  EClass getIncrement();

  /**
   * Returns the meta object for the attribute list '{@link language404.language404.Increment#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Name</em>'.
   * @see language404.language404.Increment#getName()
   * @see #getIncrement()
   * @generated
   */
  EAttribute getIncrement_Name();

  /**
   * Returns the meta object for class '{@link language404.language404.ForCondition <em>For Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>For Condition</em>'.
   * @see language404.language404.ForCondition
   * @generated
   */
  EClass getForCondition();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.ForCondition#isName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see language404.language404.ForCondition#isName()
   * @see #getForCondition()
   * @generated
   */
  EAttribute getForCondition_Name();

  /**
   * Returns the meta object for class '{@link language404.language404.ForExpression <em>For Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>For Expression</em>'.
   * @see language404.language404.ForExpression
   * @generated
   */
  EClass getForExpression();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.ForExpression#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see language404.language404.ForExpression#getName()
   * @see #getForExpression()
   * @generated
   */
  EAttribute getForExpression_Name();

  /**
   * Returns the meta object for class '{@link language404.language404.IntVariable <em>Int Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Int Variable</em>'.
   * @see language404.language404.IntVariable
   * @generated
   */
  EClass getIntVariable();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.IntVariable#getCondition <em>Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Condition</em>'.
   * @see language404.language404.IntVariable#getCondition()
   * @see #getIntVariable()
   * @generated
   */
  EReference getIntVariable_Condition();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.IntVariable#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see language404.language404.IntVariable#getExpression()
   * @see #getIntVariable()
   * @generated
   */
  EReference getIntVariable_Expression();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.IntVariable#getStatement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Statement</em>'.
   * @see language404.language404.IntVariable#getStatement()
   * @see #getIntVariable()
   * @generated
   */
  EReference getIntVariable_Statement();

  /**
   * Returns the meta object for class '{@link language404.language404.StringVariable <em>String Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>String Variable</em>'.
   * @see language404.language404.StringVariable
   * @generated
   */
  EClass getStringVariable();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.StringVariable#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see language404.language404.StringVariable#getName()
   * @see #getStringVariable()
   * @generated
   */
  EAttribute getStringVariable_Name();

  /**
   * Returns the meta object for class '{@link language404.language404.Modifier <em>Modifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Modifier</em>'.
   * @see language404.language404.Modifier
   * @generated
   */
  EClass getModifier();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.Modifier#isStatic <em>Static</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Static</em>'.
   * @see language404.language404.Modifier#isStatic()
   * @see #getModifier()
   * @generated
   */
  EAttribute getModifier_Static();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.Modifier#isFinal <em>Final</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Final</em>'.
   * @see language404.language404.Modifier#isFinal()
   * @see #getModifier()
   * @generated
   */
  EAttribute getModifier_Final();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.Modifier#getVisibility <em>Visibility</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Visibility</em>'.
   * @see language404.language404.Modifier#getVisibility()
   * @see #getModifier()
   * @generated
   */
  EAttribute getModifier_Visibility();

  /**
   * Returns the meta object for class '{@link language404.language404.Module <em>Module</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Module</em>'.
   * @see language404.language404.Module
   * @generated
   */
  EClass getModule();

  /**
   * Returns the meta object for the containment reference list '{@link language404.language404.Module#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statements</em>'.
   * @see language404.language404.Module#getStatements()
   * @see #getModule()
   * @generated
   */
  EReference getModule_Statements();

  /**
   * Returns the meta object for class '{@link language404.language404.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement</em>'.
   * @see language404.language404.Statement
   * @generated
   */
  EClass getStatement();

  /**
   * Returns the meta object for class '{@link language404.language404.Definition <em>Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Definition</em>'.
   * @see language404.language404.Definition
   * @generated
   */
  EClass getDefinition();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.Definition#getReturnType <em>Return Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Return Type</em>'.
   * @see language404.language404.Definition#getReturnType()
   * @see #getDefinition()
   * @generated
   */
  EReference getDefinition_ReturnType();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.Definition#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see language404.language404.Definition#getName()
   * @see #getDefinition()
   * @generated
   */
  EAttribute getDefinition_Name();

  /**
   * Returns the meta object for the containment reference list '{@link language404.language404.Definition#getTypes <em>Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Types</em>'.
   * @see language404.language404.Definition#getTypes()
   * @see #getDefinition()
   * @generated
   */
  EReference getDefinition_Types();

  /**
   * Returns the meta object for the containment reference list '{@link language404.language404.Definition#getArgs <em>Args</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Args</em>'.
   * @see language404.language404.Definition#getArgs()
   * @see #getDefinition()
   * @generated
   */
  EReference getDefinition_Args();

  /**
   * Returns the meta object for class '{@link language404.language404.DeclaredParameter <em>Declared Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Declared Parameter</em>'.
   * @see language404.language404.DeclaredParameter
   * @generated
   */
  EClass getDeclaredParameter();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.DeclaredParameter#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Name</em>'.
   * @see language404.language404.DeclaredParameter#getName()
   * @see #getDeclaredParameter()
   * @generated
   */
  EReference getDeclaredParameter_Name();

  /**
   * Returns the meta object for class '{@link language404.language404.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see language404.language404.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for class '{@link language404.language404.IntegerType <em>Integer Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Type</em>'.
   * @see language404.language404.IntegerType
   * @generated
   */
  EClass getIntegerType();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.IntegerType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see language404.language404.IntegerType#getType()
   * @see #getIntegerType()
   * @generated
   */
  EAttribute getIntegerType_Type();

  /**
   * Returns the meta object for class '{@link language404.language404.StringType <em>String Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>String Type</em>'.
   * @see language404.language404.StringType
   * @generated
   */
  EClass getStringType();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.StringType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see language404.language404.StringType#getType()
   * @see #getStringType()
   * @generated
   */
  EAttribute getStringType_Type();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.StringType#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see language404.language404.StringType#getValue()
   * @see #getStringType()
   * @generated
   */
  EAttribute getStringType_Value();

  /**
   * Returns the meta object for class '{@link language404.language404.Evaluation <em>Evaluation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Evaluation</em>'.
   * @see language404.language404.Evaluation
   * @generated
   */
  EClass getEvaluation();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.Evaluation#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see language404.language404.Evaluation#getExpression()
   * @see #getEvaluation()
   * @generated
   */
  EReference getEvaluation_Expression();

  /**
   * Returns the meta object for class '{@link language404.language404.Constant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant</em>'.
   * @see language404.language404.Constant
   * @generated
   */
  EClass getConstant();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.Constant#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see language404.language404.Constant#getType()
   * @see #getConstant()
   * @generated
   */
  EReference getConstant_Type();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.Constant#getConstName <em>Const Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Const Name</em>'.
   * @see language404.language404.Constant#getConstName()
   * @see #getConstant()
   * @generated
   */
  EAttribute getConstant_ConstName();

  /**
   * Returns the meta object for class '{@link language404.language404.VarName <em>Var Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Name</em>'.
   * @see language404.language404.VarName
   * @generated
   */
  EClass getVarName();

  /**
   * Returns the meta object for the attribute '{@link language404.language404.VarName#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see language404.language404.VarName#getValue()
   * @see #getVarName()
   * @generated
   */
  EAttribute getVarName_Value();

  /**
   * Returns the meta object for class '{@link language404.language404.IntegerAtom <em>Integer Atom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Atom</em>'.
   * @see language404.language404.IntegerAtom
   * @generated
   */
  EClass getIntegerAtom();

  /**
   * Returns the meta object for class '{@link language404.language404.FunctionCall <em>Function Call</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function Call</em>'.
   * @see language404.language404.FunctionCall
   * @generated
   */
  EClass getFunctionCall();

  /**
   * Returns the meta object for the containment reference '{@link language404.language404.FunctionCall#getSource <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Source</em>'.
   * @see language404.language404.FunctionCall#getSource()
   * @see #getFunctionCall()
   * @generated
   */
  EReference getFunctionCall_Source();

  /**
   * Returns the meta object for the containment reference list '{@link language404.language404.FunctionCall#getArgs <em>Args</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Args</em>'.
   * @see language404.language404.FunctionCall#getArgs()
   * @see #getFunctionCall()
   * @generated
   */
  EReference getFunctionCall_Args();

  /**
   * Returns the meta object for class '{@link language404.language404.DefinitionRef <em>Definition Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Definition Ref</em>'.
   * @see language404.language404.DefinitionRef
   * @generated
   */
  EClass getDefinitionRef();

  /**
   * Returns the meta object for the reference '{@link language404.language404.DefinitionRef#getRefName <em>Ref Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Ref Name</em>'.
   * @see language404.language404.DefinitionRef#getRefName()
   * @see #getDefinitionRef()
   * @generated
   */
  EReference getDefinitionRef_RefName();

  /**
   * Returns the meta object for enum '{@link language404.language404.Visibility <em>Visibility</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Visibility</em>'.
   * @see language404.language404.Visibility
   * @generated
   */
  EEnum getVisibility();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  Language404Factory getLanguage404Factory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link language404.language404.impl.IfModelImpl <em>If Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.IfModelImpl
     * @see language404.language404.impl.Language404PackageImpl#getIfModel()
     * @generated
     */
    EClass IF_MODEL = eINSTANCE.getIfModel();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_MODEL__STATEMENT = eINSTANCE.getIfModel_Statement();

    /**
     * The meta object literal for the '{@link language404.language404.impl.IfStatementImpl <em>If Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.IfStatementImpl
     * @see language404.language404.impl.Language404PackageImpl#getIfStatement()
     * @generated
     */
    EClass IF_STATEMENT = eINSTANCE.getIfStatement();

    /**
     * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_STATEMENT__CONDITION = eINSTANCE.getIfStatement_Condition();

    /**
     * The meta object literal for the '<em><b>Then</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_STATEMENT__THEN = eINSTANCE.getIfStatement_Then();

    /**
     * The meta object literal for the '<em><b>Else</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_STATEMENT__ELSE = eINSTANCE.getIfStatement_Else();

    /**
     * The meta object literal for the '{@link language404.language404.impl.IfExpressionImpl <em>If Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.IfExpressionImpl
     * @see language404.language404.impl.Language404PackageImpl#getIfExpression()
     * @generated
     */
    EClass IF_EXPRESSION = eINSTANCE.getIfExpression();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IF_EXPRESSION__NAME = eINSTANCE.getIfExpression_Name();

    /**
     * The meta object literal for the '{@link language404.language404.impl.ForModelImpl <em>For Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.ForModelImpl
     * @see language404.language404.impl.Language404PackageImpl#getForModel()
     * @generated
     */
    EClass FOR_MODEL = eINSTANCE.getForModel();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOR_MODEL__STATEMENT = eINSTANCE.getForModel_Statement();

    /**
     * The meta object literal for the '{@link language404.language404.impl.ForStatementImpl <em>For Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.ForStatementImpl
     * @see language404.language404.impl.Language404PackageImpl#getForStatement()
     * @generated
     */
    EClass FOR_STATEMENT = eINSTANCE.getForStatement();

    /**
     * The meta object literal for the '{@link language404.language404.impl.IncrementImpl <em>Increment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.IncrementImpl
     * @see language404.language404.impl.Language404PackageImpl#getIncrement()
     * @generated
     */
    EClass INCREMENT = eINSTANCE.getIncrement();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INCREMENT__NAME = eINSTANCE.getIncrement_Name();

    /**
     * The meta object literal for the '{@link language404.language404.impl.ForConditionImpl <em>For Condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.ForConditionImpl
     * @see language404.language404.impl.Language404PackageImpl#getForCondition()
     * @generated
     */
    EClass FOR_CONDITION = eINSTANCE.getForCondition();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FOR_CONDITION__NAME = eINSTANCE.getForCondition_Name();

    /**
     * The meta object literal for the '{@link language404.language404.impl.ForExpressionImpl <em>For Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.ForExpressionImpl
     * @see language404.language404.impl.Language404PackageImpl#getForExpression()
     * @generated
     */
    EClass FOR_EXPRESSION = eINSTANCE.getForExpression();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FOR_EXPRESSION__NAME = eINSTANCE.getForExpression_Name();

    /**
     * The meta object literal for the '{@link language404.language404.impl.IntVariableImpl <em>Int Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.IntVariableImpl
     * @see language404.language404.impl.Language404PackageImpl#getIntVariable()
     * @generated
     */
    EClass INT_VARIABLE = eINSTANCE.getIntVariable();

    /**
     * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INT_VARIABLE__CONDITION = eINSTANCE.getIntVariable_Condition();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INT_VARIABLE__EXPRESSION = eINSTANCE.getIntVariable_Expression();

    /**
     * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INT_VARIABLE__STATEMENT = eINSTANCE.getIntVariable_Statement();

    /**
     * The meta object literal for the '{@link language404.language404.impl.StringVariableImpl <em>String Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.StringVariableImpl
     * @see language404.language404.impl.Language404PackageImpl#getStringVariable()
     * @generated
     */
    EClass STRING_VARIABLE = eINSTANCE.getStringVariable();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRING_VARIABLE__NAME = eINSTANCE.getStringVariable_Name();

    /**
     * The meta object literal for the '{@link language404.language404.impl.ModifierImpl <em>Modifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.ModifierImpl
     * @see language404.language404.impl.Language404PackageImpl#getModifier()
     * @generated
     */
    EClass MODIFIER = eINSTANCE.getModifier();

    /**
     * The meta object literal for the '<em><b>Static</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MODIFIER__STATIC = eINSTANCE.getModifier_Static();

    /**
     * The meta object literal for the '<em><b>Final</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MODIFIER__FINAL = eINSTANCE.getModifier_Final();

    /**
     * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MODIFIER__VISIBILITY = eINSTANCE.getModifier_Visibility();

    /**
     * The meta object literal for the '{@link language404.language404.impl.ModuleImpl <em>Module</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.ModuleImpl
     * @see language404.language404.impl.Language404PackageImpl#getModule()
     * @generated
     */
    EClass MODULE = eINSTANCE.getModule();

    /**
     * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODULE__STATEMENTS = eINSTANCE.getModule_Statements();

    /**
     * The meta object literal for the '{@link language404.language404.impl.StatementImpl <em>Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.StatementImpl
     * @see language404.language404.impl.Language404PackageImpl#getStatement()
     * @generated
     */
    EClass STATEMENT = eINSTANCE.getStatement();

    /**
     * The meta object literal for the '{@link language404.language404.impl.DefinitionImpl <em>Definition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.DefinitionImpl
     * @see language404.language404.impl.Language404PackageImpl#getDefinition()
     * @generated
     */
    EClass DEFINITION = eINSTANCE.getDefinition();

    /**
     * The meta object literal for the '<em><b>Return Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEFINITION__RETURN_TYPE = eINSTANCE.getDefinition_ReturnType();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DEFINITION__NAME = eINSTANCE.getDefinition_Name();

    /**
     * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEFINITION__TYPES = eINSTANCE.getDefinition_Types();

    /**
     * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEFINITION__ARGS = eINSTANCE.getDefinition_Args();

    /**
     * The meta object literal for the '{@link language404.language404.impl.DeclaredParameterImpl <em>Declared Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.DeclaredParameterImpl
     * @see language404.language404.impl.Language404PackageImpl#getDeclaredParameter()
     * @generated
     */
    EClass DECLARED_PARAMETER = eINSTANCE.getDeclaredParameter();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECLARED_PARAMETER__NAME = eINSTANCE.getDeclaredParameter_Name();

    /**
     * The meta object literal for the '{@link language404.language404.impl.TypeImpl <em>Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.TypeImpl
     * @see language404.language404.impl.Language404PackageImpl#getType()
     * @generated
     */
    EClass TYPE = eINSTANCE.getType();

    /**
     * The meta object literal for the '{@link language404.language404.impl.IntegerTypeImpl <em>Integer Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.IntegerTypeImpl
     * @see language404.language404.impl.Language404PackageImpl#getIntegerType()
     * @generated
     */
    EClass INTEGER_TYPE = eINSTANCE.getIntegerType();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_TYPE__TYPE = eINSTANCE.getIntegerType_Type();

    /**
     * The meta object literal for the '{@link language404.language404.impl.StringTypeImpl <em>String Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.StringTypeImpl
     * @see language404.language404.impl.Language404PackageImpl#getStringType()
     * @generated
     */
    EClass STRING_TYPE = eINSTANCE.getStringType();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRING_TYPE__TYPE = eINSTANCE.getStringType_Type();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRING_TYPE__VALUE = eINSTANCE.getStringType_Value();

    /**
     * The meta object literal for the '{@link language404.language404.impl.EvaluationImpl <em>Evaluation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.EvaluationImpl
     * @see language404.language404.impl.Language404PackageImpl#getEvaluation()
     * @generated
     */
    EClass EVALUATION = eINSTANCE.getEvaluation();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVALUATION__EXPRESSION = eINSTANCE.getEvaluation_Expression();

    /**
     * The meta object literal for the '{@link language404.language404.impl.ConstantImpl <em>Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.ConstantImpl
     * @see language404.language404.impl.Language404PackageImpl#getConstant()
     * @generated
     */
    EClass CONSTANT = eINSTANCE.getConstant();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONSTANT__TYPE = eINSTANCE.getConstant_Type();

    /**
     * The meta object literal for the '<em><b>Const Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONSTANT__CONST_NAME = eINSTANCE.getConstant_ConstName();

    /**
     * The meta object literal for the '{@link language404.language404.impl.VarNameImpl <em>Var Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.VarNameImpl
     * @see language404.language404.impl.Language404PackageImpl#getVarName()
     * @generated
     */
    EClass VAR_NAME = eINSTANCE.getVarName();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VAR_NAME__VALUE = eINSTANCE.getVarName_Value();

    /**
     * The meta object literal for the '{@link language404.language404.impl.IntegerAtomImpl <em>Integer Atom</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.IntegerAtomImpl
     * @see language404.language404.impl.Language404PackageImpl#getIntegerAtom()
     * @generated
     */
    EClass INTEGER_ATOM = eINSTANCE.getIntegerAtom();

    /**
     * The meta object literal for the '{@link language404.language404.impl.FunctionCallImpl <em>Function Call</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.FunctionCallImpl
     * @see language404.language404.impl.Language404PackageImpl#getFunctionCall()
     * @generated
     */
    EClass FUNCTION_CALL = eINSTANCE.getFunctionCall();

    /**
     * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION_CALL__SOURCE = eINSTANCE.getFunctionCall_Source();

    /**
     * The meta object literal for the '<em><b>Args</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION_CALL__ARGS = eINSTANCE.getFunctionCall_Args();

    /**
     * The meta object literal for the '{@link language404.language404.impl.DefinitionRefImpl <em>Definition Ref</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.impl.DefinitionRefImpl
     * @see language404.language404.impl.Language404PackageImpl#getDefinitionRef()
     * @generated
     */
    EClass DEFINITION_REF = eINSTANCE.getDefinitionRef();

    /**
     * The meta object literal for the '<em><b>Ref Name</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEFINITION_REF__REF_NAME = eINSTANCE.getDefinitionRef_RefName();

    /**
     * The meta object literal for the '{@link language404.language404.Visibility <em>Visibility</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see language404.language404.Visibility
     * @see language404.language404.impl.Language404PackageImpl#getVisibility()
     * @generated
     */
    EEnum VISIBILITY = eINSTANCE.getVisibility();

  }

} //Language404Package
