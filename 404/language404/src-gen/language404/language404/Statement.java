/**
 */
package language404.language404;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see language404.language404.Language404Package#getStatement()
 * @model
 * @generated
 */
public interface Statement extends EObject
{
} // Statement
